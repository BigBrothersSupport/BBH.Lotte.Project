﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BBC.Core.WebService;

using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
namespace BBH.LotteFE.Repository
{
    public class PointRepository : WCFClient<IPointsServices>, IPointsServices
    {
        //PointsServicesClient service = new PointsServicesClient();
        public bool InsertTransactionPoint(TransactionPointsBO objTransactionPointsBO)
        {
            try
            {
                return Proxy.InsertTransactionPoint(objTransactionPointsBO);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public IEnumerable<TransactionPointsBO> ListTransactionPointByMember(int memberID, int start, int end)
        {
            try
            {
                return Proxy.ListTransactionPointByMember(memberID, start, end);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<TransactionPointsBO> ListTransactionPointBySearch(int memberID, System.DateTime? fromDate, System.DateTime? toDate, int start, int end)
        {
            try
            {
                return Proxy.ListTransactionPointBySearch(memberID, fromDate, toDate, start, end);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ExchangePointBO ListExchangePointByMember(int memberID)
        {
            try
            {
                return Proxy.ListExchangePointByMember(memberID);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public ExchangeTicketBO ListExchangeTicketByMember(int memberID, string strCoinID)
        {
            try
            {
                return Proxy.ListExchangeTicketByMember(memberID, strCoinID);
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }
        public bool InsertTransactionCoin(TransactionCoinBO objTransactionCoinBO)
        {
            try
            {
                return Proxy.InsertTransactionCoin(objTransactionCoinBO);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Edited date: 12.01.2018
        /// Description: Thay DateTime -> DateTime?
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<TransactionCoinBO> ListTransactionWalletBySearch(int memberID, System.DateTime? fromDate, System.DateTime? toDate, int start, int end)
        {
            try
            {
                return Proxy.ListTransactionWalletBySearch(memberID, fromDate, toDate, start, end);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public IEnumerable<TransactionCoinBO> ListTransactionWalletByMember(int memberID, int start, int end)
        {
            try
            {
                return Proxy.ListTransactionWalletByMember(memberID, start, end);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public bool CheckExistTransactionBitcoin(string strTransactionID, int intMemberID)
        {
            try
            {
                return Proxy.CheckExistTransactionBitcoin(strTransactionID, intMemberID);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Created date: 11.01.2018
        /// Description: Load Info Point Wallet Address
        /// </summary>
        /// <param name="intMemberID"></param>
        /// <returns></returns>
        public MemberWalletBO LoadInfoPointWallet(int intMemberID)
        {
            try
            {
                return Proxy.LoadInfoPointWallet(intMemberID);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
