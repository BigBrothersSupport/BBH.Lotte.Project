﻿using BBC.Core.WebService;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Repository
{
    public class TicketWinningRepository : WCFClient<ITicketWinningServices>, ITicketWinningServices
    {
        public TiketWinningBO GetListTicketWinningByBookingID(int BookingID, decimal awardvalue, decimal awardfee, int proprity, int intDrawID, int intMemberID)
        {
            return Proxy.GetListTicketWinningByBookingID(BookingID, awardvalue, awardfee, proprity, intDrawID, intMemberID);
        }

        public IEnumerable<TiketWinningBO> GetListTicketWinningByMemberID(int intMemberID, int start, int end)
        {
            return Proxy.GetListTicketWinningByMemberID(intMemberID,start, end);
        }
        public IEnumerable<TiketWinningBO> GetListTicketWinningBySearch(int intMemberID, int start, int end, DateTime? startdate, DateTime? enddate)
        {
            return Proxy.GetListTicketWinningBySearch(intMemberID, start, end, startdate, enddate);
        }
    }
}
