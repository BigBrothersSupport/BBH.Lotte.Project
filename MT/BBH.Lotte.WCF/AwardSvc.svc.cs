﻿using System;
using BBH.Lotte.CLP.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AwardSvc" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AwardSvc.svc or AwardSvc.svc.cs at the Solution Explorer and start debugging.
    public class AwardSvc : IAwardSvc
    {

        public bool InsertAward(AwardBO award)
        {
           return BBH.Lotte.CLP.Data.AwardBusiness.InsertAward(award);
        }

        public bool UpdateAward(AwardBO award)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.UpdateAward(award);
        }

        public bool LockAndUnlockAward(int awardID, int isActive)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.LockAndUnlockAward(awardID, isActive);
        }

        

        public IEnumerable<AwardBO> GetListAward(int start, int end)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.GetListAward(start, end);
        }

        public IEnumerable<AwardNumberBO> GetListAwardNumber(int start, int end)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.GetListAwardNumber(start, end);
        }

        public bool InsertAwardNumber(AwardNumberBO award)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.InsertAwardNumber(award);
        }

        public bool UpdateAwardNumber(AwardNumberBO award)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.UpdateAwardNumber(award);
        }

        public IEnumerable<AwardNumberBO> GetListAwardNumberByDate(int start, int end, DateTime fromDate, DateTime toDate)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.GetListAwardNumberByDate(start, end, fromDate, toDate);
        }

        public bool LockAndUnlockAwardNumber(int numberID, int isActive)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.LockAndUnlockAwardNumber(numberID, isActive);
        }

        public IEnumerable<WinnerBO> GetListWinner(int start, int end, DateTime fromDate, DateTime toDate)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.GetListWinner(start, end, fromDate, toDate);
        }


        public bool CheckAwardNameEnglishExists(string awardNameEnglish)
        {
            return BBH.Lotte.CLP.Data.AwardBusiness.CheckAwardNameEnglishExists(awardNameEnglish);
        }
    }
}
