﻿using BBH.Lotte.CLP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.CLP.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAwardSvc" in both code and config file together.
    [ServiceContract]
    public interface IAwardSvc
    {
        [OperationContract]
        bool InsertAward(AwardBO award);

        [OperationContract]
        bool UpdateAward(AwardBO award);

        [OperationContract]
        bool LockAndUnlockAward(int awardID, int isActive);

        [OperationContract]
        bool CheckAwardNameEnglishExists(string awardNameEnglish);

        [OperationContract]
        IEnumerable<AwardBO> GetListAward(int start, int end);

        [OperationContract]
        IEnumerable<AwardNumberBO> GetListAwardNumber(int start, int end);

        [OperationContract]
        bool InsertAwardNumber(AwardNumberBO award);

        [OperationContract]
        bool UpdateAwardNumber(AwardNumberBO award);

        [OperationContract]
        IEnumerable<AwardNumberBO> GetListAwardNumberByDate(int start, int end, DateTime fromDate, DateTime toDate);

        [OperationContract]
        bool LockAndUnlockAwardNumber(int numberID, int isActive);

        [OperationContract]
        IEnumerable<WinnerBO> GetListWinner(int start, int end, DateTime fromDate, DateTime toDate);

      
    }
}
