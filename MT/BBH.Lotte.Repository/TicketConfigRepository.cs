﻿using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.Domain;
using BBH.Lotte.Domain.Interfaces;
using BBH.Lotte.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Repository
{
    public class TicketConfigRepository : WCFClient<ITicketConfigServices>, ITicketConfigServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));

        public ObjResultMessage InsertTicketConfig(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.InsertTicketConfig(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "TicketConfigRepository -> InsertTicketConfig -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "TicketConfigRepository -> InsertTicketConfig : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(pathLog, ex.Message);
            }
            return objResult;
        }

        public ObjResultMessage UpdateTicketConfig(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.UpdateTicketConfig(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "TicketConfigRepository -> UpdateTicketConfig -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "TicketConfigRepository -> UpdateTicketConfig : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(pathLog, ex.Message);
            }
            return objResult;
        }

        public ObjResultMessage DeleteTicketConfig(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.DeleteTicketConfig(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "TicketConfigRepository -> DeleteTicketConfig -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "TicketConfigRepository -> DeleteTicketConfig : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(pathLog, ex.Message);
            }
            return objResult;
        }
       
        //public ObjResultMessage ListTicketConfig(ModelRSA objRsa, ref List<TicketConfigBO> lstTicketConfig)
        //{
        //    ObjResultMessage objResult = new ObjResultMessage();
        //    try
        //    {
               
        //        bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
        //        if (isCheckToken)
        //        {
        //            objResult = Proxy.ListTicketConfig(objRsa, ref lstTicketConfig);
        //        }
        //        else
        //        {
        //            objResult.IsError = true;
        //            objResult.Result = MessageResult.ErrorAuthenticate;
        //            objResult.MessageDetail = "TicketConfigRepository -> ListTicketConfig -> Token not found ";
        //            objResult.MessageDate = DateTime.Now;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objResult.IsError = true;
        //        objResult.Result = MessageResult.ErrorAuthenticate;
        //        objResult.MessageDetail = "TicketConfigRepository -> ListTicketConfig : " + ex.Message;
        //        objResult.MessageDate = DateTime.Now;
        //        Utility.WriteLog(pathLog, ex.Message);
        //    }
        //    return objResult;
        //}
        public IEnumerable<TicketConfigBO> ListTicketConfig()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<TicketConfigBO> ListTicketConfig = Proxy.ListTicketConfig();
                return ListTicketConfig;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }
        public ObjResultMessage GetTicketConfigDetail(ModelRSA objRsa, ref TicketConfigBO objTicketConfig)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.GetTicketConfigDetail(objRsa, ref objTicketConfig);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "TicketConfigRepository -> GetTicketConfigDetail -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "TicketConfigRepository -> GetTicketConfigDetail : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(pathLog, ex.Message);
            }
            return objResult;
        }

       public ObjResultMessage LockAndUnlockConfigTicket(ModelRSA objRsa)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.LockAndUnlockConfigTicket(objRsa);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "TicketConfigRepository -> LockAndUnlockConfigTicket -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "TicketConfigRepository -> LockAndUnlockConfigTicket : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utility.WriteLog(pathLog, ex.Message);
            }
            return objResult;
        }

        public bool CheckCoinID(int configID, string coinID)
        {
            bool kq = false;
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                kq = Proxy.CheckCoinID(configID, coinID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }
    }
}
