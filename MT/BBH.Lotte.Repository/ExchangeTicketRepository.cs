﻿using BBC.Core.WebService;
using BBH.Lotte.Domain;
using BBH.Lotte.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Repository
{
    public class ExchangeTicketRepository : WCFClient<IExchangeTicketServices>, IExchangeTicketServices
    {
        static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public IEnumerable<ExchangeTicketBO> ListAllExchangeTicket()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<ExchangeTicketBO> lstExchangeTicket = Proxy.ListAllExchangeTicket();
                return lstExchangeTicket;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<ExchangeTicketBO> ListAllExchangeTicketPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<ExchangeTicketBO> lstExchangeTicket = Proxy.ListAllExchangeTicketPaging(start, end);
                return lstExchangeTicket;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public IEnumerable<ExchangeTicketBO> ListExchangeTicketBySearch(DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<ExchangeTicketBO> lstExchangeTicket = Proxy.ListExchangeTicketBySearch(fromDate,toDate,start,end);
                return lstExchangeTicket;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public bool LockAndUnlockExchangeTicket(int exchangeID, int isActive)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            bool kq = false;
            try
            {
                kq = Proxy.LockAndUnlockExchangeTicket(exchangeID,isActive);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateStatusExchangeTicket(int exchangeID, int status, DateTime deleteDate, string deleteUser)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            bool kq = false;
            try
            {
                kq = Proxy.UpdateStatusExchangeTicket(exchangeID, status, deleteDate, deleteUser);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }


        public bool InsertExchangeTicket(ExchangeTicketBO exchangeTicket)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            bool kq = false;
            try
            {
                kq = Proxy.InsertExchangeTicket(exchangeTicket);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public bool UpdateExchangeTicket(ExchangeTicketBO exchangeTicket)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            bool kq = false;
            try
            {
                kq = Proxy.UpdateExchangeTicket(exchangeTicket);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

        public IEnumerable<CoinBO> ListAllCoin()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            try
            {
                IEnumerable<CoinBO> lstCoin = Proxy.ListAllCoin();
                return lstCoin;
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
                return null;
            }
        }

        public bool CheckCoinIDExists(string coinID, int exchangeID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog));
            bool kq = false;
            try
            {
                kq = Proxy.CheckCoinIDExists(coinID, exchangeID);
            }
            catch (Exception ex)
            {
                Utility.WriteLog(fileLog, ex.Message);
            }
            return kq;
        }

    }
}
