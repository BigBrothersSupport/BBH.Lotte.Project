﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBC.Core.WebService;
using BBH.Lotte.Domain;
using BBH.Lotte.Domain.Interfaces;
using BBH.Lotte.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Repository
{
    public class BookingMegaballRepository : WCFClient<IBookingMegaballServices>, IBookingMegaballServices
    {
        public IEnumerable<BookingMegaballBO> ListAllBookingMegaball()
        {
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingMegaball();
                return lstBooking;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingPage(int start, int end)
        {

            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingPage(start, end);
                return lstBooking;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end)
        {

            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingBySearch(memberID, fromDate, toDate, status, start, end);
                return lstBooking;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
        }
        public IEnumerable<BookingMegaballBO> GetListMemberBySearch(string keyword, DateTime fromDate, DateTime toDate, int start, int end)
        {
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.GetListMemberBySearch(keyword, fromDate, toDate, start, end);
                return lstBooking;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
        }

        public bool UpdateStatusBookingMegaball(int bookingID, int status)
        {
            bool kq = false;

            try
            {
                kq = Proxy.UpdateStatusBookingMegaball(bookingID, status);
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            return kq;
        }

        public ObjResultMessage CountAllBookingMegaball(ModelRSA objRsa, ref BookingMegaballBO objBooking)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                bool isCheckToken = Utility.CheckToken(objRsa.UserName, objRsa.Token);
                if (isCheckToken)
                {
                    objResult = Proxy.CountAllBookingMegaball(objRsa, ref objBooking);
                }
                else
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.ErrorAuthenticate;
                    objResult.MessageDetail = "BookingMegaballRepository -> CountAllBookingMegaball -> Token not found ";
                    objResult.MessageDate = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.ErrorAuthenticate;
                objResult.MessageDetail = "BookingMegaballRepository -> CountAllBookingMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            return objResult;
        }

        public System.Threading.Tasks.Task<BookingMegaballBO[]> ListAllBookingMegaballAsync()
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<BookingMegaballBO[]> ListAllBookingPageAsync(int start, int end)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<BookingMegaballBO[]> ListAllBookingBySearchAsync(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end)
        {
            throw new NotImplementedException();
        }

        public System.Threading.Tasks.Task<bool> UpdateStatusBookingMegaballAsync(int bookingID, int status)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaballByDate(DateTime dtDate)
        {
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingMegaballByDate(dtDate);
                return lstBooking;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaballByDraw(int intDrawID)
        {
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingMegaballByDraw(intDrawID);
                return lstBooking;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
        }

        public decimal GetAwardNotJackpot(string strNumberCheck, string strExtraNumber, int intDrawID)
        {
            decimal value = 0;
            try
            {
                value = Proxy.GetAwardNotJackpot(strNumberCheck, strExtraNumber, intDrawID);
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                value = 0;
            }
            return value;
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaballForRevenueFreeByDraw(int intIsRevenueFreeTicket)
        {
            try
            {
                IEnumerable<BookingMegaballBO> lstBooking = Proxy.ListAllBookingMegaballForRevenueFreeByDraw(intIsRevenueFreeTicket);
                return lstBooking;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
        }

        public bool UpdateIsRevenueFreeTicket(string stringBookingID)
        {
            bool rs = false;
            try
            {
                rs = Proxy.UpdateIsRevenueFreeTicket(stringBookingID);
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
        }
    }
}
