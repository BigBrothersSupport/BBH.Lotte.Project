﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain
{
    public class AwardNumberBO
    {
        public int NumberID { get; set; }
        public int AwardID { get; set; }
        public string NumberValue { get; set; }
        public DateTime CreateDate { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }
        public int Priority { get; set; }

        public string AwardName { get; set; }

        public string StationName { get; set; }

        public string AwardNameEnglish { get; set; }

        public string StationNameEnglish { get; set; }


    }
}
