﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain
{
    public class GroupAdminBO
    {
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public int IsActive { get; set; }
    }
}
