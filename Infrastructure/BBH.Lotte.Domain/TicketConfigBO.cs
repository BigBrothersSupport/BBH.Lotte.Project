﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain
{
    [Serializable]
    [DataContract]
    public class TicketConfigBO
    {
        [DataMember]
        public int ConfigID { get; set; }
        [DataMember]
        public string ConfigName { get; set; }
        [DataMember]
        public decimal CoinValues { get; set; }
        [DataMember]
        public int NumberTicket { get; set; }
        [DataMember]
        public string CoinID { get; set; }
        [DataMember]
        public int IsActive { get; set; }
        [DataMember]
        public int IsDeleted { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string CreatedUser { get; set; }
        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public string UpdatedUser { get; set; }
        [DataMember]
        public DateTime DeletedDate { get; set; }
        [DataMember]
        public string DeletedUser { get; set; }
        [DataMember]
        public int TotalRecord { get; set; }
    }
}
