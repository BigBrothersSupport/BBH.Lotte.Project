﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BBH.Lotte.Domain
{
    public class BookingMegaballBO
    {
        public int BookingID { get; set; }

        public int MemberID { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime OpenDate { get; set; }

        public int Status { get; set; }

        public string Email { get; set; }

        public string TransactionCode { get; set; }

        public int TotalRecord { get; set; }

        public string Note { get; set; }

        public string NoteEnglish { get; set; }

        public int FirstNumber { get; set; }

        public int SecondNumber { get; set; }

        public int ThirdNumber { get; set; }

        public int FourthNumber { get; set; }

        public int FivethNumber { get; set; }

        public int ExtraNumber { get; set; }
        public string NumberWin { get; set; }

        public int TotalAllBookingMegaball { get; set; }

        public int TotalNewBookingMegaball { get; set; }

        public int Quantity { get; set; }

        public int DrawID { get; set; }

        public int IsFreeTicket { get; set; }

        public int IsRevenueFreeTicket { get; set; }
    }
}
