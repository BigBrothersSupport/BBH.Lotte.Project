﻿using BBC.Core.Database;
using BBH.Lotte.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain.Interfaces
{
    [ServiceContract]
    public interface ITransactionCoinServices
    {
        [OperationContract]
        IEnumerable<TransactionCoinBO> ListTransactionCoin();

        [OperationContract]
        IEnumerable<TransactionCoinBO> ListTransactionCoinPaging(int start, int end);

        [OperationContract]
        IEnumerable<TransactionCoinBO> ListTransactionCoinBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end);

        [OperationContract]
        bool UpdateStatusTransaction(string transactionID, int status);

        [OperationContract]
        ObjResultMessage CountAllCoin(ModelRSA objRsa, ref TransactionCoinBO objTransaction);
    }
}
