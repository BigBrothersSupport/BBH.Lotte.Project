﻿using BBC.Core.Database;
using BBH.Lotte.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain.Interfaces
{
    [ServiceContract]
    public interface ICountryServices
    {
        [OperationContract]
         ObjResultMessage InsertCountry(ModelRSA objRsa);

        [OperationContract]
        IEnumerable<CountryBO> GetListCountry(int start, int end);

        [OperationContract]
        ObjResultMessage UpdateCountry(ModelRSA objRsa);

        [OperationContract]
        ObjResultMessage LockAndUnlockCountry(ModelRSA objRsa);

        [OperationContract]
        bool CheckCountryNameExists(int countryID, string countryName);

        [OperationContract]
        bool CheckPhoneZipCodeExists(int countryID, string phoneZipCode);
        [OperationContract]
        IEnumerable<CountryBO> GetListCountryBySearch(string keyword, int start, int end);
    }
}
