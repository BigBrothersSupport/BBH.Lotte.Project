﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.Domain.Interfaces
{
    [ServiceContract]
    public interface IAdminServices
    {
        [OperationContract]
        AdminBO LoginManagerAccount(string username, string password);

        [OperationContract]
        IEnumerable<AdminBO> ListAllAdmin();

        [OperationContract]
        AdminBO GerAdminDetailByUserName(string username);

        [OperationContract]
        int InsertAdmin(AdminBO admin);

        [OperationContract]
        AdminBO GerAdminDetailByID(int adminID);

        [OperationContract]
        bool CheckUserNameExists(string userName);

        [OperationContract]
        bool CheckEmailExists(string email);

        [OperationContract]
        int GetManagerIDNewest();

        [OperationContract]
        bool UpdateStatusAdmin(int adminID, int visible);

        [OperationContract]
        bool UpdateUserNameAndPasswordAdmin(int adminID, string userName, string password);

        [OperationContract]
        bool UpdateUserNamedAdmin(int adminID, string userName);

        [OperationContract]
        bool UpdateAdmin(int adminID, AdminBO admin);

        [OperationContract]
        bool InsertAccessRight(AccessRightBO access);

        [OperationContract]
        bool DeleteAccessRight(int adminID);

        [OperationContract]
        IEnumerable<AccessRightBO> ListAccessRightByManagerID(int adminID);

        [OperationContract]
        IEnumerable<GroupAdminBO> ListGroupAdmin();

        [OperationContract]
        GroupAdminBO GetGroupAdminDetail(int groupID);

        [OperationContract]
        bool InsertAdminfomation(AdminInfomationBO member);

        [OperationContract]
        bool UpdateAdminInfomation(AdminInfomationBO member, int adminID);

        [OperationContract]
        IEnumerable<AdminBO> ListAllAdminPaging(int start, int end);

    }
}
