﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.Domain.Interfaces
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INumberGenerralSvc" in both code and config file together.
    [ServiceContract]
    public interface INumberGenerralServices
    {
        [OperationContract]
        bool InsertNumberGeneral(NumberGeneralBO number);

        [OperationContract]
        bool UpdateNumberGeneral(string numberValue, int quantity, int numberID);

        [OperationContract]
        bool UpdateQuantityNumberGeneral(string numberValue, int quantity);

        [OperationContract]
        bool LockAndUnlockNumberGeneral(int numberID, int isActive);

        [OperationContract]
        NumberGeneralBO GetNumberGeneralDetail(int numberID);

        [OperationContract]
        bool CheckNumberExists(string numberValue);

        [OperationContract]
        IEnumerable<NumberGeneralBO> GetListNumberGeneralPaging(int start, int end);

        [OperationContract]
        IEnumerable<NumberGeneralBO> GetListNumberGeneral(int start, int end, DateTime fromDate, DateTime toDate);

    }
}
