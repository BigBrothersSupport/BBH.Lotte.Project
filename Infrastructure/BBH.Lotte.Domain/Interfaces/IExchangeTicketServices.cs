﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain.Interfaces
{
    [ServiceContract]
    public interface IExchangeTicketServices
    {
        [OperationContract]
        IEnumerable<ExchangeTicketBO> ListAllExchangeTicket();

        [OperationContract]
         IEnumerable<ExchangeTicketBO> ListAllExchangeTicketPaging(int start, int end);

        [OperationContract]
        IEnumerable<ExchangeTicketBO> ListExchangeTicketBySearch(DateTime? fromDate, DateTime? toDate, int start, int end);

        [OperationContract]
        bool LockAndUnlockExchangeTicket(int exchangeID, int isActive);
        [OperationContract]
        bool UpdateStatusExchangeTicket(int exchangeID, int status, DateTime deleteDate, string deleteUser);

        [OperationContract]
        bool InsertExchangeTicket(ExchangeTicketBO exchangeTicket);

        [OperationContract]
        bool UpdateExchangeTicket(ExchangeTicketBO exchangeTicket);

        [OperationContract]
         IEnumerable<CoinBO> ListAllCoin();

        [OperationContract]
         bool CheckCoinIDExists(string coinID, int exchangeID);


    }
}
