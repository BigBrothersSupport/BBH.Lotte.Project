﻿using BBC.Core.Database;
using BBH.Lotte.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain.Interfaces
{
    [ServiceContract]
    public interface IAwardWithdrawServices
    {
        [OperationContract]
        ObjResultMessage ListAwardWithdraw(ModelRSA objRsa, ref ModelRSA objRsaReturn);

        [OperationContract]
        ObjResultMessage UpdateStatusAwardWithdraw(ModelRSA objRsa);

        [OperationContract]
        ObjResultMessage GetAwardWithdrawDetail(ModelRSA objRsa, ref ModelRSA objRsaReturn);
    }
}
