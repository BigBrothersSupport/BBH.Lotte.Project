﻿using BBC.Core.Database;
using BBH.Lotte.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace BBH.Lotte.Domain.Interfaces
{
    [ServiceContract]
    public interface IAwardMegaballServices
    {
        [OperationContract]
        ObjResultMessage InsertAwardMegaball(ModelRSA objRsa);

        [OperationContract]
        ObjResultMessage UpdateAwardMegaball(ModelRSA objRsa);

        [OperationContract]
        ObjResultMessage LockAndUnlockAwardMegaball(ModelRSA objRsa);

        [OperationContract]
        IEnumerable<AwardMegaballBO> GetListAwardMegaball(int start, int end);

        [OperationContract]
        IEnumerable<AwardMegaballBO> ListAllAwardMegaballBySearch(DateTime? fromDate, DateTime? toDate, int start, int end);

        [OperationContract]
        IEnumerable<AwardMegaballBO> GetListAwardNumberByDate(int start, int end, DateTime fromDate, DateTime toDate);

        [OperationContract]
        ObjResultMessage CountAllAwardMegaball(ModelRSA objRsa, ref AwardMegaballBO objAwardMegaball);

        [OperationContract]
        List<WinnerBO> GetListWinner(int intNumberID);

        [OperationContract]
        bool CheckCreateDateExists( DateTime createDate);

        [OperationContract]
        AwardMegaballBO GetAwardMegabalByDrawID(int intDrawID);
        [OperationContract]
        ObjResultMessage UpdateJackpotAwardMegaball(decimal decJackpot, int intDrawID, decimal totalwon);

        [OperationContract]
        ObjResultMessage InsertWinningNumber(TicketWinningBO objTicket, DateTime dtmCreatedDate, ref int intWinnerJackPotCount);

        [OperationContract]
        int UpdateAwardValue(decimal dcJackPotValue, int intDrawID);

        [OperationContract]
        int CheckJackPotWinnerByDrawID(TicketWinningBO objTicket);

        //[OperationContract]
        //int InsertAwardMegaballLog(string strEmail, float fValue, DateTime dtmCreatedDate);
    }
}
