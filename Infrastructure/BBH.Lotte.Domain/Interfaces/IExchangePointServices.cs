﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain.Interfaces
{
    [ServiceContract]
    public interface IExchangePointServices
    {
        [OperationContract]
        IEnumerable<ExchangePointBO> ListAllExchangePoint();

        [OperationContract]
        IEnumerable<ExchangePointBO> ListAllExchangePointPaging(int start, int end);

        [OperationContract]
        IEnumerable<ExchangePointBO> ListExchangePointBySearch(DateTime? fromDate, DateTime? toDate, int start, int end);

        [OperationContract]
        bool LockAndUnlockExchangePoint(int exchangeID, int isActive);

        [OperationContract]
        bool UpdateStatusExchangePoint(int exchangeID, int status, DateTime deleteDate, string deleteUser);
        [OperationContract]
        bool InsertExchangePoint(ExchangePointBO exchangePoint);

        [OperationContract]
        bool UpdateExchangePoint(ExchangePointBO exchangePoint);
    }
}
