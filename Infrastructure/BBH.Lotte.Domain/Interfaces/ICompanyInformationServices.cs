﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Domain.Interfaces
{
    [ServiceContract]
    public interface ICompanyInformationServices
    {
        [OperationContract]
        IEnumerable<CompanyInformationBO> ListAllCompanyPaging(int start, int end);

        [OperationContract]
        IEnumerable<CompanyInformationBO> GetListCompanyBySearch(string keyword);
        [OperationContract]
        IEnumerable<CompanyInformationBO> GetCompanyByMemberID(int memberID);
        [OperationContract]
        IEnumerable<MemberBO> GetMemberByMemberID(int memberID);

    }
}
