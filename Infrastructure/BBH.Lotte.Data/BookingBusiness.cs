﻿using BBC.Core.Database;
using BBH.Lotte.Domain;
using BBH.Lotte.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace BBH.Lotte.Data
{
    public class BookingBusiness : IBookingServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public IEnumerable<BookingBO> ListAllBooking()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingBO> lstTransaction = new List<BookingBO>();
                string sql = "SP_ListAllBooking";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingBO booking = new BookingBO();
                    booking.MemberID = int.Parse(reader["MemberID"].ToString());

                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    booking.NumberValue = reader["NumberValue"].ToString();
                    booking.Email = reader["Email"].ToString();
                    booking.E_Wallet = reader["E_Wallet"].ToString();
                    lstTransaction.Add(booking);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public IEnumerable<BookingBO> ListAllBookingPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingBO> lstTransaction = new List<BookingBO>();
                string sql = "SP_ListAllBookingPaging";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingBO booking = new BookingBO();
                    booking.MemberID = int.Parse(reader["MemberID"].ToString());

                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    booking.NumberValue = reader["NumberValue"].ToString();
                    booking.Email = reader["Email"].ToString();
                    booking.E_Wallet = reader["E_Wallet"].ToString();
                    booking.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(booking);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingBO> ListAllBookingBySearch(int memberID, DateTime fromDate, DateTime toDate, int status, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingBO> lstTransaction = new List<BookingBO>();
                //string sql = "select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end";
                string sql = "SP_ListBookingBySearchCMS";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@memberID", memberID);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                pa[5] = new SqlParameter("@status", status);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingBO booking = new BookingBO();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());

                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    booking.NumberValue = reader["NumberValue"].ToString();
                    booking.Email = reader["Email"].ToString();
                    booking.E_Wallet = reader["E_Wallet"].ToString();
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    booking.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(booking);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateStatusBooking(int bookingID, int status)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateStatusBooking";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@status", status);
                pa[1] = new SqlParameter("@bookingID", bookingID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public BookingBO CountAllBooking()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                BookingBO booking = null;
                string sql = "CountAllBooking";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    booking = new BookingBO();
                    booking.TotalAllBooking = int.Parse(reader["TotalAllBooking"].ToString());
                    booking.TotalNewBooking = int.Parse(reader["TotalNewBooking"].ToString());
                }
                return booking;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
