﻿using BBC.Core.Database;
using BBH.Lotte.Domain;
using BBH.Lotte.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.Data
{
    public class CompanyInformationBusiness : ICompanyInformationServices
    {
        public string pathLog = ConfigurationManager.AppSettings["PathLog"];

        public IEnumerable<CompanyInformationBO> ListAllCompanyPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<CompanyInformationBO> lstCompany = new List<CompanyInformationBO>();
                string sql = "SP_ListAllCompanypaging";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CompanyInformationBO company = new CompanyInformationBO();

                    company.MemberID = int.Parse(reader["MemberID"].ToString());
                    company.Email = reader["Email"].ToString();
                    company.CompanyName = reader["CompanyName"].ToString();
                    company.Address = reader["Address"].ToString();
                    company.Phone = reader["Phone"].ToString();
                    company.Description = reader["Description"].ToString();
                    company.Website = reader["Website"].ToString();
                    company.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    company.FullName = reader["FullName"].ToString();

                    lstCompany.Add(company);

                }
                return lstCompany;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<CompanyInformationBO> GetListCompanyBySearch(string keyword)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<CompanyInformationBO> lstCompany = new List<CompanyInformationBO>();
                string sql = "SP_SearchCompanyByKeyword";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@keyword", keyword);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    CompanyInformationBO company = new CompanyInformationBO();

                    company.MemberID = int.Parse(reader["MemberID"].ToString());
                    company.CompanyName = reader["CompanyName"].ToString();
                    company.Email = reader["Email"].ToString();
                    company.Phone = reader["Phone"].ToString();
                    company.Address =  reader["Address"].ToString();
                    company.Website = reader["Website"].ToString();
                    company.Description = reader["Description"].ToString();

                    //member.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstCompany.Add(company);

                }
                return lstCompany;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        
        public IEnumerable<CompanyInformationBO> GetCompanyByMemberID(int memberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");

            try
            {
                CompanyInformationBO company = null;
                List<CompanyInformationBO> lstcompany = new List<CompanyInformationBO>();
                string sql = "SP_GetCompanyByMemberID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    company = new CompanyInformationBO();
                    company.MemberID = int.Parse(reader["MemberID"].ToString());
                    company.CompanyName = reader["CompanyName"].ToString();
                    company.Phone = reader["Phone"].ToString();
                    company.Email = reader["Email"].ToString();
                    company.Address = reader["Address"].ToString();
                    company.Website = reader["Website"].ToString();
                    company.Description = reader["Description"].ToString();
                    lstcompany.Add(company);
                }
                return lstcompany;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<MemberBO> GetMemberByMemberID(int memberID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                MemberBO member = null;
                List<MemberBO> lstMember = new List<MemberBO>();
                //string sql = "SP_GetCompanyByMemberID";
                string sql = "select * from Member where MemberID=@memberID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.E_Wallet = reader["E_Wallet"].ToString();
                    member.Email = reader["Email"].ToString();
                    member.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    member.IsActive = int.Parse(reader["IsActive"].ToString());

                    lstMember.Add(member);
                }
                return lstMember;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

    }
}
