﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBH.Lotte.Domain;
using BBC.Core.Database;
using System.Configuration;
using System.IO;
using BBH.Lotte.Domain.Interfaces;

namespace BBH.Lotte.Data
{
    public class AdminBusiness : IAdminServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public int InsertAdmin(AdminBO admin)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                int adminID = 0;
                SqlParameter[] pa = new SqlParameter[11];
                string sql = "SP_InsertAdmin";
                pa[0] = new SqlParameter("@userName", admin.UserName);
                pa[1] = new SqlParameter("@pass", admin.Password);
                pa[2] = new SqlParameter("@hashKey", admin.Hashkey);
                pa[3] = new SqlParameter("@isActive", admin.IsActive);
                pa[4] = new SqlParameter("@createDate", admin.CreateDate);
                pa[5] = new SqlParameter("@fullName", admin.FullName);
                pa[6] = new SqlParameter("@email", admin.Email);
                pa[7] = new SqlParameter("@mobile", admin.Mobile);
                pa[8] = new SqlParameter("@address", admin.Address);
                pa[9] = new SqlParameter("@avatar", admin.Avatar);
                pa[10] = new SqlParameter("@isDelete", admin.IsDelete);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                adminID = Convert.ToInt32(command.ExecuteScalar());
                return adminID;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception insert admin : " + ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

        public AdminBO LoginManagerAccount(string username, string password)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                AdminBO admin = null;
                string sql = "SP_LoginManagerAccount";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@userName", username);
                pa[1] = new SqlParameter("@pass", password);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    admin = new AdminBO();
                    admin.UserName = reader["UserName"].ToString();
                    if (!string.IsNullOrEmpty(reader["GroupID"].ToString()))
                    {
                        admin.GroupID = Convert.ToInt32(reader["GroupID"].ToString());
                    }
                }
                return admin;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog,"Exception login admin : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<AdminBO> ListAllAdmin()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));

            Sqlhelper helper = new Sqlhelper("","ConnectionString");

            try
            {

                List<AdminBO> list = new List<AdminBO>();
                //string sql = "select a.*,ac.GroupID,g.GroupName from [Admin] a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID";
                string sql = "SP_ListAllAdmin";

                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AdminBO admin = new AdminBO();
                    admin.AdminID = int.Parse(reader["AdminID"].ToString());
                    admin.UserName = reader["UserName"].ToString();
                    admin.Password = reader["Password"].ToString();
                    admin.Hashkey = reader["HashKey"].ToString();
                    admin.IsActive = int.Parse(reader["IsActive"].ToString());
                    if (!string.IsNullOrEmpty(reader["IsDelete"].ToString()))
                    {
                        admin.IsDelete = Convert.ToInt32(reader["IsDelete"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["FullName"].ToString()))
                    {
                        admin.FullName = reader["FullName"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        admin.Email = reader["Email"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Mobile"].ToString()))
                    {
                        admin.Mobile = reader["Mobile"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Address"].ToString()))
                    {
                        admin.Address = reader["Address"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Avatar"].ToString()))
                    {
                        admin.Avatar = reader["Avatar"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["GroupName"].ToString()))
                    {
                        admin.GroupName = reader["GroupName"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["GroupID"].ToString()))
                    {
                        admin.GroupID = Convert.ToInt32(reader["GroupID"].ToString());
                    }
                    list.Add(admin);
                }
                return list;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception get list admind : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<AdminBO> ListAllAdminPaging(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");

            try
            {
                List<AdminBO> list = new List<AdminBO>();
                //string sql = "select a.*,ac.GroupID,g.GroupName from [Admin] a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID";
                string sql = "SP_ListAllAdminPaging";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql,pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AdminBO admin = new AdminBO();
                    admin.AdminID = int.Parse(reader["AdminID"].ToString());
                    admin.UserName = reader["UserName"].ToString();
                    admin.Password = reader["Password"].ToString();
                    admin.Hashkey = reader["HashKey"].ToString();
                    admin.IsActive = int.Parse(reader["IsActive"].ToString());
                    if (!string.IsNullOrEmpty(reader["IsDelete"].ToString()))
                    {
                        admin.IsDelete = Convert.ToInt32(reader["IsDelete"].ToString());
                    }
                    if (!string.IsNullOrEmpty(reader["FullName"].ToString()))
                    {
                        admin.FullName = reader["FullName"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        admin.Email = reader["Email"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Mobile"].ToString()))
                    {
                        admin.Mobile = reader["Mobile"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Address"].ToString()))
                    {
                        admin.Address = reader["Address"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Avatar"].ToString()))
                    {
                        admin.Avatar = reader["Avatar"].ToString();
                    }
                    try
                    {
                        if (!string.IsNullOrEmpty(reader["GroupName"].ToString()))
                        {
                            admin.GroupName = reader["GroupName"].ToString();
                        }
                    }
                    catch { }
                    try
                    {
                        if (!string.IsNullOrEmpty(reader["GroupID"].ToString()))
                        {
                            admin.GroupID = Convert.ToInt32(reader["GroupID"].ToString());
                        }
                    }
                    catch { }
                    admin.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    list.Add(admin);
                }
                return list;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception get list admind : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public bool CheckUserNameExists(string userName)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_CheckUserNameAdmin";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@userName", userName);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception CheckUsername admin : " + ex.Message); return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool CheckEmailExists(string email)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                bool rs = false;
                string sql = "SP_CheckEmailAdmin";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@email", email);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception CheckEmail admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public AdminBO GerAdminDetailByUserName(string username)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {

                AdminBO admin = null;
                string sql = "SP_GerAdminDetail";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@userName", username);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    admin = new AdminBO();
                    admin.AdminID = int.Parse(reader["AdminID"].ToString());
                    admin.UserName = reader["UserName"].ToString();
                    admin.Password = reader["Password"].ToString();
                    admin.Hashkey = reader["HashKey"].ToString();
                    admin.IsActive = int.Parse(reader["IsActive"].ToString());
                    if (!string.IsNullOrEmpty(reader["FullName"].ToString()))
                    {
                        admin.FullName = reader["FullName"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        admin.Email = reader["Email"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Mobile"].ToString()))
                    {
                        admin.Mobile = reader["Mobile"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Address"].ToString()))
                    {
                        admin.Address = reader["Address"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Avatar"].ToString()))
                    {
                        admin.Avatar = reader["Avatar"].ToString();
                    }
                }
                return admin;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception Get admindetail : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public AdminBO GerAdminDetailByID(int adminID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {

                AdminBO admin = null;
                string sql = "SP_GerAdminDetailByID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@adminID", adminID);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    admin = new AdminBO();
                    admin.AdminID = int.Parse(reader["AdminID"].ToString());
                    admin.UserName = reader["UserName"].ToString();
                    admin.Password = reader["Password"].ToString();
                    admin.Hashkey = reader["HashKey"].ToString();
                    admin.IsActive = int.Parse(reader["IsActive"].ToString());
                    if (!string.IsNullOrEmpty(reader["FullName"].ToString()))
                    {
                        admin.FullName = reader["FullName"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Email"].ToString()))
                    {
                        admin.Email = reader["Email"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Mobile"].ToString()))
                    {
                        admin.Mobile = reader["Mobile"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Address"].ToString()))
                    {
                        admin.Address = reader["Address"].ToString();
                    }
                    if (!string.IsNullOrEmpty(reader["Avatar"].ToString()))
                    {
                        admin.Avatar = reader["Avatar"].ToString();
                    }
                }
                return admin;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception get admindetail : " + ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public int GetManagerIDNewest()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {

                string sql = "SP_GetManagerIDNewest";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader dr = command.ExecuteReader();
                int managerID = 0;
                if (dr.Read())
                {
                    managerID = int.Parse(dr["AdminID"].ToString());
                }
                return managerID;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception get managerid newest : " + ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateStatusAdmin(int adminID, int visible)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                string sql = "SP_UpdateStatusAdmin";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@adminID", adminID);
                pa[1] = new SqlParameter("@isDelete", visible);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception update status admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateUserNameAndPasswordAdmin(int adminID, string userName, string password)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog,"Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                string sql = "SP_UpdateUserNameAndPasswordAdmin";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@adminID", adminID);
                pa[1] = new SqlParameter("@userName", userName);
                pa[2] = new SqlParameter("@password", password);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception update username and password admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateUserNamedAdmin(int adminID, string userName)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                string sql = "SP_UpdateUserNamedAdmin";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@adminID", adminID);
                pa[1] = new SqlParameter("@userName", userName);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception update username admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateAdmin(int adminID, AdminBO admin)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                string sql = "SP_UpdateAdmin";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@adminID", adminID);
                pa[1] = new SqlParameter("@fullName", admin.FullName);
                pa[2] = new SqlParameter("@email", admin.Email);
                pa[3] = new SqlParameter("@mobile", admin.Mobile);
                pa[4] = new SqlParameter("@address", admin.Address);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "Exception update admin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool InsertAccessRight(AccessRightBO access)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                bool rs = false;
                string sql = "SP_InsertAccessRight";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@adminID", access.AdminID);
                pa[1] = new SqlParameter("@groupID", access.GroupID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch
            {
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool DeleteAccessRight(int adminID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                bool rs = false;
                string sql = "SP_DeleteAccessRight";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@adminID", adminID);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch
            {
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<AccessRightBO> ListAccessRightByManagerID(int adminID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                List<AccessRightBO> list = new List<AccessRightBO>();
                string sql = "SP_ListAccessRightByManagerID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@adminID", adminID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AccessRightBO access = new AccessRightBO();
                    access.AdminID = int.Parse(reader["AdminID"].ToString());
                    access.GroupID = int.Parse(reader["GroupID"].ToString());
                    list.Add(access);
                }
                return list;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<GroupAdminBO> ListGroupAdmin()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                List<GroupAdminBO> list = new List<GroupAdminBO>();
                string sql = "SP_ListGroupAdmin";

                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    GroupAdminBO group = new GroupAdminBO();
                    group.GroupID = int.Parse(reader["GroupID"].ToString());
                    group.GroupName = reader["GroupName"].ToString();
                    group.IsActive = int.Parse(reader["IsActive"].ToString());
                    list.Add(group);
                }
                return list;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public GroupAdminBO GetGroupAdminDetail(int groupID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                GroupAdminBO group = null;
                string sql = "SP_GetGroupAdminDetail";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@groupID", groupID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    group = new GroupAdminBO();
                    group.GroupID = int.Parse(reader["GroupID"].ToString());
                    group.GroupName = reader["GroupName"].ToString();
                    group.IsActive = int.Parse(reader["IsActive"].ToString());

                }
                return group;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public bool InsertAdminfomation(AdminInfomationBO member)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                bool rs = false;
                string sql = "insert into admininfomation(AdminID,FullName,Email,Mobile,Address,BirthDay,Gender,Avatar) values(@adminID,@fullname,@email,@mobile,@addressMember,@birthday,@gender,@avatar)";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@adminID", member.AdminID);
                pa[1] = new SqlParameter("@fullname", member.FullName);
                pa[2] = new SqlParameter("@email", member.Email);

                pa[3] = new SqlParameter("@mobile", member.Mobile);

                pa[4] = new SqlParameter("@gender", member.Gender);
                if (member.Birthday != null)
                {
                    pa[5] = new SqlParameter("@birthday", member.Birthday);
                }
                else
                {
                    pa[5] = new SqlParameter("@birthday", DBNull.Value);
                }

                pa[6] = new SqlParameter("@avatar", member.Avatar);
                pa[7] = new SqlParameter("@addressMember", member.Address);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateAdminInfomation(AdminInfomationBO member, int adminID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString"); 
            try
            {
                bool rs = false;
                string sql = "update admininfomation set FullName=@fullName,Email=@email,Mobile=@mobile,Gender=@gender,BirthDay=@birthday,Avatar=@avatar,Address=@addressMember where AdminID=@adminID";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@adminID", adminID);
                pa[1] = new SqlParameter("@fullname", member.FullName);
                pa[2] = new SqlParameter("@email", member.Email);

                pa[3] = new SqlParameter("@mobile", member.Mobile);

                pa[4] = new SqlParameter("@gender", member.Gender);
                pa[5] = new SqlParameter("@birthday", member.Birthday);

                pa[6] = new SqlParameter("@avatar", member.Avatar);
                pa[7] = new SqlParameter("@addressMember", member.Address);

                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {

                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        //public static bool CheckUserNameExists(string userName)
        //{
        //    string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        //    Sqlhelper helper = new Sqlhelper("", "ConnectionString");
        //    try
        //    {
        //        bool rs = false;
        //        string sql = "SP_CheckUserNameAdmin";
        //        SqlParameter[] pa = new SqlParameter[1];
        //        pa[0] = new SqlParameter("@userName", userName);
        //        SqlCommand command = helper.GetCommand(sql, pa, true);
        //        SqlDataReader reader = command.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            rs = true;
        //        }
        //        return rs;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(fileLog, "Exception CheckUsername admin : " + ex.Message); return false;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        //public static bool CheckEmailExists(string email)
        //{
        //    string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        //    Sqlhelper helper = new Sqlhelper("", "ConnectionString");
        //    try
        //    {
        //        bool rs = false;
        //        string sql = "SP_CheckEmailAdmin";
        //        SqlParameter[] pa = new SqlParameter[1];
        //        pa[0] = new SqlParameter("@email", email);
        //        SqlCommand command = helper.GetCommand(sql, pa, true);
        //        SqlDataReader reader = command.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            rs = true;
        //        }
        //        return rs;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(fileLog, "Exception CheckEmail admin : " + ex.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}
    }
}
