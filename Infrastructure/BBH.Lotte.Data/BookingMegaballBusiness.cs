﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BBC.Core.Database;
using BBH.Lotte.Domain;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using BBH.Lotte.Domain.Interfaces;
using BBH.Lotte.Shared;
using BBC.Core.Common.Log;

namespace BBH.Lotte.Data
{
    public class BookingMegaballBusiness : IBookingMegaballServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        public static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaball()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaballBO> lstTransaction = new List<BookingMegaballBO>();
                string sql = "SP_ListAllBookingMegaball";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaballBO booking = new BookingMegaballBO();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    //booking.Note = reader["Note"].ToString();
                    //booking.NoteEnglish = reader["NoteEnglish"].ToString();
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    //booking.NumberWin = reader["NumberWin"].ToString();

                    //booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    //booking.SecondNumber = int.Parse(reader["Status"].ToString());
                    //booking.ThirdNumber = int.Parse(reader["Status"].ToString());
                    //booking.FourthNumber = int.Parse(reader["Status"].ToString());
                    //booking.FivethNumber = int.Parse(reader["Status"].ToString());
                    //booking.ExtraNumber = int.Parse(reader["Status"].ToString());

                    lstTransaction.Add(booking);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public IEnumerable<BookingMegaballBO> ListAllBookingPage(int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaballBO> lstTransaction = new List<BookingMegaballBO>();
                string sql = "SP_ListAllBookingMegaballPaging";

                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaballBO booking = new BookingMegaballBO();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.Email = reader["Email"].ToString();

                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());

                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    //booking.NumberWin = reader["NumberWin"].ToString();
                    booking.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());

                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());

                    lstTransaction.Add(booking);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int status, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaballBO> lstTransaction = new List<BookingMegaballBO>();
                string sql = "SP_ListBookingMegaballBySearchCMS";

                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@memberID", memberID);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                pa[5] = new SqlParameter("@status", status);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaballBO booking = new BookingMegaballBO();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());

                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    try
                    {
                        booking.Email = reader["Email"].ToString();
                    }
                    catch { }
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    booking.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());

                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());

                    lstTransaction.Add(booking);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateStatusBookingMegaball(int bookingID, int status)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateStatusBookingMegaball";
                SqlParameter[] pa = new SqlParameter[2];

                pa[0] = new SqlParameter("@bookingID", bookingID);
                pa[1] = new SqlParameter("@status", status);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public ObjResultMessage CountAllBookingMegaball(ModelRSA objRsa, ref BookingMegaballBO objBooking)
        {
            ObjResultMessage objResult = new ObjResultMessage();

            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {

                string sql = "SP_CountAllBookingMegaball";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    objBooking = new BookingMegaballBO();
                    objBooking.TotalAllBookingMegaball = int.Parse(reader["TotalAllBookingMegaball"].ToString());
                    objBooking.TotalNewBookingMegaball = int.Parse(reader["TotalNewBookingMegaball"].ToString());
                }

            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "BookingMegaballBusiness -> CountAllBookingMegaball : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                Utilitys.WriteLog(fileLog, ex.Message);

            }
            finally
            {
                helper.destroy();
            }
            return objResult;
        }

        public IEnumerable<BookingMegaballBO> GetListMemberBySearch(string keyword, DateTime fromDate, DateTime toDate, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaballBO> lstTransaction = new List<BookingMegaballBO>();
                string sql = "SP_SearchMemberByBookingMegaball";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@keyword", keyword);
                pa[1] = new SqlParameter("@fromDate", fromDate);
                pa[2] = new SqlParameter("@toDate", toDate);
                pa[3] = new SqlParameter("@start", start);
                pa[4] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaballBO booking = new BookingMegaballBO();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    try
                    {
                        booking.Email = reader["Email"].ToString();
                    }
                    catch { }
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());

                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    //booking.NumberWin = reader["NumberWin"].ToString();
                    booking.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());

                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());

                    lstTransaction.Add(booking);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaballByDate(DateTime dtDate)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaballBO> lstTransaction = new List<BookingMegaballBO>();
                string sql = "SP_ListAllBookingMegaballByDate";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@date", dtDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaballBO booking = new BookingMegaballBO();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    lstTransaction.Add(booking);
                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaballByDraw(int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaballBO> lstTransaction = new List<BookingMegaballBO>();
                string sql = "SP_ListAllBookingMegaballByDraw";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@DrawID", intDrawID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaballBO booking = new BookingMegaballBO();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    booking.DrawID = int.Parse(reader["DrawID"].ToString());
                    booking.IsFreeTicket = int.Parse(reader["IsFreeTicket"].ToString());
                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    lstTransaction.Add(booking);
                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

       
        private IEnumerable<AwardBO> GetAllListAward()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "SP_GetAllAwardFE";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardBO number = new AwardBO();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.AwardName = reader["AwardName"].ToString();
                    number.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    number.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    number.NumberBallNormal = int.Parse(reader["NumberBallNormal"].ToString());
                    number.NumberBallGold = int.Parse(reader["NumberBallGold"].ToString());
                    number.AwardFee = decimal.Parse(reader["AwardFee"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public decimal GetAwardNotJackpot(string strNumberCheck, string strExtraNumber, int intDrawID)
        {
            decimal totalWon = 0;
            try
            {
                string[] arrNumber = strNumberCheck.Split(',');
                IEnumerable<BookingMegaballBO> lstBookingMegaball = new List<BookingMegaballBO>();
                lstBookingMegaball = ListAllBookingMegaballByDraw(intDrawID);
                if (lstBookingMegaball != null || lstBookingMegaball.Count() > 0)
                {
                    List<int> lstTicketWinner = new List<int>();
                    foreach (var item in lstBookingMegaball)
                    {
                        int countBallNornal = 0;
                        int countBallGold = 0;
                        var lstTemp = lstTicketWinner.Where(x => x == item.BookingID);
                        if (lstTemp.Count() == 0)
                        {
                            for (int i = 0; i < arrNumber.Length; i++)
                            {
                                if (arrNumber[i].Trim() == item.FirstNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.SecondNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.ThirdNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.FourthNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.FivethNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                            }
                            if (strExtraNumber.Trim() == item.ExtraNumber.ToString())
                            {
                                countBallGold++;
                            }
                            IEnumerable<AwardBO> lstAward = GetAllListAward();
                            if (lstAward != null && lstAward.Count() > 0)
                            {
                                List<AwardBO> lstTempAward = lstAward.Where(x => x.NumberBallNormal == countBallNornal && x.NumberBallGold == countBallGold).ToList();
                                if (lstTempAward != null && lstTempAward.Count() > 0)
                                {
                                    foreach (var itemTempAward in lstTempAward)
                                    {
                                        if (itemTempAward.Priority == 2)
                                        {
                                            totalWon += itemTempAward.AwardValue;
                                        }
                                    }
                                    lstTicketWinner.Add(item.BookingID);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                totalWon = 0;
                Utilitys.WriteLog(fileLog, ex.Message);
            }
            return totalWon;
        }

        public IEnumerable<BookingMegaballBO> ListAllBookingMegaballForRevenueFreeByDraw(int intIsRevenueFreeTicket)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaballBO> lstTransaction = new List<BookingMegaballBO>();
                string sql = "SP_ListAllBookingMegaballForRevenueFreeByDraw";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@IsRevenueFreeTicket", intIsRevenueFreeTicket);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaballBO booking = new BookingMegaballBO();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    booking.DrawID = int.Parse(reader["DrawID"].ToString());
                    booking.IsFreeTicket = int.Parse(reader["IsFreeTicket"].ToString());
                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    booking.IsRevenueFreeTicket = int.Parse(reader["IsRevenueFreeTicket"].ToString());
                    lstTransaction.Add(booking);
                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateIsRevenueFreeTicket(string strBookingID)
        {
            bool rs = false;
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_UpdateBookingMaegaball_FreeTicket";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@BookingID", strBookingID);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            finally
            {
                helper.destroy();
            }
            return rs;
        }
    }
}
