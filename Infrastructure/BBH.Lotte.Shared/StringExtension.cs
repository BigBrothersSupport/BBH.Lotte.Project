﻿using Newtonsoft.Json;
using System;

namespace BBH.Lotte.Shared
{
    public static class StringExtension
    {
        public static bool EqualTo(this string src, string dest)
        {
            if (src == null)
            {
                if (dest == null)
                    return true;
                else
                    return false;
            }
            return src.Equals(dest, StringComparison.CurrentCultureIgnoreCase);
        }

        public static string ToJsonString(this object src, bool hasFormat = true)
        {
            return JsonConvert.SerializeObject(src, hasFormat ? Formatting.Indented : Formatting.None);
        }
    }
}
