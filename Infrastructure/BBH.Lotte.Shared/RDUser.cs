﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.Shared
{
    public class RDUser : ActionFilterAttribute
    {
        public static int MemberID
        {
            get
            {
                return Convert.ToInt32(HttpContext.Current.Session[SessionKey.MEMBERID]);
            }
        }
        public static string FullName
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey._FULLNAME]);
            }
        }
        public static string Email
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey.EMAIL]);
            }
        }
        public static string E_Wallet
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey.EWALLET]);
            }
        }
        public static double BTCPoint
        {
            get
            {
                return Convert.ToDouble(HttpContext.Current.Session[SessionKey.BTCPOINT]);
            }
        }
        public static double CarcoinPoint
        {
            get
            {
                return Convert.ToDouble(HttpContext.Current.Session[SessionKey.CARCOINPOINT]);
            }
        }
        public static string BTCAddress
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey.BTCADDRESS]);
            }
        }
        public static string CARCOINADDRESS
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey.CARCOINADDRESS]);
            }
        }
        public static string NumberTickets
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey.NUMBERTICKET]);
            }
        }

        public static string NumberTicketsFree
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey.NUMBERTICKETFREE]);
            }
        }
        public static string RawNumber
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey.RawNumber]);
            }
        }
        public static string LinkReference
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Request.Url.Host + "/play-game?strLinkReference=" + HttpContext.Current.Session[SessionKey.LinkReference]);
            }
        }
        /// <summary>
        /// Specific log in Url
        /// </summary>
        public string SignInUrl { get; set; }
        /// <summary>
        /// true: is logedIn, false: not login
        /// </summary>
        /// <returns></returns>
        public static bool IsLogedIn()
        {
            if (System.Web.HttpContext.Current.Session[SessionKey.EMAIL] == null)
                return false;
            return true;
        }

        /// <summary>
        /// true: is logedIn, false: not login
        /// </summary>
        /// <returns></returns>
        public static bool IsEmptyPass()
        {
            if (System.Web.HttpContext.Current.Session[SessionKey.ISPASSWORD].ToString() == "false")
                return false;
            return true;
        }

        /// <summary>
        /// Created date: 15.03.2018
        /// Description: call when refresh web page -> check if not login then redirect to page Login
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!IsLogedIn())
            {
                SignInUrl = "/play-game";
                //context.HttpContext.Response.Redirect(SignInUrl, true);
                context.Result = new RedirectResult(SignInUrl, true);
            }
            else if (!IsEmptyPass())
            {
                SignInUrl = "/change-password";
                //context.HttpContext.Response.Redirect(SignInUrl, true);
                context.Result = new RedirectResult(SignInUrl, true);
            } 
        }

        //public override void OnActionExecuted(ActionExecutedContext filterContext)
        //{
        //    filterContext.HttpContext.Response.SetCookie(new HttpCookie("RefreshFilter", filterContext.HttpContext.Request.Url.ToString()));
        //}
    }

    public class RDUserAdmin
    {
        public static string UserName
        {
            get
            {
                return Convert.ToString(HttpContext.Current.Session[SessionKey.USERNAMEADMIN]);
            }
        }
    }
}