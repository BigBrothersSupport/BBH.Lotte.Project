﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Domain
{
    [DataContract]
    [Serializable]
    public class MemberReferenceBO
    {
        [DataMember]
        public int MemberID { get; set; }
        [DataMember]
        public string LinkReference { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public int MemberIDReference { get; set; }
        [DataMember]
        public string LockID { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public decimal Amount { get; set; }
    }
}
