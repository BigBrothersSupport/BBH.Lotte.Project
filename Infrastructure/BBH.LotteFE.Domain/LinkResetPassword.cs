﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Domain
{
    public class LinkResetPassword
    {
        public string Email { get; set; }
        public string LinkReset { get; set; }
        public int Status { get; set; }
        public int NumberSend { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ExpireLink { get; set; }
        public string IPAddress { get; set; }
    }
}
