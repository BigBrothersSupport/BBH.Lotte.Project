﻿using BBC.Core.Database;
using BBH.Lotte.Shared;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace BBH.LotteFE.Domain.Interfaces
{
    [ServiceContract]
    public interface IAwardWithdrawServices
    {
        [OperationContract]
        ObjResultMessage InsertAwardWithdraw(AwardWithdrawBO awardWithdraw);

        [OperationContract]
        ObjResultMessage GetAwardWithdrawDetail(string requestMemberID, DateTime awardDate,int bookingID, ref AwardWithdrawBO objAwardWithdraw);

        [OperationContract]
         IEnumerable<AwardWithdrawBO> GetAllListAward();

        [OperationContract]
        ObjResultMessage UpdateStatusAwardWithdraw(ModelRSA objRsa);
    }
}
