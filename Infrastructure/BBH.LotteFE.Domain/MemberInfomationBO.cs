﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Domain
{
    public class MemberInfomationBO
    {
        public int MemberID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
        public DateTime Birthday { get; set; }
        public int Gender { get; set; }
        public double Points { get; set; }
        public string UserName { get; set; }

        public int IsActive { get; set; }
    }
}
