﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Domain
{
    [Serializable]
    [DataContract]
    public class AwardWithdrawBO
    {
        [DataMember]
        public string TransactionID { get; set; }

        [DataMember]
        public string RequestMemberID { get; set; }

        [DataMember]
        public string RequestWalletAddress { get; set; }

        [DataMember]
        public int RequestStatus { get; set; }

        [DataMember]
        public DateTime RequestDate { get; set; }

        [DataMember]
        public DateTime AwardDate { get; set; }

        [DataMember]
        public string CoinIDWithdraw { get; set; }

        [DataMember]
        public decimal ValuesWithdraw { get; set; }

        [DataMember]
        public string AdminIDApprove { get; set; }

        [DataMember]
        public DateTime ApproveDate { get; set; }

        [DataMember]
        public string ApproveNote { get; set; }

        [DataMember]
        public int IsDeleted { get; set; }

        [DataMember]
        public string UpdatedUser { get; set; }

        [DataMember]
        public DateTime UpdatedDate { get; set; }
        [DataMember]
        public string DeletedUser { get; set; }

        [DataMember]
        public DateTime DeletedDate { get; set; }

        [DataMember]
        public int TotalRecord { get; set; }
        [DataMember]
         public int BookingID { get; set; }
    }
}
