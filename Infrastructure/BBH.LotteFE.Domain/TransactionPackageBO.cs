﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Data;
namespace BBH.LotteFE.Domain
{
    [Serializable]
    [DataContract]
    public class TransactionPackageBO
    {
        [DataMember]
        public int TransactionID { get; set; }
        [DataMember]
        public int MemberID { get; set; }
        [DataMember]
        public int PackageID { get; set; }
        [DataMember]
        public DateTime CreateDate { get; set; }
        [DataMember]
        public DateTime ExpireDate { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public string PackageName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string E_Wallet { get; set; }
        [DataMember]
        public string TransactionValue { get; set; }
        [DataMember]
        public string Note { get; set; }

        [DataMember]
        public int TotalRecord { get; set; }

        [DataMember]
        public string NoteEnglish { get; set; }

        [DataMember]
        public string PackageNameEnglish { get; set; }

    }
}
