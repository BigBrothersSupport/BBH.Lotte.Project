﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Data
{
    public class TicketConfigBusiness : ITicketConfigServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public IEnumerable<TicketConfigBO> GetListTicketConfig()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<TicketConfigBO> lstTicketConfigBO = new List<TicketConfigBO>();
                string sql = "SP_ListTicketConfig";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    TicketConfigBO objTicketConfigBO = new TicketConfigBO();

                    objTicketConfigBO.CoinID = reader["CoinID"].ToString();
                    objTicketConfigBO.CoinValues = double.Parse(reader["CoinValues"].ToString());
                    objTicketConfigBO.ConfigID = int.Parse(reader["ConfigID"].ToString());
                    objTicketConfigBO.ConfigName = reader["ConfigName"].ToString();
                    objTicketConfigBO.NumberTicket = int.Parse(reader["NumberTicket"].ToString());
                    lstTicketConfigBO.Add(objTicketConfigBO);
                }
                return lstTicketConfigBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
