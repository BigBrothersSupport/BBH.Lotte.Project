﻿using BBC.Core.Common.Log;
using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBH.Lotte.Shared;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using BBC.CWallet.ServiceListener.ServiceContract;
namespace BBH.LotteFE.Data
{
    public class BookingMegaballBusiness : IBookingMegaballServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public ObjResultMessage InsertBooking(BookingMegaball booking)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                string sql = "SP_InsertBooking_FE";
                SqlParameter[] pa = new SqlParameter[17];
                pa[0] = new SqlParameter("@memberID", booking.MemberID);
                pa[1] = new SqlParameter("@note", booking.Note);
                pa[2] = new SqlParameter("@quantity", booking.Quantity);
                pa[3] = new SqlParameter("@createDate", booking.CreateDate);
                pa[4] = new SqlParameter("@openDate", booking.OpenDate);
                pa[5] = new SqlParameter("@status", booking.Status);
                pa[6] = new SqlParameter("@transactionCode", booking.TransactionCode);
                pa[7] = new SqlParameter("@noteEnglish", booking.NoteEnglish);
                pa[8] = new SqlParameter("@firstNumber", booking.FirstNumber);
                pa[9] = new SqlParameter("@secondNumber", booking.SecondNumber);
                pa[10] = new SqlParameter("@thirdNumber", booking.ThirdNumber);
                pa[11] = new SqlParameter("@fourthNumber", booking.FourthNumber);
                pa[12] = new SqlParameter("@fivethNumber", booking.FivethNumber);
                pa[13] = new SqlParameter("@extraNumber", booking.ExtraNumber);
                pa[14] = new SqlParameter("@ValuePoint", booking.ValuePoint);
                pa[15] = new SqlParameter("@DrawID", booking.DrawID);
                pa[16] = new SqlParameter("@IsFreeTicket", booking.IsFreeTicket);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    objResult.IsError = false;
                }
            }
            catch (Exception ex)
            {
                objResult.IsError = true;
                objResult.Result = MessageResult.Error;
                objResult.MessageDetail = "BookingMegaballBusiness -> InsertBooking : " + ex.Message;
                objResult.MessageDate = DateTime.Now;
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText(ex.Message);

            }
            finally
            {
                helper.destroy();
            }
            return objResult;
        }
        //select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,m.*,mb.UserName,p.ProvinceName,d.DistrictName,s.SquareName,pr.PriceName,c.Categoryname,dr.DirectionName from memberhobby m,province p, member mb, district d,priceproperty pr, squareproperty s,direction dr,categorynews c  where m.MemberID=mb.MemberID and m.ProvinceID=p.ProvinceID and m.DistrictID=d.DistrictID and m.SquareID=s.SquareID and m.PriceID=pr.PriceID and m.DirectionID=dr.DirectionID and m.CategoryID=c.CategoryID ) as Products  where Row>=@start and Row<=@end
        public IEnumerable<BookingMegaball> ListTransactionBookingByMember(int memberID, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListTransactionBookingMegaballByMemberFE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    //transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public List<BookingMegaball> ListTransactionBookingWinnigByMemberSearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListBookingWinningBySearch_FE";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    transaction.DrawID = int.Parse(reader["DrawID"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    if (!string.IsNullOrEmpty(reader["AwardWithdrawStatus"].ToString()))
                    {
                        transaction.AwardWithdrawStatus = int.Parse(reader["AwardWithdrawStatus"].ToString());

                    }
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public List<BookingMegaball> ListAllBooking()
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListAllBookingMegaball_FE";

                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball booking = new BookingMegaball();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    booking.DrawID = int.Parse(reader["DrawID"].ToString());
                    booking.IsFreeTicket = int.Parse(reader["IsFreeTicket"].ToString());
                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    lstTransaction.Add(booking);
                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingMegaball> ListTransactionBookingOrderByDate(int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListTransactionBookingOrderByDateFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    //transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.DrawID = int.Parse(reader["DrawID"].ToString());
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<BookingMegaball> ListBookingByDraw(int intDrawID, int start, int end)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListBookingMegaballByDraw";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@DrawID", intDrawID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball booking = new BookingMegaball();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    booking.DrawID = int.Parse(reader["DrawID"].ToString());
                    booking.IsFreeTicket = int.Parse(reader["IsFreeTicket"].ToString());
                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    lstTransaction.Add(booking);
                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<BookingMegaball> ListTransactionBookingByMemberPaging(int memberID, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListTransactionBookingByMemberPagingFE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    // transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());
                    transaction.DrawID = int.Parse(reader["DrawID"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        /// <summary>

        /// Edited date: 12.01.2018
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<BookingMegaball> ListTransactionBookingBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListBookingMegaballBySearchFrontEnd";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    //transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());
                    transaction.DrawID = int.Parse(reader["DrawID"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<BookingMegaball> ListTransactionBookingByDate(DateTime fromDate, DateTime toDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListTransactionBookingByDate_FE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@fromDate", fromDate);
                pa[1] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    //transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    //transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public bool UpdateStatusBooking(int bookingID, int status)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateStatusBooking_FE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@status", status);
                pa[1] = new SqlParameter("@bookingID", bookingID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        public int CountMemberBuyNumber(int memberID, int firstNumber, int secondNumber, int thirdNumber, int fourthNumber, int fivethNumber, DateTime fromDate, DateTime toDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                int count = 0;
                string sql = "SP_CountMemberBuyNumber_FE";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@fromDate", fromDate);
                pa[2] = new SqlParameter("@toDate", toDate);
                pa[3] = new SqlParameter("@firstNumber", firstNumber);
                pa[4] = new SqlParameter("@secondNumber", secondNumber);
                pa[5] = new SqlParameter("@thirdNumber", thirdNumber);
                pa[6] = new SqlParameter("@fourthNumber", fourthNumber);
                pa[7] = new SqlParameter("@fivethNumber", fivethNumber);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    count = int.Parse(reader["TotalRecord"].ToString());
                }
                return count;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }
        public ObjResultMessage InsertListBooking(ModelRSA objRsa, double doublePoint, int intNumberID, double dblTotalJackPot)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            try
            {
                IEnumerable<BookingMegaball> lstBooking = Algorithm.DecryptionObjectRSA<IEnumerable<BookingMegaball>>(objRsa.ParamRSAFirst);
                string strTransactionTranfer = objRsa.ParamRSASecond;
                if (lstBooking == null || lstBooking.Count() == 0)
                {
                    objResult.IsError = true;
                    objResult.Result = MessageResult.Error;
                    objResult.MessageDetail = "BookingMegaballBusiness -> InsertListBooking : Null Object IEnumerable<BookingMegaball>";
                    objResult.MessageDate = DateTime.Now;
                }
                else
                {
                    bool rs = false;
                    int intMemberID = 0;
                    if (lstBooking != null && lstBooking.Count() > 0)
                    {
                        foreach (var item in lstBooking)
                        {
                            intMemberID = item.MemberID;
                            item.TransactionCode = strTransactionTranfer;
                            objResult = InsertBooking(item);
                            if (objResult.IsError)
                            {
                                break;
                            }
                        }
                        //Update point of member
                        //if (rs)
                        //{
                        //    rs = UpdatePointMember(intMemberID, doublePoint, intNumberID, dblTotalJackPot);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText(ex.Message);
            }
            return objResult;
        }
        public bool UpdatePointMember(int intMemberID, double intPoint, int intNumberID, double dblTotalJackPot)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdatePointMemberFE";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@Points", intPoint);
                pa[1] = new SqlParameter("@MemberID", intMemberID);
                pa[2] = new SqlParameter("@NumberID", intNumberID);
                pa[3] = new SqlParameter("@TotalJackPot", dblTotalJackPot);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public float SumValuePoint()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                float sum = 0;
                string sql = "SP_SumValuePoints";
                //SqlParameter[] pa = new SqlParameter[1];
                //pa[0] = new SqlParameter("@createdate", createdate);

                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    sum = float.Parse(reader["TotalCount"].ToString());
                }
                return sum;
            }
            catch (Exception ex)
            {
                // WriteErrorLog.WriteLogsToFileText(ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingMegaball> ListBookingByAwardDate(int intAwardNumber)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_GetListBookingByRaw";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@Raw", intAwardNumber);

                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();

                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.Note = reader["Note"].ToString();
                    transaction.NoteEnglish = reader["NoteEnglish"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.ValuePoint = double.Parse(reader["ValuePoint"].ToString() == "" ? "0" : reader["ValuePoint"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingMegaball> ListBookingMegaballByDate(DateTime dtmDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListBookingMegaballByDate";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@Date", dtmDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball transaction = new BookingMegaball();
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    transaction.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    transaction.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    transaction.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    transaction.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    transaction.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<BookingMegaball> ListAllBookingMegaballByDraw(int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListAllBookingMegaballByDraw";
                SqlParameter[] pa = new SqlParameter[1];

                pa[0] = new SqlParameter("@DrawID", intDrawID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball booking = new BookingMegaball();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    booking.DrawID = int.Parse(reader["DrawID"].ToString());
                    booking.IsFreeTicket = int.Parse(reader["IsFreeTicket"].ToString());
                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    lstTransaction.Add(booking);
                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingMegaball> SP_ListBookingByDraw_FE(int intDrawID)
        {
            string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingMegaball> lstTransaction = new List<BookingMegaball>();
                string sql = "SP_ListBookingByDraw_FE";
                SqlParameter[] pa = new SqlParameter[1];

                pa[0] = new SqlParameter("@DrawID", intDrawID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingMegaball booking = new BookingMegaball();

                    booking.MemberID = int.Parse(reader["MemberID"].ToString());
                    booking.BookingID = int.Parse(reader["BookingID"].ToString());
                    booking.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    booking.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    booking.Status = int.Parse(reader["Status"].ToString());
                    booking.Quantity = int.Parse(reader["Quantity"].ToString());
                    if (!string.IsNullOrEmpty(reader["TransactionCode"].ToString()))
                    {
                        booking.TransactionCode = reader["TransactionCode"].ToString();
                    }
                    booking.DrawID = int.Parse(reader["DrawID"].ToString());
                    booking.IsFreeTicket = int.Parse(reader["IsFreeTicket"].ToString());
                    booking.FirstNumber = int.Parse(reader["FirstNumber"].ToString());
                    booking.SecondNumber = int.Parse(reader["SecondNumber"].ToString());
                    booking.ThirdNumber = int.Parse(reader["ThirdNumber"].ToString());
                    booking.FourthNumber = int.Parse(reader["FourthNumber"].ToString());
                    booking.FivethNumber = int.Parse(reader["FivethNumber"].ToString());
                    booking.ExtraNumber = int.Parse(reader["ExtraNumber"].ToString());
                    lstTransaction.Add(booking);
                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
        public IEnumerable<AwardBO> GetAllListAward()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "SP_GetAllAwardFE";
                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardBO number = new AwardBO();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.AwardName = reader["AwardName"].ToString();
                    number.AwardValue = decimal.Parse(reader["AwardValue"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    number.AwardNameEnglish = reader["AwardNameEnglish"].ToString();
                    number.NumberBallNormal = int.Parse(reader["NumberBallNormal"].ToString());
                    number.NumberBallGold = int.Parse(reader["NumberBallGold"].ToString());
                    number.AwardFee = decimal.Parse(reader["AwardFee"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public int InsertTicketNumberTimeLog(string strEmail, int intTicketNumber, DateTime dtmCreatedDate)
        {
            int intResultID = 0;
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "SP_TicketNumberTimeLog_Add";

                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@Email", strEmail);
                pa[1] = new SqlParameter("@TicketNumber", intTicketNumber);
                pa[2] = new SqlParameter("@CreatedDate", dtmCreatedDate);

                SqlParameter paraId = new SqlParameter();
                paraId.ParameterName = "@Id";
                paraId.DbType = DbType.Int32;
                paraId.Direction = ParameterDirection.Output;
                pa[3] = paraId;

                SqlCommand command = helper.GetCommand(sql, pa, true);

                int intQuery = command.ExecuteNonQuery();

                var objOutput = command.Parameters["@Id"];
                if (intQuery > 0 && (objOutput != null && objOutput.Value != null))
                {
                    intResultID = Convert.ToInt32(objOutput.Value);
                }
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(fileLog, "BookingMegaballBusiness -> InsertTicketNumberTimeLog: " + ex.Message);
            }
            finally
            {
                helper.destroy();
            }

            return intResultID;
        }
    }
}
