﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.LotteFE.Data
{
    public class BookingBusiness: IBookingServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public bool InsertBooking(BookingBO booking)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_InsertBookingFE";
                SqlParameter[] pa = new SqlParameter[7];
                pa[0] = new SqlParameter("@memberID", booking.MemberID);
                pa[1] = new SqlParameter("@numberValue", booking.NumberValue);
                pa[2] = new SqlParameter("@quantity", booking.Quantity);
                pa[3] = new SqlParameter("@createDate", booking.CreateDate);
                pa[4] = new SqlParameter("@openDate", booking.OpenDate);
                pa[5] = new SqlParameter("@status", booking.Status);
                pa[6] = new SqlParameter("@transactionCode", booking.TransactionCode);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
        //select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,m.*,mb.UserName,p.ProvinceName,d.DistrictName,s.SquareName,pr.PriceName,c.Categoryname,dr.DirectionName from memberhobby m,province p, member mb, district d,priceproperty pr, squareproperty s,direction dr,categorynews c  where m.MemberID=mb.MemberID and m.ProvinceID=p.ProvinceID and m.DistrictID=d.DistrictID and m.SquareID=s.SquareID and m.PriceID=pr.PriceID and m.DirectionID=dr.DirectionID and m.CategoryID=c.CategoryID ) as Products  where Row>=@start and Row<=@end
        public IEnumerable<BookingBO> ListTransactionBookingByMember(int memberID)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingBO> lstTransaction = new List<BookingBO>();
                string sql = "SP_ListTransactionBookingByMemberFE";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingBO transaction = new BookingBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public IEnumerable<BookingBO> ListTransactionBookingByMember(int memberID, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingBO> lstTransaction = new List<BookingBO>();
                string sql = "SP_ListTransactionsBookingByMemberFE";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingBO transaction = new BookingBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        /// <summary>
        
        /// Edited date: 12.01.2018
        /// Description: Thay param DateTime -> DateTime? (doi voi truong hop mac dinh la null)
        /// </summary>
        /// <param name="memberID"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IEnumerable<BookingBO> ListTransactionBookingBySearch(int memberID, DateTime? fromDate, DateTime? toDate, int start, int end)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingBO> lstTransaction = new List<BookingBO>();
                string sql = "SP_ListBookingBySearchFrontEnd";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@start", start);
                pa[2] = new SqlParameter("@end", end);
                pa[3] = new SqlParameter("@fromDate", fromDate);
                pa[4] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingBO transaction = new BookingBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();
                    transaction.TotalRecord = int.Parse(reader["TOTALROWS"].ToString());
                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


        public IEnumerable<BookingBO> ListTransactionBookingByDate(DateTime fromDate, DateTime toDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<BookingBO> lstTransaction = new List<BookingBO>();
                string sql = "SP_ListTransactionBookingByDateFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@fromDate", fromDate);
                pa[1] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    BookingBO transaction = new BookingBO();
                    transaction.MemberID = int.Parse(reader["MemberID"].ToString());

                    transaction.Status = int.Parse(reader["Status"].ToString());
                    transaction.BookingID = int.Parse(reader["BookingID"].ToString());
                    transaction.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    transaction.OpenDate = DateTime.Parse(reader["OpenDate"].ToString());
                    transaction.NumberValue = reader["NumberValue"].ToString();
                    transaction.Quantity = int.Parse(reader["Quantity"].ToString());
                    transaction.Email = reader["Email"].ToString();
                    transaction.E_Wallet = reader["E_Wallet"].ToString();
                    transaction.TransactionCode = reader["TransactionCode"].ToString();

                    lstTransaction.Add(transaction);

                }
                return lstTransaction;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public bool UpdateStatusBooking(int bookingID, int status)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                bool rs = false;
                string sql = "SP_UpdateStatusBookingFE";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@status", status);
                pa[1] = new SqlParameter("@bookingID", bookingID);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public int CountMemberBuyNumber(int memberID, string numberValue, DateTime fromDate, DateTime toDate)
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                int count = 0;
                string sql = "SP_CountMemberBuyNumberFE";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@numberValue", numberValue);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, true);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    count = int.Parse(reader["TotalRecord"].ToString());
                }
                return count;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

    }
}
