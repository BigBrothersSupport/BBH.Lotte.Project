﻿using BBC.Core.Common.Log;
using BBC.Core.Database;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace BBH.LotteFE.Data
{
    public class FreeTicketBusiness: IFreeTicketServices
    {
        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        public FreeTicketBO GetFreeTicketActive()
        {
            Sqlhelper helper = new Sqlhelper("", "ConnectionString");
            try
            {
                List<FreeTicketBO> lstFreeTicketBO = new List<FreeTicketBO>();
                string sql = "SP_GetFreeTicketDailyActive";

                SqlCommand command = helper.GetCommandNonParameter(sql, true);
                SqlDataReader reader = command.ExecuteReader();
                FreeTicketBO objFreeTicketBO = new FreeTicketBO();
                if (reader.Read())
                {
                    objFreeTicketBO.FreeTicketID = int.Parse(reader["FreeTicketID"].ToString());
                    objFreeTicketBO.Date = DateTime.Parse(reader["Date"].ToString());
                    objFreeTicketBO.Total = int.Parse(reader["Total"].ToString());
                    objFreeTicketBO.IsActive = int.Parse(reader["IsActive"].ToString());
                    objFreeTicketBO.CreatedDate = DateTime.Parse(reader["CreatedDate"].ToString());
                }
                return objFreeTicketBO;
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}
