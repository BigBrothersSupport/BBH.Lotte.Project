﻿using BBC.Core.Common.IoC;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BBH.Lotte.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //AuthConfig.RegisterAuth();
            DependencyFactory.Initialize();
        }
        void Application_Error(object sender, EventArgs e)
        {
            //get the last error of server  
            var error = Server.GetLastError();

            //get the http code of error: 400, 404, 500...  
            var code = (error is HttpException) ? (error as HttpException).GetHttpCode() : 500;

            //write log  
            Exception ex = error.GetBaseException();
            string strErrorMessage = string.Empty;
            strErrorMessage += "\r\n";
            strErrorMessage += "******************************************************************";

            strErrorMessage += "\r\n";
            strErrorMessage += "\nCODE: " + code.ToString();

            strErrorMessage += "\r\n";
            strErrorMessage += "\nMESSAGE: " + ex.Message;

            strErrorMessage += "\r\n";
            strErrorMessage += "\nSOURCE: " + ex.Source;

            strErrorMessage += "\r\n";
            strErrorMessage += "\nINSTANCE: " + ex.InnerException;

            strErrorMessage += "\r\n";
            strErrorMessage += "\nDATA: " + ex.Data;

            strErrorMessage += "\r\n";
            strErrorMessage += "\nTARGETSITE: " + ex.TargetSite;

            strErrorMessage += "\r\n";
            strErrorMessage += "\nSTACKTRACE: " + ex.StackTrace;

            strErrorMessage += "\r\n";
            strErrorMessage += "\n******************************************************************";

            BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText(strErrorMessage);
            //send email  

            //clear response stream  
            Response.Clear();

            //clear server's error  
            Server.ClearError();

            //redirect to error page  
            //Response.Redirect(string.Format("~/Error/Index/{0}", code
            Response.Redirect(string.Format("/Error"));
        }
    }
}
