﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BBH.Lotte.Web.Models
{
    public class RegisterViewModel
    {
        public bool IsTrue => true;

        public string FirtName { get; set; }
        public string LastName { get; set; }

        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        [Remote("CheckExistEmail", "Validate")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please input your password")]
        [DataType(DataType.Password)]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "Passwords must be at least 8 characters and contain at 3 of 4 of the following: upper case (A-Z), lower case (a-z), number (0-9) and special character (e.g. !@#$%^&*)")]
        public string Password { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("TermsAndConditions", ErrorMessage = "You must accept the terms and conditions")]

        public bool TermsAndConditions { get; set; }

        public string LinkReference { get; set; }

    }
}