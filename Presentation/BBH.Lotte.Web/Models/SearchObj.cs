﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.Web.Models
{
    public class SearchObj
    {
        public string ContentResult { get; set; }
        public string PagingResult { get; set; }
        public int Totalrecord { get; set; }
    }
}