﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.Web.Models
{
    public class Destination
    {
        public string address { get; set; }
        public Amount amount { get; set; }
        public string tag { get; set; }
    }
}