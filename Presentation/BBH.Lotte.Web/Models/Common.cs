﻿using BBC.Core.Database;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BBH.Lotte.Web.Models
{
    public class Common
    {
        public const string KeyListAward = "KeyListAward";
        public const string KeyListBooking = "KeyListBooking";
        public const string KeyListTransactionCoin = "KeyListTransactionCoin";
        public const string KeyListAwardBO = "KeyListAwardBO";
        public const string KeyTicketConfig = "KeyTicketConfig";

        public const string KeyAwardWithdraw = "KeyAwardWithdraw";

        public const string KeyAwardWithdrawDetail = "KeyAwardWithdrawDetail_";
        public const string ListCacheBookingMember = "ListCacheBookingMember";

        public const string KeyBooking = "KeyBooking";
        public const string KeyAward = "KeyAward";

        public static T ParseObjectFromAsync<T>(ObjResultMessage objResult)
        {

            if (objResult == null || objResult.ObjResultData == null)
                return default(T);

            string strJson = JsonConvert.SerializeObject(objResult.ObjResultData);

            return JsonConvert.DeserializeObject<T>(strJson);
        }

        public static string GetLocalIPAddress()
        {
            string ipAddress = "";
            //var host = Dns.GetHostEntry(Dns.GetHostName());
            //foreach (var ip in host.AddressList)
            //{
            //    if (ip.AddressFamily == AddressFamily.InterNetwork)
            //    {
            //        ipAddress = ip.ToString();
            //    }
            //}
            //return ipAddress;

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static string GetHostServices()
        {
            string hostServices = string.Empty;
            string servicesName = string.Empty;

            hostServices = ConfigurationManager.AppSettings["Host:Webservices"];
            servicesName = ConfigurationManager.AppSettings["Host:ServicesName"];
            hostServices = hostServices + "/" + servicesName;
            return hostServices;
        }
    }
}