﻿using BBH.Lotte.Shared;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.Web.Models
{
    public class TransactionsRipple
    {
        public string type { get; set; }
        public string address { get; set; }
        public string sequence { get; set; }
        public string id { get; set; }
        public Specification specification { get; set; }
        public Outcome outcome { get; set; }

        string strHttpRippleApi = ConfigurationManager.AppSettings[KeyManager._RIPPLE_API];
        string strPathLog = Convert.ToString(ConfigurationManager.AppSettings[KeyManager.PATH_LOG]);
        string TimeExpired = ConfigurationManager.AppSettings["TimeExpired"];
        string ClientID = ConfigurationManager.AppSettings["ClientID"];
        string RippleTag = ConfigurationManager.AppSettings["RippleTag"];

        [Dependency]
        protected IPointsServices objPointRepository { get; set; }
        [Dependency]
        protected IMemberServices memberServices { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="strAddress"></param>
        ///// <returns></returns>
        //public async Task<List<TransactionsRipple>> GetTransactionsRippleAsync(string strAddress = "rEvfUKt2sYVDe7Tto3Aek1yAVEdB1k4oAZ")
        //{
        //    string linkGetNewsAdressRipple = strHttpRippleApi + "/getTransactions/" + strAddress;
        //    string str = "";
        //    List<TransactionsRipple> lst = new List<TransactionsRipple>();
        //    List<TransactionsRipple> lstTemp = new List<TransactionsRipple>();
        //    try
        //    {
        //        JsonResult json = null;
        //        System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

        //        using (var client = new HttpClient())
        //        {
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        //            using (var response = await client.GetAsync(linkGetNewsAdressRipple))
        //            {
        //                if (response.IsSuccessStatusCode)
        //                {
        //                    str = await response.Content.ReadAsStringAsync();
        //                    lst = JsonConvert.DeserializeObject<List<TransactionsRipple>>(str);
        //                    lstTemp = lst.Where(x => x.type == "payment").ToList();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        lstTemp = null;
        //    }
        //    return lstTemp;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="objMember"></param>
        ///// <returns></returns>
        //public async Task<List<string>> UpdateTransactionPointRipple(MemberBO objMember, IPointsServices objPointServices, IMemberServices objMemberServices)
        //{
        //    try
        //    {//Transaction XRP
        //        if (objMember != null)
        //        {
        //            List<string> lstResult = new List<string>();
        //            double dbMemeberPoints = 0;
        //            int intNumberTicket = 0;
        //            bool bolResult = true;
        //            string transactionCode = string.Empty;

        //            List<TransactionsRipple> lstTransactionsRipple = await GetTransactionsRippleAsync(objMember.RippleAddress);
        //            if (lstTransactionsRipple != null && lstTransactionsRipple.Count > 0)
        //            {
        //                foreach (TransactionsRipple item in lstTransactionsRipple)
        //                {
        //                    if (item.specification.source.maxAmount.currency == "XRP" && item.specification.destination.tag == (RippleTag + objMember.MemberID)) // ex: (Ripple tag : 2018123)
        //                    {
        //                        bool boolCheckExistTransactionID = objPointServices.CheckExistTransactionBitcoin(item.id.ToString(), objMember.MemberID);
        //                        if (!boolCheckExistTransactionID)
        //                        {
        //                            string strCode = Utility.GenCode();
        //                            string tick = DateTime.Now.Ticks.ToString();
        //                            transactionCode = Utility.MaHoaMD5(strCode + tick);
        //                            TransactionCoinBO objTransactionCoinBO = new TransactionCoinBO
        //                            {
        //                                CreateDate = DateTime.Now,
        //                                ExpireDate = DateTime.Now.AddMinutes(double.Parse(TimeExpired)),
        //                                MemberID = objMember.MemberID,
        //                                Note = "Received Coins",
        //                                QRCode = "",
        //                                Status = 0,
        //                                TransactionBitcoin = item.id.ToString(),
        //                                TransactionID = transactionCode,
        //                                TypeTransactionID = 0,
        //                                ValueTransaction = double.Parse(item.specification.source.maxAmount.value.ToString()),
        //                                WalletAddressID = objMember.RippleAddress,
        //                                WalletID = "",
        //                                CoinID = "XRP"
        //                            };
        //                            bool result_ = objPointServices.InsertTransactionCoin(objTransactionCoinBO);
        //                            if (result_)
        //                            {
        //                                //BTC
        //                                ExchangeTicketBO objExchangeTicketBO = objPointServices.ListExchangeTicketByMember(int.Parse(ClientID), "XRP");//XRP
        //                                double PointValue = 1;
        //                                double TicketNumber = 1;
        //                                if (objExchangeTicketBO != null && objExchangeTicketBO.CoinID != null && objExchangeTicketBO.CoinID.Trim() == "XRP")
        //                                {
        //                                    PointValue = objExchangeTicketBO.PointValue;
        //                                    TicketNumber = objExchangeTicketBO.TicketNumber;
        //                                }
        //                                double pointChange = (item.specification.source.maxAmount.value * TicketNumber) / PointValue;
        //                                bolResult = objMemberServices.UpdatePointsMemberFE(objMember.MemberID, pointChange);
        //                                if (!bolResult)
        //                                {
        //                                    break;
        //                                }
        //                            }
        //                        }
        //                    }

        //                }
        //            }
        //            if (bolResult)
        //            {
        //                dbMemeberPoints = objMemberServices.GetPointsMember(objMember.MemberID);
        //                intNumberTicket = (int)dbMemeberPoints;
        //                //result = memeberPoints.ToString();
        //                System.Web.HttpContext.Current.Session[SessionKey.POINT] = dbMemeberPoints;
        //                System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = intNumberTicket;

        //                lstResult.Add(Convert.ToString(dbMemeberPoints));
        //                lstResult.Add(Convert.ToString(intNumberTicket));
        //            }

        //            return lstResult;
        //        }
        //        return null;
        //    }
        //    catch (Exception objEx)
        //    {
        //        Utility.WriteLog(strPathLog, objEx.Message);
        //        return null;
        //    }
        //}
    }
}