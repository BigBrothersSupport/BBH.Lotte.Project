﻿$('#txtNumber').on("keypress", function (e) {
        var lenght = $(this).val().length;
        
        if(lenght>4)
        {
            e.preventDefault();
        }
});

function CountMemberBuyTicket(ticket)
{
    if (ticket.length < 5)
    {
        $('#lbMessage').text('Vé phải đủ 5 số');
        return false;
    }
    else
    {
        if (isNaN(ticket)) {
            $('#lbMessage').text('Vui lòng nhập giá trị số');
        }
        else
        {
            $.ajax({
                type: "get",
                url: "/Booking/CountAllMemberBuyTicket",
                async: true,
                data: { ticket: ticket },
                beforeSend: function () {
                },
                success: function (d) {
                    $('#lbMessage').text('Có '+d +' người mua vé này.');
                }
            });
        }
    }
}
function SubmitBuyTicket()
{
    var checkReg = true;
    var ticket = $('#txtNumber').val();
    var quantity = $('#cbQuantity').val();
    if (ticket == '' || ticket.length <5||ticket.length>5) {
        $('#lbMessage').text('Vé phải đủ 5 số');
        checkReg = false;
    }
    if (quantity=='0') {
        $('#lbQuantity').text('Vui lòng chọn số lượng mua');
        checkReg = false;
    }
    if(!checkReg)
    {
        return false;
    }
    else {
        if (isNaN(ticket)) {
            $('#lbMessage').text('Vui lòng nhập giá trị số');
        }
        else {
            if (isNaN(quantity)) {
                $('#lbQuantity').text('Vui lòng nhập giá trị số');
            }
            else {
                var confirmMessage = confirm('Bạn có chắc chọn mua vé này?');
                if (!confirmMessage) {
                    return false;
                }
                else {
                    $('#imgLoading').css("display", "");
                    $.ajax({
                        type: "post",
                        url: "/Booking/BookingTicketMember",
                        async: true,
                        data: { ticket: ticket, quantity: quantity },
                        beforeSend: function () {
                            $('#imgLoading').css("display", "");
                        },
                        success: function (d) {
                            $('#imgLoading').css("display", "none");
                            if (d == 'InsertBookingSuccess') {
                                $('#txtNumber').val('');
                                $('#cbQuantity option[value=0]').attr('selected',true);
                                $('#lbMessage').text('');
                                alertify.alert('Bạn đã đăng ký mua vé xong.')
                            }
                            else if(d=='NotEnoughPoint')
                            {
                                alertify.alert('Điểm hiện tại của bạn không đủ để mua '+ quantity +' vé. Bạn cần nạp thêm điểm để mua')
                            }
                            else if(d=='PackageExpire')
                            {
                                alertify.alert('Gói chơi của bạn hiện tại hết hạn. Vui lòng mua gói để tiếp tục chơi.');
                            }
                            else {
                                alertify.error('Không đăng ký mua vé được. Vui lòng liên hệ admin');
                            }

                        },
                        error: function () {

                        }
                    });
                }
            }
        }
    }
}

function ResetField(id)
{
    if (id == 1) {
        $('#lbMessage').text('');
    }
    if (id == 2) {
        $('#lbQuantity').text('');
    }
}