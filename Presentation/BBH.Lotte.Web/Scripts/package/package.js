﻿function SubmitPackageMember()
{
    var language = $('#hdLanguage').val();
    var ewallet = $('#hdEwallet').val();
    var packageID = $('#cbPackage').val();
    var expireNumber = $('#cbPackage option[value=' + packageID + ']').attr('title');
    var pakageValue = $('#cbPackage option[value=' + packageID + ']').attr('id');
    if(packageID==0)
    {
        if (language == 'EN') {
            $('#lbPackage').text('Please select package');
        }
        else
        {
            $('#lbPackage').text('Vui lòng chọn gói');
        }
    }
    else
    {
        var textMessage = '';
        if (language == 'EN') {
            textMessage = 'Are you sure want to buy this package?';
        }
        else {
            textMessage = "Bạn có chắc mua gói này?";
        }
        var confirmMessage = confirm(textMessage);
        if(!confirmMessage)
        {
            return false;
        }
        else
        {
            $('#imgLoading').css("display", "");
            $.ajax({
                type: "post",
                url: "/Package/InsertPackageMember",
                async: false,
                data: { packageID: packageID, expireNumber: expireNumber},
                beforeSend: function () {
                    $('#imgLoading').css("display", "");
                },
                success: function (d) {
                    $('#imgLoading').css("display", "none");
                    if (d == 'InsertPackageSuccess') {
                        $('#cbPackage option[value=0]').prop('selected', true);
                        if (language == 'EN') {
                            alertify.alert('Buy success. We will check and active this package in 30 minutes <br/> Please transfer BTC to Bitcoin Wallet address: <a href="https://weboffice.ilgamos.com/?ewallet=' + ewallet + '&euro=' + pakageValue + '" target="_blank" >' + ewallet + '</a> to active.')
                        }
                        else
                        {
                            alertify.alert('Mua gói xong. Chúng tôi sẽ kiểm tra và kích hoạt gói của bạn trong 30 phút <br/> Vui lòng chuyển BTC đến địa chỉ ví điện tử: <a href="https://weboffice.ilgamos.com/?ewallet=' + ewallet + '&euro=' + pakageValue + '" target="_blank" >' + ewallet + '</a> để kích hoạt.')
                        }
                    }
                   
                    else  {
                        if (language == 'EN') {
                            alertify.error('Buy error. Please contact with administrator');
                        }
                        else {
                            alertify.error('Không mua gói được. Vui lòng liên hệ admin');
                        }
                    }
                    
                },
                error: function () {
                    
                }
            });
        }
    }
}

function ResetField()
{
    $('#lbPackage').text('');
}