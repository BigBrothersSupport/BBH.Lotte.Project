﻿ function CheckResultNumber()
    {
        var strBlockHash = $("#bhash").val();
        $("#divLotteryResult").show();
        $("#over").show();
        $.ajax({
            type: "post",
            url: "/ResultNumber/CheckResultNumber",
            async: true,
            data: { strBlockHash: strBlockHash},
            beforeSend: function () {
            },
            success: function (d) {
                $("#show_lottery_results").html(d);
            },
            error: function () {
                alertify.error('An error has occurred !');
            }
        });
    }
    function CloseLotteryResult()
    {
        $("#divLotteryResult").hide();
        $("#over").hide();
    }