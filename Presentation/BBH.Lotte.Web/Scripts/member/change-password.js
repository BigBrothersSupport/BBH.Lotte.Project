﻿var maxItemNotification = 1;
var checkComplexPass = true;
$(document).ready(function () {
    $(function () {
        var $formResetPassword= $('#frResetPassword');
        if ($.validator.unobtrusive != undefined) {
            $.validator.unobtrusive.parse($formResetPassword);
        }
        $formResetPassword.unbind('submit').submit(function () {
            if ($formResetPassword.valid()) {
                if (checkComplexPass) {
                    $.ajax({
                        url: "/Member/UpdatePassMember",
                        async: false,
                        type: 'POST',
                        data: $(this).serialize(),
                        beforeSend: function (xhr, opts) {
                            showLoadingNew();
                        },
                        complete: function () { },
                        success: function (data) {
                            if (data.isLogin) {
                                if (data.success) {
                                    alertify.success('Update password successfully.');
                                    checkMaxShowAlertNoti(maxItemNotification);
                                    hideLoadingNew();
                                    setTimeout(function () {
                                        window.location.href = '/logout';
                                    }, 2000);
                                }
                                else {
                                    alertify.error('Update fails! Please try again.');
                                    checkMaxShowAlertNoti(maxItemNotification);
                                    hideLoadingNew();
                                }
                            }
                            else {
                                alertify.error('Plase login to continue.');
                                checkMaxShowAlertNoti(maxItemNotification);
                                hideLoadingNew();
                            }
                        },
                        error: function (data) {
                            hideLoadingNew();
                            alertify.error('An error occurred! Please try again after.');
                            checkMaxShowAlertNoti(maxItemNotification);
                        }
                    });
                }
            }
            return false;
        });
    });
});

