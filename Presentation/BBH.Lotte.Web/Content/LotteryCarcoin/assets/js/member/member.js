﻿function ResetField(id) {
    if (id == 1) {
        $('#lbPassword').text('');

    }
    if (id == 2) {
        $('#lbRePass').text('');

    }
}

function ChangePassWord() {
    var language = $('#hdLanguage').val();
    var checkReg = true;
    var password = $('#txtNewPass').val();
    var rePassword = $('#txtRePass').val();
    if (password == '' || password.length < 8) {
        $('#lbPassword').text('Password at least 8 character');

        checkReg = false;
    }
    if (password != rePassword) {
        $('#lbRePass').text('Password confirm wrong');

        checkReg = false;
    }
    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            url: "/Member/UpdatePassMember",
            async: true,
            data: { password: password },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                if (d == 'UpdatePassSuccess') {
                    $('#txtNewPass').val('');
                    $('#txtRePass').val('');
                    showMessageAlert('Congratulations! You have successfully updated.', backToPagePrev)
                }

                else {

                    alertify.error('Update error. Please contact with administrator');

                }
                hideLoading();
            },
            error: function () {
                alertify.error('An error occurred! Please try again after.');
                hideLoading();
            }
        });
    }
}

function backToPagePrev() {
    setTimeout(function () { window.history.back() }, 1000);
}

function ShowBookingDetail(bookingID, transactionCode, valuePoints, firstNumber, secondNumber, thirdNumber, fourthNumber, fivethNumber, extraNumber, quantity, bookingDate, openDate, statusName, status) {
    if (typeof $('#standardModal') != 'undefined' && $('#standardModal').length > 0) {
        $('#standardModal').show();
    }
    // $('#standardModal').css("display", "block");
    $('#hdBookingID').val(bookingID);
    $('#hdQuantity').val(quantity);
    if (status == '0') {
        $('#2lvlModal-learnMore').css('display', '');

    }
    else {
        $('#2lvlModal-learnMore').css('display', 'none');
    }
    var strTicket = '<span class="ticketNumber">' + firstNumber + '</span>&nbsp;<span class="ticketNumber">' + secondNumber + '</span>&nbsp;<span class="ticketNumber">' + thirdNumber + '</span>&nbsp;<span class="ticketNumber">' + fourthNumber + '</span>&nbsp;<span class="ticketNumber">' + fivethNumber + '</span>&nbsp;<span class="ticketNumber oneMore">' + extraNumber + '</span>';
    $('#txtBookingID').val(transactionCode);
    $('#txtPoints').val(valuePoints);
    $('#spanTicket').html(strTicket);
    $('#txtQuantity').val(quantity);
    $('#txtBookingDate').val(bookingDate);
    $('#txtOpenDate').val(openDate);
    //$('#txtStatus').val(statusName);
}
function ShowTicketWinning(bookingID, transactionCode, valuePoints, firstNumber, secondNumber, thirdNumber, fourthNumber, fivethNumber, extraNumber, quantity, bookingDate, openDate, statusName, status) {
    if (typeof $('#standardModal') != 'undefined' && $('#standardModal').length > 0) {
        $('#standardModal').show();
    }
    // $('#standardModal').css("display", "block");
    $('#hdBookingID').val(bookingID);
    $('#hdQuantity').val(quantity);
    if (status == '0') {
        $('#2lvlModal-learnMore').css('display', '');

    }
    else {
        $('#2lvlModal-learnMore').css('display', 'none');
    }
    var strTicket = '<span class="ticketNumber">' + firstNumber + '</span>&nbsp;<span class="ticketNumber">' + secondNumber + '</span>&nbsp;<span class="ticketNumber">' + thirdNumber + '</span>&nbsp;<span class="ticketNumber">' + fourthNumber + '</span>&nbsp;<span class="ticketNumber">' + fivethNumber + '</span>&nbsp;<span class="ticketNumber oneMore">' + extraNumber + '</span>';
    $('#txtBookingID').val(transactionCode);
    $('#txtPoints').val(valuePoints);
    $('#spanTicket').html(strTicket);
    $('#txtQuantity').val(quantity);
    $('#txtBookingDate').val(bookingDate);
    $('#txtOpenDate').val(openDate);
    //$('#txtStatus').val(statusName);
}
function ShowTransactionPackageDetail(transactionCode, ewallet, packageName, bookingDate, expireDate, statusName) {
    //$('#txtBookingID').val(bookingID);
    $('#standardModal').css("display", "block");
    $('#txtEwallet').val(ewallet);
    $('#txtPackageName').val(packageName);
    $('#txtBookingDate').val(bookingDate);
    $('#txtExpireDate').val(expireDate);
    $('#txtStatus').val(statusName);
    $('#txtTransaction').val(transactionCode);
}

function ShowTransactionPointDetail(transactionCode, ewallet, points, bookingDate, statusName) {
    $('#standardModal').css("display", "block");
    $('#txtEwallet').val(ewallet);
    $('#txtPoints').val(points);
    $('#txtBookingDate').val(bookingDate);
    $('#txtStatus').val(statusName);
    $('#txtTransaction').val(transactionCode);
}

function ConfirmBooking() {
    var bookingID = $('#hdBookingID').val();
    var quantity = $('#hdQuantity').val();
    $('#imgLoadingBooking').css("display", "");
    $.ajax({
        type: "post",
        url: "/Member/ConfirmBooking",
        async: true,
        data: { bookingID: bookingID, quantity: quantity },
        beforeSend: function () {
            $('#imgLoadingBooking').css("display", "");
        },
        success: function (d) {
            $('#imgLoadingBooking').css("display", "none");
            if (d == 'BookingSuccess') {

                alertify.alert('Buy Success.')
                setTimeout(function () { window.location.reload(); }, 3000);
            }

            else {
                alertify.error('Buy error. Please contact with administrator');
            }

        },
        error: function () {

        }
    });
}


///Edited date: 19.01.2018
///Description:  $('#standardModal').hide();
function CloseModal() {
    if (typeof $('#standardModal') != 'undefined' && $('#standardModal').length > 0) {
        $('#standardModal').hide();
    }
    $('#standardModal').css("display", "none");
    $('.modal-backdrop.fade.show').css('opacity', '0');
    $('.modal-backdrop.fade.show').css('display', 'none');
}

function SearchBooking() {
    var page = 1;
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Member/SearchTransactionBooking",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListBooking').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
            hideLoading();
        },
        error: function (e) {
            alertify.error('An error occurred! Please try again after.');
            hideLoading();
        }
    });
}

function PagingSearchBooking(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Member/SearchTransactionBooking",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListBooking').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }

        },
        error: function (e) {

        }
    });
}


function SearchPoint() {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();

    var page = 1;
    $.ajax({
        type: "get",
        async: true,
        url: "/Member/SearchTransactionPoint",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")

            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPoint').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function PagingSearchPoint(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Member/SearchTransactionPoint",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")

            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPoint').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}


function SearchPackage() {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();

    var page = 1;
    $.ajax({
        type: "get",
        async: true,
        url: "/Member/SearchTransactionPackage",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")

            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPackage').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}


function PagingSearchPackage(page) {
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Member/SearchTransactionPackage",
        data: { fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            $('#imgLoadingSearch').css("display", "")
        },
        success: function (d) {
            $('#imgLoadingSearch').css("display", "none")

            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListPackage').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}