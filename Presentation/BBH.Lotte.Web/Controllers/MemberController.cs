﻿using BBC.Core.Common.Cache;
using BBC.Core.Common.Log;
using BBH.Lotte.Shared;
using BBH.Lotte.Web.Models;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using Microsoft.Practices.Unity;
using NBitcoin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BBC.Core.Database;
using BBC.CWallet.ServiceListener.ServiceContract;
using System.Threading.Tasks;
using BBH.Lotte.CLP.Wallet.Repository;
using System.Web;
using BBH.LotteFE.Data;
using BBC.Core.Common.Utils;
using System.Data.SqlClient;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using RestSharp;

namespace BBH.Lotte.Web.Controllers
{
    public static class SMSUtility
    {
        //bổ vô webconfig
        private static string[] mobiles = ConfigurationManager.AppSettings[KeyManager.Mobiles].Split(',');
        private static int index = 0;

        public static string SentMobileCodeAuto(string smsCode, string mobile, string phonecode)
        {
            string fromNumber;
            lock ((object)index)
            {
                fromNumber = mobiles[index++];
                if (index == mobiles.Length - 1)
                    index = 0;
            }


            string sid = string.Empty;
            string toPhone = phonecode.Trim() + mobile;

            //bổ vô webconfig
            string accountSid = ConfigurationManager.AppSettings[KeyManager.AccountSid];
            string authToken = ConfigurationManager.AppSettings[KeyManager.AuthToken];

            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
              body: "Code: '" + smsCode + "'",
                from: new Twilio.Types.PhoneNumber(fromNumber),
              to: new Twilio.Types.PhoneNumber(toPhone)
          );
            sid = message.Sid;
            return sid;
        }
    }
    public class MemberController : Controller
    {
        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];
        string AdminID = ConfigurationManager.AppSettings[KeyManager.AdminID];
        string FreeWalletID = ConfigurationManager.AppSettings[KeyManager.FreeWalletID];
        string FreeWalletAddress = ConfigurationManager.AppSettings[KeyManager.FreeWalletAddress];

        string MaxCountSendMail = ConfigurationManager.AppSettings[KeyManager.MaxCountSendMail];
        string TimeExpired = ConfigurationManager.AppSettings[KeyManager.TimeExpired];
        string NumberTicketFree = ConfigurationManager.AppSettings[KeyManager.NumberTicketFree];
        string KeyCodeSHA = Convert.ToString(ConfigurationManager.AppSettings[KeyManager._KEYCODESHA]);

        string TimeDaysCookies = ConfigurationManager.AppSettings[KeyManager.TimeDaysCookies];
        string masterKeyWan2Lot = ConfigurationManager.AppSettings["KeyWan2Lot"];
        string Bitcoin = ConfigurationManager.AppSettings["Bitcoin"];

        string strPathLog = Convert.ToString(ConfigurationManager.AppSettings[KeyManager.PATH_LOG]);

        string FeeWalletID = ConfigurationManager.AppSettings[KeyManager.FeeWalletID];
        string FeeWalletAddress = ConfigurationManager.AppSettings[KeyManager.FeeWalletAddress];

        string AwardWalletID = ConfigurationManager.AppSettings[KeyManager.AwardWalletID];
        string AwardWalletAddress = ConfigurationManager.AppSettings[KeyManager.AwardWalletAddress];

        string JackPotID = ConfigurationManager.AppSettings[KeyManager.JackPotID];
        string JackPotWalletAddress = ConfigurationManager.AppSettings[KeyManager.JackPotWalletAddress];

        string ProgressivePrizeID = ConfigurationManager.AppSettings[KeyManager.ProgressivePrizeID];
        string ProgressivePrizeWalletAddress = ConfigurationManager.AppSettings[KeyManager.ProgressivePrizeWalletAddress];

        string FeePrizeID = ConfigurationManager.AppSettings[KeyManager.FeePrizeID];
        string FeePrizeWalletAddress = ConfigurationManager.AppSettings[KeyManager.FeePrizeWalletAddress];

        string LoanWalletID = ConfigurationManager.AppSettings[KeyManager.LoanWalletID];
        string LoanWalletAddress = ConfigurationManager.AppSettings[KeyManager.LoanWalletAddress];

        double ExpieDate = double.Parse(ConfigurationManager.AppSettings["ExpieDate"]);
        int TotalsSMS = Convert.ToInt32(ConfigurationManager.AppSettings["TotalsSMS"]);

        //api carcoin
        string ApiMyCarCoin = ConfigurationManager.AppSettings[KeyManager.ApiMyCarCoin];
        string HashToken = ConfigurationManager.AppSettings[KeyManager.HashToken];
        //string HashSalt = ConfigurationManager.AppSettings[KeyManager.HashSalt];
        string EmailDefault = ConfigurationManager.AppSettings[KeyManager.EmailDefault];

        string RequestByconfig = ConfigurationManager.AppSettings["RequestBy"];

        RedisCache objRedisCache = new RedisCache();
        static String[] suffixes ={ "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th","th", "th", "th", "th",
                                    "th", "th", "th", "th", "th", "th","th", "st", "nd", "rd", "th","th", "th", "th",
                                    "th", "th","th", "st" };

        [Dependency]
        protected IAwardMegaballServices awardServices { get; set; }

        [Dependency]
        protected IMemberServices services { get; set; }

        [Dependency]
        protected IPointsServices pointService { get; set; }

        [Dependency]
        protected IBookingMegaballServices bookingServices { get; set; }

        [Dependency]
        protected IAwardServices awardNumberServices { get; set; }
        [Dependency]
        protected IAwardMegaballServices awardMegaballServices { get; set; }
        [Dependency]
        protected IAwardWithdrawServices repository { get; set; }

        [Dependency]
        protected ITicketConfigServices ticketConfigServices { get; set; }
        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }
        [Dependency]
        protected IFreeTicketServices freeTicketServices { get; set; }
        [Dependency]
        protected ITicketWinningServices ticketWinningServices { get; set; }
        public ActionResult Index()
        {
            if (Session["Language"] == null)
            {
                Session["Language"] = "EN";
            }

            return View();
        }
        public ActionResult PopupInputMobile()
        {
            IEnumerable<CountryBO> lstCountry = null;
            lstCountry = services.GetListCountry(0, 1000);

            ViewData["lstCountry"] = lstCountry;
            return View();
        }
        public ActionResult TransactionPointMember(string p)
        {
            if (Session["Language"] == null)
            {
                Session["Language"] = "EN";
            }
            int totalRecord = 0;
            int intPageSize = 10;
            int start = 0, end = 10;
            string keyPointMember = string.Empty;
            if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] == null)
            {
                Response.Redirect("/login");
            }
            else
            {
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                int page = 1;
                try
                {
                    if (p != null && p != "")
                    {
                        page = int.Parse(p);
                    }
                }
                catch
                {

                }

                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);

                MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                if (member != null)
                {
                    IEnumerable<TransactionPointsBO> lstTransaction = null;
                    if (lstTransaction == null)
                    {
                        lstTransaction = pointService.ListTransactionPointByMember(member.MemberID, start, end);

                    }
                    if (lstTransaction != null && lstTransaction.Count() > 0)
                    {
                        totalRecord = lstTransaction.ElementAt(0).TotalRecord;
                    }

                    string strClientExtKey = masterKeyWan2Lot;
                    RandomUtils.Random = new UnsecureRandom();
                    ExtKey masterPubKey = new BitcoinExtKey(strClientExtKey, Network.TestNet);
                    ExtKey pubkey = masterPubKey.Derive(RDUser.MemberID, hardened: true);

                    var userBitPK = pubkey.PrivateKey.GetBitcoinSecret(Network.TestNet).GetAddress();
                    ViewData["EWallet"] = userBitPK;

                    ViewData["ListTransaction"] = lstTransaction;
                    TempData["TotalRecord"] = totalRecord;
                    ViewData["Points"] = member.Points;
                }

            }
            return View();
        }
        [RDUser]
        public ActionResult TransactionBookingTicketMember(string p)
        {
            List<string> lstHtml = new List<string>();
            int start = 0, end = 10;
            int totalRecord = 0;
            int intPageSize = 10;
            int intPage = 1;
            int number1 = 0; int number2 = 0; int number3 = 0; int number4 = 0; int number5 = 0; int extranumber = 0;
            try
            {
                if (Session["Language"] == null)
                {
                    Session["Language"] = "EN";
                }
                string keyBooking = string.Empty;
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                if (!string.IsNullOrEmpty(p))
                {
                    intPage = Convert.ToInt32(p);
                }

                start = (intPage - 1) * intPageSize + 1;
                end = (intPage * intPageSize);


                MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                if (member != null)
                {
                    IEnumerable<BookingMegaball> lstTransaction = null;
                    if (lstTransaction == null)
                    {
                        lstTransaction = bookingServices.ListTransactionBookingByMemberPaging(member.MemberID, start, end);
                    }
                    if (lstTransaction != null && lstTransaction.Count() > 0)
                    {
                        totalRecord = lstTransaction.ElementAt(0).TotalRecord;
                    }

                    lstHtml = GenHtmlCheckingRusult(lstTransaction.ToList(), number1, number2, number3, number4, number5, extranumber, start, end, intPage, intPageSize);
                    TempData["TotalRecord"] = totalRecord;
                }
            }
            catch (Exception objEx)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText(objEx.Message);
            }
            if (lstHtml == null)
            {
                lstHtml = new List<string>();
            }
            return View(lstHtml);
        }
        public List<string> GenHtmlCheckingRusult(List<BookingMegaball> lstTransaction, int number1, int number2, int number3, int number4, int number5, int extranumber, int start, int end, int intPage, int intPageSize)
        {
            string strHtml = string.Empty;
            List<string> lst = null;
            string totalRecord = string.Empty;
            StringBuilder strBuilder = new StringBuilder();

            DateTime OpenDate = DateTime.Today;
            DateTime CreateDate = DateTime.Today;
            try
            {
                MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                if (member != null)
                {

                    IEnumerable<AwardMegaball> lstAwardMegaball = null;
                    if (lstAwardMegaball == null)
                    {
                        lstAwardMegaball = awardServices.GetListAwardOrderByDate();
                    }
                    if (lstTransaction != null)
                    {
                        int stt = 1;
                        if (intPage == 1)
                        {
                            stt = intPage * stt;
                        }
                        else
                        {
                            stt = (intPage * intPageSize - intPageSize) + 1;
                        }
                        foreach (var transaction in lstTransaction)
                        {
                            totalRecord = transaction.TotalRecord.ToString().Trim();
                            number1 = transaction.FirstNumber;
                            number2 = transaction.SecondNumber;
                            number3 = transaction.ThirdNumber;
                            number4 = transaction.FourthNumber;
                            number5 = transaction.FivethNumber;
                            extranumber = transaction.ExtraNumber;
                            OpenDate = transaction.OpenDate;
                            CreateDate = transaction.CreateDate;

                            string strOpenDate = OpenDate.ToString("MM/dd/yyyy").Trim();
                            string strCreateDate = CreateDate.ToString("MM/dd/yyyy").Trim();

                            if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                            {
                                int i = 0;

                                List<AwardMegaball> lstTemp = lstAwardMegaball.Where(x => x.DrawID == transaction.DrawID).ToList();

                                if(lstTemp != null && lstTemp.Count() >0)
                                {
                                    string strResult = "";
                                    AwardMegaball item = lstTemp[0];
                                    int[] arrNumber = new int[5];
                                    arrNumber[0] = item.FirstNumber;
                                    arrNumber[1] = item.SecondNumber;
                                    arrNumber[2] = item.ThirdNumber;
                                    arrNumber[3] = item.FourthNumber;
                                    arrNumber[4] = item.FivethNumber;
                                    Array.Sort(arrNumber);
                                    string statusName = "";
                                    if (transaction.Status == 0)
                                    {
                                        statusName = "InActive";
                                    }
                                    else if (transaction.Status == 1)
                                    {
                                        statusName = "Active";
                                    }
                                    else if (transaction.Status == 2)
                                    {
                                        statusName = "Deleted";
                                    }
                                    string styleColor = "#fff";
                                    if (stt % 2 == 0)
                                    {
                                        styleColor = "#ccc";
                                    }
                                    strBuilder.Append("<tr class=\"info\">");
                                    strBuilder.Append("<td class=\"pointer\" onclick =\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" data-toggle=\"modal\" data-target=\"#myModal\">" + stt + "</td>");
                                    strBuilder.Append("<td class=\"text-center pointer balls nowrap\" onclick =\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" data-toggle=\"modal\" data-target=\"#myModal\">");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FirstNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.SecondNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ThirdNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FourthNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FivethNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ExtraNumber + "</span>");
                                    strBuilder.Append("</td>");
                                    strBuilder.Append("<td class=\"text-center pointer\" onclick =\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" data-toggle=\"modal\" data-target=\"#myModal\">" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + " UTC" + "</td>");
                                    strBuilder.Append("<td class=\"text-center pointer balls nowrap\" onclick =\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" data-toggle=\"modal\" data-target=\"#myModal\">");
                                    strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[0], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[0], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[0] + "" + (Checknumber(arrNumber[0], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[1], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[1], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[1] + "" + (Checknumber(arrNumber[1], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[2], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[2], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[2] + "" + (Checknumber(arrNumber[2], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[3], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[3], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[3] + "" + (Checknumber(arrNumber[3], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[4], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[4], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[4] + "" + (Checknumber(arrNumber[4], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber last-child" + (extranumber == item.ExtraNumber ? "" : "disable") + " " + (extranumber == item.ExtraNumber ? "" : "disable") + "\">" + item.ExtraNumber + "" + (extranumber == item.ExtraNumber ? "" : "") + "</span>");
                                    strBuilder.Append("</td>");
                                    if ((CountNumberExistFivenumber(arrNumber, item.ExtraNumber, number1, number2, number3, number4, number5, ref strResult) >= 2) || (CountNumberExistNumberGold(arrNumber, item.ExtraNumber, extranumber, ref strResult) >= 1) || ((CountNumberExistFivenumber(arrNumber, item.ExtraNumber, number1, number2, number3, number4, number5, ref strResult) >= 1) && (CountNumberExistNumberGold(arrNumber, item.ExtraNumber, extranumber, ref strResult) >= 1)))
                                    {
                                        strBuilder.Append("<td class =\"text-center your-match\">" + (CountNumberExist(arrNumber, item.ExtraNumber, number1, number2, number3, number4, number5, extranumber, ref strResult) == 0 ? "- " : strResult + " Balls") + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "<a href =\"/tickets-winning-history\"><i class=\"fa fa-link\" title=\"Link Award\"></i></a>" + "</td>");
                                    }
                                    else
                                    {
                                        strBuilder.Append("<td class =\"text-center\">" + (CountNumberExist(arrNumber, item.ExtraNumber, number1, number2, number3, number4, number5, extranumber, ref strResult) == 0 ? "- " : strResult + " Balls") + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "</td>");
                                    }
                                    strBuilder.Append("</tr>");

                                    stt++;
                                }
                                else
                                {
                                    string statusName = "";

                                    if (transaction.Status == 0)
                                    {
                                        statusName = "InActive";
                                    }
                                    else if (transaction.Status == 1)
                                    {
                                        statusName = "Active";
                                    }


                                    strBuilder.Append("<tr class=\"info\">");
                                    strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"pointer\">" + stt + "</td>");
                                    strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"text-center pointer balls nowrap\">");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FirstNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.SecondNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ThirdNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FourthNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FivethNumber + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ExtraNumber + "</span>");
                                    strBuilder.Append("</td>");


                                    strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"text-center pointer\">" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + " UTC" + "</td>");


                                    strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"text-center nowrap pointer\">");
                                    strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                    strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                    strBuilder.Append("</td>");
                                    strBuilder.Append("<td class =\"text-center\">" + "- Balls" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "</td>");
                                    strBuilder.Append("</tr>");

                                    stt++;
                                }
                            }
                            else
                            {
                                string statusName = "";

                                if (transaction.Status == 0)
                                {
                                    statusName = "InActive";
                                }
                                else if (transaction.Status == 1)
                                {
                                    statusName = "Active";
                                }
                                strBuilder.Append("<tr class=\"info\">");
                                strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"pointer\">" + stt + "</td>");
                                strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"text-center pointer balls nowrap\">");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FirstNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.SecondNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ThirdNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FourthNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FivethNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ExtraNumber + "</span>");
                                strBuilder.Append("</td>");
                                strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"text-center pointer\">" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + " UTC" + "</td>");
                                strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode.Trim() + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"text-center pointer nowrap\">");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("</td>");
                                strBuilder.Append("<td class =\"text-center\">" + "- Balls" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "</td>");
                                strBuilder.Append("</tr>");
                                stt++;
                            }

                            lst = new List<string>();
                            lst.Add(strBuilder.ToString());
                            BBH.Lotte.Web.Controllers.PagingController Paging = new PagingController();
                            lst.Add(PagingModels.GenPaging(Convert.ToString(intPage), int.Parse(totalRecord)));

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                strHtml = "";
            }
            if (lst == null)
                lst = new List<string>();
            return lst;
        }

        [RDUser]
        public PartialViewResult TransactionTicketWinning(string p)
        {
            int intMemberID = 0;
            List<string> lstHtml = new List<string>();
            IEnumerable<BookingMegaball> lstTransaction = null;
            ObjJsonResult result = new ObjJsonResult();

            int start = 0, end = 10;
            int intPageSize = 10;
            int intPage = 1;
            int number1 = 0; int number2 = 0; int number3 = 0; int number4 = 0; int number5 = 0; int extranumber = 0;
            try
            {
                if (Session["Language"] == null)
                {
                    Session["Language"] = "EN";
                }
                string keyBooking = string.Empty;

                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);



                if (!string.IsNullOrEmpty(p))
                {
                    intPage = Convert.ToInt32(p);
                }

                start = (intPage - 1) * intPageSize + 1;
                end = (intPage * intPageSize);


                DateTime toD = DateTime.Now;
                DateTime fromD = toD.AddDays(-30);

                MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                if (member != null)
                {
                    intMemberID = member.MemberID;
                    if (lstTransaction == null)
                    {
                        lstTransaction = bookingServices.ListTransactionBookingWinnigByMemberSearch(member.MemberID, fromD, toD, 0, 10000);
                        foreach (var item in lstTransaction)
                        {
                            number1 = item.FirstNumber;
                            number2 = item.SecondNumber;
                            number3 = item.ThirdNumber;
                            number4 = item.FourthNumber;
                            number5 = item.FivethNumber;
                            extranumber = item.ExtraNumber;
                        }
                    }
                    ViewData["ListTransaction"] = lstTransaction;
                }
            }
            catch (Exception objEx)
            {
                Utility.WriteLog(strPathLog + "/TransactionTicketWinning.txt", objEx.Message);
            }

            try
            {
                int totalRow = 0;
                result = GenHTMLRusultTicketWinning(lstTransaction, ref totalRow, intPage);
                result = null;
            }
            catch (Exception ex)
            {
            }
            if (result == null)
            {
                result = new ObjJsonResult();
            }
            else
            {
                lstHtml = result.LstString;
            }
            if (lstHtml == null)
            {
                lstHtml = new List<string>();
            }
            int stt = 0;

            stt = (intPageSize * intPage) - intPageSize;
            int maxRecord = intPageSize * intPage;
            if (maxRecord > lstHtml.Count())
            {
                maxRecord = lstHtml.Count();
            }
            List<string> lstResultHtml = new List<string>();
            for (int i = stt; i < maxRecord; i++)
            {
                lstResultHtml.Add(lstHtml[i]);
            }
            ObjJsonResult resultHtml = new ObjJsonResult();
            resultHtml.LstString = lstResultHtml;
            resultHtml.StrValue = result.StrValue;
            return PartialView(resultHtml);
        }


        public ObjJsonResult GenHTMLRusultTicketWinning(IEnumerable<BookingMegaball> lstTransaction, ref int totalRecord, int intPage)
        {
            Dictionary<List<string>, string> dicResult = new Dictionary<List<string>, string>();
            List<string> lst = new List<string>();
            string strPaging = string.Empty;
            string strResult = "";
            ObjJsonResult result = new ObjJsonResult();
            int stt = 1;
            result.LstString = new List<string>();
            IEnumerable<AwardBO> lstAward = null;
            if (lstAward == null)
            {
                lstAward = awardNumberServices.GetAllListAward();
                if (lstAward != null && lstAward.Count() > 0)
                {
                    objRedisCache.SetCache<IEnumerable<AwardBO>>(Common.KeyListAwardBO, lstAward);
                }
            }
            if (lstTransaction != null && lstTransaction.Count() > 0)
            {
                IEnumerable<AwardMegaball> lstAwardMegaball = null;
                if (lstAwardMegaball == null)
                {
                    lstAwardMegaball = awardServices.GetListAwardOrderByDate();
                }

                //Get jackpot
                decimal jackpot = 0;
                decimal jacpotTotal = 0;
                decimal awardFee = 0;
                decimal jacpotTotalTemp = 0;
                string strJackpotAddress = string.Empty;
                //get address btc
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = JackPotID;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = false;
                objAddressInfoRequest.CreatedBy = JackPotID;
                objAddressInfoRequest.WalletId = short.Parse(WalletID);

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    strJackpotAddress = objAddressInfo.AddressId;
                    jackpot = objAddressInfo.Available;
                }
                WriteErrorLog.WriteLogsToFileText(JsonConvert.SerializeObject(lstTransaction).ToString());
                foreach (var itemTransaction in lstTransaction)
                {
                    int number1 = itemTransaction.FirstNumber;
                    int number2 = itemTransaction.SecondNumber;
                    int number3 = itemTransaction.ThirdNumber;
                    int number4 = itemTransaction.FourthNumber;
                    int number5 = itemTransaction.FivethNumber;
                    int extranumber = itemTransaction.ExtraNumber;
                    DateTime Opendate = itemTransaction.OpenDate;

                    DateTime createDate = itemTransaction.CreateDate;

                    int intNormalBall = 0;
                    int intNormalGold = 0;

                    List<AwardMegaball> lstAwardMegaballTemp = lstAwardMegaball.Where(x => x.DrawID == itemTransaction.DrawID).ToList();

                    if (lstAwardMegaballTemp != null)
                    {
                        foreach (var itemAwardMegaballTemp in lstAwardMegaballTemp)
                        {
                            if (itemTransaction.FirstNumber == itemAwardMegaballTemp.FirstNumber || itemTransaction.FirstNumber == itemAwardMegaballTemp.SecondNumber || itemTransaction.FirstNumber == itemAwardMegaballTemp.ThirdNumber ||
                                itemTransaction.FirstNumber == itemAwardMegaballTemp.FourthNumber || itemTransaction.FirstNumber == itemAwardMegaballTemp.FivethNumber)
                            {
                                intNormalBall++;
                            }
                            if (itemTransaction.SecondNumber == itemAwardMegaballTemp.FirstNumber || itemTransaction.SecondNumber == itemAwardMegaballTemp.SecondNumber || itemTransaction.SecondNumber == itemAwardMegaballTemp.ThirdNumber ||
                                itemTransaction.SecondNumber == itemAwardMegaballTemp.FourthNumber || itemTransaction.SecondNumber == itemAwardMegaballTemp.FivethNumber)
                            {
                                intNormalBall++;
                            }
                            if (itemTransaction.ThirdNumber == itemAwardMegaballTemp.FirstNumber || itemTransaction.ThirdNumber == itemAwardMegaballTemp.SecondNumber || itemTransaction.ThirdNumber == itemAwardMegaballTemp.ThirdNumber ||
                                itemTransaction.ThirdNumber == itemAwardMegaballTemp.FourthNumber || itemTransaction.ThirdNumber == itemAwardMegaballTemp.FivethNumber)
                            {
                                intNormalBall++;
                            }
                            if (itemTransaction.FourthNumber == itemAwardMegaballTemp.FirstNumber || itemTransaction.FourthNumber == itemAwardMegaballTemp.SecondNumber || itemTransaction.FourthNumber == itemAwardMegaballTemp.ThirdNumber ||
                                itemTransaction.FourthNumber == itemAwardMegaballTemp.FourthNumber || itemTransaction.FourthNumber == itemAwardMegaballTemp.FivethNumber)
                            {
                                intNormalBall++;
                            }
                            if (itemTransaction.FivethNumber == itemAwardMegaballTemp.FirstNumber || itemTransaction.FivethNumber == itemAwardMegaballTemp.SecondNumber || itemTransaction.FivethNumber == itemAwardMegaballTemp.ThirdNumber ||
                                itemTransaction.FivethNumber == itemAwardMegaballTemp.FourthNumber || itemTransaction.FivethNumber == itemAwardMegaballTemp.FivethNumber)
                            {
                                intNormalBall++;
                            }
                            if (itemTransaction.ExtraNumber == itemAwardMegaballTemp.ExtraNumber)
                            {
                                intNormalGold++;
                            }

                        }
                    }

                    List<AwardBO> lstAwardTemp = lstAward.Where(x => x.NumberBallNormal == intNormalBall && x.NumberBallGold == intNormalGold).ToList();
                    if (lstAwardTemp != null && lstAwardTemp.Count() > 0)
                    {
                        StringBuilder strBuilder = new StringBuilder();
                        strBuilder.Append("<tr class=\"info pointer\">");
                        strBuilder.Append("<td>" + stt + "</td>");
                        strBuilder.Append("<td class=\"text-center balls nowrap\">");
                        strBuilder.Append("<span class=\"ticketNumber\">" + itemTransaction.FirstNumber + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber\">" + itemTransaction.SecondNumber + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber\">" + itemTransaction.ThirdNumber + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber\">" + itemTransaction.FourthNumber + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber\">" + itemTransaction.FivethNumber + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber\">" + itemTransaction.ExtraNumber + "</span>");
                        strBuilder.Append("</td>");
                        strBuilder.Append("<td class=\"text-center\">" + itemTransaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + " UTC" + "</td>");
                        strBuilder.Append("<td class=\"text-center balls nowrap\">");
                        strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(lstAwardMegaballTemp[0].FirstNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(lstAwardMegaballTemp[0].FirstNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + lstAwardMegaballTemp[0].FirstNumber + "" + (Checknumber(lstAwardMegaballTemp[0].FirstNumber, number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(lstAwardMegaballTemp[0].SecondNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(lstAwardMegaballTemp[0].SecondNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + lstAwardMegaballTemp[0].SecondNumber + "" + (Checknumber(lstAwardMegaballTemp[0].SecondNumber, number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(lstAwardMegaballTemp[0].ThirdNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(lstAwardMegaballTemp[0].ThirdNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + lstAwardMegaballTemp[0].ThirdNumber + "" + (Checknumber(lstAwardMegaballTemp[0].ThirdNumber, number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(lstAwardMegaballTemp[0].FourthNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(lstAwardMegaballTemp[0].FourthNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + lstAwardMegaballTemp[0].FourthNumber + "" + (Checknumber(lstAwardMegaballTemp[0].FourthNumber, number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(lstAwardMegaballTemp[0].FivethNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(lstAwardMegaballTemp[0].FivethNumber, number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + lstAwardMegaballTemp[0].FivethNumber + "" + (Checknumber(lstAwardMegaballTemp[0].FivethNumber, number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                        strBuilder.Append("<span class=\"ticketNumber last-child " + (lstAwardMegaballTemp[0].ExtraNumber == extranumber ? "" : "disable") + " " + (lstAwardMegaballTemp[0].ExtraNumber == extranumber ? "" : "disable") + "\">" + lstAwardMegaballTemp[0].ExtraNumber + "" + (lstAwardMegaballTemp[0].ExtraNumber == extranumber ? "" : "") + "</span>");
                        strBuilder.Append("</td>");
                        strBuilder.Append("<td class=\"text-center balls nowrap\">");
                        if (intNormalBall > 0)
                        {
                            for (int i = 0; i < intNormalBall; i++)
                            {
                                strBuilder.Append("<i class=\"balls-icon\"></i>");
                            }
                        }
                        if (intNormalGold > 0)
                        {
                            strBuilder.Append("<i class=\"balls-icon orange\"></i>");
                        }
                        strBuilder.Append("</td>");
                        if (GetAwardJackPot(itemTransaction.DrawID) > 1)
                        {
                            jacpotTotal = jackpot / GetAwardJackPot(itemTransaction.DrawID);
                            awardFee = (jacpotTotal * (decimal)lstAwardTemp[0].AwardPercent) / 100;
                            if (itemTransaction.AwardWithdrawStatus == -1)
                            {
                                strBuilder.Append("<td class=\"text-center\">" + (lstAwardTemp[0].Priority == 2 ? lstAwardTemp[0].AwardValue : jacpotTotal).ToString("G29") + "</td>");
                            }
                            else if (itemTransaction.AwardWithdrawStatus == (int)AwardWithdrawStatus.Recieved)
                            {
                                strBuilder.Append("<td class=\"text-center\">" + (lstAwardTemp[0].Priority == 2 ? lstAwardTemp[0].AwardValue : (decimal)itemTransaction.ValuesWithdraw).ToString("G29") + "</td>");
                            }
                        }
                        else
                        {
                            if ((GetAwardJackPot(itemTransaction.DrawID) == 1))
                            {
                                if (itemTransaction.AwardWithdrawStatus == -1)
                                {
                                    strBuilder.Append("<td class=\"text-center\">" + (lstAwardTemp[0].Priority == 2 ? lstAwardTemp[0].AwardValue : jacpotTotal).ToString("G29") + "</td>");
                                }
                                else if (itemTransaction.AwardWithdrawStatus == (int)AwardWithdrawStatus.Recieved)
                                {
                                    strBuilder.Append("<td class=\"text-center\">" + (lstAwardTemp[0].Priority == 2 ? lstAwardTemp[0].AwardValue : (decimal)itemTransaction.ValuesWithdraw).ToString("G29") + "</td>");
                                }
                            }
                            else
                            {
                                strBuilder.Append("<td class=\"text-center\">" + (lstAwardTemp[0].AwardValue).ToString("G29") + "</td>");
                            }
                        }
                        strBuilder.Append("<td class=\"text-center\" style=\"display: none\">" + lstAwardTemp[0].AwardFee.ToString("G29") + "</td>");
                        strBuilder.Append("<td class=\"text-center\" style=\"display: none\">" + lstAwardTemp[0].Priority + "</td>");
                        strBuilder.Append("<td class=\"text-center\">");
                        if (itemTransaction.AwardWithdrawStatus == -1)
                        {
                            if (GetAwardJackPot(itemTransaction.DrawID) >= 1)
                            {
                                strBuilder.Append("<button class=\"btn btn-warning btn-sm\" data-target=\"#verify-getaward\" data-toggle=\"modal\" onclick =\"ShowBookingWinnerDetail('" + itemTransaction.BookingID + "', '" + (lstAwardTemp[0].Priority == 2 ? lstAwardTemp[0].AwardValue : jacpotTotal) + "', '" + awardFee + "' ,'" + itemTransaction.FirstNumber + "', '" + itemTransaction.SecondNumber + "', '" + itemTransaction.ThirdNumber + "', '" + itemTransaction.FourthNumber + "', '" + itemTransaction.FivethNumber + "', '" + itemTransaction.ExtraNumber + "', '" + itemTransaction.OpenDate + "','" + RDUser.BTCAddress + "','" + lstAwardTemp[0].Priority + "','" + itemTransaction.DrawID + "')\">Get award</button>");
                            }
                            else
                            {
                                strBuilder.Append("<button class=\"btn btn-warning btn-sm\" data-target=\"#verify-getaward\" data-toggle=\"modal\" onclick =\"ShowBookingWinnerDetail('" + itemTransaction.BookingID + "', '" + lstAwardTemp[0].AwardValue + "', '" + lstAwardTemp[0].AwardFee + "' ,'" + itemTransaction.FirstNumber + "', '" + itemTransaction.SecondNumber + "', '" + itemTransaction.ThirdNumber + "', '" + itemTransaction.FourthNumber + "', '" + itemTransaction.FivethNumber + "', '" + itemTransaction.ExtraNumber + "', '" + itemTransaction.OpenDate + "','" + RDUser.BTCAddress + "','" + lstAwardTemp[0].Priority + "','" + itemTransaction.DrawID + "')\">Get award</button>");
                            }
                        }
                        else if (itemTransaction.AwardWithdrawStatus == (int)AwardWithdrawStatus.Recieved)
                        {
                            strBuilder.Append("<button class=\"btn btn-success btn-sm disabled\">Received</button>");
                        }
                        strBuilder.Append("</td>");
                        strBuilder.Append("</tr>");
                        totalRecord++;
                        stt++;
                        result.LstString.Add(strBuilder.ToString());
                    }
                    TempData["TotalRecord"] = totalRecord;
                }
            }

            BBH.Lotte.Web.Controllers.PagingController Paging = new PagingController();
            strPaging = PagingModels.GenPaging(Convert.ToString(intPage), totalRecord);
            result.StrValue = strPaging;
            return result;
        }

        public int CountNumberJactpot(int[] lstAwardTemp, int extraNumberCheck, int number1, int number2, int number3, int number4, int number5, int extranumber, ref string strResult)
        {
            int count = 0;
            if (CountNumberExist(lstAwardTemp, extraNumberCheck, number1, number2, number3, number4, number5, extranumber, ref strResult) == 6)
            {
                count++;
            }
            return count;
        }
        public decimal GetAwardJackPot(int intDrawID)
        {
            string strNumberCheck = string.Empty;
            string strExtraNumber = string.Empty;

            decimal totalWon = 0;
            try
            {
                AwardMegaball awardMegaballBO = awardMegaballServices.GetAwardMegabalByDrawID(intDrawID);
                if (awardMegaballBO != null && awardMegaballBO.DrawID == intDrawID)
                {
                    strNumberCheck = awardMegaballBO.FirstNumber + "," + awardMegaballBO.SecondNumber + "," + awardMegaballBO.ThirdNumber + "," + awardMegaballBO.FourthNumber + "," + awardMegaballBO.FivethNumber;
                    strExtraNumber = awardMegaballBO.ExtraNumber.ToString();
                }
                if (!string.IsNullOrEmpty(strNumberCheck) && !string.IsNullOrWhiteSpace(strExtraNumber))
                {
                    totalWon = CountAwardJackpot(strNumberCheck, strExtraNumber, intDrawID);
                }
            }
            catch (Exception ex)
            {
                totalWon = 0;
                WriteErrorLog.WriteLogsToFileText("Error (SumTotalWonNotJackpot) Message: " + ex.Message);
            }
            return totalWon;
        }
        public decimal CountAwardJackpot(string strNumberCheck, string strExtraNumber, int intDrawID)
        {
            //decimal totalWon = 0;
            int countTicketWon = 0;
            try
            {
                string[] arrNumber = strNumberCheck.Split(',');
                IEnumerable<BookingMegaball> lstBookingMegaball = new List<BookingMegaball>();
                lstBookingMegaball = bookingServices.SP_ListBookingByDraw_FE(intDrawID);
                if (lstBookingMegaball != null || lstBookingMegaball.Count() > 0)
                {
                    List<int> lstTicketWinner = new List<int>();
                    foreach (var item in lstBookingMegaball)
                    {
                        int countBallNornal = 0;
                        int countBallGold = 0;

                        var lstTemp = lstTicketWinner.Where(x => x == item.BookingID);
                        if (lstTemp.Count() == 0)
                        {
                            for (int i = 0; i < arrNumber.Length; i++)
                            {
                                if (arrNumber[i].Trim() == item.FirstNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.SecondNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.ThirdNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.FourthNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                                if (arrNumber[i].Trim() == item.FivethNumber.ToString())
                                {
                                    countBallNornal++;
                                }
                            }
                            if (strExtraNumber.Trim() == item.ExtraNumber.ToString())
                            {
                                countBallGold++;
                            }
                            IEnumerable<AwardBO> lstAward = awardNumberServices.GetAllListAward();
                            if (lstAward != null && lstAward.Count() > 0)
                            {
                                List<AwardBO> lstTempAward = lstAward.Where(x => x.NumberBallNormal == countBallNornal && x.NumberBallGold == countBallGold).ToList();
                                if (lstTempAward != null && lstTempAward.Count() > 0)
                                {
                                    foreach (var itemTempAward in lstTempAward)
                                    {
                                        if (itemTempAward.Priority == 1 && item.AwardWithdrawStatus <= 0)//<= 0: staus not received
                                        {
                                            countTicketWon++;
                                        }
                                    }
                                    lstTicketWinner.Add(item.BookingID);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                countTicketWon = 0;
                WriteErrorLog.WriteLogsToFileText("MemberController-->GetAwardJackpot - Exception : " + objEx.Message);
            }
            return countTicketWon;
        }

        public int CountNumberExist(int[] arrNumber, int extraNumberCheck, int number1, int number2, int number3, int number4, int number5, int extranumber, ref string strResult)
        {
            int count = 0;
            int[] arrArrayCheck = { number1, number2, number3, number4, number5 };
            for (int i = 0; i < arrNumber.Length; i++)
            {
                for (int j = 0; j < arrArrayCheck.Length; j++)
                {
                    if (arrNumber[i] == arrArrayCheck[j])
                    {
                        count++;
                    }
                }
            }
            if (extraNumberCheck == extranumber)
            {
                count++;
            }
            strResult = count.ToString();
            return count;
        }

        public int CountNumberExistFivenumber(int[] arrNumber, int extraNumberCheck, int number1, int number2, int number3, int number4, int number5, ref string strResult)
        {
            int count = 0;
            int[] arrArrayCheck = { number1, number2, number3, number4, number5 };
            for (int i = 0; i < arrNumber.Length; i++)
            {
                for (int j = 0; j < arrArrayCheck.Length; j++)
                {
                    if (arrNumber[i] == arrArrayCheck[j])
                    {
                        count++;
                    }
                }
            }

            strResult = count.ToString();
            return count;
        }

        public int CountNumberExistNumberGold(int[] arrNumber, int extraNumberCheck, int extranumber, ref string strResult)
        {
            int count = 0;

            if (extraNumberCheck == extranumber)
            {
                count++;
            }
            strResult = count.ToString();
            return count;
        }


        public bool Checknumber(int number, int number1, int number2, int number3, int number4, int number5)
        {
            bool check = false;
            if (number == number1)
            {
                check = true;
            }
            if (number == number2)
            {
                check = true;
            }
            if (number == number3)
            {
                check = true;
            }
            if (number == number4)
            {
                check = true;
            }
            if (number == number5)
            {
                check = true;
            }
            return check;
        }

        public bool CheckNumber(int number1, int number2, int number3, int number4, int number5, int extranumber)
        {
            bool result = true;
            try
            {
                if (number1 < 1 || number1 > 49)
                {
                    result = false;
                }
                if (number2 < 1 || number2 > 49)
                {
                    result = false;
                }
                if (number3 < 1 || number3 > 49)
                {
                    result = false;
                }
                if (number3 < 1 || number3 > 49)
                {
                    result = false;
                }
                if (number4 < 1 || number4 > 49)
                {
                    result = false;
                }
                if (number5 < 1 || number5 > 49)
                {
                    result = false;
                }
                if (extranumber < 1 || extranumber > 26)
                {
                    result = false;
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }

        public ActionResult ChangePasswordMember(ResetPasswordModel objResetPasswordModel)
        {

            if (!ModelState.IsValid)
            {
                return View(objResetPasswordModel);
            }
            return View();
        }

        [HttpPost]
        public JsonResult UpdatePassMember(ResetPasswordModel objResetPasswordModel)
        {
            bool isSuccess = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (RDUser.IsLogedIn())
                    {
                        MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                        if (member != null)
                        {
                            string passmd5 = BBH.Lotte.Shared.Utility.MaHoaMD5(objResetPasswordModel.Password);
                            bool rs = services.UpdatePasswordMember(member.Email, passmd5);
                            if (rs)
                            {
                                isSuccess = true;
                            }
                        }
                    }
                    else
                    {
                        return Json(new { success = isSuccess, isLogin = false }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { success = isSuccess, isLogin = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("MemberController-->UpdatePassMember - Exception : " + objEx.Message);
                return Json(new { success = false, isLogin = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public string ConfirmBooking(int bookingID, int quantity)
        {
            string result = "";
            return result;
        }


        [HttpGet]
        public string SearchTransactionBooking(string fromDate, string toDate, int page)
        {
            DateTime? fromD = DateTime.Now;
            DateTime? toD = DateTime.Now;
            StringBuilder strBuilder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int intPageSize = 10;
            int start = 0, end = 10;
            int totalRecord = 0;
            string json = "";
            int number1 = 0; int number2 = 0; int number3 = 0; int number4 = 0; int number5 = 0; int extranumber = 0;
            DateTime OpenDate = DateTime.Today;
            List<string> lst = null;

            MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
            if (member != null)
            {
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                }
                else
                {
                    if (string.IsNullOrEmpty(fromDate))
                    {
                        if (string.IsNullOrEmpty(toDate))
                        {
                            fromD = null;
                            toD = DateTime.Parse("01/01/1990");
                        }
                        else
                        {
                            fromD = DateTime.Parse("01/01/1990");
                            toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                        }
                    }
                    else
                    {
                        fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                        toD = DateTime.Now;
                    }
                }

                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);

                IEnumerable<BookingMegaball> lstTransaction = bookingServices.ListTransactionBookingBySearch(member.MemberID, fromD, toD, start, end);
                if (lstTransaction != null && lstTransaction.Count() > 0)
                {

                    IEnumerable<AwardMegaball> lstAwardMegaball = null;
                    if (lstAwardMegaball == null)
                    {
                        lstAwardMegaball = awardServices.GetAllListAward();
                    }
                    if (lstTransaction != null)
                    {
                        int stt = 1;

                        if (page == 1)
                        {
                            stt = page * stt;
                        }
                        else
                        {
                            stt = (page * intPageSize - intPageSize) + 1;
                        }

                        foreach (var transaction in lstTransaction)
                        {
                            totalRecord = transaction.TotalRecord;
                            TempData["TotalRecord"] = transaction.TotalRecord;
                            number1 = transaction.FirstNumber;
                            number2 = transaction.SecondNumber;
                            number3 = transaction.ThirdNumber;
                            number4 = transaction.FourthNumber;
                            number5 = transaction.FivethNumber;
                            extranumber = transaction.ExtraNumber;
                            OpenDate = transaction.OpenDate;
                            DateTime CreateDate = transaction.CreateDate;

                            //string strOpenDate = OpenDate.ToString("dd/MM/yyyy").Trim();
                            string strOpenDate = OpenDate.ToString("MM/dd/yyyy").Trim();
                            string strCreateDate = CreateDate.ToString("MM/dd/yyyy").Trim();

                            List<AwardMegaball> lstTemp = lstAwardMegaball.Where(x => x.DrawID == transaction.DrawID).ToList();
                            string strResult = "";

                            if (lstTemp != null && lstTemp.Count() > 0)
                            {
                                AwardMegaball item = lstTemp[0];
                                int i = 0;
                                string strClass = "";
                                int[] arrNumber = new int[5];
                                arrNumber[0] = item.FirstNumber;
                                arrNumber[1] = item.SecondNumber;
                                arrNumber[2] = item.ThirdNumber;
                                arrNumber[3] = item.FourthNumber;
                                arrNumber[4] = item.FivethNumber;
                                Array.Sort(arrNumber);
                                string statusName = "";
                                if (transaction.Status == 0)
                                {
                                    statusName = "InActive";
                                }
                                else if (transaction.Status == 1)
                                {
                                    statusName = "Active";
                                }
                                else if (transaction.Status == 2)
                                {
                                    statusName = "Deleted";
                                }
                                string styleColor = "#fff";
                                if (stt % 2 == 0)
                                {
                                    styleColor = "#ccc";
                                }



                                strBuilder.Append("<tr class=\"info\">");
                                strBuilder.Append("<td class=\"pointer\" onclick =\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" data-toggle=\"modal\" data-target=\"#myModal\">" + stt + "</td>");
                                strBuilder.Append("<td class=\"text-center pointer balls nowrap\" onclick =\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" data-toggle=\"modal\" data-target=\"#myModal\">");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FirstNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.SecondNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ThirdNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FourthNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FivethNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ExtraNumber + "</span>");
                                strBuilder.Append("</td>");
                                strBuilder.Append("<td class=\"text-center pointer\" onclick =\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" data-toggle=\"modal\" data-target=\"#myModal\">" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + " UTC" + "</td>");
                                strBuilder.Append("<td class=\"text-center pointer balls nowrap\" onclick =\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" data-toggle=\"modal\" data-target=\"#myModal\">");
                                strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[0], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[0], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[0] + "" + (Checknumber(arrNumber[0], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[1], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[1], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[1] + "" + (Checknumber(arrNumber[1], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[2], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[2], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[2] + "" + (Checknumber(arrNumber[2], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[3], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[3], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[3] + "" + (Checknumber(arrNumber[3], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber " + (Checknumber(arrNumber[4], number1, number2, number3, number4, number5) == true ? "" : "disable") + " " + (Checknumber(arrNumber[4], number1, number2, number3, number4, number5) == true ? "" : "disable") + "\">" + arrNumber[4] + "" + (Checknumber(arrNumber[4], number1, number2, number3, number4, number5) == true ? "" : "") + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber last-child" + (extranumber == item.ExtraNumber ? "" : "disable") + " " + (extranumber == item.ExtraNumber ? "" : "disable") + "\">" + item.ExtraNumber + "" + (extranumber == item.ExtraNumber ? "" : "") + "</span>");
                                strBuilder.Append("</td>");
                                if ((CountNumberExistFivenumber(arrNumber, item.ExtraNumber, number1, number2, number3, number4, number5, ref strResult) >= 2) || (CountNumberExistNumberGold(arrNumber, item.ExtraNumber, extranumber, ref strResult) >= 1) || ((CountNumberExistFivenumber(arrNumber, item.ExtraNumber, number1, number2, number3, number4, number5, ref strResult) >= 1) && (CountNumberExistNumberGold(arrNumber, item.ExtraNumber, extranumber, ref strResult) >= 1)))
                                {
                                    strBuilder.Append("<td class =\"text-center your-match\">" + (CountNumberExist(arrNumber, item.ExtraNumber, number1, number2, number3, number4, number5, extranumber, ref strResult) == 0 ? "- " : strResult + " Balls") + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "<a href =\"/tickets-winning-history\"><i class=\"fa fa-link\" title=\"Link Award\"></i></a>" + "</td>");
                                }
                                else
                                {
                                    strBuilder.Append("<td class =\"text-center\">" + (CountNumberExist(arrNumber, item.ExtraNumber, number1, number2, number3, number4, number5, extranumber, ref strResult) == 0 ? "- " : strResult + " Balls") + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "</td>");
                                }
                                strBuilder.Append("</tr>");
                                stt++;
                            }
                            else
                            {
                                string statusName = "";
                                if (transaction.Status == 0)
                                {
                                    statusName = "InActive";
                                }
                                else if (transaction.Status == 1)
                                {
                                    statusName = "Active";
                                }


                                strBuilder.Append("<tr class=\"info\">");
                                strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"pointer\">" + stt + "</td>");
                                strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"text-center pointer balls nowrap\">");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FirstNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.SecondNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ThirdNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FourthNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.FivethNumber + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\">" + transaction.ExtraNumber + "</span>");
                                strBuilder.Append("</td>");


                                strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"text-center pointer\">" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + " UTC" + "</td>");
                                strBuilder.Append("<td data-target=\"#myModal\" data-toggle=\"modal\" onclick=\"ShowBookingDetail('" + transaction.BookingID + "', '" + transaction.TransactionCode + "', '" + transaction.ValuePoint + "', '" + transaction.FirstNumber + "', '" + transaction.SecondNumber + "', '" + transaction.ThirdNumber + "', '" + transaction.FourthNumber + "', '" + transaction.FivethNumber + "', '" + transaction.ExtraNumber + "', '" + transaction.Quantity + "', '" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + transaction.OpenDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "', '" + statusName + "', '" + transaction.Status + "')\" class=\"text-center pointer nowrap\">");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("<span class=\"ticketNumber\"> " + "<lable class=\"processing\"></lable>" + "</span>");
                                strBuilder.Append("</td>");
                                strBuilder.Append("<td class =\"text-center\">" + "- Balls" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "&nbsp;" + "</td>");
                                strBuilder.Append("</tr>");

                                stt++;
                            }
                        }
                        string linkPage = "", nextLink = "", previewLink = "", firstLink = "", lastLink = "";
                        firstLink = "?p=1";
                        if (page <= 1)
                        {
                            previewLink = "?p=1";
                        }
                        else
                        {
                            previewLink = "?p=" + (page - 1).ToString();
                        }

                        builderPaging.Append("<nav class=\"text-center\">");
                        builderPaging.Append("<ul id=\"ulPaging\" class=\"pagination\">");

                        int pagerank = 5;
                        int next = 10;
                        int prev = 1;


                        if (TempData["TotalRecord"] != null)
                        {
                            int totalRecore = (int)TempData["TotalRecord"];
                            int totalPage = totalRecore / intPageSize;
                            int balance = totalRecore % intPageSize;
                            if (balance != 0)
                            {
                                totalPage += 1;
                            }
                            if (page >= totalPage)
                            {
                                nextLink = "?p=" + totalPage.ToString();
                            }
                            else
                            {
                                nextLink = "?p=" + (page + 1).ToString();
                            }
                            int currentPage = page;
                            var m = 1;
                            if (totalPage > 10)
                            {
                                if (page > pagerank + 1)
                                {
                                    next = (page + pagerank) - 1;
                                    m = page - pagerank;
                                }
                                if (page <= pagerank)
                                {
                                    prev = (page - pagerank);
                                    m = 1;
                                }
                                if (next > totalPage)
                                {
                                    next = totalPage;
                                }
                                if (prev < 1)
                                {
                                    prev = 1;
                                }
                            }
                            else
                            {
                                next = totalPage;
                                prev = 1;
                            }
                            lastLink = "?p=" + totalPage;
                            if (totalPage > 1)
                            {
                                if (currentPage > 1)
                                {

                                    builderPaging.Append("<li class=\"page-item\">");
                                    builderPaging.Append("<a href=\"" + previewLink + "\" class=\"page-link\" aria-label=\"Previous\">");
                                    builderPaging.Append("<span aria-hidden=\"true\">");
                                    builderPaging.Append("<i class=\"fa fa-angle-double-left\" area-hidden=\"hidden\"></i>");
                                    builderPaging.Append("</span>");
                                    builderPaging.Append("</a>");
                                    builderPaging.Append(" </li>");
                                }
                            }

                            if (totalPage > 1)
                            {

                                for (; m <= next; m++)
                                {
                                    linkPage = "?p=" + m;
                                    if (m == currentPage)
                                    {
                                        builderPaging.Append("<li class=\"page-item active\"><a class=\"page-link\">" + m + "</a></li>");
                                    }
                                    else
                                    {
                                        builderPaging.Append("<li class=\"page-item\"><a href=\"javascript:void(0)\" onclick=\"PagingSearchBooking('" + m + "')\" class=\"page-link\">" + m + "</a></li>");
                                    }
                                }
                            }

                            if (totalPage > 1)
                            {
                                if (currentPage < totalPage)
                                {
                                    builderPaging.Append("<li class=\"page-item\">");
                                    builderPaging.Append("<a href=\"" + nextLink + "\" class=\"page-link\" aria-label=\"Next\">");
                                    builderPaging.Append("<span aria-hidden=\"true\">");
                                    builderPaging.Append("<i class=\"fa fa-angle-double-right\" area-hidden=\"hidden\"></i>");
                                    builderPaging.Append("</span>");
                                    builderPaging.Append("</a>");
                                    builderPaging.Append("</li>");
                                }
                            }
                        }


                        builderPaging.Append("</ul>");
                        builderPaging.Append("</nav>");

                    }
                    else
                    {
                        strBuilder.Append("<tr><td colspan=\"5\">No Result</td></tr>");
                    }
                }
                else
                {

                    strBuilder.Append("<tr><td colspan=\"5\">No Result</td></tr>");
                }
            }
            SearchObj obj = new SearchObj();
            obj.ContentResult = strBuilder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            json = JsonConvert.SerializeObject(obj);

            return json;
        }
        [HttpGet]
        public string SearchTransactionPoint(string fromDate, string toDate, int page)
        {
            DateTime? fromD = DateTime.Now;
            DateTime? toD = DateTime.Now;
            StringBuilder builder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int intPageSize = 10;
            int start = 0, end = 5;
            int totalRecord = 0;
            string json = "";
            MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
            if (member != null)
            {
                Int32.TryParse(ConfigurationManager.AppSettings[SessionKey.NUMBERRECORDPAGE], out intPageSize);

                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                    toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                }
                else
                {
                    if (string.IsNullOrEmpty(fromDate))
                    {
                        if (string.IsNullOrEmpty(toDate))
                        {
                            fromD = null;
                            toD = DateTime.Parse("01/01/1990");
                        }
                        else
                        {
                            fromD = DateTime.Parse("01/01/1990");
                            toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                        }
                    }
                    else
                    {
                        fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                        toD = DateTime.Now;
                    }
                }
                start = (page - 1) * intPageSize + 1;
                end = (page * intPageSize);

                IEnumerable<TransactionPointsBO> lstTransaction = pointService.ListTransactionPointBySearch(member.MemberID, fromD, toD, start, end);
                if (lstTransaction != null && lstTransaction.Count() > 0)
                {
                    totalRecord = lstTransaction.ElementAt(0).TotalRecord;
                    int stt = 1;
                    foreach (TransactionPointsBO transaction in lstTransaction)
                    {
                        string statusName = "";
                        if (transaction.Status == 0)
                        {
                            statusName = "InActive";
                        }
                        else if (transaction.Status == 1)
                        {
                            statusName = "Active";
                        }
                        else if (transaction.Status == 2)
                        {
                            statusName = "Deleted";
                        }
                        string styleColor = "#fff";
                        if (stt % 2 == 0)
                        {
                            styleColor = "#ccc";
                        }
                        builder.Append("<tr class=\"none-top-border\" style=\"cursor:pointer; background-color:" + styleColor + "\" onclick=\"ShowTransactionPointDetail('" + transaction.TransactionCode + "','" + transaction.E_Wallet + "','" + transaction.Points + "','" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDate) + "','" + statusName + "')\" data-toggle=\"modal\" data-target=\"#standardModal\">");
                        builder.Append("<td class='textAlignCenter'>" + stt + "</td>");

                        builder.Append("<td class='textAlignCenter'>" + string.Format("{0:0,0}", transaction.Points) + "</td>");
                        builder.Append("<td class='textAlignCenter'>" + transaction.CreateDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDate) + "</td>");

                        builder.Append("</tr>");
                        stt++;
                    }
                    int totalPage = totalRecord / intPageSize;
                    int balance = totalRecord % intPageSize;
                    if (balance != 0)
                    {
                        totalPage += 1;
                    }

                    if (totalPage > 1)
                    {

                        for (int m = 1; m <= totalPage; m++)
                        {

                            if (m == page)
                            {
                                builderPaging.Append("<li class=\"page-item active\"><a class=\"page-link\">" + m + "</a></li>");
                            }
                            else
                            {
                                builderPaging.Append("<li class=\"page-item\"><a href=\"javascript:void(0)\" onclick=\"PagingSearchPoint('" + m + "')\" class=\"page-link\">" + m + "</a></li>");

                            }
                        }
                    }

                }
                else
                {
                    builder.Append("<tr><td colspan=\"3\">No Result</td></tr>");
                }

                SearchObj obj = new SearchObj();
                obj.ContentResult = builder.ToString();
                obj.PagingResult = builderPaging.ToString();
                obj.Totalrecord = totalRecord;
                json = JsonConvert.SerializeObject(obj);
            }
            return json;
        }

        [HttpPost]
        public JsonResult InsertAward(decimal amount, decimal awardFee, string addressWallet, string oppenDateAward, int bookingID, int priority, int intDrawID)
        {
            ObjResultMessage objResult = new ObjResultMessage();
            AwardWithdrawBO awardWithdrawBO = new AwardWithdrawBO();
            IEnumerable<AwardBO> lstAward = new List<AwardBO>();
            AwardBusiness awardWithdrawBus = new AwardBusiness();
            TransferRequest objTransferRequest = new TransferRequest();
            string strResult = string.Empty;
            if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERID] == null)
            {
                Response.Redirect("/login");
            }
            int MemberID = (int)System.Web.HttpContext.Current.Session[SessionKey.MEMBERID];
            if (!ChecklBookID(bookingID, amount, awardFee, priority, intDrawID, MemberID))
            {
                return Json(new { objResult = "AmountFails" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] == null)
                {
                    Response.Redirect("/login");
                }
                else
                {
                    if (priority == 1)
                    {
                        try
                        {
                            objTransferRequest.UserId = AwardWalletID;
                            objTransferRequest.FromAddress = AwardWalletAddress;
                            objTransferRequest.ClientID = short.Parse(ClientID);
                            objTransferRequest.SystemID = short.Parse(SystemID);
                            objTransferRequest.Amount = awardFee;
                            objTransferRequest.Description = "Payment fee Withdraw";
                            objTransferRequest.ToClientId = short.Parse(ClientID);
                            objTransferRequest.ToSystemId = short.Parse(SystemID);
                            objTransferRequest.ToUserId = FeePrizeID;
                            objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                            objTransferRequest.CreatedBy = RequestBy;

                            TransactionRepository objTransactionRepository = new TransactionRepository();
                            TransactionInfo objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                            // BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer(objTransferRequest 1");
                            if (objTransactionInfo != null)
                            {
                                if (objTransactionInfo.Amount == -1)
                                {
                                    objResult.MessageDetail = "Failse";
                                }
                                else
                                {
                                    decimal totalAmount = amount - awardFee;

                                    objTransferRequest.UserId = AwardWalletID;
                                    objTransferRequest.FromAddress = AwardWalletAddress;
                                    objTransferRequest.ClientID = short.Parse(ClientID);
                                    objTransferRequest.SystemID = short.Parse(SystemID);
                                    objTransferRequest.Amount = totalAmount;
                                    objTransferRequest.Description = "Total Amount Receive";
                                    objTransferRequest.ToClientId = short.Parse(ClientID);
                                    objTransferRequest.ToSystemId = short.Parse(SystemID);
                                    objTransferRequest.ToUserId = RDUser.Email;
                                    objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                                    objTransferRequest.CreatedBy = RequestBy;

                                    objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                                    // BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer: 2, ");
                                    if (objTransactionInfo != null)
                                    {
                                        IPConfigBO objConfig = new IPConfigBO();
                                        objConfig.IPAddress = Common.GetLocalIPAddress();
                                        objConfig.ServiceDomain = Common.GetHostServices();
                                        objConfig.UserName = (string)Session[SessionKey.EMAIL];
                                        string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                                        ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                                        objRsa.UserName = objConfig.UserName;
                                        objResult = repositoryCommon.CheckAuthenticate(objRsa);

                                        if (!objResult.IsError)
                                        {
                                            objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                                            if (objRsa.Token != null && objRsa.Token != string.Empty)
                                            {
                                                AwardWithdrawBO objAward = new AwardWithdrawBO();
                                                objAward.TransactionID = objTransactionInfo.TransactionId.ToString();

                                                objAward.BookingID = bookingID;
                                                objAward.ValuesWithdraw = amount;
                                                objAward.RequestWalletAddress = addressWallet;
                                                objAward.RequestMemberID = Session[SessionKey.EMAIL].ToString();
                                                objAward.RequestDate = DateTime.Now.ToUniversalTime();
                                                objAward.RequestStatus = 1;
                                                objAward.AwardDate = DateTime.Parse(oppenDateAward);
                                                objAward.CoinIDWithdraw = Bitcoin;
                                                objAward.IsDeleted = 0;

                                                string strParameterUpdate = Algorithm.EncryptionObjectRSA<AwardWithdrawBO>(objAward);
                                                ModelRSA objRsa2 = new ModelRSA(strParameterUpdate);
                                                objRsa2.Token = objRsa.Token;
                                                objRsa2.UserName = objConfig.UserName;
                                                objResult = repository.InsertAwardWithdraw(objAward);
                                                if (!objResult.IsError)
                                                {
                                                    objResult.MessageDetail = "InsertSuccess";
                                                    objRedisCache.RemoveCache(Common.KeyAwardWithdrawDetail + (string)Session[SessionKey.EMAIL] + "_" + bookingID);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        objResult.MessageDetail = "InsertFailse";
                                    }
                                }
                            }
                        }
                        catch (Exception objEx)
                        {
                            WriteErrorLog.WriteLogsToFileText("Member-->InsertAwardJactpot - Exception : " + objEx.Message);
                            return Json(new { isSuccess = "Failse", messageError = objEx.Message }, JsonRequestBehavior.AllowGet);
                        }
                        //}
                        //else
                        //{
                        //    objResult.MessageDetail = "InsertFailse";
                        //}
                    }
                    else
                    {
                        try
                        {
                            objTransferRequest.UserId = AwardWalletID;
                            objTransferRequest.FromAddress = AwardWalletAddress;
                            objTransferRequest.ClientID = short.Parse(ClientID);
                            objTransferRequest.SystemID = short.Parse(SystemID);
                            objTransferRequest.Amount = awardFee;
                            objTransferRequest.Description = "Payment fee Withdraw";
                            objTransferRequest.ToClientId = short.Parse(ClientID);
                            objTransferRequest.ToSystemId = short.Parse(SystemID);
                            objTransferRequest.ToUserId = FeePrizeID;
                            objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                            objTransferRequest.CreatedBy = RequestBy;

                            TransactionRepository objTransactionRepository = new TransactionRepository();
                            TransactionInfo objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                            //BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer(objTransferRequest.3: " + (objTransactionInfo != null ? objTransactionInfo.TransactionId.ToString() : " null"));
                            if (objTransactionInfo != null)
                            {
                                if (objTransactionInfo.Amount == -1)
                                {
                                    objResult.MessageDetail = "Failse";
                                }
                                else
                                {
                                    decimal totalAmount = amount - awardFee;

                                    objTransferRequest.UserId = AwardWalletID;
                                    objTransferRequest.FromAddress = AwardWalletAddress;
                                    objTransferRequest.ClientID = short.Parse(ClientID);
                                    objTransferRequest.SystemID = short.Parse(SystemID);
                                    objTransferRequest.Amount = totalAmount;
                                    objTransferRequest.Description = "Total Amount Receive";
                                    objTransferRequest.ToClientId = short.Parse(ClientID);
                                    objTransferRequest.ToSystemId = short.Parse(SystemID);
                                    objTransferRequest.ToUserId = RDUser.Email;
                                    objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                                    objTransferRequest.CreatedBy = RequestBy;

                                    objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                                    if (objTransactionInfo != null)
                                    {
                                        IPConfigBO objConfig = new IPConfigBO();
                                        objConfig.IPAddress = Common.GetLocalIPAddress();
                                        objConfig.ServiceDomain = Common.GetHostServices();
                                        objConfig.UserName = (string)Session[SessionKey.EMAIL];
                                        string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                                        ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                                        objRsa.UserName = objConfig.UserName;
                                        objResult = repositoryCommon.CheckAuthenticate(objRsa);

                                        if (!objResult.IsError)
                                        {
                                            objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                                            if (objRsa.Token != null && objRsa.Token != string.Empty)
                                            {
                                                AwardWithdrawBO objAward = new AwardWithdrawBO();
                                                objAward.TransactionID = objTransactionInfo.TransactionId.ToString();
                                                objAward.BookingID = bookingID;
                                                objAward.ValuesWithdraw = amount;
                                                objAward.RequestWalletAddress = addressWallet;
                                                objAward.RequestMemberID = Session[SessionKey.EMAIL].ToString();
                                                objAward.RequestDate = DateTime.Now.ToUniversalTime();
                                                objAward.RequestStatus = 1;
                                                objAward.AwardDate = DateTime.Parse(oppenDateAward);
                                                objAward.CoinIDWithdraw = Bitcoin;
                                                objAward.IsDeleted = 0;

                                                string strParameterUpdate = Algorithm.EncryptionObjectRSA<AwardWithdrawBO>(objAward);
                                                ModelRSA objRsa2 = new ModelRSA(strParameterUpdate);
                                                objRsa2.Token = objRsa.Token;
                                                objRsa2.UserName = objConfig.UserName;
                                                objResult = repository.InsertAwardWithdraw(objAward);

                                                // BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer: 4, ");
                                                if (!objResult.IsError)
                                                {
                                                    objResult.MessageDetail = "InsertSuccess";
                                                    objRedisCache.RemoveCache(Common.KeyAwardWithdrawDetail + (string)Session[SessionKey.EMAIL] + "_" + bookingID);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        objResult.MessageDetail = "InsertFailse";
                                    }
                                }
                            }
                        }
                        catch (Exception objEx)
                        {
                            WriteErrorLog.WriteLogsToFileText("Member-->InsertAwardAward - Exception : " + objEx.Message);
                            return Json(new { isSuccess = "Failse", messageError = objEx.Message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            return Json(new { objResult = objResult.MessageDetail }, JsonRequestBehavior.AllowGet);
        }
        private bool ChecklBookID(int BookingID, decimal awardvalue, decimal awardfee, int proprity, int intDrawID, int intMemberID)
        {
            bool result = false;
            try
            {
                TiketWinningBO objTiketWinningBO = ticketWinningServices.GetListTicketWinningByBookingID(BookingID, awardvalue, awardfee, proprity, intDrawID, intMemberID);
                if (objTiketWinningBO != null && objTiketWinningBO.BookingID == BookingID)
                {
                    result = true;
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }
        public ActionResult ChangePassword()
        {
            return View();
        }

        public PartialViewResult _PartialViewRegister()
        {
            RegisterViewModel objRegisterViewModel = new RegisterViewModel();
            try
            {

            }
            catch
            {

            }
            return PartialView("_PartialViewRegister", objRegisterViewModel);
        }

        [HttpPost]
        public JsonResult MemberRegister(RegisterViewModel objRegisterViewModel)
        {
            int intMemberID = 0;
            bool isSuccess = false;
            bool isExistEmail = false;
            try
            {
                if (ModelState.IsValid)
                {
                    string strCode = Utility.GenCode();
                    string tick = DateTime.Now.Ticks.ToString();
                    string keyCode = Utility.MaHoaMD5(strCode + tick);

                    string linkreference = objRegisterViewModel.Email + "/" + Utility.MaHoaMD5(strCode + tick + objRegisterViewModel.Email);
                    MemberBO member = new MemberBO();
                    member.IsActive = 0;
                    member.IsDelete = 0;
                    member.Password = Utility.MaHoaMD5(objRegisterViewModel.Password);
                    member.Points = 0;
                    member.CreateDate = DateTime.Now;
                    member.Email = objRegisterViewModel.Email;
                    member.FullName = objRegisterViewModel.FirtName + " " + objRegisterViewModel.LastName;
                    member.Gender = 1;
                    member.Birthday = DateTime.Parse("01/01/1990");
                    member.Avatar = string.Empty;
                    member.Mobile = "";
                    member.LinkLogin = objRegisterViewModel.Email + "/" + keyCode;
                    member.NumberTicketFree = 0;
                    member.LinkReference = linkreference;
                    bool checkEmailExists = services.CheckEmailExists(objRegisterViewModel.Email);
                    if (checkEmailExists)
                    {
                        isExistEmail = true;
                    }
                    else
                    {
                        bool rs = services.InsertMember(member, ref intMemberID);
                        if (rs)
                        {
                            
                            AccountCreationRequest objAccountCreationRequest = new AccountCreationRequest();
                            objAccountCreationRequest.ClientID = short.Parse(ClientID);
                            objAccountCreationRequest.Email = member.Email;
                            objAccountCreationRequest.Name = member.Email;
                            objAccountCreationRequest.PhoneNumber = "";
                            objAccountCreationRequest.RequestedBy = RequestBy;
                            objAccountCreationRequest.SystemID = short.Parse(SystemID);
                            objAccountCreationRequest.UserID = member.Email;
                            objAccountCreationRequest.WalletID = long.Parse(WalletID);
                            //objAccountCreationRequest.CurrencyCodes = lstCurrencyCode;

                            AccountInfo objAccountInfo = new AccountInfo();
                            AccountRepository objAccountRepository = new AccountRepository();
                            objAccountInfo = objAccountRepository.Create(objAccountCreationRequest);

                            if (objAccountInfo != null && objAccountInfo.WalletID != 0)
                            {
                                isSuccess = true;
                                bool rsSendMail = SentMailServicesModels.WSSentMail.SendMailByLotteryCarcoin(objRegisterViewModel.Email, keyCode);

                            }
                            else
                            {
                                AccountInfoRequest objAccountInfoRequest = new AccountInfoRequest();
                                objAccountInfoRequest.ClientID = short.Parse(ClientID);
                                objAccountInfoRequest.SystemID = short.Parse(SystemID);
                                objAccountInfoRequest.UserID = member.Email;

                                objAccountRepository = new AccountRepository();
                                objAccountInfo = objAccountRepository.Get(objAccountInfoRequest);
                                if (objAccountInfo != null && objAccountInfo.WalletID != 0)
                                {
                                    isSuccess = true;
                                    bool rsSendMail = SentMailServicesModels.WSSentMail.SendMailByLotteryCarcoin(objRegisterViewModel.Email, keyCode);
                                }
                            }

                            //Create add to BOS carcoin
                            try
                            {
                                var client = new RestClient(ApiMyCarCoin);

                                var request = new RestRequest("/create-user", Method.POST);
                                request.AddParameter("api_key", HashToken);
                                request.AddParameter("referral_email", EmailDefault);
                                request.AddParameter("email", objRegisterViewModel.Email);

                                IRestResponse<ContentResponse> response = client.Execute<ContentResponse>(request);
                                string result = response.Data.result;
                                if (result == "0")
                                {
                                    WriteErrorLog.WriteLogsToFileText("Call api BOS :Error" + response.Data.error_msg);
                                }

                            }
                            catch (Exception ex)
                            {
                                WriteErrorLog.WriteLogsToFileText("call api BOS :create_user" + ex.Message.Trim());
                            }
                        }
                        if (!isSuccess)
                        {
                            services.DeleteMember(intMemberID);
                        }
                        else
                        {
                            double PointValue = 1;
                            IEnumerable<TicketConfigBO> lstTicketConfigBO = null;
                            if (lstTicketConfigBO == null || lstTicketConfigBO.Count() == 0)
                            {
                                lstTicketConfigBO = ticketConfigServices.GetListTicketConfig();
                            }

                            if (lstTicketConfigBO != null && lstTicketConfigBO.Count() > 0)
                            {
                                foreach (var item in lstTicketConfigBO)
                                {
                                    if (item.CoinID.Trim() == "BTC")
                                    {
                                        PointValue = item.CoinValues / item.NumberTicket;
                                    }
                                }
                            }

                            decimal AmountTicketFree = decimal.Parse((int.Parse(NumberTicketFree) * PointValue).ToString());

                            MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                            objMemberReferenceBO.MemberID = intMemberID;
                            objMemberReferenceBO.MemberIDReference = intMemberID;
                            objMemberReferenceBO.LinkReference = linkreference;
                            objMemberReferenceBO.LockID = "";
                            objMemberReferenceBO.Status = 0;
                            objMemberReferenceBO.Amount = AmountTicketFree;

                            services.InsertMemberReference(objMemberReferenceBO);
                            if (objRegisterViewModel.LinkReference != "-1" && !string.IsNullOrEmpty(objRegisterViewModel.LinkReference))
                            {
                                try
                                {
                                    int memberID = 1;
                                    string link_ = string.Empty;
                                    string linkref = objRegisterViewModel.LinkReference;
                                    string[] array = linkref.Split('/');
                                    if (array.Length > 0)
                                    {
                                        string strEmail = array[0];
                                        MemberBO obj = services.GetMemberDetailByEmail(strEmail);
                                        if (obj != null && obj.MemberID != 0)
                                        {
                                            memberID = obj.MemberID;
                                            link_ = obj.LinkReference;
                                        }
                                    }
                                    
                                    //Update free ticket for member reference
                                    objMemberReferenceBO = new MemberReferenceBO();
                                    objMemberReferenceBO.MemberID = memberID;
                                    objMemberReferenceBO.MemberIDReference = intMemberID;
                                    objMemberReferenceBO.LinkReference = link_;
                                    objMemberReferenceBO.LockID = "";
                                    objMemberReferenceBO.Status = 0;
                                    objMemberReferenceBO.Amount = AmountTicketFree;
                                    services.InsertMemberReference(objMemberReferenceBO);
                                }
                                catch
                                {
                                    Response.Redirect("/error");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception objEx)
            {
                if (intMemberID != 0)
                {
                    services.DeleteMember(intMemberID);
                }
                WriteErrorLog.WriteLogsToFileText("MemberController-->MemberRegister - Exception : " + objEx.Message);
                return Json(new { success = false, ExistEmail = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = isSuccess, ExistEmail = isExistEmail }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _PartialViewRecoverPassword()
        {
            RecoverPasswordViewModel objRecoverPasswordViewModel = new RecoverPasswordViewModel();
            try
            {

            }
            catch
            {

            }
            return PartialView("_PartialViewRecoverPassword", objRecoverPasswordViewModel);
        }

        [HttpPost]
        public JsonResult SubmitRecoverPassword(RecoverPasswordViewModel objRecoverPasswordViewModel)
        {
            bool isSuccess = false;
            bool isExists = true;
            try
            {
                if (ModelState.IsValid)
                {
                    bool checkEmailExists = services.CheckEmailExists(objRecoverPasswordViewModel.EmailRecoverassword);
                    if (checkEmailExists)
                    {
                        string strCode = Utility.GenCode();
                        string tick = DateTime.Now.Ticks.ToString();
                        string keyCode = Utility.MaHoaMD5(strCode + tick);
                        IEnumerable<LinkResetPassword> lstLinkResetPassword = services.GetListLinkResetPassword(DateTime.Now, objRecoverPasswordViewModel.EmailRecoverassword);
                        int countSendMail = 0;
                        if (lstLinkResetPassword != null)
                        {
                            countSendMail = lstLinkResetPassword.Count();
                        }
                        if (countSendMail < int.Parse(MaxCountSendMail))
                        {
                            bool rsSendMail = SentMailServicesModels.WSSentMail.SendResetPasswprdByLotteryCarcoin(objRecoverPasswordViewModel.EmailRecoverassword, keyCode);
                            //if (rsSendMail)
                            //{
                            LinkResetPassword objLinkResetPassword = new LinkResetPassword();
                            objLinkResetPassword.Email = objRecoverPasswordViewModel.EmailRecoverassword;
                            objLinkResetPassword.ExpireLink = DateTime.Now.AddMinutes(int.Parse(TimeExpired));
                            objLinkResetPassword.IPAddress = "";
                            objLinkResetPassword.LinkReset = "/email=" + objRecoverPasswordViewModel.EmailRecoverassword + "&&key=" + keyCode;
                            bool result = services.InsertLinkResetPassword(objLinkResetPassword);
                            isSuccess = true;
                            //}
                        }
                        else
                        {
                            return Json(new { success = false, ismaxsendmail = int.Parse(MaxCountSendMail), exists = isExists }, JsonRequestBehavior.AllowGet);
                        }
                        isSuccess = true;
                    }
                    else
                    {
                        isExists = false;
                    }
                }
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("MemberController-->SubmitRecoverPassword - Exception : " + objEx.Message);
                return Json(new { success = false, ismaxsendmail = 0, exists = isExists }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = isSuccess, ismaxsendmail = 0, exists = isExists }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RessetPassword(string email, string key)
        {
            try
            {
                bool bolIsResetPassword = false;
                LinkResetPassword objLinkResetPassword = new LinkResetPassword();
                objLinkResetPassword = services.GetDetailLinkResetPassword("/email=" + email + "&&key=" + key + "");
                if (objLinkResetPassword != null && !string.IsNullOrEmpty(objLinkResetPassword.LinkReset))
                {
                    bolIsResetPassword = services.ResetPasswordMember(email);
                }
                else
                {
                    return Redirect("/link-expired");
                }
                if (bolIsResetPassword)
                {
                    MemberBO objMemberBO = new MemberBO();
                    objMemberBO = services.GetMemberDetailByEmail(email);
                    if (objMemberBO != null)
                    {
                        string strBTCAddress = string.Empty;
                        string strCarcoinAddress = string.Empty;
                        decimal numberCoinBTCAvailable = 0;
                        decimal numberCarcoinAvailable = 0;
                        //get address btc
                        AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                        objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                        objAddressInfoRequest.UserID = objMemberBO.Email;
                        objAddressInfoRequest.ClientID = short.Parse(ClientID);
                        objAddressInfoRequest.SystemID = short.Parse(SystemID);
                        objAddressInfoRequest.IsCreateAddr = false;
                        objAddressInfoRequest.CreatedBy = objMemberBO.Email;
                        objAddressInfoRequest.WalletId = short.Parse(WalletID);

                        AddressRepository objAddressRepository = new AddressRepository();
                        AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                        if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                        {
                            strBTCAddress = objAddressInfo.AddressId;
                            numberCoinBTCAvailable = objAddressInfo.Available;

                        }

                        int isSetSession = -1;
                        if (!string.IsNullOrEmpty(strBTCAddress))
                        {
                            isSetSession = SetSessionLogin(objMemberBO, strBTCAddress, strCarcoinAddress, numberCoinBTCAvailable, numberCarcoinAvailable);
                        }
                        if (isSetSession == 0)
                        {
                            return Redirect("/change-password");
                        }
                        else
                        {
                            return Redirect("/error");
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return Redirect("/error");
            }

            return View();
        }
        private int SetSessionLogin(MemberBO objMember, string strBTCAddress, string strCarcoinAddress, decimal douCoinBTCAvailable, decimal douCarcoinAvailable)
        {
            try
            {
                BBC.CWallet.Share.BBCLoggerManager.Info("SetSessionLogin: " + objMember);

                if (objMember != null)
                {
                    if (Session[SessionKey.USERNAME] == null)
                    {

                        Session[SessionKey.USERNAME] = objMember.Email;

                    }
                    System.Web.HttpContext.Current.Session[SessionKey.MEMBERID] = objMember.MemberID;
                    System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] = objMember;
                    System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = douCoinBTCAvailable;
                    System.Web.HttpContext.Current.Session[SessionKey.CARCOINPOINT] = douCarcoinAvailable;
                    System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = douCarcoinAvailable;
                    System.Web.HttpContext.Current.Session[SessionKey.EMAIL] = objMember.Email;
                    System.Web.HttpContext.Current.Session[SessionKey.BTCADDRESS] = strBTCAddress;
                    System.Web.HttpContext.Current.Session[SessionKey.CARCOINADDRESS] = strCarcoinAddress;
                    System.Web.HttpContext.Current.Session[SessionKey.LinkReference] = objMember.Email;/* Utility.EncryptText(objMember.LinkReference, KeyCodeSHA);*/
                    System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKETFREE] = objMember.NumberTicketFree;
                    string checkIsEmptyPass = "true";
                    if (string.IsNullOrEmpty(objMember.Password))
                    {
                        checkIsEmptyPass = "false";
                    }
                    System.Web.HttpContext.Current.Session[SessionKey.ISPASSWORD] = checkIsEmptyPass;
                    return 0;
                }

                BBC.CWallet.Share.BBCLoggerManager.Info("SetSessionLogin -> System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION]: " + System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION]);
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText(objEx.Message);
            }
            return 1;//isResult = 1: Error login fail
        }

        public PartialViewResult _PartialViewLogin()
        {
            LoginViewModel objLoginViewModel = new LoginViewModel();
            try
            {

            }
            catch
            {

            }
            return PartialView("_PartialViewLogin", objLoginViewModel);
        }
        [HttpPost]
        public JsonResult SubmitLoginMember(LoginViewModel objLoginViewModel)
        {
            bool isSuccess = false;
            bool isLogin = true;
            try
            {
                if (ModelState.IsValid)
                {
                    MemberBO member = services.LoginMember(objLoginViewModel.EmailLogin, Utility.MaHoaMD5(objLoginViewModel.PasswordLogin));
                    if (member != null)
                    {
                        //Set remember me 
                        if (objLoginViewModel.IsRemeberme)
                        {
                            System.Web.HttpCookie cookie = new System.Web.HttpCookie("LotteryCookiesLogin");
                            cookie.Values.Add("Email", member.Email);
                            cookie.Values.Add("Password", member.Password);
                            cookie.Expires = DateTime.Now.AddDays(int.Parse(TimeDaysCookies));
                            Response.Cookies.Add(cookie);
                        }
                        string strBTCAddress = string.Empty;
                        string strCarcoinAddress = string.Empty;
                        decimal numberCoinBTCAvailable = 0;
                        decimal numberCarcoinAvailable = 0;
                        //get address btc
                        AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                        objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                        objAddressInfoRequest.UserID = member.Email;
                        objAddressInfoRequest.ClientID = short.Parse(ClientID);
                        objAddressInfoRequest.SystemID = short.Parse(SystemID);
                        objAddressInfoRequest.IsCreateAddr = true;
                        objAddressInfoRequest.CreatedBy = member.Email;
                        objAddressInfoRequest.WalletId = short.Parse(WalletID);

                        AddressRepository objAddressRepository = new AddressRepository();
                        AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                        if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                        {
                            strBTCAddress = objAddressInfo.AddressId;
                            numberCoinBTCAvailable = objAddressInfo.Available;

                        }

                        BBC.CWallet.Share.BBCLoggerManager.Info("!string.IsNullOrEmpty(strBTCAddress) || !string.IsNullOrEmpty(strCarcoinAddress): " + (!string.IsNullOrEmpty(strBTCAddress) || !string.IsNullOrEmpty(strCarcoinAddress) ? true : false));

                        int isSetSession = -1;
                        if (!string.IsNullOrEmpty(strBTCAddress) || !string.IsNullOrEmpty(strCarcoinAddress))
                        {
                            isSetSession = SetSessionLogin(member, strBTCAddress, strCarcoinAddress, numberCoinBTCAvailable, numberCarcoinAvailable);
                        }
                        if (isSetSession == 0)
                        {
                            isSuccess = true;
                        }

                    }
                    else
                    {
                        isLogin = false;
                    }
                }
            }
            catch (Exception objEx)
            {
                isSuccess = false;
                WriteErrorLog.WriteLogsToFileText("MemberController-->SubmitRecoverPassword - Exception : " + objEx.Message);
            }
            return Json(new { success = isSuccess, login = isLogin }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LinkExpired()
        {
            return View();
        }

        public PartialViewResult _PartialUpdatePassword()
        {
            UpdatePasswordViewModel objUpdatePasswordViewModel = new UpdatePasswordViewModel();
            try
            {

            }
            catch
            {

            }
            return PartialView("_PartialUpdatePassword", objUpdatePasswordViewModel);
        }

        [HttpPost]
        public JsonResult UpdatePassword(UpdatePasswordViewModel objUpdatePasswordViewModel)
        {
            bool isSuccess = true;
            try
            {
                if (ModelState.IsValid)
                {
                    string passmd5 = BBH.Lotte.Shared.Utility.MaHoaMD5(objUpdatePasswordViewModel.PasswordUpdate);
                    bool rs = services.UpdatePasswordMember(objUpdatePasswordViewModel.EmailUpdatePass, passmd5);
                    if (rs)
                    {
                        //Get price ticket
                        IEnumerable<TicketConfigBO> lstTicketConfigBO = ticketConfigServices.GetListTicketConfig();
                        double coinValues = 0;
                        if (lstTicketConfigBO != null && lstTicketConfigBO.Count() > 0)
                        {
                            foreach (var item in lstTicketConfigBO)
                            {
                                if (item.CoinID.Trim() == "BTC")
                                {
                                    coinValues = item.CoinValues / item.NumberTicket;
                                }
                            }
                        }
                        double amountFreeTicket = 0;

                        amountFreeTicket = int.Parse(NumberTicketFree) * coinValues;
                        //Call block address wallet free
                        TransactionLockRequest objTransactionLockRequest = new TransactionLockRequest();
                        objTransactionLockRequest.UserId = FreeWalletID;
                        objTransactionLockRequest.ClientID = short.Parse(ClientID);
                        objTransactionLockRequest.SystemID = short.Parse(SystemID);
                        objTransactionLockRequest.FromAddress = FreeWalletAddress;
                        objTransactionLockRequest.Amount = decimal.Parse(amountFreeTicket.ToString());
                        objTransactionLockRequest.CurrencyCode = CurrencyCode.BTC;
                        objTransactionLockRequest.Reason = "Lock for free ticket";
                        objTransactionLockRequest.CreatedBy = RequestBy;

                        TransactionInfo objTransactionInfo = new TransactionInfo();
                        TransactionRepository objTransactionRepository = new TransactionRepository();

                        objTransactionInfo = objTransactionRepository.Lock(objTransactionLockRequest);

                        if (objTransactionInfo != null && !string.IsNullOrEmpty(objTransactionInfo.TransactionId.ToString()))
                        {
                            //update free tickeet member register success
                            //Get member
                            MemberBO objMember = services.GetMemberDetailByEmail(objUpdatePasswordViewModel.EmailUpdatePass);
                            if (objMember != null && objMember.MemberID != 0)
                            {
                                try
                                {
                                    //services.UpdateFreeTicketMember(objMember.MemberID, 1);
                                    //insert member reference
                                    MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                                    objMemberReferenceBO.MemberID = objMember.MemberID;
                                    objMemberReferenceBO.MemberIDReference = objMember.MemberID;
                                    objMemberReferenceBO.LinkReference = objMember.LinkReference;
                                    objMemberReferenceBO.LockID = objTransactionInfo.TransactionId.ToString();
                                    objMemberReferenceBO.Status = 0;
                                    objMemberReferenceBO.Amount = decimal.Parse(amountFreeTicket.ToString());

                                    services.InsertMemberReference(objMemberReferenceBO);
                                }
                                catch
                                {
                                    isSuccess = false;
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(objUpdatePasswordViewModel.LinkReferenceUpdatePassword) && objUpdatePasswordViewModel.LinkReferenceUpdatePassword != "-1")
                        {
                            try
                            {
                                string strEmail = string.Empty;
                                int memberID = 1;
                                string link_ = string.Empty;
                                string linkref = objUpdatePasswordViewModel.LinkReferenceUpdatePassword; /*Utility.DecryptText(objUpdatePasswordViewModel.LinkReferenceUpdatePassword, KeyCodeSHA);*/
                                string[] array = linkref.Split('/');
                                if (array.Length > 0)
                                {
                                    strEmail = array[0];
                                    MemberBO obj = services.GetMemberDetailByEmail(strEmail);
                                    if (obj != null && obj.MemberID != 0)
                                    {
                                        memberID = obj.MemberID;
                                        link_ = obj.LinkReference;
                                    }
                                }

                                //Call block address wallet free for member reference
                                objTransactionLockRequest = new TransactionLockRequest();
                                objTransactionLockRequest.UserId = FreeWalletID;
                                objTransactionLockRequest.ClientID = short.Parse(ClientID);
                                objTransactionLockRequest.SystemID = short.Parse(SystemID);
                                objTransactionLockRequest.FromAddress = FreeWalletAddress;
                                objTransactionLockRequest.Amount = decimal.Parse(amountFreeTicket.ToString());
                                objTransactionLockRequest.CurrencyCode = CurrencyCode.BTC;
                                objTransactionLockRequest.Reason = "Lock for free ticket";
                                objTransactionLockRequest.CreatedBy = RequestBy;

                                objTransactionInfo = new TransactionInfo();
                                objTransactionRepository = new TransactionRepository();

                                objTransactionInfo = objTransactionRepository.Lock(objTransactionLockRequest);
                                if (objTransactionInfo != null && !string.IsNullOrEmpty(objTransactionInfo.TransactionId.ToString()))
                                {
                                    //Get member
                                    MemberBO objMemberRef = services.GetMemberDetailByEmail(objUpdatePasswordViewModel.EmailUpdatePass);
                                    MemberBO objMember = services.GetMemberDetailByEmail(strEmail);
                                    if (objMember != null && objMember.MemberID != 0)
                                    {
                                        try
                                        {
                                            services.UpdateFreeTicketMember(objMember.MemberID, objMember.NumberTicketFree + int.Parse(NumberTicketFree));
                                            //insert member reference
                                            MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                                            objMemberReferenceBO.MemberID = objMember.MemberID;
                                            objMemberReferenceBO.MemberIDReference = objMemberRef.MemberID;
                                            objMemberReferenceBO.LinkReference = objMember.LinkReference;
                                            objMemberReferenceBO.LockID = objTransactionInfo.TransactionId.ToString();
                                            objMemberReferenceBO.Status = 0;
                                            objMemberReferenceBO.Amount = decimal.Parse(amountFreeTicket.ToString());

                                            services.InsertMemberReference(objMemberReferenceBO);

                                        }
                                        catch
                                        {
                                            isSuccess = false;
                                        }
                                    }
                                }
                            }
                            catch
                            {
                                Response.Redirect("/error");
                            }
                        }

                        MemberBO member = services.GetMemberDetailByEmail(objUpdatePasswordViewModel.EmailUpdatePass);
                        if (member != null && member.MemberID != 0)
                        {
                            string strBTCAddress = string.Empty;
                            string strCarcoinAddress = string.Empty;
                            decimal numberCoinBTCAvailable = 0;
                            decimal numberCarcoinAvailable = 0;

                            //get address btc
                            AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                            objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                            objAddressInfoRequest.UserID = member.Email;
                            objAddressInfoRequest.ClientID = short.Parse(ClientID);
                            objAddressInfoRequest.SystemID = short.Parse(SystemID);
                            objAddressInfoRequest.IsCreateAddr = true;
                            objAddressInfoRequest.CreatedBy = member.Email;
                            objAddressInfoRequest.WalletId = short.Parse(WalletID);

                            AddressRepository objAddressRepository = new AddressRepository();
                            AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                            if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                            {
                                strBTCAddress = objAddressInfo.AddressId;
                                numberCoinBTCAvailable = objAddressInfo.Available;

                                //get coin for test 
                                Tranfer(LoanWalletID, LoanWalletAddress, objAddressInfo.UserId, 1);
                            }

                            SetSessionLogin(member, strBTCAddress, strCarcoinAddress, numberCoinBTCAvailable, numberCarcoinAvailable);
                        }

                    }
                }
                return Json(new { success = isSuccess }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("MemberController-->UpdatePassMember - Exception : " + objEx.Message);
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SentMobile(string email, string mobile, string phonecode)
        {
            // BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer(objTransferRequest.1: ");
            MemberBO member = new MemberBO();
            string isSuccess = string.Empty;
            DateTime dateTime = DateTime.Now;
            if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] == null)
            {
                Response.Redirect("/login");
            }
            else
            {
                if (mobile == string.Empty || mobile.Trim().Length == 0)
                {
                    return Json(new { result = "", mobile = mobile }, JsonRequestBehavior.AllowGet);
                }
                try
                {
                    //   BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer(objTransferRequest.2: ");
                    //member = services.GetMemberDetailByEmail(email);
                    bool checkMobile = false;
                    checkMobile = services.CheckMobileExists(mobile);
                    if (checkMobile)
                    {
                        //isSuccess = "MobileExist";
                        member = services.GetMemberDetailByEmail(email);
                        bool checkMobileEmailDetails = services.CheckMobileMemberExists(mobile, email);
                        if (checkMobileEmailDetails)
                        {
                            if (member.IsIndentifySms == 1)
                            {
                                member.TotalSmsCodeInput += 1;
                                isSuccess = "GetFreeTicketToRecived";
                                // BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer(objTransferRequest.7: ");
                            }
                            else if (member.IsIndentifySms == 0)
                            {
                                if (member.TotalSmsCodeInput < TotalsSMS /*|| member.ExpireSmSCode < dateTime*/)
                                {
                                    member.Email = email;
                                    member.Mobile = mobile;
                                    member.SmsCode = Utility.GenCode();
                                    member.IsIndentifySms = 0;
                                    member.ExpireSmSCode = DateTime.Now.AddHours(ExpieDate);
                                    member.TotalSmsCodeInput += 1;

                                    bool rs = services.UpdateInformationMember(member, email);
                                    if (rs)
                                    {
                                        isSuccess = "UpdateSuccess";
                                        SMSUtility.SentMobileCodeAuto(member.SmsCode, mobile, phonecode);
                                        // BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer(objTransferRequest.3: " );
                                    }
                                    else
                                    {
                                        isSuccess = "Failse";
                                        // BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer(objTransferRequest.4: " );
                                    }
                                }
                                else
                                {
                                    isSuccess = "FullNumberSents";
                                }
                            }
                        }
                        else
                        {
                            isSuccess = "ExitMobile";
                        }
                    }
                    else
                    {
                        member = services.GetMemberDetailByEmail(email);
                        if (member.IsIndentifySms == 1)
                        {
                            member.TotalSmsCodeInput += 1;
                            isSuccess = "GetFreeTicketToRecived";
                        }
                        else if (member.IsIndentifySms == 0)
                        {
                            if (member.TotalSmsCodeInput < TotalsSMS /*|| member.ExpireSmSCode < dateTime*/)
                            {
                                member.Email = email;
                                member.Mobile = mobile;
                                member.SmsCode = Utility.GenCode();
                                member.IsIndentifySms = 0;
                                member.ExpireSmSCode = DateTime.Now.AddHours(ExpieDate);
                                member.TotalSmsCodeInput += 1;

                                bool rs = services.UpdateInformationMember(member, email);

                                if (rs)
                                {
                                    isSuccess = "UpdateSuccess";
                                    SMSUtility.SentMobileCodeAuto(member.SmsCode, mobile, phonecode);
                                    // BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer(objTransferRequest.5: ");
                                }
                                else
                                {
                                    isSuccess = "Failse";
                                    //BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objTransactionRepository.Transfer(objTransferRequest.6: ");
                                }
                            }
                            else
                            {
                                isSuccess = "FullNumberSents";
                            }
                        }
                    }
                    return Json(new { isSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception objEx)
                {
                    WriteErrorLog.WriteLogsToFileText("Member-->SentMobile - Exception : " + objEx.Message);
                    return Json(new { isSuccess = "Failse", messageError = objEx.Message }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { isSuccess = isSuccess }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetFreeTicket(string email, string smsCode)
        {
            bool CheckStoreFreeTicketIsEmpty = true;
            MemberBO member = new MemberBO();
            string isSuccess = string.Empty;
            DateTime dateTime = DateTime.Now;
            FreeTicketBO objFreeTicket = freeTicketServices.GetFreeTicketActive();
            if (objFreeTicket != null && objFreeTicket.Total > 0)
            {
                CheckStoreFreeTicketIsEmpty = false;
            }
            if (!CheckStoreFreeTicketIsEmpty)
            {
                if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] == null)
                {
                    Response.Redirect("/login");
                }
                else
                {
                    if (smsCode == string.Empty || smsCode.Trim().Length == 0)
                    {
                        return Json(new { result = "", mobile = smsCode }, JsonRequestBehavior.AllowGet);
                    }
                    try
                    {
                        member = services.GetMemberDetailByEmail(email);
                        if (member != null)
                        {
                            member.Email = email;
                            //member.SmsCode = smsCode;

                            if (member.ExpireSmSCode < dateTime || member.SmsCode != smsCode || member.IsIndentifySms == 1)
                            {
                                if (member.ExpireSmSCode < dateTime)
                                {
                                    isSuccess = "ExpireDate";
                                }
                                if (member.SmsCode != smsCode)
                                {
                                    isSuccess = "incorrect";
                                }
                                if (member.IsIndentifySms == 1)
                                {
                                    isSuccess = "Getcodebefore";
                                }
                            }
                            else
                            {
                                member.IsIndentifySms = 1;
                                bool rs = services.UpdateInformationMember(member, email);
                                if (rs)
                                {


                                    //Get price ticket
                                    IEnumerable<TicketConfigBO> lstTicketConfigBO = ticketConfigServices.GetListTicketConfig();
                                    double coinValues = 0;
                                    if (lstTicketConfigBO != null && lstTicketConfigBO.Count() > 0)
                                    {
                                        foreach (var item in lstTicketConfigBO)
                                        {
                                            if (item.CoinID.Trim() == "BTC")
                                            {
                                                coinValues = item.CoinValues / item.NumberTicket;
                                            }
                                        }
                                    }
                                    double amountFreeTicket = 0;
                                    amountFreeTicket = int.Parse(NumberTicketFree) * coinValues;
                                    //Call block address wallet free for new member
                                    TransactionLockRequest objTransactionLockRequest = new TransactionLockRequest();
                                    objTransactionLockRequest.UserId = FreeWalletID;
                                    objTransactionLockRequest.ClientID = short.Parse(ClientID);
                                    objTransactionLockRequest.SystemID = short.Parse(SystemID);
                                    objTransactionLockRequest.FromAddress = FreeWalletAddress;
                                    objTransactionLockRequest.Amount = decimal.Parse(amountFreeTicket.ToString());
                                    objTransactionLockRequest.CurrencyCode = CurrencyCode.BTC;
                                    objTransactionLockRequest.Reason = "Lock for free ticket";
                                    objTransactionLockRequest.CreatedBy = RequestBy;

                                    TransactionInfo objTransactionInfo = new TransactionInfo();
                                    TransactionRepository objTransactionRepository = new TransactionRepository();

                                    objTransactionInfo = objTransactionRepository.Lock(objTransactionLockRequest);
                                    if (objTransactionInfo != null && !string.IsNullOrEmpty(objTransactionInfo.TransactionId.ToString()))
                                    {
                                        try
                                        {
                                            services.UpdateFreeTicketMember(member.MemberID, 1);
                                            //insert member reference
                                            MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                                            objMemberReferenceBO = services.GetMemberReferenceByMemberID(member.MemberID);
                                            objMemberReferenceBO.MemberID = member.MemberID;
                                            objMemberReferenceBO.MemberIDReference = member.MemberID;
                                            objMemberReferenceBO.LinkReference = member.LinkReference;
                                            objMemberReferenceBO.LockID = objTransactionInfo.TransactionId.ToString();
                                            objMemberReferenceBO.Status = 0;
                                            objMemberReferenceBO.Amount = decimal.Parse(amountFreeTicket.ToString());
                                            if (objMemberReferenceBO.MemberID == member.MemberID)
                                            {
                                                services.UpdateMemberReference(objMemberReferenceBO);
                                            }
                                            else
                                            {
                                                services.InsertMemberReference(objMemberReferenceBO);
                                            }
                                            isSuccess = "UpdateSuccess";
                                        }
                                        catch (Exception ex)
                                        {
                                            WriteErrorLog.WriteLogsToFileText("MemberController-->UpdateFreeTicketMember - Exception : " + ex.Message);
                                        }
                                    }
                                    //Call block address wallet free for  member reference
                                    List<MemberReferenceBO> lstMemberReferenceBO = services.GetDetailMemberReferenceByIDRef(member.MemberID).ToList();
                                    lstMemberReferenceBO = lstMemberReferenceBO.Where(x => x.LockID == "").ToList();
                                    if (lstMemberReferenceBO != null && lstMemberReferenceBO.Count > 0)
                                    {
                                        objTransactionLockRequest = new TransactionLockRequest();
                                        objTransactionLockRequest.UserId = FreeWalletID;
                                        objTransactionLockRequest.ClientID = short.Parse(ClientID);
                                        objTransactionLockRequest.SystemID = short.Parse(SystemID);
                                        objTransactionLockRequest.FromAddress = FreeWalletAddress;
                                        objTransactionLockRequest.Amount = decimal.Parse(amountFreeTicket.ToString());
                                        objTransactionLockRequest.CurrencyCode = CurrencyCode.BTC;
                                        objTransactionLockRequest.Reason = "Lock for free ticket";
                                        objTransactionLockRequest.CreatedBy = RequestBy;

                                        objTransactionInfo = new TransactionInfo();
                                        objTransactionRepository = new TransactionRepository();

                                        objTransactionInfo = objTransactionRepository.Lock(objTransactionLockRequest);
                                        if (objTransactionInfo != null && !string.IsNullOrEmpty(objTransactionInfo.TransactionId.ToString()))
                                        {
                                            int intmemberID = lstMemberReferenceBO[0].MemberID;
                                            MemberBO objMemberBO_ = services.GetMemberDetailByID(intmemberID);
                                            if (objMemberBO_ != null && objMemberBO_.MemberID != 0)
                                            {
                                                int number = objMemberBO_.NumberTicketFree + int.Parse(NumberTicketFree);
                                                services.UpdateFreeTicketMember(intmemberID, number);
                                            }

                                            //update member reference
                                            MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                                            objMemberReferenceBO.MemberID = intmemberID;
                                            objMemberReferenceBO.MemberIDReference = member.MemberID;
                                            objMemberReferenceBO.LockID = objTransactionInfo.TransactionId.ToString();
                                            objMemberReferenceBO.Status = 0;

                                            services.UpdateMemberReference(objMemberReferenceBO);
                                            isSuccess = "UpdateSuccess";
                                        }

                                    }

                                }
                                else
                                {
                                    isSuccess = "Failse";
                                }

                            }
                        }

                        return Json(new { isSuccess = isSuccess, FreeTickIsZero = CheckStoreFreeTicketIsEmpty }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception objEx)
                    {
                        WriteErrorLog.WriteLogsToFileText("Member-->getfreeticket - Exception : " + objEx.Message);
                        return Json(new { isSuccess = "Failse", FreeTickIsZero = CheckStoreFreeTicketIsEmpty, messageError = objEx.Message }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { isSuccess = isSuccess, FreeTickIsZero = CheckStoreFreeTicketIsEmpty }, JsonRequestBehavior.AllowGet);
        }

        private bool Tranfer(string fromID, string fromAddress, string toID, decimal value)
        {
            try
            {
                TransferRequest objTransferRequest = new TransferRequest();
                objTransferRequest.UserId = fromID;
                objTransferRequest.FromAddress = fromAddress;
                objTransferRequest.ClientID = short.Parse(ClientID);
                objTransferRequest.SystemID = short.Parse(SystemID);
                objTransferRequest.Amount = value;
                objTransferRequest.Description = "Tranfer -> from: " + fromID + " to: " + toID;
                objTransferRequest.ToClientId = short.Parse(ClientID);
                objTransferRequest.ToSystemId = short.Parse(SystemID);
                objTransferRequest.ToUserId = toID;
                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                objTransferRequest.CreatedBy = RequestBy;

                TransactionInfo objTransactionInfo = new TransactionInfo();
                TransactionRepository objTransactionRepository = new TransactionRepository();

                objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                if (objTransactionInfo != null)
                {
                    WriteErrorLog.WriteLogsToFileText("Success LoginController(Tranfer) transactionID: " + objTransactionInfo.TransactionId);
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error LoginController(Tranfer) Message: " + ex.Message);
                return false;
            }
            return false;
        }

        private bool checkAmountBTCJackpot(decimal amount, int intDrawID)
        {
            string strNumberCheck = string.Empty;
            string strExtraNumber = string.Empty;

            bool rs = false;
            //Get jackpot
            decimal jackpot = 0;
            string strJackpotAddress = string.Empty;
            //get address btc
            AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
            objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
            objAddressInfoRequest.UserID = JackPotID;
            objAddressInfoRequest.ClientID = short.Parse(ClientID);
            objAddressInfoRequest.SystemID = short.Parse(SystemID);
            objAddressInfoRequest.IsCreateAddr = false;
            objAddressInfoRequest.CreatedBy = JackPotID;
            objAddressInfoRequest.WalletId = short.Parse(WalletID);

            AddressRepository objAddressRepository = new AddressRepository();
            AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

            if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
            {
                strJackpotAddress = objAddressInfo.AddressId;
                jackpot = objAddressInfo.Available;
            }
            if (GetAwardJackPot(intDrawID) > 0)
            {
                decimal totalJacpot = jackpot / GetAwardJackPot(intDrawID);
                if (amount <= totalJacpot)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return rs;
        }
        [RDUser]
        public ActionResult TicketWinning(string p)
        {
            try
            {

                List<string> lstString = new List<string>();
                int intPageSize = 5, page = 1;
                int start = 0, end = 10;
                ViewBag.TitlePage = "Ticket Winning";
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                try
                {
                    if (p != null && p != "")
                    {
                        page = int.Parse(p);
                    }
                    start = (page - 1) * intPageSize + 1;
                    end = (page * intPageSize);
                }
                catch
                {

                }

                MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                if (member != null)
                {

                    List<string> lstStringHTML = new List<string>();
                    int totalRow = 0;
                    lstStringHTML.Add(GenHTMLRusultTicketWinning_Temp(start, end, page));
                    //lstStringHTML.Add(GenPaging(p));
                    BBH.Lotte.Web.Controllers.PagingController Paging = new PagingController();
                    lstStringHTML.Add(PagingModels.GenPaging(Convert.ToString(p), int.Parse(TempData["TotalRecord"].ToString())));
                    return View(lstStringHTML);
                }
            }
            catch
            {
                ViewBag.ListTicketWinning = null;
            }
            return View();
        }

        public string GenHTMLRusultTicketWinning_Temp(int start, int end, int intpPage)
        {
            try
            {
                StringBuilder strBuilder = new StringBuilder();
                int intPageSize = 10;
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                int count = 1;

                if (intpPage == 1)
                {
                    count = intpPage * count;
                }
                else
                {
                    count = (intpPage * intPageSize - intPageSize) + 1;
                }
                int strTotalRows = 0;

                MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                List<TiketWinningBO> lstTiketWinningBO = new List<TiketWinningBO>();
                if (member != null)
                {
                    lstTiketWinningBO = ticketWinningServices.GetListTicketWinningByMemberID(member.MemberID, start, end).ToList();
                }
                if (lstTiketWinningBO != null && lstTiketWinningBO.Count() > 0)
                {
                    foreach (var item in lstTiketWinningBO)
                    {
                        strBuilder.Append("<tr class=\"info pointer\">" +
                        "<td>" + count + "</td>" +
                        "<td class=\"text-center balls nowrap\">" +
                        "<span class=\"ticketNumber\">" + item.FirstNumberBuy + "</span>" +
                        "<span class=\"ticketNumber\">" + item.SecondNumberBuy + "</span>" +
                        "<span class=\"ticketNumber\">" + item.ThirdNumberBuy + "</span>" +
                        "<span class=\"ticketNumber\">" + item.FourthNumberBuy + "</span>" +
                        "<span class=\"ticketNumber\">" + item.FivethNumberBuy + "</span>" +
                        "<span class=\"ticketNumber\">" + item.ExtraNumberBuy + "</span>" +
                        "</td>" +
                        "<td class=\"text-center\">" + item.AwardDate.ToString("MM/dd/yyyy hh:mm:ss") + " UTC</td>" +
                        "<td class=\"text-center balls nowrap\">" +
                        "<span class=\"ticketNumber " + CheckNumberNormal(item.FirstNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.FirstNumberAward + "</span>" +
                        "<span class=\"ticketNumber " + CheckNumberNormal(item.SecondNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.SecondNumberAward + "</span>" +
                        "<span class=\"ticketNumber " + CheckNumberNormal(item.ThirdNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.ThirdNumberAward + "</span>" +
                        "<span class=\"ticketNumber " + CheckNumberNormal(item.FourthNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.FourthNumberAward + "</span>" +
                        "<span class=\"ticketNumber " + CheckNumberNormal(item.FivethNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.FivethNumberAward + "</span>" +
                        "<span class=\"ticketNumber last-child " + (item.ExtraNumberAward != item.ExtraNumberBuy ? "disable" : "") + "\">" + item.ExtraNumberAward + "</span>" +
                        "</td>" +
                        "<td class=\"text-center balls nowrap\">");
                        for (int i = 0; i < item.NumberBallNormal; i++)
                        {
                            strBuilder.Append("<i class=\"balls-icon\"></i>");
                        }
                        if (item.NumberBallGold > 0)
                        {
                            strBuilder.Append("<i class=\"balls-icon orange\"></i>");
                        }
                        strBuilder.Append("</td>" +
                        "<td class=\"text-center\">" + item.AwardValue + "</td>" +
                        "<td class=\"text-center\" style=\"display: none\">0</td>" +
                        "<td class=\"text-center\" style=\"display: none\">2</td>" +
                        "<td class=\"text-center\">");
                        if (item.AwardWithdrawStatus == -1)
                        {
                            strBuilder.Append("<button class=\"btn btn-sm btn-warning\" data-target=\"#verify-getaward\" data-toggle=\"modal\" onclick=\"ShowBookingWinnerDetail('" + item.BookingID + "', '" + item.AwardValue + "', '" + item.AwardFee + "' ,'" + item.FirstNumberBuy + "', '" + item.SecondNumberBuy + "', '" + item.ThirdNumberBuy + "', '" + item.FourthNumberBuy + "', '" + item.FivethNumberBuy + "', '" + item.ExtraNumberBuy + "','" + item.AwardDate + "','" + RDUser.BTCAddress + "','" + item.Priority + "','" + item.DrawID + "')\">Get award</button>");
                        }
                        else if (item.AwardWithdrawStatus == (int)AwardWithdrawStatus.Recieved)
                        {
                            strBuilder.Append("<button class=\"btn btn-success btn-sm disabled\">Received</button>");
                        }
                        strBuilder.Append("</td>" +
                        "</tr>");
                        count++;
                        strTotalRows = item.TotalRecord;
                    }
                }

                else
                {
                    strBuilder.Append("<tr><td colspan=\"7\" class=\"\">No Result</td></tr>");
                }

                TempData["TotalRecord"] = strTotalRows;
                return strBuilder.ToString();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private string CheckNumberNormal(int numberCheck, int number1, int number2, int number3, int number4, int number5)
        {
            string strCss = "disable";
            if (numberCheck == number1)
            {
                return string.Empty;
            }
            if (numberCheck == number2)
            {
                return string.Empty;
            }
            if (numberCheck == number3)
            {
                return string.Empty;
            }
            if (numberCheck == number4)
            {
                return string.Empty;
            }
            if (numberCheck == number5)
            {
                return string.Empty;
            }
            return strCss;
        }

        [HttpGet]
        public string GenHtmlSearchTicketWinning(string fromDate, string toDate, int page)
        {

            DateTime? fromD = DateTime.Now;
            DateTime? toD = DateTime.Now;
            StringBuilder strBuilder = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            int intPageSize = 10;
            int start = 0, end = 10;
            int totalRecord = 0;
            string json = "";
            DateTime OpenDate = DateTime.Today;
            List<string> lst = null;

            MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
            if (member != null)
            {
                try
                {
                    Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                    if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                    {
                        fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                        toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(fromDate))
                        {
                            if (string.IsNullOrEmpty(toDate))
                            {
                                fromD = null;
                                toD = DateTime.Parse("01/01/1990");
                            }
                            else
                            {
                                fromD = DateTime.Parse("01/01/1990");
                                toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                            }
                        }
                        else
                        {
                            fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                            toD = DateTime.Now;
                        }
                    }

                    start = (page - 1) * intPageSize + 1;
                    end = (page * intPageSize);
                    //MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                    List<TiketWinningBO> lstTiketWinningBO = new List<TiketWinningBO>();
                    if (member != null)
                    {
                        lstTiketWinningBO = ticketWinningServices.GetListTicketWinningBySearch(member.MemberID, start, end, fromD, toD).ToList();
                    }
                    if (lstTiketWinningBO != null && lstTiketWinningBO.Count() > 0)
                    {
                        int count = 1;
                        if (page == 1)
                        {
                            count = page * count;
                        }
                        else
                        {
                            count = (page * intPageSize - intPageSize) + 1;
                        }
                        foreach (var item in lstTiketWinningBO)
                        {
                            strBuilder.Append("<tr class=\"info pointer\">" +
                            "<td>" + count + "</td>" +
                            "<td class=\"text-center balls nowrap\">" +
                            "<span class=\"ticketNumber\">" + item.FirstNumberBuy + "</span>" +
                            "<span class=\"ticketNumber\">" + item.SecondNumberBuy + "</span>" +
                            "<span class=\"ticketNumber\">" + item.ThirdNumberBuy + "</span>" +
                            "<span class=\"ticketNumber\">" + item.FourthNumberBuy + "</span>" +
                            "<span class=\"ticketNumber\">" + item.FivethNumberBuy + "</span>" +
                            "<span class=\"ticketNumber\">" + item.ExtraNumberBuy + "</span>" +
                            "</td>" +
                            "<td class=\"text-center\">" + item.AwardDate.ToString("MM/dd/yyyy hh:mm:ss") + " UTC</td>" +
                            "<td class=\"text-center balls nowrap\">" +
                            "<span class=\"ticketNumber " + CheckNumberNormal(item.FirstNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.FirstNumberAward + "</span>" +
                            "<span class=\"ticketNumber " + CheckNumberNormal(item.SecondNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.SecondNumberAward + "</span>" +
                            "<span class=\"ticketNumber " + CheckNumberNormal(item.ThirdNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.ThirdNumberAward + "</span>" +
                            "<span class=\"ticketNumber " + CheckNumberNormal(item.FourthNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.FourthNumberAward + "</span>" +
                            "<span class=\"ticketNumber " + CheckNumberNormal(item.FivethNumberAward, item.FirstNumberBuy, item.SecondNumberBuy, item.ThirdNumberBuy, item.FourthNumberBuy, item.FivethNumberBuy) + "\">" + item.FivethNumberAward + "</span>" +
                            "<span class=\"ticketNumber last-child " + (item.ExtraNumberAward != item.ExtraNumberBuy ? "disable" : "") + "\">" + item.ExtraNumberAward + "</span>" +
                            "</td>" +
                            "<td class=\"text-center balls nowrap\">");
                            for (int i = 0; i < item.NumberBallNormal; i++)
                            {
                                strBuilder.Append("<i class=\"balls-icon\"></i>");
                            }
                            if (item.NumberBallGold > 0)
                            {
                                strBuilder.Append("<i class=\"balls-icon orange\"></i>");
                            }
                            strBuilder.Append("</td>" +
                            "<td class=\"text-center\">" + item.AwardValue + "</td>" +
                            "<td class=\"text-center\" style=\"display: none\">0</td>" +
                            "<td class=\"text-center\" style=\"display: none\">2</td>" +
                            "<td class=\"text-center\">");
                            if (item.AwardWithdrawStatus == -1)
                            {
                                strBuilder.Append("<button class=\"btn btn-sm btn-warning\" data-target=\"#verify-getaward\" data-toggle=\"modal\" onclick=\"ShowBookingWinnerDetail('" + item.BookingID + "', '" + item.AwardValue + "', '" + item.AwardFee + "' ,'" + item.FirstNumberBuy + "', '" + item.SecondNumberBuy + "', '" + item.ThirdNumberBuy + "', '" + item.FourthNumberBuy + "', '" + item.FivethNumberBuy + "', '" + item.ExtraNumberBuy + "','" + item.AwardDate + "','" + RDUser.BTCAddress + "','" + item.Priority + "','" + item.DrawID + "')\">Get award</button>");
                            }
                            else if (item.AwardWithdrawStatus == (int)AwardWithdrawStatus.Recieved)
                            {
                                strBuilder.Append("<button class=\"btn btn-success btn-sm disabled\">Received</button>");
                            }
                            strBuilder.Append("</td>" +
                            "</tr>");
                            count++;
                            totalRecord = (int)item.TotalRecord;
                        }

                        TempData["TotalRecord"] = (int)totalRecord;


                        string linkPage = "", nextLink = "", previewLink = "", firstLink = "", lastLink = "";
                        firstLink = "?p=1";
                        if (page <= 1)
                        {
                            previewLink = "?p=1";
                        }
                        else
                        {
                            previewLink = "?p=" + (page - 1).ToString();
                        }

                        builderPaging.Append("<nav class=\"text-center\">");
                        builderPaging.Append("<ul id=\"ulPaging\" class=\"pagination\">");

                        int pagerank = 5;
                        int next = 10;
                        int prev = 1;


                        if (TempData["TotalRecord"] != null)
                        {
                            int totalRecore = (int)TempData["TotalRecord"];
                            int totalPage = totalRecore / intPageSize;
                            int balance = totalRecore % intPageSize;
                            if (balance != 0)
                            {
                                totalPage += 1;
                            }
                            if (page >= totalPage)
                            {
                                nextLink = "?p=" + totalPage.ToString();
                            }
                            else
                            {
                                nextLink = "?p=" + (page + 1).ToString();
                            }
                            int currentPage = page;
                            var m = 1;
                            if (totalPage > 10)
                            {
                                if (page > pagerank + 1)
                                {
                                    next = (page + pagerank) - 1;
                                    m = page - pagerank;
                                }
                                if (page <= pagerank)
                                {
                                    prev = (page - pagerank);
                                    m = 1;
                                }
                                if (next > totalPage)
                                {
                                    next = totalPage;
                                }
                                if (prev < 1)
                                {
                                    prev = 1;
                                }
                            }
                            else
                            {
                                next = totalPage;
                                prev = 1;
                            }
                            lastLink = "?p=" + totalPage;
                            if (totalPage > 1)
                            {
                                if (currentPage > 1)
                                {

                                    builderPaging.Append("<li class=\"page-item\">");
                                    builderPaging.Append("<a href=\"" + previewLink + "\" class=\"page-link\" aria-label=\"Previous\">");
                                    builderPaging.Append("<span aria-hidden=\"true\">");
                                    builderPaging.Append("<i class=\"fa fa-angle-double-left\" area-hidden=\"hidden\"></i>");
                                    builderPaging.Append("</span>");
                                    builderPaging.Append("</a>");
                                    builderPaging.Append(" </li>");
                                }
                            }

                            if (totalPage > 1)
                            {

                                for (; m <= next; m++)
                                {
                                    linkPage = "?p=" + m;
                                    if (m == currentPage)
                                    {
                                        builderPaging.Append("<li class=\"page-item active\"><a class=\"page-link\">" + m + "</a></li>");
                                    }
                                    else
                                    {
                                        builderPaging.Append("<li class=\"page-item\"><a href=\"javascript:void(0)\" onclick=\"SearchTransactionBookingWinning('" + m + "')\" class=\"page-link\">" + m + "</a></li>");
                                    }
                                }
                            }

                            if (totalPage > 1)
                            {
                                if (currentPage < totalPage)
                                {
                                    builderPaging.Append("<li class=\"page-item\">");
                                    builderPaging.Append("<a href=\"" + nextLink + "\" class=\"page-link\" aria-label=\"Next\">");
                                    builderPaging.Append("<span aria-hidden=\"true\">");
                                    builderPaging.Append("<i class=\"fa fa-angle-double-right\" area-hidden=\"hidden\"></i>");
                                    builderPaging.Append("</span>");
                                    builderPaging.Append("</a>");
                                    builderPaging.Append("</li>");
                                }
                            }
                        }


                        builderPaging.Append("</ul>");
                        builderPaging.Append("</nav>");
                    }
                    else
                    {
                        strBuilder.Append("<tr><td colspan=\"7\" class=\"\">No Result</td></tr>");
                    }
                }
                catch (Exception ex)
                {
                    WriteErrorLog.WriteLogsToFileText(ex.Message);
                    strBuilder.Append("<tr><td colspan=\"7\">No Result</td></tr>");
                }

            }
            SearchObj obj = new SearchObj();
            obj.ContentResult = strBuilder.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = totalRecord;
            json = JsonConvert.SerializeObject(obj);
            return json;
        }

    }
}
