﻿using BBC.Core.Common.Log;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.Shared;
using BBH.Lotte.CLP.Wallet.Repository;
using BBH.LotteFE.Domain.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.Web.Controllers
{
    public class ValidateController : Controller
    {
        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];
        string AdminID = ConfigurationManager.AppSettings[KeyManager.AdminID];
        string FeeBTC = ConfigurationManager.AppSettings[KeyManager.FeeBTC];
        string FeeCarCoin = ConfigurationManager.AppSettings[KeyManager.FeeCarCoin];

        [Dependency]
        protected IMemberServices memberservices { get; set; }
        // GET: Validate
        public ActionResult Index()
        {
            return View();
        }

        [RDUser]
        public JsonResult CheckAvailableBTC(double AmountBTC)
        {
            try
            {
                string strBTCAddress = string.Empty;
                decimal numberCoinBTCAvailable = 0;
                string str = string.Empty;
                //get address btc
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = RDUser.Email;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = true;
                objAddressInfoRequest.CreatedBy = RDUser.Email;
                objAddressInfoRequest.WalletId = short.Parse(WalletID);

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    strBTCAddress = objAddressInfo.AddressId;
                    numberCoinBTCAvailable = objAddressInfo.Available;
                }

                if (AmountBTC > (double)numberCoinBTCAvailable)
                {
                    str = string.Format("Amount need to be present and less or equal to available balance!");
                    return Json(str, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("ValidateController -> CheckAvailableBTC: " + objEx.Message);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [RDUser]
        public JsonResult CheckTotalBTCWithdraw(double TotalBTC)
        {
            try
            {
                string strBTCAddress = string.Empty;
                decimal numberCoinBTCAvailable = 0;
                double Fee = double.Parse(FeeBTC);
                string str = string.Empty;
                //get address btc
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                objAddressInfoRequest.UserID = RDUser.Email;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = true;
                objAddressInfoRequest.CreatedBy = RDUser.Email;
                objAddressInfoRequest.WalletId = short.Parse(WalletID);

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    strBTCAddress = objAddressInfo.AddressId;
                    numberCoinBTCAvailable = objAddressInfo.Available;
                }

                if ((TotalBTC + Fee) > (double)numberCoinBTCAvailable)
                {
                    str = string.Format("Amount need to be present and less or equal to available balance!");
                    return Json(str, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("ValidateController -> CheckTotalBTCWithdraw: " + objEx.Message);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [RDUser]
        public JsonResult CheckAvailableCarCoin(double AmountCarCoin)
        {
            try
            {
                string strCarCoinAddress = string.Empty;
                decimal numberCoinCarCoinAvailable = 0;
                string str = string.Empty;
                //get address btc
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.Carcoin;
                objAddressInfoRequest.UserID = RDUser.Email;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = true;
                objAddressInfoRequest.CreatedBy = RDUser.Email;
                objAddressInfoRequest.WalletId = short.Parse(WalletID);

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    strCarCoinAddress = objAddressInfo.AddressId;
                    numberCoinCarCoinAvailable = objAddressInfo.Available;
                }

                if (AmountCarCoin > (double)numberCoinCarCoinAvailable)
                {
                    str = string.Format("Amount need to be present and less or equal to available balance!");
                    return Json(str, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("ValidateController -> CheckAvailableCarCoin: " + objEx.Message);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [RDUser]
        public JsonResult CheckTotalCarCoinWithdraw(double TotalCarCoin)
        {
            try
            {
                string strCarCoinAddress = string.Empty;
                decimal numberCoinCarCoinAvailable = 0;
                double Fee = double.Parse(FeeCarCoin);
                string str = string.Empty;
                //get address btc
                AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                objAddressInfoRequest.CurrencyCode = CurrencyCode.Carcoin;
                objAddressInfoRequest.UserID = RDUser.Email;
                objAddressInfoRequest.ClientID = short.Parse(ClientID);
                objAddressInfoRequest.SystemID = short.Parse(SystemID);
                objAddressInfoRequest.IsCreateAddr = true;
                objAddressInfoRequest.CreatedBy = RDUser.Email;
                objAddressInfoRequest.WalletId = short.Parse(WalletID);

                AddressRepository objAddressRepository = new AddressRepository();
                AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                {
                    strCarCoinAddress = objAddressInfo.AddressId;
                    numberCoinCarCoinAvailable = objAddressInfo.Available;
                }

                if ((TotalCarCoin + Fee) > (double)numberCoinCarCoinAvailable)
                {
                    str = string.Format("Amount need to be present and less or equal to available balance!");
                    return Json(str, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("ValidateController -> CheckTotalCarCoinWithdraw: " + objEx.Message);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [RDUser]
        public JsonResult CheckExistEmail(string Email)
        {
            try
            {
                bool checkEmailExists = memberservices.CheckEmailExists(Email);
                string str = string.Empty;
                if (checkEmailExists)
                {
                    str = string.Format("Your email address is already in use.");
                    return Json(str, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("ValidateController -> CheckTotalCarCoinWithdraw: " + objEx.Message);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}