﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.Web.Controllers
{
    public class ResultNumberController : Controller
    {
        // GET: ResultNumber
        public ActionResult Index()
        {
            return View();
        }

        public string CheckResultNumber(string strBlockHash)
        {
            string strHtml = "";
            char[] array = strBlockHash.ToCharArray();
            if (array.Length > 0)
            {
                List<string> lst = new List<string>();
                string str = "";
                int k = 0;
                for (int i = array.Length - 1; i >= 0; i--)
                {
                    str = str + array[i];
                    k++;
                    if (k % 4 == 0)
                    {
                        string number = ReverseNumber(str);
                        k = 0;
                        lst.Add(number);
                        str = "";
                    }
                }
                List<int> lstResultNumber = new List<int>();

                if (lst != null && lst.Count > 0)
                {
                    foreach (var item in lst)
                    {
                        int test = Convert.ToInt32(item, 16);
                        if (lstResultNumber.Count < 5)
                        {
                            test = test % 49;
                            if (!lstResultNumber.Contains(test))
                            {
                                lstResultNumber.Add(test);
                            }
                        }
                        else if (lstResultNumber.Count == 5)
                        {
                            test = test % 26;
                            lstResultNumber.Add(test);
                            break;
                        }
                    }
                }
                foreach (var item3 in lstResultNumber)
                {
                    strHtml += "<li>" + item3 + "</li>";
                }
            }
            return strHtml;
        }
        private string ReverseNumber(string number)
        {
            string str = "";
            char[] array = number.ToCharArray();
            if (array.Length > 0)
            {
                for (int i = array.Length - 1; i >= 0; i--)
                {
                    str = str + array[i];
                }
            }
            return str;
        }
    }
}