﻿using BBC.Core.Common.Cache;
using BBC.Core.Common.Log;
using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.Shared;
using BBH.Lotte.CLP.Wallet.Repository;
using BBH.Lotte.Web.Models;
using BBH.LotteFE.Domain;
using BBH.LotteFE.Domain.Interfaces;
using Microsoft.Practices.Unity;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.Web.Controllers
{
    public class BookingController : Controller
    {
        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];
        string SysAdminID = ConfigurationManager.AppSettings[KeyManager.SysAdminID];
        string FreeWalletID = ConfigurationManager.AppSettings[KeyManager.FreeWalletID];
        string FreeWalletAddress = ConfigurationManager.AppSettings[KeyManager.FreeWalletAddress];
        string DayOpen = ConfigurationManager.AppSettings[KeyManager.DayOpen];
        //api carcoin
        string ApiMyCarCoin = ConfigurationManager.AppSettings[KeyManager.ApiMyCarCoin];
        string HashToken = ConfigurationManager.AppSettings[KeyManager.HashToken];

        public static string pathLog = ConfigurationManager.AppSettings["PathLog"];
        static string fileLog = Path.GetDirectoryName(Path.Combine(pathLog, "Logs"));
        int intTimeClosed = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_CLOSED_COUNT_DOWN]);
        int intTimeOpened = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_OPENED_COUNT_DOWN]);
        RedisCache objRedisCache = new RedisCache();
        [Dependency]
        protected IPointsServices objPointRepository { get; set; }
        [Dependency]
        protected IBookingMegaballServices services { get; set; }
        [Dependency]
        protected IMemberServices memberServices { get; set; }
        [Dependency]
        protected ITicketConfigServices ticketConfigServices { get; set; }
        [Dependency]
        protected ICommonServices commonServices { get; set; }
        [Dependency]
        protected IDrawServices drawServices { get; set; }
        public ActionResult Index()
        {
            if (Session["Language"] == null)
            {
                Session["Language"] = "EN";
            }

            if (System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION] == null)
            {
                Response.Redirect("/login");
            }
            int hour = DateTime.Now.Hour;
            if (hour >= intTimeClosed && hour < intTimeOpened)
            {
                ViewData[KeyManager._CHECK_LOCK_BOOKING] = 1;
            }
            else
            {
                ViewData[KeyManager._CHECK_LOCK_BOOKING] = 0;
            }

            return View();
        }

        [HttpPost]
        public string BookingTicketMember(string ticket, int quantity)
        {
            string result = "";
            return result;
        }

        [HttpPost]
        public JsonResult BookingListTicketMember(string lstTicket, int intNumberMegaballID, double dblJackPot)
        {
            int intIsError = 0; //1: close time, 2: login, 3: Your points not enough, 4: other
            double dblPoint = 0;
            try
            {
                if (!RDUser.IsLogedIn())
                {
                    //chưa login
                    return Json(new
                    {
                        redirectUrl = "/play-game",
                        isRedirect = true
                    });
                }
                if (!RDUser.IsEmptyPass())
                {
                    //chưa login
                    return Json(new
                    {
                        redirectUrl = "/change-password",
                        isRedirect = true
                    });
                }
                dblPoint = RDUser.BTCPoint;
                //if (DateTime.Now.Hour >= intTimeClosed && DateTime.Now.Hour < intTimeOpened)
                if (!CheckTimeBooking())
                {
                    intIsError = 1;
                }
                else
                {
                    DrawBO objDraw = drawServices.GetNewestDrawID();
                    if (objDraw != null && objDraw.DrawID > 0)
                    {
                        MemberBO member = (MemberBO)System.Web.HttpContext.Current.Session[SessionKey.MEMBERINFOMATION];
                        List<BookingMegaball> lstBookingMegaball = new List<BookingMegaball>();
                        List<BookingMegaball> lstBookingMegaball2 = new List<BookingMegaball>();
                        if (member != null)
                        {
                            int intFreeTicket = (int)Session[SessionKey.NUMBERTICKETFREE];
                            double PointValue = 1;
                            float TicketNumber = 1;

                            string strCode = Models.Utilitys.GenCode();
                            string tick = DateTime.Now.ToUniversalTime().Ticks.ToString();
                            string transactionCode = Models.Utilitys.MaHoaMD5(strCode + tick);

                            IEnumerable<TicketConfigBO> lstTicketConfigBO = (IEnumerable<TicketConfigBO>)objRedisCache.GetCache<IEnumerable<TicketConfigBO>>(Common.KeyTicketConfig);
                            if (lstTicketConfigBO == null || lstTicketConfigBO.Count() == 0)
                            {
                                lstTicketConfigBO = ticketConfigServices.GetListTicketConfig();
                                objRedisCache.SetCache<IEnumerable<AwardMegaball>>(Common.KeyTicketConfig, lstTicketConfigBO);
                            }

                            if (lstTicketConfigBO != null && lstTicketConfigBO.Count() > 0)
                            {
                                foreach (var item in lstTicketConfigBO)
                                {
                                    if (item.CoinID.Trim() == "BTC")
                                    {
                                        PointValue = item.CoinValues / item.NumberTicket;
                                    }
                                }
                            }
                            int totalQuantity = 0;
                            if (lstTicket != null && lstTicket != "")
                            {
                                double totalTicket = 0;
                                string[] arrList = lstTicket.Split(',');
                                if (arrList != null & arrList.Length > 0)
                                {
                                    totalTicket = arrList.Length - 1;
                                    int maxLength = 50;
                                    if (arrList.Length < 50)
                                    {
                                        maxLength = arrList.Length;
                                    }
                                    for (int i = 0; i < maxLength; i++)
                                    {
                                        if (arrList[i] != "")
                                        {
                                            double valuePoints = PointValue / TicketNumber;

                                            string[] arr = arrList[i].Split(';');
                                            if (arr != null && arr.Length > 0)
                                            {
                                                int quantity = 1;
                                                BookingMegaball booking = new BookingMegaball();
                                                booking.CreateDate = DateTime.Now.ToUniversalTime();
                                                booking.MemberID = member.MemberID;
                                                booking.FirstNumber = Convert.ToInt32(arr[0]);
                                                booking.SecondNumber = Convert.ToInt32(arr[1]);
                                                booking.ThirdNumber = Convert.ToInt32(arr[2]);
                                                booking.FourthNumber = Convert.ToInt32(arr[3]);
                                                booking.FivethNumber = Convert.ToInt32(arr[4]);
                                                if (!String.IsNullOrEmpty(arr[5]))
                                                {
                                                    booking.ExtraNumber = Convert.ToInt32(arr[5]);
                                                }
                                                booking.ValuePoint = valuePoints;
                                                booking.Quantity = quantity;
                                                booking.Status = 1;
                                                booking.TransactionCode = transactionCode;
                                                booking.Note = "";
                                                booking.NoteEnglish = "";
                                                string hour = ConfigurationManager.AppSettings["OpenHour"];
                                                booking.OpenDate = GetOpenDate();
                                                booking.DrawID = objDraw.DrawID;
                                                booking.IsFreeTicket = intFreeTicket > 0 ? 1 : 0;
                                                lstBookingMegaball.Add(booking);
                                                intFreeTicket--;
                                            }
                                        }
                                    }

                                    if (arrList.Length > 50)
                                    {
                                        for (int i = maxLength; i < arrList.Length; i++)
                                        {
                                            if (arrList[i] != "")
                                            {
                                                double valuePoints = PointValue / TicketNumber;

                                                string[] arr = arrList[i].Split(';');
                                                if (arr != null && arr.Length > 0)
                                                {

                                                    int quantity = 1;
                                                    BookingMegaball booking = new BookingMegaball();
                                                    booking.CreateDate = DateTime.Now.ToUniversalTime();
                                                    booking.MemberID = member.MemberID;
                                                    booking.FirstNumber = Convert.ToInt32(arr[0]);
                                                    booking.SecondNumber = Convert.ToInt32(arr[1]);
                                                    booking.ThirdNumber = Convert.ToInt32(arr[2]);
                                                    booking.FourthNumber = Convert.ToInt32(arr[3]);
                                                    booking.FivethNumber = Convert.ToInt32(arr[4]);
                                                    if (!String.IsNullOrEmpty(arr[5]))
                                                    {
                                                        booking.ExtraNumber = Convert.ToInt32(arr[5]);
                                                    }
                                                    booking.ValuePoint = valuePoints;
                                                    booking.Quantity = quantity;
                                                    booking.Status = 1;
                                                    booking.TransactionCode = transactionCode;
                                                    booking.Note = "";
                                                    booking.NoteEnglish = "";
                                                    booking.OpenDate = GetOpenDate();
                                                    booking.DrawID = objDraw.DrawID;
                                                    booking.IsFreeTicket = intFreeTicket > 0 ? 1 : 0;
                                                    lstBookingMegaball2.Add(booking);
                                                    intFreeTicket--;
                                                }

                                            }
                                        }
                                    }
                                }
                                IPConfigBO objConfig = new IPConfigBO();
                                objConfig.IPAddress = Common.GetLocalIPAddress();
                                objConfig.ServiceDomain = Common.GetHostServices();
                                objConfig.UserName = (string)Session[SessionKey.EMAIL];
                                
                                string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                                ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                                objRsa.UserName = objConfig.UserName;
                                ObjResultMessage objResult = commonServices.CheckAuthenticate(objRsa);
                                if (!objResult.IsError)
                                {
                                    objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                                    if (objRsa.Token != null && objRsa.Token != string.Empty)
                                    {
                                        double totalPoint = 0;
                                        int numberFreeTicket = 0;
                                        int number = 0;
                                        double totalPointFree = 0;

                                        numberFreeTicket = (int)Session[SessionKey.NUMBERTICKETFREE];

                                        totalPoint = (totalTicket * PointValue) / TicketNumber;
                                        totalPointFree = (numberFreeTicket * PointValue) / TicketNumber;
                                        if ((RDUser.BTCPoint + totalPointFree) >= totalPoint)
                                        {

                                            string strBTCAddress = string.Empty;
                                            string strCarcoinAddress = string.Empty;
                                            string strUserID = string.Empty;
                                            string strTransactionTranferID = string.Empty;
                                            decimal numberCoinBTCAvailable = 0;
                                            //get address btc
                                            AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
                                            objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
                                            objAddressInfoRequest.UserID = member.Email;
                                            objAddressInfoRequest.ClientID = short.Parse(ClientID);
                                            objAddressInfoRequest.SystemID = short.Parse(SystemID);
                                            objAddressInfoRequest.IsCreateAddr = true;
                                            objAddressInfoRequest.CreatedBy = member.Email;

                                            AddressRepository objAddressRepository = new AddressRepository();
                                            AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);

                                            if (objAddressInfo != null && objAddressInfo.Status == Status.Active)
                                            {
                                                strUserID = objAddressInfo.UserId;
                                                strBTCAddress = objAddressInfo.AddressId;
                                                numberCoinBTCAvailable = objAddressInfo.Available;

                                            }
                                            //Get list lockid 
                                            TransferRequest objTransferRequest = new TransferRequest();
                                            if (totalPoint <= totalPointFree)
                                            {
                                                List<MemberReferenceBO> lstMemberReferenceBO = new List<MemberReferenceBO>();
                                                lstMemberReferenceBO = memberServices.GetListMemberReferenceByMemberID(member.MemberID).ToList();

                                                List<string> lstTransactionID = new List<string>();
                                                if (lstMemberReferenceBO != null && lstMemberReferenceBO.Count() > 0)
                                                {
                                                    int count = 0;
                                                    foreach (var item in lstMemberReferenceBO)
                                                    {
                                                        if (!string.IsNullOrEmpty(item.LockID) && item.Status == 0)
                                                        {
                                                            if (count < totalTicket)
                                                            {
                                                                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText(count + " < " + totalTicket);

                                                                objTransferRequest = new TransferRequest();
                                                                objTransferRequest.UserId = FreeWalletID;
                                                                objTransferRequest.FromAddress = FreeWalletAddress;
                                                                objTransferRequest.ClientID = short.Parse(ClientID);
                                                                objTransferRequest.SystemID = short.Parse(SystemID);
                                                                objTransferRequest.Amount = item.Amount;
                                                                objTransferRequest.Description = "Payment free ticket lottery";
                                                                objTransferRequest.ToClientId = short.Parse(ClientID);
                                                                objTransferRequest.ToSystemId = short.Parse(SystemID);
                                                                objTransferRequest.ToUserId = SysAdminID;
                                                                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                                                                objTransferRequest.CreatedBy = strUserID;
                                                                objTransferRequest.TransactionId = Guid.Parse(item.LockID);

                                                                TransactionInfo objTransactionInfo = new TransactionInfo();
                                                                TransactionRepository objTransactionRepository = new TransactionRepository();

                                                                objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                                                                MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                                                                objMemberReferenceBO.MemberID = item.MemberID;
                                                                objMemberReferenceBO.MemberIDReference = item.MemberIDReference;
                                                                objMemberReferenceBO.LockID = item.LockID;
                                                                objMemberReferenceBO.Status = 1;

                                                                memberServices.UpdateMemberReference(objMemberReferenceBO);
                                                                count++;
                                                            }
                                                        }
                                                    }
                                                }
                                                //update free ticket
                                                memberServices.UpdateFreeTicketMember(member.MemberID, (numberFreeTicket - int.Parse(totalTicket.ToString())));
                                                number = numberFreeTicket - int.Parse(totalTicket.ToString());
                                            }
                                            else
                                            {
                                                TransactionInfo objTransactionInfo = new TransactionInfo();
                                                TransactionRepository objTransactionRepository = new TransactionRepository();
                                                if (totalPointFree > 0)
                                                {
                                                    List<MemberReferenceBO> lstMemberReferenceBO = new List<MemberReferenceBO>();
                                                    lstMemberReferenceBO = memberServices.GetListMemberReferenceByMemberID(member.MemberID).ToList();

                                                    if (lstMemberReferenceBO != null && lstMemberReferenceBO.Count() > 0)
                                                    {
                                                        int count = 0;
                                                        foreach (var item in lstMemberReferenceBO)
                                                        {
                                                            if (!string.IsNullOrEmpty(item.LockID) && item.Status == 0)
                                                            {
                                                                if (count < totalTicket)
                                                                {
                                                                    objTransferRequest = new TransferRequest();
                                                                    objTransferRequest.UserId = FreeWalletID;
                                                                    objTransferRequest.FromAddress = FreeWalletAddress;
                                                                    objTransferRequest.ClientID = short.Parse(ClientID);
                                                                    objTransferRequest.SystemID = short.Parse(SystemID);
                                                                    objTransferRequest.Amount = item.Amount;
                                                                    objTransferRequest.Description = "Payment free ticket lottery";
                                                                    objTransferRequest.ToClientId = short.Parse(ClientID);
                                                                    objTransferRequest.ToSystemId = short.Parse(SystemID);
                                                                    objTransferRequest.ToUserId = SysAdminID;
                                                                    objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                                                                    objTransferRequest.CreatedBy = strUserID;
                                                                    objTransferRequest.TransactionId = Guid.Parse(item.LockID);

                                                                    objTransactionInfo = new TransactionInfo();
                                                                    objTransactionRepository = new TransactionRepository();

                                                                    objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                                                                    //strTransactionTranferID = objTransactionInfo.TransactionId.ToString();

                                                                    //update status = 1 memberreference
                                                                    MemberReferenceBO objMemberReferenceBO = new MemberReferenceBO();
                                                                    objMemberReferenceBO.MemberID = item.MemberID;
                                                                    objMemberReferenceBO.MemberIDReference = item.MemberIDReference;
                                                                    objMemberReferenceBO.LockID = item.LockID;
                                                                    objMemberReferenceBO.Status = 1;

                                                                    memberServices.UpdateMemberReference(objMemberReferenceBO);
                                                                    count++;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                objTransferRequest = new TransferRequest();
                                                objTransferRequest.UserId = strUserID;
                                                objTransferRequest.FromAddress = strBTCAddress;
                                                objTransferRequest.ClientID = short.Parse(ClientID);
                                                objTransferRequest.SystemID = short.Parse(SystemID);
                                                objTransferRequest.Amount = decimal.Parse((totalPoint - totalPointFree).ToString());
                                                objTransferRequest.Description = "Payment ticket lottery";
                                                objTransferRequest.ToClientId = short.Parse(ClientID);
                                                objTransferRequest.ToSystemId = short.Parse(SystemID);
                                                objTransferRequest.ToUserId = SysAdminID;
                                                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                                                objTransferRequest.CreatedBy = strUserID;

                                                objTransactionInfo = new TransactionInfo();
                                                objTransactionRepository = new TransactionRepository();

                                                objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                                                //update free ticket
                                                memberServices.UpdateFreeTicketMember(member.MemberID, 0);
                                            }
                                            dblJackPot += totalPoint;
                                            string strParameterObj = Algorithm.EncryptionObjectRSA(lstBookingMegaball);
                                            ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                            objRsa2.ParamRSASecond = transactionCode;
                                            objRsa2.Token = objRsa.Token;
                                            objRsa2.UserName = objRsa.UserName;
                                            objResult = services.InsertListBooking(objRsa2, dblPoint, intNumberMegaballID, dblJackPot);

                                            if (lstBookingMegaball2 != null && lstBookingMegaball2.Count() > 0)
                                            {
                                                string strParameterObj2 = Algorithm.EncryptionObjectRSA(lstBookingMegaball2);
                                                ModelRSA objRsa3 = new ModelRSA(strParameterObj2);
                                                objRsa3.ParamRSASecond = transactionCode;
                                                objRsa3.Token = objRsa.Token;
                                                objRsa3.UserName = objRsa.UserName;
                                                objResult = services.InsertListBooking(objRsa3, dblPoint, intNumberMegaballID, dblJackPot);

                                            }
                                            BBC.CWallet.Share.BBCLoggerManager.Info("services.InsertListBooking: " + objResult);
                                            if (!objResult.IsError)
                                            {
                                                System.Web.HttpContext.Current.Session[SessionKey.BTCPOINT] = dblPoint.ToString();
                                                System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKET] = (int)dblPoint;
                                                System.Web.HttpContext.Current.Session[SessionKey.NUMBERTICKETFREE] = number;

                                                //buy ticket success 
                                                //call api update ticket BOS
                                                TicketObjInput objOut = new TicketObjInput();
                                                objOut.id = 0;
                                                objOut.email = RDUser.Email;

                                                if(lstBookingMegaball != null && lstBookingMegaball.Count > 0)
                                                {
                                                    //only get ticket not free
                                                    lstBookingMegaball = lstBookingMegaball.Where(c => c.IsFreeTicket == 0).ToList();
                                                }

                                                if (lstBookingMegaball2 != null && lstBookingMegaball2.Count > 0)
                                                {
                                                    //only get ticket not free
                                                    lstBookingMegaball2 = lstBookingMegaball2.Where(c => c.IsFreeTicket == 0).ToList();
                                                }
                                                
                                                int intTicketNumber = (lstBookingMegaball != null ? lstBookingMegaball.Count : 0) + (lstBookingMegaball2 != null ? lstBookingMegaball2.Count : 0);

                                                BBC.CWallet.Share.BBCLoggerManager.Info("lstBookingMegaball: " + lstBookingMegaball + ", lstBookingMegaball2: " + lstBookingMegaball2 + ", intTicketNumber: " + intTicketNumber);

                                                UpdateTicketBOSApi(objOut, intTicketNumber);
                                            }
                                            else
                                            {
                                                intIsError = 4;
                                            }
                                        }
                                        else
                                        {
                                            intIsError = 3;
                                        }
                                    }
                                    else
                                    {
                                        intIsError = 2;
                                    }
                                }
                                else
                                {
                                    intIsError = 2;
                                }

                            }
                            else
                            {
                                intIsError = 5;
                            }
                        }
                        else
                        {
                            intIsError = 2;
                        }
                    }
                    else
                    {
                        intIsError = 1;
                    }
                }
            }
            catch (Exception objEx)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText(objEx.Message);
                intIsError = 4;
            }
            return Json(new { isError = intIsError, intTimeOpened = intTimeOpened, dblPoint = dblPoint, dblJackPot = dblJackPot, isRedirect = false }, JsonRequestBehavior.AllowGet);
        }

        public void UpdateTicketBOSApi(TicketObjInput objInput, int intTicketNumber)
        {
            List<TicketObjInput> lstTicketInput = new List<TicketObjInput>();

            Task.Run(() =>
            {
                int intTimeCount = 0;
                while (intTimeCount < Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.RepeatTimes]))
                {
                    //only allow resend 3 time if send api failed
                    if (objInput.id == 0)
                    {
                        //if id = 0: insert db lottery
                        DateTime dtmCreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Eastern Standard Time");
                        objInput.id = services.InsertTicketNumberTimeLog(objInput.email, intTicketNumber, dtmCreatedDate);

                        if (objInput.id > 0)
                        {
                            objInput.ticket = Convert.ToString(intTicketNumber);
                            objInput.date_time = dtmCreatedDate.ToString("yyyy/MM/dd HH:mm:ss");//timezone America/NewYork

                            lstTicketInput.Add(objInput);
                        }
                    }

                    if (lstTicketInput != null && lstTicketInput.Count > 0)
                    {
                        //insert db lottery success -> send api update ticket
                        string strInputLog = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(lstTicketInput);
                        BBC.CWallet.Share.BBCLoggerManager.Info("SendUpdateTicketBOSApi -> Input: lstTicketInput: " + strInputLog + ", Times: " + (intTimeCount + 1));
                        ApiUpdateTicketResponse objOut = SendUpdateTicketBOSApi(lstTicketInput);

                        string strDataSuccessLog = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(objOut.sucess_list);
                        BBC.CWallet.Share.BBCLoggerManager.Info("SendUpdateTicketBOSApi -> Output: result: " + objOut.result + ", objOut.sucess_list: " + strDataSuccessLog);

                        if (objOut != null && objOut.result == "1")
                        {
                            //success
                            break;
                        }
                    }
                    intTimeCount++;
                }
            });
        }

        public ApiUpdateTicketResponse SendUpdateTicketBOSApi(List<TicketObjInput> lstInput)
        {
            var client = new RestClient(ApiMyCarCoin);
            //client.Authenticator = new HttpBasicAuthenticator("api_key", password);

            var request = new RestRequest("/update-ticket", Method.POST);
            request.AddParameter("api_key", HashToken);
            //request.AddParameter("second_token", Utility.MaHoaMD5(objMember.Email + HashSalt)); // adds to POS
            string strData = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(lstInput);
            request.AddParameter("data", strData);

            IRestResponse<ApiUpdateTicketResponse> response = client.Execute<ApiUpdateTicketResponse>(request);

            return response.Data;
        }

        [HttpGet]
        public string CountAllMemberBuyTicket(string ticket)
        {
            int count = 0;
            return count.ToString();
        }
        enum Days { Saturday = 7, Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6 };

        private DateTime GetOpenDate()
        {
            DateTime dtNow = DateTime.Now.ToUniversalTime();
            DrawBO objDrawID = drawServices.GetNewestDrawID();
            if (objDrawID != null)
            {
                dtNow = objDrawID.EndDate;
            }
            return dtNow;
        }
        private bool CheckTimeBooking()
        {
            Type weekdays = typeof(Days);
            string dayofWeek = DateTime.Now.ToUniversalTime().DayOfWeek.ToString();
            int intDays = (int)Enum.Parse(weekdays, dayofWeek); ;
            string strDayCheck = DayOpen;//key config
            string[] arrDayCheck = strDayCheck.Split(',');
            int[] arrintDayCheck;
            if (arrDayCheck.Length > 0)
            {
                arrintDayCheck = new int[arrDayCheck.Length];
                for (int j = 0; j < arrDayCheck.Length; j++)
                {
                    arrintDayCheck[j] = (int)Enum.Parse(weekdays, arrDayCheck[j].ToString());
                }
                Array.Sort(arrintDayCheck);

                if (arrintDayCheck.Length > 0)
                {
                    for (int k = 0; k < arrintDayCheck.Length; k++)
                    {
                        if (intDays == arrintDayCheck[k])
                        {
                            int TimeClosedCountDown = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_CLOSED_COUNT_DOWN]);
                            int TimeOpenedCountDown = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_OPENED_COUNT_DOWN]);

                            DateTime dtBooking = DateTime.Now.ToUniversalTime();
                            DateTime dtOpenDate = DateTime.Now.ToUniversalTime().Date.Add(new TimeSpan(TimeOpenedCountDown, 0, 0));
                            DateTime dtClosedDate = DateTime.Now.ToUniversalTime().Date.Add(new TimeSpan(TimeClosedCountDown, 0, 0));

                            DateTime dtBookingTemp = new DateTime(dtBooking.Year, dtBooking.Month, dtBooking.Day, dtBooking.Hour, dtBooking.Minute, dtBooking.Second);

                            DateTime dtOpenDateTemp = new DateTime(dtOpenDate.Year, dtOpenDate.Month, dtOpenDate.Day, dtOpenDate.Hour, dtOpenDate.Minute, dtOpenDate.Second);
                            DateTime dtClosedDateTemp = new DateTime(dtClosedDate.Year, dtClosedDate.Month, dtClosedDate.Day, dtClosedDate.Hour, dtClosedDate.Minute, dtClosedDate.Second);

                            int resultOpen = DateTime.Compare(dtBookingTemp, dtOpenDateTemp);
                            int resultClosed = DateTime.Compare(dtBookingTemp, dtClosedDateTemp);
                            if (resultClosed >= 0 && resultOpen <= 0)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}


