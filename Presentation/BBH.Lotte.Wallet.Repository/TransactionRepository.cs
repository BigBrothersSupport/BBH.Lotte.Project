﻿using BBC.Core.WebService;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBC.CWallet.ServiceListener.ServiceContract.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Wallet.Repository
{
    public class TransactionRepository : WCFClient<ITransactionServices>, ITransactionServices
    {
        public TransactionInfo Deposit(DepositRequest objInput)
        {
            return Proxy.Deposit(objInput);
        }
        public TransactionInfo Get(TransactionInfoRequest objInput)
        {
            return Proxy.Get(objInput);
        }

        public decimal GetTotalAmountForUser(StatisticFilterRequest objInput)
        {
            return Proxy.GetTotalAmountForUser(objInput);
        }

        public CWalletList<TransactionInfo> List(TransactionFilterRequest objInput)
        {
            return Proxy.List(objInput);
        }
        public TransactionInfo Lock(TransactionLockRequest objInput)
        {
            return Proxy.Lock(objInput);
        }
        public TransactionInfo Return(ReturnRequest objInput)
        {
            return Proxy.Return(objInput);
        }
        public TransactionInfo Transfer(TransferRequest objInput)
        {
            return Proxy.Transfer(objInput);
        }
        public TransactionInfo TransferToMasterAddress(TransferToMasterAddressRequest objInput)
        {
            return Proxy.TransferToMasterAddress(objInput);
        }
        public TransactionInfo UnLock(TransactionUnLockRequest objInput)
        {
            return Proxy.UnLock(objInput);
        }

        public TransactionInfo UpdateStatusTransferToMasterAddr(TransactionUpdateRequest objInput)
        {
            return Proxy.UpdateStatusTransferToMasterAddr(objInput);
        }

        public TransactionInfo UpdateTransStatusDeposit(TransactionUpdateRequest objInput)
        {
            return Proxy.UpdateTransStatusDeposit(objInput);
        }

        public TransactionInfo UpdateTransStatusWithdraw(TransactionUpdateRequest objInput)
        {
            return Proxy.UpdateTransStatusWithdraw(objInput);
        }

        public TransactionInfo Withdraw(WithdrawRequest objInput)
        {
            return Proxy.Withdraw(objInput);
        }
       
    }
}
