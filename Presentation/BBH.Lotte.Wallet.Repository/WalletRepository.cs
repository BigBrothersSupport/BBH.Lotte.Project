﻿using BBC.Core.WebService;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBC.CWallet.ServiceListener.ServiceContract.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBH.Lotte.CLP.Wallet.Repository
{
    public class WalletRepository : WCFClient<IWalletServices>, IWalletServices
    {
        public WalletInfo Create(WalletCreationRequest objInput)
        {
            return Proxy.Update(objInput);
        }

        public WalletInfo Get(WalletInfoRequest objInput)
        {
            return Proxy.Get(objInput);
        }

        public CWalletList<WalletInfo> List(SystemFilterRequest objInput)
        {
            return Proxy.List(objInput);
        }

        public CWalletList<WalletEventInfo> ListEventsHistory(WalletEventFilterRequest objInput)
        {
            return Proxy.ListEventsHistory(objInput);
        }

        public WalletEventInfo Lock(WalletLockRequest objInput)
        {
            return Proxy.Lock(objInput);
        }

        public WalletEventInfo UnLock(WalletLockRequest objInput)
        {
            return Proxy.UnLock(objInput);
        }

        public WalletInfo Update(WalletCreationRequest objInput)
        {
            return Proxy.Update(objInput);
        }
    }
}
