﻿using BBC.Core.Database;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;

namespace BBH.Lotte.CLP.CMSAdmin.Models
{
    public class Common
    {
        public const string KeyListAward = "KeyListAward";
        public const string KeyListBooking = "KeyListBooking";
        public const string KeyListTransactionCoin = "KeyListTransactionCoin";
        public const string KeyListAwardBO = "KeyListAwardBO";
        public const string KeyTicketConfig = "KeyTicketConfig";

        public const string KeyAwardWithdraw = "KeyAwardWithdraw";

        public const string KeyAwardWithdrawDetail = "KeyAwardWithdrawDetail_";
        public const string ListCacheBookingMember = "ListCacheBookingMember";
        public static T ParseObjectFromAsync<T>(ObjResultMessage objResult)
        {

            if (objResult == null || objResult.ObjResultData == null)
                return default(T);

            string strJson = JsonConvert.SerializeObject(objResult.ObjResultData);

            return JsonConvert.DeserializeObject<T>(strJson);
        }

        public static string GetLocalIPAddress()
        {
            string ipAddress = "";
            //var host = Dns.GetHostEntry(Dns.GetHostName());
            //foreach (var ip in host.AddressList)
            //{
            //    if (ip.AddressFamily == AddressFamily.InterNetwork)
            //    {
            //        ipAddress = ip.ToString();
            //    }
            //}
            //return ipAddress;

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static string GetHostServices(string serviceName)
        {
            string hostServices = string.Empty;
            hostServices = ConfigurationManager.AppSettings["Host:Webservices"];
            hostServices = hostServices + "/" + serviceName;
            return hostServices;
        }

        public static DateTime ConvertDateTime(string strDate)
        {
            if (strDate == null || strDate == "") return DateTime.Now;
            string[] arr = strDate.Split('/');
            string strValue = string.Empty;
            if (arr != null && arr.Length > 0)
            {
                string d = arr[0];
                string m = arr[1];
                string year = arr[2];
                strValue = m + "/" + d + "/" + year;
            }
            return DateTime.Parse(strValue);
        }
    }
}