﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BBH.Lotte.CLP.CMSAdmin.Models
{
    public class ObjRevenue
    {
        public int Month { get; set; }

        public double BTCValues { get; set; }
    }
}