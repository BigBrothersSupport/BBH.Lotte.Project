﻿using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.CMSAdmin.Models;
using BBH.Lotte.CLP.Wallet.Repository;
using BBH.Lotte.CMSAdmin.Models;
using BBH.Lotte.Domain;
using BBH.Lotte.Domain.Interfaces;
using BBH.Lotte.Shared;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class HomeController : Controller
    {
        //MemberRepository repositoryMember = new MemberRepository();
        [Dependency]
        protected IMemberServices repositoryMember { get; set; }

        [Dependency]
        protected IBookingServices repositoryBooking { get; set; }

        [Dependency]
        protected IBookingMegaballServices repositoryBookingMegaball { get; set; }

        [Dependency]
        protected ITransactionPackageServices repositoryTransactionPackage { get; set; }

        [Dependency]
        protected ITransactionPointsServices repositoryTransactionPoints { get; set; }
       
        [Dependency]
        protected IAwardMegaballServices repositoryAwardMegaball { get; set; }

        [Dependency]
        protected ITransactionCoinServices repositoryCoin { get; set; }

        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }

        [Dependency]
        protected IBookingSummaryServices repositorySummary { get; set; }

        [Dependency]
        protected IAwardServices repositoryAward { get; set; }

        [Dependency]
        protected ITicketConfigServices repositoryTicket { get; set; }

        int groupIDAdmin = 0;

        [Authorization]
        public ActionResult Index()
        {
            //if (Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME] == null)
            //{
            //    Response.Redirect("/login");
            //}
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            return View();
        }

        public PartialViewResult LeftMenu()
        {
            return PartialView();
        }

        public PartialViewResult HeaderMenu()
        {
            return PartialView();
        }

        public PartialViewResult SummaryMember()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {
                int totalNewMember = 0, totalAllMember = 0;
                IPConfigBO objConfig = new IPConfigBO();
                objConfig.IPAddress = Common.GetLocalIPAddress();
                objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
                objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

                string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
                if (!objResult.IsError)
                {
                    objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                    if (objRsa.Token != null && objRsa.Token != string.Empty)
                    {
                        MemberBO member = null;
                        objResult = repositoryMember.CountAllMember(objRsa,ref member);
                       
                        if (member != null)
                        {
                            totalAllMember = member.TotalAllMember;
                            totalNewMember = member.TotalNewMember;
                        }
                    }
                }
                ViewBag.TotalAllMember = totalAllMember;
                ViewBag.TotalNewMember = totalNewMember;
            }
            catch
            {
                ViewBag.TotalAllMember = 0;
                ViewBag.TotalNewMember = 0;
            }
            return PartialView();
        }


        public PartialViewResult SummaryTicket()
        {
            try
            {
                int maxTicket = 0;
                int year = DateTime.Now.Year;
                int totalTicket = 0;
                IEnumerable<BookingSummaryBO> lstTicketByPoint = repositorySummary.ListTicketSummaryByPoint(year, 0);

                IEnumerable<BookingSummaryBO> lstTicketByFree = repositorySummary.ListTicketSummaryByFree(year, 1);

                List<ObjTicketSummary> lstTicket = new List<ObjTicketSummary>();
                for (int i = 1; i <= 12; i++)
                {
                    ObjTicketSummary obj = new ObjTicketSummary();
                    obj.Month = i;
                    obj.TotalTicket = 0;
                    obj.TotalTicketFree = 0;
                    if (lstTicketByPoint != null && lstTicketByPoint.Count() > 0)
                    {
                        foreach (BookingSummaryBO objBooking in lstTicketByPoint)
                        {
                            if (objBooking.Month == i)
                            {
                                obj.TotalTicket = objBooking.TotalTicket;
                                break;
                            }
                        }
                    }

                    if (lstTicketByFree != null && lstTicketByFree.Count() > 0)
                    {
                        foreach (BookingSummaryBO objBooking in lstTicketByFree)
                        {
                            if (objBooking.Month == i)
                            {
                                obj.TotalTicketFree = objBooking.TotalTicket;
                                break;
                            }
                        }
                    }
                    totalTicket += obj.TotalTicket;
                    totalTicket += obj.TotalTicketFree;
                    lstTicket.Add(obj);
                }

                if(lstTicket!=null&&lstTicket.Count()>0)
                {
                    foreach(ObjTicketSummary obj in lstTicket)
                    {
                        if(maxTicket<obj.TotalTicket)
                        {
                            maxTicket = obj.TotalTicket;
                        }
                    }
                }

                ViewData["ListTicket"] = lstTicket;
                ViewBag.MaxTicket = (maxTicket + 10);
                ViewBag.TotalTicket = totalTicket;
                ViewData["ListTicketByPoints"] = lstTicketByPoint;
                ViewData["ListTicketByFree"] = lstTicketByFree;
            }
            catch
            {

            }
            return PartialView();
        }

        [HttpGet]
        public string GetDataTicketSummary()
        {
            string json = string.Empty;
            int year = DateTime.Now.Year;

            IEnumerable<BookingSummaryBO> lstTicketByPoint = repositorySummary.ListTicketSummaryByPoint(year, 0);

            IEnumerable<BookingSummaryBO> lstTicketByFree = repositorySummary.ListTicketSummaryByFree(year, 1);

            List<ObjTicketSummary> lstTicket = new List<ObjTicketSummary>();
            for (int i = 1; i <= 12; i++)
            {
                ObjTicketSummary obj = new ObjTicketSummary();
                obj.Month = i;
                obj.TotalTicket = 0;
                obj.TotalTicketFree = 0;
                if (lstTicketByPoint != null && lstTicketByPoint.Count() > 0)
                {
                    foreach (BookingSummaryBO objBooking in lstTicketByPoint)
                    {
                        if (objBooking.Month == i)
                        {
                            obj.TotalTicket = objBooking.TotalTicket;
                            break;
                        }
                    }
                }

                if (lstTicketByFree != null && lstTicketByFree.Count() > 0)
                {
                    foreach (BookingSummaryBO objBooking in lstTicketByFree)
                    {
                        if (objBooking.Month == i)
                        {
                            obj.TotalTicketFree = objBooking.TotalTicket;
                            break;
                        }
                    }
                }

                lstTicket.Add(obj);
            }
            json = JsonConvert.SerializeObject(lstTicket);

            return json;
        }


        public PartialViewResult SummaryRevenue()
        {
            try
            {
                double totalRevenue = 0;
                double maxRevenue = 0;
                double feeTicket = 0;
                int year = DateTime.Now.Year;
                IEnumerable<TicketConfigBO> lstTicket = repositoryTicket.ListTicketConfig();
                if(lstTicket!=null&&lstTicket.Count()>0)
                {
                    feeTicket = (double)lstTicket.ElementAt(0).CoinValues;
                }

                IEnumerable<BookingSummaryBO> lstTicketByPoint = repositorySummary.ListTicketSummaryByPoint(year, 0);
                List<ObjRevenue> lstRevenue = new List<ObjRevenue>();
                for (int i = 1; i <= 12; i++)
                {
                    ObjRevenue obj = new ObjRevenue();
                    obj.Month = i;
                    obj.BTCValues = 0;
                  
                    if (lstTicketByPoint != null && lstTicketByPoint.Count() > 0)
                    {
                        foreach (BookingSummaryBO objBooking in lstTicketByPoint)
                        {
                            if (objBooking.Month == i)
                            {
                                obj.BTCValues = objBooking.TotalTicket * feeTicket;
                                break;
                            }
                        }
                    }

                    totalRevenue += obj.BTCValues;
                    lstRevenue.Add(obj);
                }

                if(lstRevenue!=null&&lstRevenue.Count()>0)
                {
                    foreach(ObjRevenue item in lstRevenue)
                    {
                        if(maxRevenue<item.BTCValues)
                        {
                            maxRevenue = item.BTCValues;
                        }
                    }
                }
                ViewBag.MaxRevenue = maxRevenue;
                ViewBag.TotalRevenue = totalRevenue;
                ViewData["ListRevenue"] = lstRevenue;
            }
            catch
            {

            }
            return PartialView();
        }

        public string json = string.Empty;
        public PartialViewResult SummaryPrize()
        {
            // string json = string.Empty;
            try
            {
                double totalPrize = 0;
                List<PrizeChartJson> lstJson = new List<PrizeChartJson>();
                IEnumerable<PrizeBO> lstPrize = repositorySummary.ListPrize();

                IEnumerable<AwardBO> lstAward = repositoryAward.GetListAward(0, 50);
                List<ObjPrize> lst = new List<ObjPrize>();
                if (lstAward != null && lstAward.Count() > 0)
                {
                    foreach (AwardBO obj in lstAward)
                    {
                        ObjPrize item = new ObjPrize();
                        item.AwardID = obj.AwardID;
                        item.AwardName = obj.AwardName;
                        item.AwardValues = 0;
                        item.TotalMember = 0;
                        if (lstPrize != null && lstPrize.Count() > 0)
                        {
                            foreach (PrizeBO prize in lstPrize)
                            {
                                if (prize.AwardID == obj.AwardID)
                                {
                                    item.AwardValues = prize.AwardValues;
                                    item.TotalMember = prize.TotalMember;
                                    break;
                                }

                            }
                        }
                        totalPrize += item.AwardValues;
                        lst.Add(item);
                    }
                }
                if (lst != null && lst.Count() > 0)
                {
                    lst = lst.OrderBy(x => x.AwardID).ToList();
                    if (lst != null && lst.Count() > 0)
                    {
                        foreach (ObjPrize item in lst)
                        {
                            PrizeChartJson chart = new PrizeChartJson();
                            chart.data = item.AwardValues;
                            chart.label = item.AwardName.Trim();
                            lstJson.Add(chart);
                        }
                    }
                    json = JsonConvert.SerializeObject((object)lstJson);
                }

                ViewBag.TotalPrize = totalPrize;
                ViewBag.Data = json;
                ViewData["ListPrize"] = lstJson;
                ViewData["ListPrizeBO"] = lst;
            }
            catch
            {

            }
            return PartialView();
        }

        public PartialViewResult SummaryTotal()
        {
            int year = DateTime.Now.Year;
            int totalTicket = 0;
            IEnumerable<BookingSummaryBO> lstTicket = repositorySummary.ListTicketSummaryByPoint(year, -1);
            if(lstTicket!=null&&lstTicket.Count()>0)
            {
                foreach(BookingSummaryBO objBooking in lstTicket)
                {
                    totalTicket += objBooking.TotalTicket;
                }
            }
            ViewBag.TotalTicket = totalTicket;


            double totalRevenue = 0;
            double feeTicket = 0;
          
            IEnumerable<TicketConfigBO> lstTicketConfig = repositoryTicket.ListTicketConfig();
            if (lstTicketConfig != null && lstTicketConfig.Count() > 0)
            {
                feeTicket = (double)lstTicketConfig.ElementAt(0).CoinValues;
            }

            IEnumerable<BookingSummaryBO> lstTicketByPoint = repositorySummary.ListTicketSummaryByPoint(year, 0);
            List<ObjRevenue> lstRevenue = new List<ObjRevenue>();
            for (int i = 1; i <= 12; i++)
            {
                ObjRevenue obj = new ObjRevenue();
                obj.Month = i;
                obj.BTCValues = 0;

                if (lstTicketByPoint != null && lstTicketByPoint.Count() > 0)
                {
                    foreach (BookingSummaryBO objBooking in lstTicketByPoint)
                    {
                        if (objBooking.Month == i)
                        {
                            obj.BTCValues = objBooking.TotalTicket * feeTicket;
                            break;
                        }
                    }
                }

                totalRevenue += obj.BTCValues;
               
            }

            ViewBag.TotalRevenue = totalRevenue;

            double totalPrize = 0;
            IEnumerable<PrizeBO> lstPrize = repositorySummary.ListPrize();
            IEnumerable<AwardBO> lstAward = repositoryAward.GetListAward(0, 50);
            List<ObjPrize> lst = new List<ObjPrize>();
            if (lstAward != null && lstAward.Count() > 0)
            {
                foreach (AwardBO obj in lstAward)
                {
                    ObjPrize item = new ObjPrize();
                    item.AwardID = obj.AwardID;
                    item.AwardName = obj.AwardName;
                    item.AwardValues = 0;
                    if (lstPrize != null && lstPrize.Count() > 0)
                    {
                        foreach (PrizeBO prize in lstPrize)
                        {
                            if (prize.AwardID == obj.AwardID)
                            {
                                item.AwardValues = prize.AwardValues;
                                break;
                            }

                        }
                    }
                    totalPrize += item.AwardValues;
                  
                }
            }

            ViewBag.TotalPrize = totalPrize;
            string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
            string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
            string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
            string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];

            string LoanWalletID = ConfigurationManager.AppSettings[KeyManager.LoanWalletID];
            string JackPotID = ConfigurationManager.AppSettings[KeyManager.JackPotID];

            decimal totalCredit = 0, totalJackpot = 0, totalBTC = 0;
            try
            {
                StatisticFilterRequest objRequestCredit = new StatisticFilterRequest();
                objRequestCredit.ClientID = short.Parse(ClientID);
                objRequestCredit.CurrencyCode = CurrencyCode.BTC;
                objRequestCredit.SystemID = short.Parse(SystemID);
                objRequestCredit.WalletId = short.Parse(WalletID);
                objRequestCredit.UserId = LoanWalletID;
                objRequestCredit.IsTotalReceived = false;
                objRequestCredit.FromDate = null;
                objRequestCredit.ToDate = null;
                TransactionRepository objTransactionRepository = new TransactionRepository();

                totalCredit = objTransactionRepository.GetTotalAmountForUser(objRequestCredit);

                StatisticFilterRequest objRequestJackpot = new StatisticFilterRequest();
                objRequestJackpot.ClientID = short.Parse(ClientID);
                objRequestJackpot.CurrencyCode = CurrencyCode.BTC;
                objRequestJackpot.SystemID = short.Parse(SystemID);
                objRequestJackpot.WalletId = short.Parse(WalletID);
                objRequestJackpot.UserId = JackPotID;
                objRequestJackpot.IsTotalReceived = false;
                objRequestJackpot.FromDate = null;
                objRequestJackpot.ToDate = null;

                totalJackpot = objTransactionRepository.GetTotalAmountForUser(objRequestJackpot);

                StatisticFilterRequest objRequestBTC = new StatisticFilterRequest();
                objRequestBTC.ClientID = short.Parse(ClientID);
                objRequestBTC.CurrencyCode = CurrencyCode.BTC;
                objRequestBTC.SystemID = short.Parse(SystemID);
                objRequestBTC.WalletId = short.Parse(WalletID);
                objRequestBTC.UserId = string.Empty;
                objRequestBTC.IsTotalReceived = true;
                objRequestBTC.FromDate = null;
                objRequestBTC.ToDate = null;

                totalBTC = objTransactionRepository.GetTotalAmountForUser(objRequestBTC);
            }
            catch(Exception ex)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("Exception SummaryTotal: " + ex.Message);
            }
            ViewBag.TotalCredit = totalCredit;
            ViewBag.TotalJackpot = totalJackpot;
            ViewBag.TotalBTC = totalBTC;

            return PartialView();
        }


        [HttpGet]
        public string GetDataPrizeSummary()
        {
            string json = string.Empty;
            int year = DateTime.Now.Year;

            List<PrizeChartJson> lstJson = new List<PrizeChartJson>();
            IEnumerable<PrizeBO> lstPrize = repositorySummary.ListPrize();

            IEnumerable<AwardBO> lstAward = repositoryAward.GetListAward(0, 50);
            List<ObjPrize> lst = new List<ObjPrize>();
            if (lstAward != null && lstAward.Count() > 0)
            {
                foreach (AwardBO obj in lstAward)
                {
                    ObjPrize item = new ObjPrize();
                    item.AwardID = obj.AwardID;
                    item.AwardName = obj.AwardName;
                    item.AwardValues = 0;
                    if (lstPrize != null && lstPrize.Count() > 0)
                    {
                        foreach (PrizeBO prize in lstPrize)
                        {
                            if (prize.AwardID == obj.AwardID)
                            {
                                item.AwardValues = prize.AwardValues;
                                break;
                            }

                        }
                    }
                 
                    lst.Add(item);
                }
            }
            if (lst != null && lst.Count() > 0)
            {
                lst = lst.OrderBy(x => x.AwardID).ToList();
                if (lst != null && lst.Count() > 0)
                {
                    foreach (ObjPrize item in lst)
                    {
                        PrizeChartJson chart = new PrizeChartJson();
                        chart.data = item.AwardValues;
                        chart.label = item.AwardName.Trim();
                        lstJson.Add(chart);
                    }
                }
              
            }

            json = JsonConvert.SerializeObject(lstJson);

            return json;
        }

        [HttpGet]
        public string GetDataRevenueSummary()
        {
            string json = string.Empty;
            double feeTicket = 0;
            
            int year = DateTime.Now.Year;
            IEnumerable<TicketConfigBO> lstTicket = repositoryTicket.ListTicketConfig();
            if (lstTicket != null && lstTicket.Count() > 0)
            {
                feeTicket = (double)lstTicket.ElementAt(0).CoinValues;
            }

            IEnumerable<BookingSummaryBO> lstTicketByPoint = repositorySummary.ListTicketSummaryByPoint(year, 0);
            List<ObjRevenue> lstRevenue = new List<ObjRevenue>();
            for (int i = 1; i <= 12; i++)
            {
                ObjRevenue obj = new ObjRevenue();
                obj.Month = i;
                obj.BTCValues = 0;

                if (lstTicketByPoint != null && lstTicketByPoint.Count() > 0)
                {
                    foreach (BookingSummaryBO objBooking in lstTicketByPoint)
                    {
                        if (objBooking.Month == i)
                        {
                            obj.BTCValues = objBooking.TotalTicket * feeTicket;
                            break;
                        }
                    }
                }


                lstRevenue.Add(obj);
            }
            json = JsonConvert.SerializeObject(lstRevenue);
            return json;
        }


        public PartialViewResult SummaryBooking()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {
                int totalNewBookingMegaball = 0, totalAllBookingMegaball = 0;
                IPConfigBO objConfig = new IPConfigBO();
                objConfig.IPAddress = Common.GetLocalIPAddress();
                objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
                objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];
                
                string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
                if(!objResult.IsError)
                {
                    objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                    if (objRsa.Token != null && objRsa.Token != string.Empty)
                    {
                        BookingMegaballBO booking = null;
                        objResult = repositoryBookingMegaball.CountAllBookingMegaball(objRsa,ref booking);
                        if (booking != null)
                        {
                            totalAllBookingMegaball = booking.TotalAllBookingMegaball;
                            totalNewBookingMegaball = booking.TotalNewBookingMegaball;
                        }
                    }
                }
                ViewBag.TotalAllBookingMegaball = totalAllBookingMegaball;
                ViewBag.TotalNewBookingMegaball = totalNewBookingMegaball;
            }
            catch(Exception ex)
            {
                ViewBag.TotalAllBookingMegaball = 0;
                ViewBag.TotalNewBookingMegaball = 0;
            }
            return PartialView();
        }

        public PartialViewResult SummaryPoint()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {
                int totalNewPoint = 0, totalAllPoint = 0;
                IPConfigBO objConfig = new IPConfigBO();
                objConfig.IPAddress = Common.GetLocalIPAddress();
                objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
                objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

                string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
                if (!objResult.IsError)
                {
                    objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                    if (objRsa.Token != null && objRsa.Token != string.Empty)
                    {
                        TransactionPointsBO points = repositoryTransactionPoints.CountAllPoints();
                       
                        if (points != null)
                        {
                            totalAllPoint = points.TotalAllPoint;
                            totalNewPoint = points.TotalNewPoint;
                        }
                    }
                }
                ViewBag.TotalAllPoint = totalAllPoint;
                ViewBag.TotalNewPoint = totalNewPoint;
            }
            catch
            {
                ViewBag.TotalAllPoint = 0;
                ViewBag.TotalNewPoint = 0;
            }
            return PartialView();
        }

        public PartialViewResult SummaryCoin()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {
                int totalNewCoin = 0, totalAllCoin = 0;
                IPConfigBO objConfig = new IPConfigBO();
                objConfig.IPAddress = Common.GetLocalIPAddress();
                objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
                objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

                string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
                if (!objResult.IsError)
                {
                    objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                    if (objRsa.Token != null && objRsa.Token != string.Empty)
                    {
                        TransactionCoinBO coin = null;
                        objResult = repositoryCoin.CountAllCoin(objRsa,ref coin);
                        
                        if (coin != null)
                        {
                            totalAllCoin = coin.TotalAllCoin;
                            totalNewCoin = coin.TotalNewCoin;
                        }
                    }
                }
                ViewBag.TotalAllCoin = totalAllCoin;
                ViewBag.TotalNewCoin = totalNewCoin;
            }
            catch
            {
                ViewBag.TotalAllCoin = 0;
                ViewBag.TotalNewCoin = 0;
            }
            return PartialView();
        } 
        public PartialViewResult SummaryPackage()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {


                TransactionPackageBO package = repositoryTransactionPackage.CountAllPackage();
                int totalNewPackage = 0, totalAllPackage = 0;
                if (package != null)
                {
                    totalAllPackage = package.TotalAllPackage;
                    totalNewPackage = package.TotalNewPackage;
                }
                ViewBag.TotalAllPackage = totalAllPackage;
                ViewBag.TotalNewPackage = totalNewPackage;
            }
            catch
            {
                ViewBag.TotalAllPackage = 0;
                ViewBag.TotalNewPackage = 0;
            }
            return PartialView();
        }
        [Authorization]
        public PartialViewResult SummaryAwardMegaball()
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {
                int totalNewAward = 0, totalAllAward = 0;
                IPConfigBO objConfig = new IPConfigBO();
                objConfig.IPAddress = Common.GetLocalIPAddress();
                objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
                objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

                string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
                if (!objResult.IsError)
                {
                    objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                    if (objRsa.Token != null && objRsa.Token != string.Empty)
                    {
                        AwardMegaballBO objAwardMegaball = null;
                        objResult = repositoryAwardMegaball.CountAllAwardMegaball(objRsa, ref objAwardMegaball);
                        if (objAwardMegaball != null)
                        {
                            totalAllAward = objAwardMegaball.TotalAllAward;
                            totalNewAward = objAwardMegaball.TotalNewAward;
                        }
                    }
                }
                ViewBag.TotalAllAward = totalAllAward;
                ViewBag.TotalNewAward = totalNewAward;
            }
            catch(Exception ex)
            {
                ViewBag.TotalAllPackage = 0;
                ViewBag.TotalNewPackage = 0;
            }
            return PartialView();
        }
        [Authorization]
        public PartialViewResult ListBookingByDate(string p)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {
                IEnumerable<MemberBO> lstMember = null;
                int totalRecord = 0;
                IPConfigBO objConfig = new IPConfigBO();
                objConfig.IPAddress = Common.GetLocalIPAddress();
                objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
                objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

                string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
                ModelRSA objRsa = new ModelRSA(strRsaParameter1);
                ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
                if (!objResult.IsError)
                {
                    objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                    if (objRsa.Token != null && objRsa.Token != string.Empty)
                    {
                        DateTime fromD = DateTime.Now;
                        DateTime toD = DateTime.Now;                       
                        int intPageSize = 10;
                        int start = 0, end = 10;
                        int page = 1;
                        try
                        {
                            if (p != null && p != string.Empty)
                            {
                                page = int.Parse(p);
                            }
                        }
                        catch
                        {

                        }
                        if (page > 1)
                        {
                            start = (page - 1) * intPageSize + 1;
                            end = (page * intPageSize);
                        }
                        //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                        string fromDate = DateTime.Now.ToString("MM/dd/yyyy");
                        string toDate = DateTime.Now.ToString("MM/dd/yyyy");

                        string[] arrFrom = fromDate.Split('/');
                        if (arrFrom != null && arrFrom.Length > 0)
                        {
                            string m = arrFrom[0];
                            string d = arrFrom[1];
                            string y = arrFrom[2];
                            string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
                            fromD = DateTime.Parse(dateFrom);
                        }
                        string[] arrTo = toDate.Split('/');
                        if (arrTo != null && arrTo.Length > 0)
                        {
                            string m = arrTo[0];
                            string d = arrTo[1];
                            string y = arrTo[2];
                            string dateTo = m + "/" + d + "/" + y + " 23:59:00";
                            toD = DateTime.Parse(dateTo);
                        }
                        IEnumerable<BookingMegaballBO> lstBooking = repositoryBookingMegaball.ListAllBookingBySearch(0, fromD, toD, -1, start, end);
                        ViewData[ViewDataKey.VIEWDATA_LISTBOOKINGMEGABALL] = lstBooking;
                        if (lstBooking != null && lstBooking.Count() > 0)
                        {
                            totalRecord = lstBooking.ElementAt(0).TotalRecord;
                        }
                        lstMember = repositoryMember.GetListMember(1, 10000);
                    }
                }
                TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;
                ViewData[ViewDataKey.VIEWDATA_LISTMEMBER] = lstMember;
            }
            catch
            {

            }
            return PartialView();
        }
        [Authorization]
        [HttpGet]
        public string SearchMemberBookingMegaball(string keyword, int page)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            string json = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    DateTime fromD = DateTime.Now;
                    DateTime toD = DateTime.Now;

                    StringBuilder builder = new StringBuilder();
                    StringBuilder builderPaging = new StringBuilder();
                    string fromDate = DateTime.Now.ToString("MM/dd/yyyy");
                    string toDate = DateTime.Now.ToString("MM/dd/yyyy");

                    string[] arrFrom = fromDate.Split('/');
                    if (arrFrom != null && arrFrom.Length > 0)
                    {
                        string m = arrFrom[0];
                        string d = arrFrom[1];
                        string y = arrFrom[2];
                        string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
                        fromD = DateTime.Parse(dateFrom);
                    }
                    string[] arrTo = toDate.Split('/');
                    if (arrTo != null && arrTo.Length > 0)
                    {
                        string m = arrTo[0];
                        string d = arrTo[1];
                        string y = arrTo[2];
                        string dateTo = m + "/" + d + "/" + y + " 23:59:00";
                        toD = DateTime.Parse(dateTo);
                    }
                    int totalRecord = 0;
                    int intPageSize = 10;
                    int start = 0, end = 10;

                    if (page >= 1)
                    {
                        start = (page - 1) * intPageSize + 1;
                        end = (page * intPageSize);
                    }
                    IEnumerable<BookingMegaballBO> lstBooking = repositoryBookingMegaball.GetListMemberBySearch(keyword, fromD, toD, start, end);
                    if (lstBooking != null && lstBooking.Count() > 0)
                    {

                        totalRecord = lstBooking.ElementAt(0).TotalRecord;
                        foreach (BookingMegaballBO booking in lstBooking)
                        {
                            string titleStatus = string.Empty;
                            string statusName = string.Empty;
                            if (booking.Status == 0)
                            {
                                statusName = "Not confirm";
                                titleStatus = "Confirm";
                            }
                            else if (booking.Status == 1)
                            {
                                statusName = "Confirmed";
                                titleStatus = "Actived";
                            }
                            else if (booking.Status == 2)
                            {
                                statusName = "Deleted";
                                titleStatus = "";
                            }
                            const int maxlenght = 20;
                            string transactionCode = booking.TransactionCode.ToString();
                            string dot = "...";
                            if (transactionCode.Length > maxlenght)
                            {
                                transactionCode = transactionCode.Substring(0, maxlenght) + "" + dot;
                            }

                            int num1, num2, num3, num4, num5, extranum;
                            num1 = booking.FirstNumber;
                            num2 = booking.SecondNumber;
                            num3 = booking.ThirdNumber;
                            num4 = booking.FourthNumber;
                            num5 = booking.FirstNumber;
                            extranum = booking.ExtraNumber;

                            builder.Append(" <tr id=\"trGroup_" + booking.BookingID + "\" class=\"none-top-border\">");
                            builder.Append("<td class=\"center\"><p>" + booking.BookingID + "</p> </td>");
                            builder.Append("<td>" + booking.Email + "</td>");
                            builder.Append("<td class=\"center\" style=\"width:185px\">");
                            builder.Append("<span class=\"circle12\">" + booking.FirstNumber + "</span>");
                            builder.Append("<span class=\"circle12\">" + booking.SecondNumber + "</span>");
                            builder.Append("<span class=\"circle12\">" + booking.ThirdNumber + "</span>");
                            builder.Append("<span class=\"circle12\">" + booking.FourthNumber + "</span>");
                            builder.Append("<span class=\"circle12\">" + booking.FirstNumber + "</span>");
                            builder.Append("<span class=\"circle_ball1\">" + booking.ExtraNumber + "</span>");
                            builder.Append("</td>");

                            builder.Append("<td>" + booking.Quantity + "</td>");
                            builder.Append("<td class=\"center\" title=\"" + booking.TransactionCode + "\">" + transactionCode + "</td>");
                            builder.Append("<td class=\"center\">" + booking.CreateDate.ToString("MM/dd/yyyy HH:mm:ss") + "</td>");
                            builder.Append(" <td class=\"center\">" + booking.OpenDate.ToString(OpenDateResult.OPENDATERESULT) + "</td>");
                            builder.Append("<td class=\"center\"><span class=\"label-success label label-default\">" + statusName + "</span></td>");
                            builder.Append(" <td>");
                            if (booking.Status == 0)
                            {
                                builder.Append("<a class=\"btn btn-info btn-sm\" href=\"javascript:void(0)\" onclick=\"ConfirmBookingMegaball('" + booking.BookingID + "','1','" + booking.MemberID + "','" + booking.NumberWin + "')\" title=\"" + @titleStatus + "\"><i class=\"glyphicon glyphicon-edit icon-white\"></i>Confirm</a>");
                                builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"javascript:void(0)\" onclick=\"ConfirmBookingMegaball('" + booking.BookingID + "','2','" + booking.MemberID + "','" + booking.NumberWin + "')\" title=\"Delete\"><i class=\"glyphicon glyphicon-trash icon-white\"></i>Delete</a>");
                            }
                            else if (booking.Status == 1)
                            {
                                builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"javascript:void(0)\" onclick=\"ConfirmBookingMegaball('" + booking.BookingID + "','2','" + booking.MemberID + "','" + booking.NumberWin + "')\" title=\"Delete\"><i class=\"glyphicon glyphicon-trash icon-white\"></i>Delete</a>");
                            }
                            builder.Append("</td>");
                            builder.Append("</tr>");
                        }
                        int totalPage = totalRecord / intPageSize;
                        int balance = totalRecord % intPageSize;
                        if (balance != 0)
                        {
                            totalPage += 1;
                        }

                        if (totalPage > 1)
                        {
                            for (int m = 1; m <= totalPage; m++)
                            {
                                if (m == page)
                                {
                                    builderPaging.Append("<li class=\"active\"><a href=\"javascript:void(0)\" >" + m + "</a></li>");
                                }
                                else
                                {
                                    builderPaging.Append("<li><a href=\"javascript:void(0)\" onclick=\"PagingSearch_BookingMegaball('" + m + "')\" >" + m + "</a></li>");
                                }
                            }
                        }
                    }
                    else
                    {
                        builder.Append("<tr><td colspan=\"7\">No result found</td></tr>");
                    }

                    SearchObj obj = new SearchObj();
                    obj.ContentResult = builder.ToString();
                    obj.PagingResult = builderPaging.ToString();
                    obj.Totalrecord = totalRecord;
                    json = JsonConvert.SerializeObject(obj);
                }
            }

            return json;
        }

        [Authorization]
        [HttpGet]
        public string ExportList_BookingMegaballTicket(string keyword)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            string fileNameExcel = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                   
                    DateTime fromD = DateTime.Now;
                    DateTime toD = DateTime.Now;
                    //if(keyword==null||keyword.Trim().Length==0)
                    //{
                    //    fileNameExcel = "strResultNull";
                    //}
                    HSSFWorkbook workbook = new HSSFWorkbook();
                    Sheet sheet = workbook.CreateSheet("LIST BOOKING MEGABALL TICKET");
                    Row rowLogo = sheet.CreateRow(0);

                    rowLogo.CreateCell(0).SetCellValue("LIST BOOKING MEGABALL TICKET - LOTTERY DATE " + DateTime.Now + "");
                    rowLogo.HeightInPoints = 20;
                    CellStyle styleLogo = workbook.CreateCellStyle();
                    styleLogo.Alignment = HorizontalAlignment.CENTER;
                    NPOI.SS.UserModel.Font font = workbook.CreateFont();
                    font.FontHeightInPoints = 12;
                    font.FontName = "Times New Roman";
                    font.Boldweight = (short)FontBoldWeight.BOLD;

                    styleLogo.SetFont(font);
                    rowLogo.GetCell(0).CellStyle = styleLogo;

                    sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, 3));

                    sheet.SetColumnWidth(0, 5 * 256);
                    sheet.SetColumnWidth(1, 40 * 256);
                    sheet.SetColumnWidth(2, 35 * 256);
                    sheet.SetColumnWidth(3, 15 * 256);
                    sheet.SetColumnWidth(4, 15 * 256);
                    sheet.SetColumnWidth(5, 25 * 256);
                    sheet.SetColumnWidth(6, 25 * 256);
                    sheet.SetColumnWidth(7, 15 * 256);
                    Row titleList = sheet.CreateRow(2);

                    titleList.CreateCell(0).SetCellValue("STT");
                    titleList.HeightInPoints = 22;
                    CellStyle styleTitleList = workbook.CreateCellStyle();
                    styleTitleList.Alignment = HorizontalAlignment.LEFT;
                    styleTitleList.FillBackgroundColor = 4;
                    NPOI.SS.UserModel.Font fontTitleList = workbook.CreateFont();
                    fontTitleList.FontHeightInPoints = 12;
                    fontTitleList.FontName = "Times New Roman";
                    fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
                    fontTitleList.Color = (short)NPOI.HSSF.Util.HSSFColor.WHITE.index;
                    //NPOI.HSSF.UserModel.HSSFTextbox.LINESTYLE_SOLID
                    styleTitleList.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;
                    // styleTitleList.FillPattern = FillPatternType.THICK_VERT_BANDS;
                    styleTitleList.FillPattern = FillPatternType.THIN_FORWARD_DIAG;
                    styleTitleList.FillPattern = FillPatternType.THIN_BACKWARD_DIAG;
                    styleTitleList.FillBackgroundColor = NPOI.HSSF.Util.HSSFColor.GREEN.index;

                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(0).CellStyle = styleTitleList;

                    titleList.CreateCell(1).SetCellValue("Transaction Code");
                    titleList.HeightInPoints = 22;
                    styleTitleList.Alignment = HorizontalAlignment.LEFT;
                    styleTitleList.FillBackgroundColor = 4;
                    fontTitleList.FontHeightInPoints = 12;
                    fontTitleList.FontName = "Times New Roman";
                    fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(1).CellStyle = styleTitleList;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(1).CellStyle = styleTitleList;


                    titleList.CreateCell(2).SetCellValue("Email");
                    titleList.HeightInPoints = 22;
                    styleTitleList.Alignment = HorizontalAlignment.LEFT;
                    styleTitleList.FillBackgroundColor = 4;
                    fontTitleList.FontHeightInPoints = 12;
                    fontTitleList.FontName = "Times New Roman";
                    fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(2).CellStyle = styleTitleList;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(2).CellStyle = styleTitleList;

                    titleList.CreateCell(3).SetCellValue("Ticket");
                    titleList.HeightInPoints = 22;
                    styleTitleList.Alignment = HorizontalAlignment.CENTER;
                    styleTitleList.FillBackgroundColor = 4;
                    fontTitleList.FontHeightInPoints = 12;
                    fontTitleList.FontName = "Times New Roman";
                    fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(3).CellStyle = styleTitleList;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(3).CellStyle = styleTitleList;

                    titleList.CreateCell(4).SetCellValue("Quantity");
                    titleList.HeightInPoints = 22;
                    styleTitleList.Alignment = HorizontalAlignment.CENTER;
                    styleTitleList.FillBackgroundColor = 4;
                    fontTitleList.FontHeightInPoints = 12;
                    fontTitleList.FontName = "Times New Roman";
                    fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(4).CellStyle = styleTitleList;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(4).CellStyle = styleTitleList;

                    titleList.CreateCell(5).SetCellValue("Buy Date");
                    titleList.HeightInPoints = 22;
                    styleTitleList.Alignment = HorizontalAlignment.CENTER;
                    styleTitleList.FillBackgroundColor = 4;
                    fontTitleList.FontHeightInPoints = 12;
                    fontTitleList.FontName = "Times New Roman";
                    fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(5).CellStyle = styleTitleList;

                    titleList.CreateCell(6).SetCellValue("Open Date");
                    titleList.HeightInPoints = 22;
                    styleTitleList.Alignment = HorizontalAlignment.CENTER;
                    styleTitleList.FillBackgroundColor = 4;
                    fontTitleList.FontHeightInPoints = 12;
                    fontTitleList.FontName = "Times New Roman";
                    fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(6).CellStyle = styleTitleList;


                    titleList.CreateCell(7).SetCellValue("Status");
                    titleList.HeightInPoints = 22;
                    styleTitleList.Alignment = HorizontalAlignment.LEFT;
                    styleTitleList.FillBackgroundColor = 4;
                    fontTitleList.FontHeightInPoints = 12;
                    fontTitleList.FontName = "Times New Roman";
                    fontTitleList.Boldweight = (short)FontBoldWeight.BOLD;
                    styleTitleList.SetFont(fontTitleList);
                    titleList.GetCell(7).CellStyle = styleTitleList;

                    int rowNext = 3;

                    int number = 1;
                    int intPageSize = 10;
                    int start = 0, end = 10;
                    int totalRecord = 0;
                    Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                    StringBuilder builder = new StringBuilder();
                    StringBuilder builderPaging = new StringBuilder();
                    string fromDate = DateTime.Now.ToString("MM/dd/yyyy");
                    string toDate = DateTime.Now.ToString("MM/dd/yyyy");

                    string[] arrFrom = fromDate.Split('/');
                    if (arrFrom != null && arrFrom.Length > 0)
                    {
                        string m = arrFrom[0];
                        string d = arrFrom[1];
                        string y = arrFrom[2];
                        string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
                        fromD = DateTime.Parse(dateFrom);
                    }
                    string[] arrTo = toDate.Split('/');
                    if (arrTo != null && arrTo.Length > 0)
                    {
                        string m = arrTo[0];
                        string d = arrTo[1];
                        string y = arrTo[2];
                        string dateTo = m + "/" + d + "/" + y + " 23:59:00";
                        toD = DateTime.Parse(dateTo);
                    }
                    try
                    {
                        IEnumerable<BookingMegaballBO> lstBooking = repositoryBookingMegaball.GetListMemberBySearch(keyword, fromD, toD, 1, 10000);
                        if (lstBooking != null && lstBooking.Count() > 0)
                        {
                            totalRecord = lstBooking.ElementAt(0).TotalRecord;
                            foreach (BookingMegaballBO booking in lstBooking)
                            {
                                string titleStatus = string.Empty;
                                string statusName = string.Empty;
                                if (booking.Status == 0)
                                {
                                    statusName = "Not confirm";
                                    titleStatus = "Confirm";
                                }
                                else if (booking.Status == 1)
                                {
                                    statusName = "Confirmed";
                                    titleStatus = "Actived";
                                }
                                else if (booking.Status == 2)
                                {
                                    statusName = "Deleted";
                                    titleStatus = "";
                                }
                                Row rowListInvoice = sheet.CreateRow(rowNext);

                                rowListInvoice.CreateCell(0).SetCellValue(number);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleNumber = workbook.CreateCellStyle();
                                styleNumber.Alignment = HorizontalAlignment.LEFT;
                                NPOI.SS.UserModel.Font fontNumber = workbook.CreateFont();
                                fontNumber.FontHeightInPoints = 12;
                                fontNumber.FontName = "Times New Roman";
                                styleNumber.SetFont(fontNumber);
                                rowListInvoice.GetCell(0).CellStyle = styleNumber;
                                try
                                {
                                    rowListInvoice.CreateCell(1).SetCellValue(booking.TransactionCode);
                                    rowListInvoice.HeightInPoints = 20;
                                    CellStyle styleDate = workbook.CreateCellStyle();
                                    styleDate.Alignment = HorizontalAlignment.LEFT;
                                    NPOI.SS.UserModel.Font fontDate = workbook.CreateFont();
                                    fontDate.FontHeightInPoints = 12;
                                    fontDate.FontName = "Times New Roman";
                                    styleDate.SetFont(fontDate);
                                    rowListInvoice.GetCell(1).CellStyle = styleDate;
                                }
                                catch { }
                                try
                                {
                                    rowListInvoice.CreateCell(2).SetCellValue(booking.Email);
                                    rowListInvoice.HeightInPoints = 20;
                                    CellStyle styleDate2 = workbook.CreateCellStyle();
                                    styleDate2.Alignment = HorizontalAlignment.LEFT;
                                    NPOI.SS.UserModel.Font fontDate2 = workbook.CreateFont();
                                    fontDate2.FontHeightInPoints = 12;
                                    fontDate2.FontName = "Times New Roman";
                                    styleDate2.SetFont(fontDate2);
                                    rowListInvoice.GetCell(2).CellStyle = styleDate2;
                                }
                                catch { }
                                try
                                {
                                    string strnumber = booking.FirstNumber + " " + booking.SecondNumber + " " + booking.ThirdNumber + " " + booking.FourthNumber + " " + booking.FivethNumber + " " + booking.ExtraNumber;
                                    rowListInvoice.CreateCell(3).SetCellValue(strnumber);
                                    rowListInvoice.HeightInPoints = 20;
                                    CellStyle styleDate4 = workbook.CreateCellStyle();
                                    styleDate4.Alignment = HorizontalAlignment.CENTER;
                                    NPOI.SS.UserModel.Font fontDate4 = workbook.CreateFont();
                                    fontDate4.FontHeightInPoints = 12;
                                    fontDate4.FontName = "Times New Roman";
                                    styleDate4.SetFont(fontDate4);
                                    rowListInvoice.GetCell(3).CellStyle = styleDate4;
                                }
                                catch { }
                                rowListInvoice.CreateCell(4).SetCellValue(booking.Quantity);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleDate5 = workbook.CreateCellStyle();
                                styleDate5.Alignment = HorizontalAlignment.CENTER;
                                NPOI.SS.UserModel.Font fontDate5 = workbook.CreateFont();
                                fontDate5.FontHeightInPoints = 12;
                                fontDate5.FontName = "Times New Roman";
                                styleDate5.SetFont(fontDate5);
                                rowListInvoice.GetCell(4).CellStyle = styleDate5;

                                //Row rowDate = sheet.CreateRow(rowNext);
                                rowListInvoice.CreateCell(5).SetCellValue(booking.CreateDate.ToString("MM/dd/yyyy HH:mm:ss"));
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleEmail = workbook.CreateCellStyle();
                                styleEmail.Alignment = HorizontalAlignment.CENTER;
                                NPOI.SS.UserModel.Font fontEmail = workbook.CreateFont();
                                fontEmail.FontHeightInPoints = 12;
                                fontEmail.FontName = "Times New Roman";
                                styleEmail.SetFont(fontEmail);
                                rowListInvoice.GetCell(5).CellStyle = styleEmail;


                                //Row rowOrderID = sheet.CreateRow(rowNext);
                                rowListInvoice.CreateCell(6).SetCellValue(booking.OpenDate.ToString("MM/dd/yyyy 16:00:00"));
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleOrderID = workbook.CreateCellStyle();
                                styleOrderID.Alignment = HorizontalAlignment.CENTER;
                                NPOI.SS.UserModel.Font fontOrderID = workbook.CreateFont();
                                fontOrderID.FontHeightInPoints = 12;
                                fontOrderID.FontName = "Times New Roman";
                                styleOrderID.SetFont(fontOrderID);
                                rowListInvoice.GetCell(6).CellStyle = styleOrderID;

                                rowListInvoice.CreateCell(7).SetCellValue(statusName);
                                rowListInvoice.HeightInPoints = 20;
                                CellStyle styleEmail3 = workbook.CreateCellStyle();
                                styleEmail3.Alignment = HorizontalAlignment.LEFT;

                                NPOI.SS.UserModel.Font fontEmail3 = workbook.CreateFont();
                                fontEmail3.FontHeightInPoints = 12;
                                fontEmail3.FontName = "Times New Roman";

                                styleEmail.SetFont(fontEmail3);
                                rowListInvoice.GetCell(7).CellStyle = styleEmail3;

                                number++;
                                rowNext++;
                            }

                        }
                    }
                    catch { }
                    fileNameExcel = "Listbookingticket_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + ".xls";
                    string filename = Server.MapPath("~/FileExcels/BookingMegaballTicket/" + fileNameExcel);
                    using (var fileData = new FileStream(filename, FileMode.Create))
                    {
                        workbook.Write(fileData);
                    }
                }
            }
            return fileNameExcel;
        }
    }
}
