﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BBH.Lotte.Repository;
using Newtonsoft.Json;
using System.Configuration;
using System.Text;
using BBH.Lotte.CMSAdmin.Models;
using Microsoft.Practices.Unity;
using BBH.Lotte.Domain.Interfaces;
using BBH.Lotte.Domain;
using BBH.Lotte.CLP.CMSAdmin.Models;
using BBC.Core.Common.Utils;
using BBH.Lotte.Shared;
using BBC.Core.Database;
using BBC.Core.Common.Cache;
using BBC.Core.Common.Log;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.Wallet.Repository;
using System.Threading.Tasks;
using RestSharp;

namespace BBH.Lotte.CMSAdmin.Controllers
{
    public class AwardMegaballController : Controller
    {
        //
        // GET: /AwardMegaball/

        string Number49 = ConfigurationManager.AppSettings[ViewDataKey.NUMBER49];
        string Number26 = ConfigurationManager.AppSettings[ViewDataKey.NUMBER26];
        string JackPotDefault = ConfigurationManager.AppSettings[ViewDataKey.VIEWDATA_JACKPOTDEFAULT];

        string DayOpen = ConfigurationManager.AppSettings[KeyManager.DayOpen];

        int HoureStart = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.HoureStart]);
        int MinuteStart = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.MinuteStart]);
        int HoureEnd = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.HourEnd]);
        int MinuteEnd = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.MinuteEnd]);

        int TimeClosedBuyTicket = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_CLOSED_COUNT_DOWN]);
        int TimeOpenBuyTicket = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager._TIME_OPENED_COUNT_DOWN]);

        enum Days { Saturday = 7, Sunday = 1, Monday = 2, Tuesday = 3, Wednesday = 4, Thursday = 5, Friday = 6 };

        int PercentRevenueForFreeTicket = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForFreeTicket]);//2
        int PercentRevenueForAward = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForAward]);//40;
        int PercentRevenueForOper = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForOper]);//18;
        int PercentRevenueForBusinessActivity = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForBusinessActivity]);//20;
        int PercentRevenueForJacpot = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForJacpot]);//20;

        int PercentRevenueForGuaranteeFund = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForGuaranteeFund]);//40;
        int PercentRevenueForJacpot60 = Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.PercentRevenueForJacpot60]);//60;

        string ClientID = ConfigurationManager.AppSettings[KeyManager.ClientID];
        string SystemID = ConfigurationManager.AppSettings[KeyManager.SystemID];
        string WalletID = ConfigurationManager.AppSettings[KeyManager.WalletID];
        string RequestBy = ConfigurationManager.AppSettings[KeyManager.RequestBy];
        //sysAdmin
        string sysAdmin_UserID = ConfigurationManager.AppSettings[KeyManager.SysAdminID];
        string sysAdmin_Address = ConfigurationManager.AppSettings[KeyManager.SysAdminWalletAddress];
        //sysFreeTicket
        string sysFreeTicket_UserID = ConfigurationManager.AppSettings[KeyManager.FreeWalletID];
        string sysFreeTicket_Address = ConfigurationManager.AppSettings[KeyManager.FreeWalletAddress];
        //sysMgmt
        string sysMgmt_UserID = ConfigurationManager.AppSettings[KeyManager.sysMgmtID];
        string sysMgmt_Address = ConfigurationManager.AppSettings[KeyManager.SysMgmtWalletAddress];

        //sysProgressivePrize 
        string sysProgressivePrize_UserID = ConfigurationManager.AppSettings[KeyManager.ProgressivePrizeID];
        string sysProgressivePrize_Address = ConfigurationManager.AppSettings[KeyManager.ProgressivePrizeWalletAddress];
        //sysLoan
        string sysLoan_UserID = ConfigurationManager.AppSettings[KeyManager.LoanWalletID];
        string sysLoan_Address = ConfigurationManager.AppSettings[KeyManager.LoanWalletAddress];

        //sysAward
        string sysAward_UserID = ConfigurationManager.AppSettings[KeyManager.AwardWalletID];
        string sysAward_Address = ConfigurationManager.AppSettings[KeyManager.AwardWalletAddress];

        //sysJackpot
        string sysJackpot_UserID = ConfigurationManager.AppSettings[KeyManager.JackPotID];
        string sysJackpot_Address = ConfigurationManager.AppSettings[KeyManager.JackPotWalletAddress];

        //sysFeePrize
        string sysFeePrize_UserID = ConfigurationManager.AppSettings[KeyManager.FeePrizeID];
        string sysFeePrize_Address = ConfigurationManager.AppSettings[KeyManager.FeePrizeWalletAddress];

        //sysFeeTxn
        string sysFeeTxn_UserID = ConfigurationManager.AppSettings[KeyManager.SysFeeTxnID];
        string sysFeeTxn_Address = ConfigurationManager.AppSettings[KeyManager.SysFeeTxnWalletAddress];

        //sysFeeTxn
        string sysGuaranteeFund_UserID = ConfigurationManager.AppSettings[KeyManager.sysGuaranteeFund_UserID];
        string sysGuaranteeFund_Address = ConfigurationManager.AppSettings[KeyManager.sysGuaranteeFund_Address];

        //api carcoin
        string ApiMyCarCoin = ConfigurationManager.AppSettings[KeyManager.ApiMyCarCoin];
        string HashToken = ConfigurationManager.AppSettings[KeyManager.HashToken];

        RedisCache objRedisCache = new RedisCache();
        [Dependency]
        protected IAwardMegaballServices repository { get; set; }

        [Dependency]
        protected IAwardServices repositoryAward { get; set; }
        [Dependency]
        protected IMemberServices repositoryMember { get; set; }

        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }
        [Dependency]
        protected ISystemConfigServices repositorySystemConfig { get; set; }
        [Dependency]
        protected IDrawServices drawServices { get; set; }

        [Dependency]
        protected IBookingMegaballServices bookingMegaball { get; set; }

        [Dependency]
        protected ITicketConfigServices ticketConfigServices { get; set; }

        int groupIDAdmin = 0;
        [Authorization]
        public ActionResult Index(string p)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            int totalRecord = 0;
            IEnumerable<SystemConfigBO> lstConfig = new List<SystemConfigBO>();
            IEnumerable<AwardMegaballBO> lstAwardMegaball = null;
            IEnumerable<AwardBO> lstAward = null;
            IEnumerable<MemberBO> lstMember = null;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {

                    int intPageSize = 10;
                    int start = 0, end = 10;
                    //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                    int page = 1;
                    try
                    {
                        if (p != null && p != string.Empty)
                        {
                            page = int.Parse(p);
                        }
                    }
                    catch
                    {

                    }
                    if (page > 1)
                    {
                        start = (page - 1) * intPageSize + 1;
                        end = (page * intPageSize);
                    }
                    lstAwardMegaball = repository.GetListAwardMegaball(start, end);

                    if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                    {
                        totalRecord = lstAwardMegaball.ElementAt(0).TotalRecord;
                    }
                    lstAward = repositoryAward.GetListAward(1, 100);
                    lstMember = repositoryMember.GetListMember(1, 100);


                    //int configID = 0;
                    //if (lstConfig != null && lstConfig.Count() > 0)
                    //{
                    lstConfig = repositorySystemConfig.GetListSystemConfig();
                    ViewData["ListSystemConfig"] = lstConfig;
                    foreach (SystemConfigBO sys in lstConfig)
                    {
                        ViewBag.IsAutoLottery = sys.IsAutoLottery;
                    }
                    //}

                    ///JackPot_Numberwiner

                    //DateTime fromDate = DateTime.Now;
                    //DateTime toDate = DateTime.Now;
                    //if (Session[SessionKey.SESSION_USERNAME] == null)
                    //{
                    //    Response.Redirect("/login");
                    //}

                    //int d = DateTime.Now.Day;
                    //int m = DateTime.Now.Month;
                    //int y = DateTime.Now.Year;
                    //string fromD = m + "/" + d + "/" + y + " 00:00:00";
                    //string toD = m + "/" + d + "/" + y + " 23:59:00";
                    //try
                    //{
                    //    fromDate = DateTime.Parse(fromD);
                    //    toDate = DateTime.Parse(toD);
                    //}
                    //catch
                    //{
                    //    fromD = d + "/" + m + "/" + y + " 00:00:00";
                    //    toD = d + "/" + m + "/" + y + " 23:59:00";
                    //    fromDate = DateTime.Parse(fromD);
                    //    toDate = DateTime.Parse(toD);
                    //}
                    //int totalRecordWinner = 0;
                    //IEnumerable<WinnerBO> lstWinnerAward = repository.GetListWinner(fromDate);
                    //ViewData[ViewDataKey.VIEWDATA_LISTWINERAWARD] = lstWinnerAward;
                    //if (lstWinnerAward != null && lstWinnerAward.Count() > 0)
                    //{
                    //    totalRecord = lstWinnerAward.ElementAt(0).TotalRecordWinner;
                    //}
                    //TempData[ViewDataKey.TEAMDATA_TOTALRECORDWINNER] = totalRecord;
                }
            }
            ViewData[ViewDataKey.VIEWDATA_LISTAWARDMEGABALL] = lstAwardMegaball;
            TempData[ViewDataKey.TEAMDATA_TOTALRECORD] = totalRecord;
            ViewData[ViewDataKey.VIEWDATA_LISTAWARD] = lstAward;
            ViewData[ViewDataKey.VIEWDATA_LISTMEMBER] = lstMember;
            return View();
        }
        #region SaveAwardMegaball
        //[HttpPost]
        //public string SaveAwardMegaball(int numberID, int awardID, int firstNumber, int secondNumber, int thirdNumber, int fourthNumber, int fivethNumber, int extraNumber)
        //{
        //    string result = string.Empty;
        //    AwardMegaballBO awardMegaball = new AwardMegaballBO();
        //    AwardBO award = new AwardBO();
        //    MemberBO member = new MemberBO();

        //    if (numberID > 0)
        //    {
        //        awardMegaball.NumberID = numberID;
        //        awardMegaball.AwardID = awardID;
        //        awardMegaball.IsActive = 1;
        //        awardMegaball.IsDelete = 0;
        //        awardMegaball.Priority = 1;
        //        awardMegaball.FirstNumber = firstNumber;
        //        awardMegaball.SecondNumber = secondNumber;
        //        awardMegaball.ThirdNumber = thirdNumber;
        //        awardMegaball.FourthNumber = fourthNumber;
        //        awardMegaball.FivethNumber = fivethNumber;
        //        awardMegaball.ExtraNumber = extraNumber;

        //        bool rs = repository.UpdateAwardMegaball(awardMegaball);
        //        if (rs)
        //        {
        //            result = "success";
        //        }
        //    }
        //    else if (numberID == 0)
        //    {
        //        awardMegaball.NumberID = numberID;
        //        awardMegaball.AwardID = awardID;
        //        awardMegaball.CreateDate = DateTime.Now;
        //        awardMegaball.IsActive = 1;
        //        awardMegaball.IsDelete = 0;
        //        awardMegaball.Priority = 1;
        //        awardMegaball.FirstNumber = firstNumber;
        //        awardMegaball.SecondNumber = secondNumber;
        //        awardMegaball.ThirdNumber = thirdNumber;
        //        awardMegaball.FourthNumber = fourthNumber;
        //        awardMegaball.FivethNumber = fivethNumber;
        //        awardMegaball.ExtraNumber = extraNumber;

        //        bool rs = repository.InsertAwardMegaball(awardMegaball);
        //        if (rs)
        //        {
        //            result = "success";
        //        }
        //    }
        //    return result;
        //}
        #endregion
        [Authorization]
        [HttpPost]
        public string LockAndUnLockAwardMegaball(int drawID, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            string result = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int statusNew = -1;
                    if (status == 1)
                    {
                        statusNew = 0;
                    }
                    else
                    {
                        statusNew = 1;
                    }
                    AwardMegaballBO objAwardMegaball = new AwardMegaballBO();
                    objAwardMegaball.IsActive = statusNew;
                    objAwardMegaball.DrawID = drawID;
                    string strParameterObj = Algorithm.EncryptionObjectRSA<AwardMegaballBO>(objAwardMegaball);
                    ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;
                    objResult = repository.LockAndUnlockAwardMegaball(objRsa2);
                    if (!objResult.IsError)
                    {

                        objRedisCache.RemoveCache(Common.KeyListAward);
                        objRedisCache.RemoveCache(Common.KeyListAward + "_" + 0 + 1);
                        objRedisCache.RemoveCache(Common.KeyListAward + "_" + 0 + 10);
                        int start = 1, end = 10, intPageSize = 10;
                        for (int i = 1; i < 10; i++)
                        {
                            start = (i - 1) * intPageSize + 1;
                            end = (i * intPageSize);
                            objRedisCache.RemoveCache(Common.KeyListAward + "_" + start + end);
                        }

                        result = ResultKey.RESULT_SUCCESS;
                    }
                }
            }
            return result;
        }

        [Authorization]
        [HttpGet]
        public string SearchAwardMegaball(string fromDate, string toDate, int page)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            string json = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    DateTime? toD = null;
                    DateTime? fromD = null;

                    StringBuilder builder = new StringBuilder();
                    StringBuilder builderPaging = new StringBuilder();
                    int intPageSize = 10;
                    int start = 0, end = 10;
                    int totalRecord = 0;
                    //Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                    #region edit
                    if (fromDate == string.Empty)
                    {
                        fromD = DateTime.Parse("01/01/1990");
                        toD = DateTime.Parse("01/01/1990");
                    }
                    else if (toDate == string.Empty)
                    {
                        string[] arrFrom = fromDate.Split('/');
                        if (arrFrom != null && arrFrom.Length > 0)
                        {
                            string m = arrFrom[0];
                            string d = arrFrom[1];
                            string y = arrFrom[2];
                            string dateFrom = m + "/" + d + "/" + y;
                            fromD = DateTime.Parse(dateFrom);
                        }

                        toDate = toD.ToString();
                        string[] arrTo = toDate.Split('/');
                        if (arrTo != null && arrTo.Length > 0)
                        {
                            string m = arrTo[0];
                            string d = arrTo[1];
                            string y = arrTo[2];
                            string dateTo = m + "/" + d + "/" + y;
                            toD = DateTime.Parse(dateTo);
                        }
                    }
                    else
                    {
                        string[] arrFrom = fromDate.Split('/');
                        if (arrFrom != null && arrFrom.Length > 0)
                        {
                            string m = arrFrom[0];
                            string d = arrFrom[1];
                            string y = arrFrom[2];
                            string dateFrom = m + "/" + d + "/" + y + " 00:00:00";
                            fromD = DateTime.Parse(dateFrom);

                        }
                        string[] arrTo = toDate.Split('/');
                        if (arrTo != null && arrTo.Length > 0)
                        {
                            string m = arrTo[0];
                            string d = arrTo[1];
                            string y = arrTo[2];
                            string dateTo = m + "/" + d + "/" + y + " 23:59:00";
                            toD = DateTime.Parse(dateTo);

                        }

                    }
                    #endregion

                    if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                    {
                        fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                        toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(fromDate))
                        {
                            if (string.IsNullOrEmpty(toDate))
                            {
                                toD = DateTime.Parse("01/01/1990");
                            }
                            else
                            {
                                fromD = DateTime.Parse("01/01/1990");
                                toD = BBH.Lotte.Shared.Utility.FromDateTime(toDate + " 23:59:00");
                            }
                        }
                        else
                        {
                            fromD = BBH.Lotte.Shared.Utility.FromDateTime(fromDate + " 00:00:00");
                            toD = DateTime.Now;
                        }
                    }
                    if (page >= 1)
                    {
                        start = (page - 1) * intPageSize + 1;
                        end = (page * intPageSize);
                    }
                    IEnumerable<AwardMegaballBO> lstAwardMegaball = repository.ListAllAwardMegaballBySearch(fromD, toD, start, end);
                    if (lstAwardMegaball != null && lstAwardMegaball.Count() > 0)
                    {
                        totalRecord = lstAwardMegaball.ElementAt(0).TotalRecord;
                        foreach (AwardMegaballBO awardMegaball in lstAwardMegaball)
                        {
                            string titleStatus = "Lock";
                            string status = "Active";
                            if (awardMegaball.IsActive == 0)
                            {
                                status = "InActive";
                                titleStatus = "Unlock";
                            }
                            MemberBO member = new MemberBO();


                            int num1, num2, num3, num4, num5, extranum;
                            num1 = awardMegaball.FirstNumber;
                            num2 = awardMegaball.SecondNumber;
                            num3 = awardMegaball.ThirdNumber;
                            num4 = awardMegaball.FourthNumber;
                            num5 = awardMegaball.FivethNumber;
                            extranum = awardMegaball.ExtraNumber;

                            string NumberWiner = num1.ToString() + " " + num2.ToString() + " " + num3.ToString() + " " + num4.ToString() + " " + num5.ToString();

                            builder.Append("<tr id=\"trGroup_" + awardMegaball.NumberID + "\" class=\"none-top-border\">");
                            builder.Append("<td class=\"center\">" + awardMegaball.NumberID + "</td>");
                            builder.Append("<td class=\"center\">" + awardMegaball.AwardNameEnglish + "</td>");
                            builder.Append("<td class=\"center\">");
                            builder.Append("<span class=\"circle1\">" + num1 + "</span> ");
                            builder.Append("<span class=\"circle1\">" + num2 + "</span>");
                            builder.Append("<span class=\"circle1\">" + num3 + "</span>");
                            builder.Append("<span class=\"circle1\">" + num4 + "</span>");
                            builder.Append("<span class=\"circle1\">" + num5 + "</span> ");
                            builder.Append("&nbsp; &nbsp;<span class=\"circle_ball\">" + extranum + "</span></td>");
                            builder.Append("<td class=\"center\">" + awardMegaball.JackPot + "</td>");
                            //builder.Append("<td class=\"center\"><p class=\"circle\">"+NumberWiner+ "</p> &nbsp<span class=\"circle1\">"+extranum+"</span></td>");
                            builder.Append("<td class=\"center\">" + awardMegaball.CreateDate + "</td>");
                            builder.Append("<td class=\"center\">");
                            builder.Append("<span class=\"label-success label label-default\">" + status + "</span>");
                            builder.Append("</td>");
                            builder.Append("<td class=\"center\">");
                            builder.Append("<img src=\"/Images/loading.gif\" id=\"imgLoading_" + awardMegaball.NumberID + "\" style=\"position: absolute; display:none ; float: right; width: 30px; margin-left: 45px; \">");

                            builder.Append("<a class=\"btn btn-info btn-sm\" title=\"" + awardMegaball.NumberID + "\" href=\"javascript:void(0)\" onclick=\"ShowPopUpEditAwardMegaball('" + awardMegaball.NumberID + "','" + awardMegaball.AwardID + "','" + awardMegaball.GenCode + "','" + awardMegaball.AwardNameEnglish + "','" + awardMegaball.JackPot + "')\" data-toggle=\"modal\" data-target=\"#standardModal\"><i class=\"glyphicon glyphicon-edit icon-white\"></i>Edit</a>");

                            builder.Append("<a class=\"btn btn-warning btn-sm\" href=\"javascript:void(0)\" onclick=\"LockAndUnlockAwardMegaball('" + awardMegaball.NumberID + "','" + awardMegaball.IsActive + "')\" title=\"" + titleStatus + "\">");
                            if (awardMegaball.IsActive == 1)
                            {
                                builder.Append("<i class=\"glyphicon glyphicon-ok\"></i>");
                            }
                            else
                            {
                                builder.Append("<i class=\"glyphicon glyphicon-remove\"></i>");

                            }
                            builder.Append("</a>");
                            builder.Append("</td>");
                            builder.Append("</tr>");
                        }

                        int totalPage = totalRecord / intPageSize;
                        int balance = totalRecord % intPageSize;
                        if (balance != 0)
                        {
                            totalPage += 1;
                        }

                        if (totalPage > 1)
                        {

                            for (int m = 1; m <= totalPage; m++)
                            {

                                if (m == page)
                                {
                                    builderPaging.Append("<li class=\"active\"><a href=\"javascript:void(0)\" >" + m + "</a></li>");
                                }
                                else
                                {
                                    builderPaging.Append("<li ><a href=\"javascript:void(0)\" onclick=\"PagingSearchAwardMegaball('" + m + "')\" >" + m + "</a></li>");

                                }
                            }
                        }

                    }
                    else
                    {
                        builder.Append("<tr><td colspan=\"5\">No result found</td></tr>");
                    }

                    SearchObj obj = new SearchObj();
                    obj.ContentResult = builder.ToString();
                    obj.PagingResult = builderPaging.ToString();
                    obj.Totalrecord = totalRecord;
                    json = JsonConvert.SerializeObject(obj);
                }
            }
            return json;
        }

        [HttpPost]
        [Authorization]
        public JsonResult SaveAwardResultNumber(int numberID, int awardID, string strBlockHash/*, float jackPot*/)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }

            BBC.CWallet.Share.BBCLoggerManager.Debug("SaveAwardResultNumber: ");

            //string strTime = HoureStart + ":" + MinuteStart + " - " + HoureEnd + ":" + MinuteEnd;
            //string strDateOpen = DayOpen;
            string isSuccess = string.Empty;
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = RDUserAdmin.UserName;//(string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            //if (CheckTimeAwardMegaball())
            //{

            BBC.CWallet.Share.BBCLoggerManager.Debug("SaveAwardResultNumber -> CheckAuthenticate: " + objResult.ToJsonString());

            DrawBO objDraw = drawServices.GetNewestDrawID();

            BBC.CWallet.Share.BBCLoggerManager.Debug("SaveAwardResultNumber -> GetNewestDrawID: " + objDraw.ToJsonString());
            if (objDraw != null && objDraw.DrawID > 0)
            {
                if (!objResult.IsError)
                {
                    objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                    if (objRsa.Token != null && objRsa.Token != string.Empty)
                    {

                        List<int> lstResultNumber = new List<int>();

                        char[] array = strBlockHash.ToCharArray();
                        if (array.Length > 0)
                        {
                            List<string> lst = new List<string>();
                            string str = string.Empty;
                            int k = 0;
                            for (int i = array.Length - 1; i >= 0; i--)
                            {
                                str = str + array[i];
                                k++;
                                if (k % 4 == 0)
                                {
                                    string number = ReverseNumber(str);
                                    k = 0;
                                    lst.Add(number);
                                    str = string.Empty;
                                }
                            }
                            if (lst != null && lst.Count > 0)
                            {
                                foreach (var item in lst)
                                {
                                    try
                                    {
                                        int test = Convert.ToInt32(item, 16);
                                        if (lstResultNumber.Count < 5)
                                        {
                                            test = test % int.Parse(Number49);
                                            if (!lstResultNumber.Contains(test))
                                            {
                                                if (test == 0)
                                                {
                                                    test = int.Parse(Number49);
                                                }
                                                lstResultNumber.Add(test);
                                            }
                                        }
                                        else if (lstResultNumber.Count == 5)
                                        {
                                            test = test % int.Parse(Number26);
                                            if (test == 0)
                                            {
                                                test = int.Parse(Number26);
                                            }
                                            lstResultNumber.Add(test);
                                            break;
                                        }
                                    }
                                    catch { }
                                }
                            }
                        }
                        AwardMegaballBO awardMegaball = new AwardMegaballBO();

                        if (strBlockHash == string.Empty || strBlockHash.Trim().Length == 0)
                        {
                            isSuccess = ResultKey.RESULT_FAILES;
                        }
                        else
                        {
                            if (numberID <= 0)
                            {
                                if (lstResultNumber != null && lstResultNumber.Count() >= 6)
                                {
                                    DateTime dtToday = DateTime.Now.ToUniversalTime();
                                    try
                                    {
                                        awardMegaball.NumberID = numberID;
                                        awardMegaball.AwardID = awardID;
                                        awardMegaball.CreateDate = dtToday;
                                        awardMegaball.IsActive = 1;
                                        awardMegaball.IsDelete = 0;
                                        awardMegaball.Priority = 1;
                                        awardMegaball.JackPot = decimal.Parse(JackPotDefault);
                                        awardMegaball.DrawID = objDraw.DrawID;
                                        try
                                        {
                                            awardMegaball.FirstNumber = lstResultNumber[0];
                                            awardMegaball.SecondNumber = lstResultNumber[1];
                                            awardMegaball.ThirdNumber = lstResultNumber[2];
                                            awardMegaball.FourthNumber = lstResultNumber[3];
                                            awardMegaball.FivethNumber = lstResultNumber[4];
                                            awardMegaball.ExtraNumber = lstResultNumber[5];
                                            awardMegaball.GenCode = strBlockHash;
                                        }
                                        catch { }
                                        string strParameterObj = Algorithm.EncryptionObjectRSA<AwardMegaballBO>(awardMegaball);
                                        ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                        objRsa2.Token = objRsa.Token;
                                        objRsa2.UserName = objConfig.UserName;
                                        objResult = repository.InsertAwardMegaball(objRsa2);
                                        if (!objResult.IsError)
                                        {
                                            if (objResult.MessageDetail != "CreateDate Award Megaball Exist")
                                            {
                                                isSuccess = ResultKey.RESULT_FAILES;
                                                
                                                //transfer from Jackpot to JackPotWinning success

                                                TicketWinningBO objTicket = new TicketWinningBO()
                                                {
                                                    FirstNumberAward = awardMegaball.FirstNumber,
                                                    SecondNumberAward = awardMegaball.SecondNumber,
                                                    ThirdNumberAward = awardMegaball.ThirdNumber,
                                                    FourthNumberAward = awardMegaball.FourthNumber,
                                                    FivethNumberAward = awardMegaball.FivethNumber,
                                                    ExtraNumberAward = awardMegaball.ExtraNumber,
                                                    AwardDate = awardMegaball.CreateDate,
                                                    DrawID = awardMegaball.DrawID,
                                                    WalletID = Convert.ToInt32(WalletID),
                                                    ClientID = short.Parse(ClientID),
                                                    SystemID = short.Parse(SystemID),
                                                    SysJackpot_UserID = sysJackpot_UserID,
                                                    SysJackpot_Address = sysJackpot_Address,
                                                    SysAward_UserID = sysAward_UserID,
                                                    SysLoan_UserID = sysLoan_UserID,
                                                    SysLoan_Address = sysLoan_Address,
                                                    SysGuaranteeFund_UserID = sysGuaranteeFund_UserID,
                                                    SysGuaranteeFund_Address = sysGuaranteeFund_Address, 
                                                    Email = ""
                                                };

                                                //Revenue
                                                Revenue(dtToday, objTicket);

                                                //timezone America/New York (CreatedDate of table AwardMegaballLog)
                                                DateTime dtmCreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Eastern Standard Time");

                                                //Insert TicketWinning (lst TicketWinning) and AwardMegaballLog (call API BOS)
                                                int intWinnerJackPotCount = 0;

                                                objResult = repository.InsertWinningNumber(objTicket, dtmCreatedDate, ref intWinnerJackPotCount);
                                                if (objResult != null && (!objResult.IsError || (objResult.IsError && string.IsNullOrEmpty(objResult.MessageDetail))))
                                                {
                                                    BBC.CWallet.Share.BBCLoggerManager.Debug("InsertWinningNumber: " + objResult.ToJsonString());

                                                    ////transfer from sysJackPot to sysJackPotWinning
                                                    //TransactionInfo objTransactionInfo = null;
                                                    //if (dcJackPot > 0 && intWinnerJackPotCount > 0)
                                                    //{
                                                    //    //have to be at least 1 winner jackpot (intWinnerJackPotCount > 0) and JackPot value > 0 
                                                    //    //transfer from sysJackPot to sysJackPotWinning
                                                    //    objTransactionInfo = TransferJackpotToAward(dcJackPot);
                                                    //}

                                                    //BBC.CWallet.Share.BBCLoggerManager.Debug("objTransactionInfo: " + objTransactionInfo.ToJsonString());
                                                    //BBC.CWallet.Share.BBCLoggerManager.Debug("dcJackPot: " + dcJackPot + ", intWinnerJackPotCount: " + intWinnerJackPotCount);

                                                    List<TicketWinningOut> lstAwardMegaballLog = JsonConvert.DeserializeObject<List<TicketWinningOut>>(Convert.ToString(objResult.ObjResultData));

                                                    //decimal jackpot = GetBalance(sysJackpot_UserID);
                                                    //decimal totalwon = GetTotalWonByRaw(lstAwardMegaballLog);
                                                   // UpdateJackpot(jackpot, objDraw.DrawID, totalwon);
                                                    InsertNewDraw(dtToday);
                                                    objRedisCache.RemoveCache(Common.KeyListAward);
                                                    objRedisCache.RemoveCache(Common.KeyListAward + "_" + 0 + 1);
                                                    objRedisCache.RemoveCache(Common.KeyListAward + "_" + 0 + 10);
                                                    int start = 1, end = 10, intPageSize = 10;
                                                    for (int i = 1; i < 10; i++)
                                                    {
                                                        start = (i - 1) * intPageSize + 1;
                                                        end = (i * intPageSize);
                                                        objRedisCache.RemoveCache(Common.KeyListAward + "_" + start + end);
                                                    }

                                                    if (!string.IsNullOrEmpty(objResult.MessageDetail) && objResult.MessageDetail != "Success")
                                                    {
                                                        isSuccess = ResultKey.RESULT_EXISTCREATEDATE;
                                                    }

                                                    //update status success
                                                    isSuccess = ResultKey.RESULT_SUCCESS;

                                                    //Insert AwardMegaball success 
                                                    //call api update value Award BOS

                                                    //ContentResponse objOut = new ContentResponse();
                                                    //objOut.id = 0;
                                                    //objOut.email = RDUser.Email;
                                                    //float fValue = 5.4f;


                                                    UpdateAwardMegaballBOSApi(lstAwardMegaballLog);

                                                }
                                            }
                                            else
                                            {
                                                isSuccess = "CreateDate Award Megaball Exist";
                                            }
                                        }
                                        else
                                        {
                                            isSuccess = ResultKey.RESULT_FAILES;
                                        }
                                    }
                                    catch (Exception objEx)
                                    {
                                        BBC.CWallet.Share.BBCLoggerManager.Error(objEx);
                                        isSuccess = ResultKey.RESULT_FAILES;
                                    }
                                }
                                else
                                {
                                    isSuccess = ResultKey.RESULT_FAILES;
                                }
                            }
                            else if (numberID > 0)
                            {
                                if (lstResultNumber != null && lstResultNumber.Count() > 0)
                                {
                                    awardMegaball.NumberID = numberID;
                                    awardMegaball.AwardID = awardID;
                                    awardMegaball.CreateDate = DateTime.Now;
                                    awardMegaball.IsActive = 1;
                                    awardMegaball.IsDelete = 0;
                                    awardMegaball.Priority = 1;
                                    try
                                    {
                                        awardMegaball.FirstNumber = lstResultNumber[0];
                                        awardMegaball.SecondNumber = lstResultNumber[1];
                                        awardMegaball.ThirdNumber = lstResultNumber[2];
                                        awardMegaball.FourthNumber = lstResultNumber[3];
                                        awardMegaball.FivethNumber = lstResultNumber[4];
                                        awardMegaball.ExtraNumber = lstResultNumber[5];
                                        awardMegaball.GenCode = strBlockHash;
                                        awardMegaball.JackPot = decimal.Parse(JackPotDefault);
                                    }
                                    catch { }
                                    string strParameterObj = Algorithm.EncryptionObjectRSA<AwardMegaballBO>(awardMegaball);
                                    ModelRSA objRsa2 = new ModelRSA(strParameterObj);
                                    objRsa2.Token = objRsa.Token;
                                    objRsa2.UserName = objConfig.UserName;
                                    objResult = repository.UpdateAwardMegaball(objRsa2);
                                    if (!objResult.IsError)
                                    {
                                        objRedisCache.RemoveCache(Common.KeyListAward);
                                        objRedisCache.RemoveCache(Common.KeyListAward + "_" + 0 + 1);
                                        objRedisCache.RemoveCache(Common.KeyListAward + "_" + 0 + 10);
                                        int start = 1, end = 10, intPageSize = 10;
                                        for (int i = 1; i < 10; i++)
                                        {
                                            start = (i - 1) * intPageSize + 1;
                                            end = (i * intPageSize);
                                            objRedisCache.RemoveCache(Common.KeyListAward + "_" + start + end);
                                        }
                                        isSuccess = ResultKey.RESULT_SUCCESS;
                                    }
                                    else
                                    {
                                        isSuccess = ResultKey.RESULT_FAILES;
                                    }
                                }
                                else
                                {
                                    isSuccess = ResultKey.RESULT_FAILES;
                                }
                            }
                        }

                    }
                }
            }
            else
            {
                isSuccess = ResultKey.RESULT_CHECKTIMEOPENCLOSE;
            }
            //}
            //else
            //{
            //    isSuccess = ResultKey.RESULT_CHECKTIMEOPENCLOSE;
            //}
            return Json(new { isSuccess = isSuccess/*, strTime = strTime, strDateOpen = strDateOpen */}, JsonRequestBehavior.AllowGet);
        }

        //private decimal GetTotalWonByRaw(List<TicketWinningOut> lstAwardMegaballLog)
        //{
        //    decimal totalwon = 0;
        //    if (lstAwardMegaballLog != null && lstAwardMegaballLog.Count > 0)
        //    {
        //        totalwon = lstAwardMegaballLog.Sum(c => Convert.ToDecimal(c.value));
        //    }
        //    return totalwon;
        //}

        private DateTime GetNextDayNewDraw(DateTime dtNow)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            int intDaysPlus = 0;
            Type weekdays = typeof(Days);
            string dayofWeek = dtNow.DayOfWeek.ToString();
            int intDays = (int)Enum.Parse(weekdays, dayofWeek); ;
            string strDayCheck = DayOpen;//key config
            string[] arrDayCheck = strDayCheck.Split(',');
            int[] arrintDayCheck;
            if (arrDayCheck.Length > 0)
            {
                arrintDayCheck = new int[arrDayCheck.Length];
                for (int j = 0; j < arrDayCheck.Length; j++)
                {
                    arrintDayCheck[j] = (int)Enum.Parse(weekdays, arrDayCheck[j].ToString());
                }
                Array.Sort(arrintDayCheck);

                if (arrintDayCheck.Length > 0)
                {
                    if (arrintDayCheck.Length == 1)
                    {
                        intDaysPlus = 7;
                    }
                    else
                    {
                        if (intDays == arrintDayCheck[arrDayCheck.Length - 1])
                        {
                            if (intDays == 7)
                            {
                                intDaysPlus = arrintDayCheck[0];
                            }
                            else
                            {
                                intDaysPlus = arrintDayCheck[0] + (7 - arrintDayCheck[arrDayCheck.Length - 1]);
                            }
                        }
                        else
                        {
                            for (int i = 0; i < arrintDayCheck.Length; i++)
                            {
                                if (intDays == arrintDayCheck[i])
                                {
                                    intDaysPlus = arrintDayCheck[i + 1] - arrintDayCheck[i];
                                }
                            }
                        }
                    }
                }
            }

            return dtNow.AddDays(intDaysPlus);
        }

        

        public void UpdateAwardMegaballBOSApi(List<TicketWinningOut> lstAwardMegaballLog)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            Task.Run(() =>
            {
                int intTimeCount = 0;
                while (intTimeCount < Convert.ToInt32(ConfigurationManager.AppSettings[KeyManager.RepeatTimes]))
                {
                    //only allow resend 3 time if send api failed
                    //if (objInput.id == 0)
                    //{
                    //    //if id = 0: insert db lottery
                    //    DateTime dtmCreatedDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now, TimeZoneInfo.Local.Id, "Eastern Standard Time");
                    //    objInput.id = repository.InsertAwardMegaballLog(objInput.email, fValue, dtmCreatedDate);

                    //    objInput.value = Convert.ToString(fValue);
                    //    objInput.date_time = dtmCreatedDate.ToString("yyyy/MM/dd HH:mm:ss");//timezone America/NewYork

                    //    lstInput.Add(objInput);
                    //}

                    if (lstAwardMegaballLog != null && lstAwardMegaballLog.Count > 0)
                    {
                        //insert db lottery success -> send api update ticket
                        string strInputLog = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(lstAwardMegaballLog);
                        BBC.CWallet.Share.BBCLoggerManager.Info("SendUpdateAwardMegaballBOSApi -> Input: lstInput: " + strInputLog + ", Times: " + (intTimeCount + 1));

                        ApiUpdateTicketResponse objOut = SendUpdateAwardMegaballBOSApi(strInputLog);

                        BBC.CWallet.Share.BBCLoggerManager.Info("SendUpdateAwardMegaballBOSApi -> Output: Output: " + (objOut != null ? objOut.ToString() : "null"));

                        if (objOut != null && objOut.sucess_list != null)
                        {
                            string strDataSuccessLog = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(objOut.sucess_list);
                            BBC.CWallet.Share.BBCLoggerManager.Info("SendUpdateAwardMegaballBOSApi -> Output: result: " + objOut.result + ", objOut.sucess_list: " + strDataSuccessLog);
                        }

                        if (objOut != null && objOut.result == "1")
                        {
                            //success
                            break;
                        }
                    }
                    intTimeCount++;
                }
            });
        }

        public ApiUpdateTicketResponse SendUpdateAwardMegaballBOSApi(string strInputLog)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {
                //System.IO.File.WriteAllLines(@"C:\inetpub\wwwroot\cmslottery\Logs\2.txt", new string[1] { "1" });
                //BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("SendUpdateAwardMegaballBOSApi 1: " + (strInputLog));
                BBC.CWallet.Share.BBCLoggerManager.Info("SendUpdateAwardMegaballBOSApi 1: " + (strInputLog));

                var client = new RestClient(ApiMyCarCoin);

                var request = new RestRequest("/update-award", Method.POST);
                request.AddParameter("api_key", HashToken);
                request.AddParameter("data", strInputLog);

                IRestResponse<ApiUpdateTicketResponse> response = client.Execute<ApiUpdateTicketResponse>(request);
                return response.Data;
            }
            catch (Exception objEx)
            {
                BBC.CWallet.Share.BBCLoggerManager.Info("SendUpdateAwardMegaballBOSApi: objEx: " + objEx.Message);
                //System.IO.File.WriteAllLines(@"C:\inetpub\wwwroot\cmslottery\Logs\1.txt", new string[1] { objEx.Message });
                //BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("SendUpdateAwardMegaballBOSApi: objEx: " + objEx.Message);
                return null;
            }
        }

        private string ReverseNumber(string number)
        {
            string str = string.Empty;
            char[] array = number.ToCharArray();
            if (array.Length > 0)
            {
                for (int i = array.Length - 1; i >= 0; i--)
                {
                    str = str + array[i];
                }
            }
            return str;
        }
        //private bool CheckTimeAwardMegaball()
        //{
        //    Type weekdays = typeof(Days);
        //    string dayofWeek = DateTime.Now.ToUniversalTime().DayOfWeek.ToString();
        //    int intDays = (int)Enum.Parse(weekdays, dayofWeek); ;
        //    string strDayCheck = DayOpen;//key config
        //    string[] arrDayCheck = strDayCheck.Split(',');
        //    int[] arrintDayCheck;
        //    if (arrDayCheck.Length > 0)
        //    {
        //        arrintDayCheck = new int[arrDayCheck.Length];
        //        for (int j = 0; j < arrDayCheck.Length; j++)
        //        {
        //            arrintDayCheck[j] = (int)Enum.Parse(weekdays, arrDayCheck[j].ToString());
        //        }
        //        Array.Sort(arrintDayCheck);

        //        if (arrintDayCheck.Length > 0)
        //        {
        //            for (int k = 0; k < arrintDayCheck.Length; k++)
        //            {
        //                if (intDays == arrintDayCheck[k])
        //                {

        //                    DateTime dtAwardMegaball = DateTime.Now.ToUniversalTime();
        //                    DateTime dtOpenDate = DateTime.Now.ToUniversalTime().Date.Add(new TimeSpan(HoureEnd, MinuteEnd, 0));
        //                    DateTime dtClosedDate = DateTime.Now.ToUniversalTime().Date.Add(new TimeSpan(HoureStart, MinuteStart, 0));

        //                    DateTime dtBookingTemp = new DateTime(dtAwardMegaball.Year, dtAwardMegaball.Month, dtAwardMegaball.Day, dtAwardMegaball.Hour, dtAwardMegaball.Minute, dtAwardMegaball.Second);

        //                    DateTime dtOpenDateTemp = new DateTime(dtOpenDate.Year, dtOpenDate.Month, dtOpenDate.Day, dtOpenDate.Hour, dtOpenDate.Minute, dtOpenDate.Second);
        //                    DateTime dtClosedDateTemp = new DateTime(dtClosedDate.Year, dtClosedDate.Month, dtClosedDate.Day, dtClosedDate.Hour, dtClosedDate.Minute, dtClosedDate.Second);

        //                    int resultOpen = DateTime.Compare(dtBookingTemp, dtOpenDateTemp);
        //                    int resultClosed = DateTime.Compare(dtBookingTemp, dtClosedDateTemp);
        //                    if (resultClosed >= 0 && resultOpen <= 0)
        //                    {
        //                        return true;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    return false;
        //}
        private DateTime GetOpenDate(DateTime dtDateTime)
        {
            DateTime dtNow = DateTime.Now.ToUniversalTime();
            DrawBO objDrawID = drawServices.GetNewestDrawID();
            if (objDrawID != null)
            {
                dtNow = objDrawID.EndDate;
            }
            return dtNow;
        }

        private bool RevenueForSysMgmt(decimal value)//fix
        {
            try
            {
                TransferRequest objTransferRequest = new TransferRequest();
                objTransferRequest.UserId = sysAdmin_UserID;
                objTransferRequest.FromAddress = sysAdmin_Address;
                objTransferRequest.ClientID = short.Parse(ClientID);
                objTransferRequest.SystemID = short.Parse(SystemID);
                objTransferRequest.Amount = value;
                objTransferRequest.Description = "RevenueForSysMgmt(Oper) (" + PercentRevenueForOper + " %)";
                objTransferRequest.ToClientId = short.Parse(ClientID);
                objTransferRequest.ToSystemId = short.Parse(SystemID);
                objTransferRequest.ToUserId = sysMgmt_UserID;
                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                objTransferRequest.CreatedBy = RequestBy;

                TransactionInfo objTransactionInfo = new TransactionInfo();
                TransactionRepository objTransactionRepository = new TransactionRepository();

                objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                if (objTransactionInfo != null)
                {
                    WriteErrorLog.WriteLogsToFileText("Success (RevenueForSysMgmt) transactionID: " + objTransactionInfo.TransactionId);
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error (RevenueForSysMgmt) Message: " + ex.Message);
                return false;
            }
            return false;
        }
        private bool RevenueForAward(decimal value)
        {
            try
            {
                TransferRequest objTransferRequest = new TransferRequest();
                objTransferRequest.UserId = sysAdmin_UserID;
                objTransferRequest.FromAddress = sysAdmin_Address;
                objTransferRequest.ClientID = short.Parse(ClientID);
                objTransferRequest.SystemID = short.Parse(SystemID);
                objTransferRequest.Amount = value;
                objTransferRequest.Description = "RevenueForAward(Award) (" + PercentRevenueForAward + " %)";
                objTransferRequest.ToClientId = short.Parse(ClientID);
                objTransferRequest.ToSystemId = short.Parse(SystemID);
                objTransferRequest.ToUserId = sysAward_UserID;
                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                objTransferRequest.CreatedBy = RequestBy;

                TransactionInfo objTransactionInfo = new TransactionInfo();
                TransactionRepository objTransactionRepository = new TransactionRepository();

                objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                if (objTransactionInfo != null)
                {
                    WriteErrorLog.WriteLogsToFileText("Success (RevenueForAward) transactionID: " + objTransactionInfo.TransactionId);
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error (RevenueForAward) Message: " + ex.Message);
                return false;
            }
            return false;
        }
        private bool RevenueForJackpot(decimal value, TicketWinningBO objTicket)
        {
            try
            {
                int intJackPotCount = repository.CheckJackPotWinnerByDrawID(objTicket);
                if(intJackPotCount == 0)
                {
                    //not be jackpot winner
                    decimal AmountRevenueForGuaranteeFund = value * PercentRevenueForGuaranteeFund / 100;
                    decimal AmountPercentRevenueForJacpot60 = value * PercentRevenueForJacpot60 / 100;

                    TransferRequest objTransferRequest = new TransferRequest();
                    objTransferRequest.UserId = sysAdmin_UserID;
                    objTransferRequest.FromAddress = sysAdmin_Address;
                    objTransferRequest.ClientID = short.Parse(ClientID);
                    objTransferRequest.SystemID = short.Parse(SystemID);
                    objTransferRequest.Amount = AmountPercentRevenueForJacpot60;
                    objTransferRequest.Description = "RevenueForJackpot(Jackpot) (" + PercentRevenueForJacpot + " %)";
                    objTransferRequest.ToClientId = short.Parse(ClientID);
                    objTransferRequest.ToSystemId = short.Parse(SystemID);
                    objTransferRequest.ToUserId = sysJackpot_UserID;
                    objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                    objTransferRequest.CreatedBy = RequestBy;

                    TransactionInfo objTransactionInfo = new TransactionInfo();
                    TransactionRepository objTransactionRepository = new TransactionRepository();

                    objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                    if (objTransactionInfo != null)
                    {
                        WriteErrorLog.WriteLogsToFileText("Success BBH.Lotte.Project.CLP.GetLottoResult(RevenueForJackpot) transactionID: " + objTransactionInfo.TransactionId);
                    }

                    objTransferRequest = new TransferRequest();
                    objTransferRequest.UserId = sysAdmin_UserID;
                    objTransferRequest.FromAddress = sysAdmin_Address;
                    objTransferRequest.ClientID = short.Parse(ClientID);
                    objTransferRequest.SystemID = short.Parse(SystemID);
                    objTransferRequest.Amount = AmountRevenueForGuaranteeFund;
                    objTransferRequest.Description = "RevenueForJackpot(Jackpot) (" + PercentRevenueForJacpot + " %)";
                    objTransferRequest.ToClientId = short.Parse(ClientID);
                    objTransferRequest.ToSystemId = short.Parse(SystemID);
                    objTransferRequest.ToUserId = sysGuaranteeFund_UserID;
                    objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                    objTransferRequest.CreatedBy = RequestBy;

                    objTransactionInfo = new TransactionInfo();
                    objTransactionRepository = new TransactionRepository();

                    objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                    if (objTransactionInfo != null)
                    {
                        WriteErrorLog.WriteLogsToFileText("Success (RevenueForJackpot) transactionID: " + objTransactionInfo.TransactionId);
                    }
                }
                else {
                    TransferRequest objTransferRequest = new TransferRequest();
                    objTransferRequest.UserId = sysAdmin_UserID;
                    objTransferRequest.FromAddress = sysAdmin_Address;
                    objTransferRequest.ClientID = short.Parse(ClientID);
                    objTransferRequest.SystemID = short.Parse(SystemID);
                    objTransferRequest.Amount = value;
                    objTransferRequest.Description = "Transfer From sysAdmin -> sysJackPot: (" + value + " BTC)";
                    objTransferRequest.ToClientId = short.Parse(ClientID);
                    objTransferRequest.ToSystemId = short.Parse(SystemID);
                    objTransferRequest.ToUserId = sysJackpot_UserID;
                    objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                    objTransferRequest.CreatedBy = RequestBy;
                    
                    TransactionInfo objTransactionInfo = new TransactionRepository().Transfer(objTransferRequest);
                    if (objTransactionInfo != null)
                    {
                        WriteErrorLog.WriteLogsToFileText("Transfer From sysAdmin -> sysJackPot Success: " + objTransactionInfo.TransactionId);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error (RevenueForJackpot) Message: " + ex.Message);
                return false;
            }
            return false;
        }
        private bool RevenueForProgressivePrize(decimal value)
        {
            try
            {
                TransferRequest objTransferRequest = new TransferRequest();
                objTransferRequest.UserId = sysAdmin_UserID;
                objTransferRequest.FromAddress = sysAdmin_Address;
                objTransferRequest.ClientID = short.Parse(ClientID);
                objTransferRequest.SystemID = short.Parse(SystemID);
                objTransferRequest.Amount = value;
                objTransferRequest.Description = "RevenueForProgressivePrize(ProgressivePrize)";
                objTransferRequest.ToClientId = short.Parse(ClientID);
                objTransferRequest.ToSystemId = short.Parse(SystemID);
                objTransferRequest.ToUserId = sysProgressivePrize_UserID;
                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                objTransferRequest.CreatedBy = RequestBy;

                TransactionInfo objTransactionInfo = new TransactionInfo();
                TransactionRepository objTransactionRepository = new TransactionRepository();

                objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                if (objTransactionInfo != null)
                {
                    WriteErrorLog.WriteLogsToFileText("Success (RevenueForProgressivePrize) transactionID: " + objTransactionInfo.TransactionId);
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error (RevenueForProgressivePrize) Message: " + ex.Message);
                return false;
            }
            return false;
        }
        private decimal GetBalance(string addressID)
        {
            decimal balance = 0;
            AddressInfoRequest objAddressInfoRequest = new AddressInfoRequest();
            objAddressInfoRequest.CurrencyCode = CurrencyCode.BTC;
            objAddressInfoRequest.UserID = addressID;
            objAddressInfoRequest.ClientID = short.Parse(ClientID);
            objAddressInfoRequest.SystemID = short.Parse(SystemID);
            objAddressInfoRequest.IsCreateAddr = false;
            objAddressInfoRequest.WalletId = short.Parse(WalletID);

            AddressRepository objAddressRepository = new AddressRepository();
            AddressInfo objAddressInfo = objAddressRepository.Get(objAddressInfoRequest);
            if (objAddressInfo != null && !string.IsNullOrEmpty(objAddressInfo.AddressId))
            {
                balance = objAddressInfo.Available;
            }
            return balance;
        }
        private bool Tranfer(string fromID, string fromAddress, string toID, decimal value)
        {
            try
            {
                TransferRequest objTransferRequest = new TransferRequest();
                objTransferRequest.UserId = fromID;
                objTransferRequest.FromAddress = fromAddress;
                objTransferRequest.ClientID = short.Parse(ClientID);
                objTransferRequest.SystemID = short.Parse(SystemID);
                objTransferRequest.Amount = value;
                objTransferRequest.Description = "Tranfer -> from: " + fromID + " to: " + toID;
                objTransferRequest.ToClientId = short.Parse(ClientID);
                objTransferRequest.ToSystemId = short.Parse(SystemID);
                objTransferRequest.ToUserId = toID;
                objTransferRequest.CurrencyCode = CurrencyCode.BTC;
                objTransferRequest.CreatedBy = RequestBy;

                TransactionInfo objTransactionInfo = new TransactionInfo();
                TransactionRepository objTransactionRepository = new TransactionRepository();

                objTransactionInfo = objTransactionRepository.Transfer(objTransferRequest);
                if (objTransactionInfo != null)
                {
                    WriteErrorLog.WriteLogsToFileText("Success (RevenueForSysMgmt) transactionID: " + objTransactionInfo.TransactionId);
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error (RevenueForSysMgmt) Message: " + ex.Message);
                return false;
            }
            return false;
        }
        private decimal GetAwardNotJackPot(int intDrawID)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            string strNumberCheck = string.Empty;
            string strExtraNumber = string.Empty;

            decimal totalWon = 0;
            try
            {
                AwardMegaballBO awardMegaballBO = repository.GetAwardMegabalByDrawID(intDrawID);
                if (awardMegaballBO != null && awardMegaballBO.DrawID == intDrawID)
                {
                    strNumberCheck = awardMegaballBO.FirstNumber + "," + awardMegaballBO.SecondNumber + "," + awardMegaballBO.ThirdNumber + "," + awardMegaballBO.FourthNumber + "," + awardMegaballBO.FivethNumber;
                    strExtraNumber = awardMegaballBO.ExtraNumber.ToString();
                }
                if (!string.IsNullOrEmpty(strNumberCheck) && !string.IsNullOrWhiteSpace(strExtraNumber))
                {
                    totalWon = bookingMegaball.GetAwardNotJackpot(strNumberCheck, strExtraNumber, intDrawID);
                }
            }
            catch (Exception ex)
            {
                totalWon = 0;
                WriteErrorLog.WriteLogsToFileText("Error (SumTotalWonNotJackpot) Message: " + ex.Message);
            }
            return totalWon;
        }
        private void Revenue(DateTime dtNow, TicketWinningBO objTicket)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {
                IEnumerable<TicketConfigBO> lstTicketConfigBO = ticketConfigServices.ListTicketConfig();
                decimal CoinValue = 0;
                if (lstTicketConfigBO != null && lstTicketConfigBO.Count() > 0)
                {
                    foreach (var item in lstTicketConfigBO)
                    {
                        if (item.CoinID.Trim() == "BTC")
                        {
                            CoinValue = item.CoinValues / item.NumberTicket;
                        }
                    }
                }
                if (CoinValue > 0)
                {
                    int totalTicket = 0;
                    decimal totalRevenue = 0;
                    DrawBO objDraw = drawServices.GetNewestDrawID();
                    if (objDraw != null && objDraw.DrawID > 0)
                    {
                        List<BookingMegaballBO> lstBookingMegaBallDaily = bookingMegaball.ListAllBookingMegaballByDraw(objDraw.DrawID).ToList();
                        if (lstBookingMegaBallDaily != null && lstBookingMegaBallDaily.Count() > 0)
                        {
                            List<BookingMegaballBO> lstBookingMegaBall = lstBookingMegaBallDaily.Where(x => x.IsFreeTicket == 0).ToList();
                            if (lstBookingMegaBall != null && lstBookingMegaBallDaily.Count() > 0)
                            {
                                totalTicket = lstBookingMegaBall.Count();
                                totalRevenue = totalTicket * CoinValue;
                                RevenueForSysMgmt((totalRevenue * PercentRevenueForOper) / 100);
                                decimal totalForAward = totalRevenue * PercentRevenueForAward / 100;
                                decimal AwardPayMent = GetAwardNotJackPot(objDraw.DrawID);
                                if (totalForAward >= AwardPayMent)
                                {
                                    //do some thing
                                    decimal valueTranfer = totalForAward - AwardPayMent;
                                    RevenueForAward(AwardPayMent);
                                    if (valueTranfer > 0)
                                    {
                                        RevenueForProgressivePrize(valueTranfer);
                                    }
                                    decimal jackpot = totalRevenue * PercentRevenueForJacpot / 100;
                                    RevenueForJackpot(jackpot, objTicket);
                                    decimal businessActivity = totalRevenue * PercentRevenueForBusinessActivity / 100;
                                    RevenueForSysMgmt(businessActivity);
                                }
                                else
                                {
                                    decimal totalJackpot = totalRevenue * PercentRevenueForJacpot / 100;
                                    if ((totalForAward + totalJackpot) >= AwardPayMent)
                                    {
                                        RevenueForAward(AwardPayMent);
                                        decimal jackpot = (totalJackpot + totalForAward) - AwardPayMent;
                                        if (jackpot > 0)
                                        {
                                            RevenueForJackpot(jackpot, objTicket);
                                        }
                                        decimal businessActivity = totalRevenue * PercentRevenueForBusinessActivity / 100;
                                        RevenueForSysMgmt(businessActivity);
                                    }
                                    else
                                    {
                                        decimal businessActivity = totalRevenue * PercentRevenueForBusinessActivity / 100;
                                        if ((totalForAward + totalJackpot + businessActivity) >= AwardPayMent)
                                        {
                                            RevenueForAward(AwardPayMent);
                                            decimal valuetranfer = totalForAward + totalJackpot + businessActivity - AwardPayMent;
                                            if (valuetranfer > 0)
                                            {
                                                RevenueForSysMgmt(valuetranfer);
                                            }
                                        }
                                        else
                                        {
                                            decimal balanceSysFeePrize = GetBalance(sysFeePrize_UserID);
                                            if ((totalForAward + totalJackpot + businessActivity + balanceSysFeePrize) >= AwardPayMent)
                                            {
                                                RevenueForAward(totalForAward + totalJackpot + businessActivity);
                                                decimal valuetranfer = AwardPayMent - (totalForAward + totalJackpot + businessActivity);
                                                if (valuetranfer > 0)
                                                {
                                                    Tranfer(sysFeePrize_UserID, sysFeePrize_Address, sysAward_UserID, valuetranfer);
                                                }
                                            }
                                            else
                                            {
                                                decimal balancesysFeeTxn = GetBalance(sysFeeTxn_UserID);
                                                if ((totalForAward + totalJackpot + businessActivity + balanceSysFeePrize + balancesysFeeTxn) >= AwardPayMent)
                                                {
                                                    RevenueForAward(totalForAward + totalJackpot + businessActivity);
                                                    if (balanceSysFeePrize > 0)
                                                    {
                                                        Tranfer(sysFeePrize_UserID, sysFeePrize_Address, sysAward_UserID, balanceSysFeePrize);
                                                    }
                                                    decimal valuetranfer = AwardPayMent - (totalForAward + totalJackpot + businessActivity + balanceSysFeePrize);
                                                    if (valuetranfer > 0)
                                                    {
                                                        Tranfer(sysFeeTxn_UserID, sysFeeTxn_Address, sysAward_UserID, valuetranfer);
                                                    }
                                                }
                                                else
                                                {
                                                    decimal balancesysLoan = GetBalance(sysLoan_UserID);
                                                    RevenueForAward(totalForAward + totalJackpot + businessActivity);
                                                    if (balanceSysFeePrize > 0)
                                                    {
                                                        Tranfer(sysFeePrize_UserID, sysFeePrize_Address, sysAward_UserID, balanceSysFeePrize);
                                                    }
                                                    if (balancesysFeeTxn > 0)
                                                    {
                                                        Tranfer(sysFeeTxn_UserID, sysFeeTxn_Address, sysAward_UserID, balanceSysFeePrize);
                                                    }
                                                    decimal valuetranfer = AwardPayMent - (totalForAward + totalJackpot + businessActivity + balanceSysFeePrize + balancesysFeeTxn);
                                                    if (valuetranfer > 0)
                                                    {
                                                        Tranfer(sysLoan_UserID, sysLoan_Address, sysAward_UserID, valuetranfer);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    WriteErrorLog.WriteLogsToFileText("AwardMegaballResultController-->(Revenue) CoinValue = 0");
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("AwardMegaballResultController-->(Revenue) Message :" + ex.Message);
            }
        }
        private void InsertNewDraw(DateTime dtNow)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            // insert new draw
            try
            {
                DateTime dtToday = dtNow;
                DrawBO objDraw = new DrawBO();
                objDraw = new DrawBO();
                objDraw.StartDate = dtToday.Date.Add(new TimeSpan(HoureStart, MinuteStart, 0));
                objDraw.EndDate = GetNextDayNewDraw(dtToday).Date.Add(new TimeSpan(HoureStart, MinuteStart, 0));
                objDraw.CreatedDate = dtToday;

                ObjResultMessage objRS = drawServices.InsertDraw(objDraw);
                if (!objRS.IsError)
                {
                    WriteErrorLog.WriteLogsToFileText("Insert new draws -> success");
                }
                else
                {
                    WriteErrorLog.WriteLogsToFileText("Insert new draws -> fails");
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("InsertNewDraw -> fails. Error message: " + ex.Message);
            }
        }
        private void UpdateJackpot(decimal jackpot, int intRawID, decimal totalwon)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupIDAdmin"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/withdraw");
            }
            try
            {
                ObjResultMessage objRS = repository.UpdateJackpotAwardMegaball(jackpot, intRawID, totalwon);
                if (!objRS.IsError)
                {
                    WriteErrorLog.WriteLogsToFileText("UpdateJackpot -> success");
                }
                else
                {
                    WriteErrorLog.WriteLogsToFileText("UpdateJackpot -> fails");
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog.WriteLogsToFileText("Error (UpdateJackpot) Message: " + ex.Message);
            }
        }

    }

}
