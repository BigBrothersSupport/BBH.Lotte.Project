﻿using BBC.Core.Common.Log;
using BBC.Core.Common.Utils;
using BBC.Core.Database;
using BBC.CWallet.ServiceListener.ServiceContract;
using BBH.Lotte.CLP.CMSAdmin.Models;
using BBH.Lotte.Domain;
using BBH.Lotte.Domain.Interfaces;
using BBH.Lotte.Shared;
using BBH.Lotte.CLP.Wallet.Repository;
using BBH.Lotte.CMSAdmin.Models;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
//using System.Runtime.CompilerServices;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BBH.Lotte.CLP.CMSAdmin.Controllers
{
    public class WithdrawController : Controller
    {
        string pathLog = ConfigurationManager.AppSettings["PathLog"];
        string masterKeyWan2Lot = ConfigurationManager.AppSettings["KeyWan2Lot"];
        string ClientID = ConfigurationManager.AppSettings["ClientID"];
        string TimeExpired = ConfigurationManager.AppSettings["TimeExpired"];
        string Linktestnet = ConfigurationManager.AppSettings["LinkTestnet"];
        string RippleTag = ConfigurationManager.AppSettings["RippleTag"];
        public string Bitcoin = ConfigurationManager.AppSettings["Bitcoin"];
        public string Carcoin = ConfigurationManager.AppSettings["Carcoin"];

        public string WalletId = ConfigurationManager.AppSettings["WalletId"];
        public string ClientId = ConfigurationManager.AppSettings["ClientId"];
        public string SystemId = ConfigurationManager.AppSettings["SystemId"];
        public string Limit = ConfigurationManager.AppSettings["PageSize"];

        TransactionRepository transaction = new TransactionRepository();

        [Dependency]
        protected ICommonServices repositoryCommon { get; set; }
    
        int groupIDAdmin = 0;

        [Models.Authorization]
        public ActionResult Index(string p)
        {
            //ViewBag.LinkTestnet = Linktestnet;
            //ViewBag.Bitcoin = Bitcoin;
            Int32.TryParse(ConfigurationManager.AppSettings["GroupID"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/");
            }

            int intPageSize = 10;
            int fromIndex = 1;
            //int start = 0, end = 10;
            if (Session[Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME] == null)
            {
                Response.Redirect("/login");
            }
            else
            {
                Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
                try
                {
                    if (p != null && p != "")
                    {
                        fromIndex = int.Parse(p);
                    }
                }
                catch
                {

                }
                //start = (fromIndex - 1) * intPageSize + 1;
                //end = (fromIndex * intPageSize);

            }
            return View(GenHTMLBody_TransactionBTCWallet(RDUser.MemberID, DateTime.Parse("01/01/1990"), DateTime.Now, fromIndex, intPageSize));
        }

        public List<string> GenHTMLBody_TransactionBTCWallet(int memberID, DateTime? dtbStart, DateTime? dtbEnd, int formIndex, int intPageSize)
        {

            List<string> lst = null;
            string strTotalRows = "0";
            int stt = 1;
            Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);
            if (formIndex == 0)
            {
                stt = formIndex;
            }
            else
            {
                stt = (formIndex * intPageSize - intPageSize) + 1;
            }
            try
            {
                TransactionFilterRequest transactionRequet = new TransactionFilterRequest();

                transactionRequet.ClientID = short.Parse(ClientId);
                transactionRequet.WalletId = short.Parse(WalletId);
                transactionRequet.SystemID = short.Parse(SystemId);
                transactionRequet.Limit = intPageSize;
                //transactionRequet.UserId = (Session[Shared.SessionKey.USERNAME] != null ? Session[Shared.SessionKey.USERNAME].ToString() : null);
                transactionRequet.TransactionType = TransactionType.Withdraw;
                transactionRequet.FromDate = dtbStart;
                transactionRequet.ToDate = dtbEnd;
                transactionRequet.FromIndex = formIndex-1;
                transactionRequet.CurrencyCode = CurrencyCode.BTC;

                CWalletList<TransactionInfo> transactionInfo = transaction.List(transactionRequet);
                StringBuilder strBOBj = new StringBuilder();

                if (transactionInfo != null && transactionInfo.Data != null && transactionInfo.Data.Count > 0)
                {
                    int i = 0;
                    string color = "#eee";
                    strTotalRows = transactionInfo.TotalRecord.ToString();
                    foreach (TransactionInfo item in transactionInfo.Data)
                    {
                        string Link = item.Tx;
                        if (i % 2 != 0)
                        {
                            color = "#eee";
                        }
                        else
                        {
                            color = "#fff";
                        }
                        const int maxlenght = 30;
                        string dot = "...";

                        try
                        {
                            if (Link.Length > maxlenght)
                            {
                                Link = Link.Substring(0, maxlenght) + "" + dot;
                            }


                        }
                        catch { }
                        string dateTime = item.CreatedDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime); //ToUniversalTime()
                        string strTransactionID = item.TransactionId.ToString();
                        //string strValueTransaction = (item.Amount > 0 ? string.Format("{0:0.00000000}", item.Amount) : "0");
                        string strValueTransaction = item.Amount.ToString("G29");
                        strBOBj.Append("<tr class='none-top-border'>");
                        strBOBj.Append("<td class='center'>" + stt + "</td>");
                        strBOBj.Append("<td class='text-center'>" + item.CreatedBy + " </td>");
                        strBOBj.Append("<td class='text-center'>" + dateTime + " UTC </td>");
                        strBOBj.Append("<td id=\"txtTransactionID\" class='text-center'>" + strTransactionID + "</td>");//onclick=\"ShowTransactionWalletDetail('" + strTransactionID + "','" + item.FromAddress + "','" + item.ToAddress + "','" + strValueTransaction + "','" + Link + "','" + item.CreatedDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "')\" data-toggle=\"modal\" data-target=\"#myModal\"
                        strBOBj.Append("<td class='text-center'>" + strValueTransaction + "</td>");
                        strBOBj.Append("<td class='text-center'><a href=\"" + Linktestnet + "" + item.Tx + "\" target =\"_blank\">" + Link + "</a></td>");

                        strBOBj.Append("<td class='text-center'>");
                        strBOBj.Append("<span class=\"label-success label label-default\">" + item.Status + "</span>");
                        strBOBj.Append("</td>");
                        strBOBj.Append("<td class='text-center'>");
                        strBOBj.Append("<img src='/Images/loading.gif' id='imgLoading_" + strTransactionID + "' style='position:absolute;display:none;float:right;width:30px;margin-left:45px;'>");
                        if (item.Status.ToString() == "Pending")
                        {
                            //string strObjectTransaction = JsonConvert.SerializeObject(item).Replace("\"","'").Replace(".","").Trim();
                            strBOBj.Append("<a class='btn btn-info btn-sm' href='javascript: void(0)' onclick=\"ConfirmWithdraw('" + strTransactionID + "')\" title='Confirm'>");
                            strBOBj.Append("<i class='glyphicon glyphicon-edit icon-white'></i>");
                            strBOBj.Append("Confirm");
                            strBOBj.Append("</a>");
                        }
                        //else if (item.Status.ToString() == "ReadyToProcess")
                        //{

                        //}
                        strBOBj.Append("</td>");

                        strBOBj.Append("</tr>");
                        stt++;
                        ViewData["transactionID"] = strTransactionID;
                    }
                }
                else
                {
                    strBOBj.Append("<tr><td colspan='8'>No Data</td></tr>");
                }
                lst = new List<string>();
                lst.Add(strBOBj.ToString());
                TempData["TotalRecord"] = int.Parse(strTotalRows);

                lst.Add(GenPaging(formIndex.ToString()));
                return lst;
            }
            catch (Exception objEx)
            {
                BBC.Core.Common.Log.WriteErrorLog.WriteLogsToFileText("objEx: " + (objEx.Message));
                return null;
            }
        }

        public string GenPaging(string p)
        {
            StringBuilder builder = new StringBuilder();
            int pageSize = 10, page = 0;

            try
            {
                if (p != null && p != "")
                {
                    page = int.Parse(p);
                }
            }
            catch
            {

            }
            try
            {
                pageSize = int.Parse(ConfigurationManager.AppSettings["NumberRecordPage"]);
            }
            catch
            {

            }
            string linkPage = "", nextLink = "", previewLink = "", firstLink = "", lastLink = "";
            firstLink = "?p=1";
            if (page <= 1)
            {
                previewLink = "?p=1";
            }
            else
            {
                previewLink = "?p=" + (page - 1).ToString();
            }

            builder.Append("<ul id=\"ulPaging\" class=\"pagination pagination-centered\">");

            int pagerank = 10;
            int next = 10;
            int prev = 1;


            if (TempData["TotalRecord"] != null)
            {
                int totalRecore = (int)TempData["TotalRecord"];
                int totalPage = totalRecore / pageSize;
                int balance = totalRecore % pageSize;
                if(balance ==0)
                {
                    totalPage = 0;
                }
                if (balance != 0)
                {
                    totalPage += 1;
                }
                if (page >= totalPage)
                {
                    nextLink = "?p=" + totalPage.ToString();
                }
                else
                {
                    nextLink = "?p=" + (page + 1).ToString();
                }
                int currentPage = page;
                var m = 1;
                if (totalPage > 10)
                {
                    if (page > pagerank + 1)
                    {
                        next = (page + pagerank) - 1;
                        m = page - pagerank;
                    }
                    if (page <= pagerank)
                    {
                        prev = (page - pagerank);
                        m = 1;
                    }
                    if (next > totalPage)
                    {
                        next = totalPage;
                    }
                    if (prev < 1)
                    {
                        prev = 1;
                    }
                }
                else
                {
                    next = totalPage;
                    prev = 1;
                }
                lastLink = "?p=" + totalPage;
                if (totalPage > 1)
                {
                    if (currentPage > 1)
                    {
                        // builder.Append("<li><a href=\"" + firstLink + "\"><i class=\"fa fa-angle-double-left\"></i></a></li>");
                        builder.Append("<li >");
                        builder.Append("<a href=\"" + previewLink + "\" class=\"page-link waves-effect waves-effect\" aria-label=\"Prev\">Prev</a>");

                        builder.Append(" </li>");
                    }
                }

                if (totalPage > 1)
                {

                    for (; m <= next; m++)
                    {
                        linkPage = "?p=" + m;
                        if (m == currentPage)
                        {
                            builder.Append("<li class=\"active\"><a href=\"" + linkPage + "\" >" + m + "</a></li>");
                        }
                        else
                        {
                            builder.Append("<li ><a href=\"" + linkPage + "\" >" + m + "</a></li>");

                            //if (m < currentPage)
                            //{
                            //    builder.Append("<li class=\"page-item\"><a href=\"" + linkPage + "\" class=\"page-link waves-effect waves-effect\">" + m + "</a></li>");
                            //}
                            //else
                            //{
                            //    builder.Append("<li><a rel=\"next\" href=\"" + linkPage + "\">" + m + "</a></li>");
                            //}
                        }
                    }
                }

                if (totalPage > 1)
                {
                    if (currentPage < totalPage)
                    {
                        builder.Append(" <li >");
                        builder.Append("<a href=\"" + nextLink + "\" class=\"page-link waves-effect waves-effect\" aria-label=\"Next\">Next</a>");
                        builder.Append("</li>");
                        //.Append("<li><a href=\"" + lastLink + "\"><i class=\"fa fa-angle-double-right\"></i></a></li>");
                    }
                }
            }


            builder.Append("</ul>");


            ViewData["Paging"] = builder.ToString();
            return builder.ToString();
        }

        [Models.Authorization]
        [HttpPost]
        public JsonResult UpdateStatusWithdraw(string transactionID)//WithDrawBTCModel objWithDrawBTCModel, string transactionID)
        {
            string email = Session[Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME].ToString();

            Int32.TryParse(ConfigurationManager.AppSettings["GroupID"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/");
            }
            try
            {
                TransactionRepository objTransactionRepository = new TransactionRepository();
                TransactionInfo obj = objTransactionRepository.Get(new TransactionInfoRequest()
                {
                    ClientID = short.Parse(ClientId),
                    SystemID = short.Parse(SystemId),
                    TransactionId = Guid.Parse(transactionID)
                });

                int isSuccess = 0;
                if (obj != null)
                {
                    WithdrawRequest objWithdrawRequest = new WithdrawRequest();
                    objWithdrawRequest.Amount = obj.Amount;
                    objWithdrawRequest.ClientID = obj.FromClientId;
                    objWithdrawRequest.CreatedBy = Lotte.CMSAdmin.Models.SessionKey.UserName;
                    objWithdrawRequest.CurrencyCode = obj.CurrencyCode;
                    objWithdrawRequest.Description = "Lottery With Draw";
                    objWithdrawRequest.FromAddress = obj.FromAddress;
                    objWithdrawRequest.SystemID = obj.FromSystemId;
                    objWithdrawRequest.ToAddress = obj.ToAddress;
                    objWithdrawRequest.UserId = obj.FromUserId;
                    objWithdrawRequest.WalletID = long.Parse(WalletId);
                    objWithdrawRequest.Fee = obj.Fee;
                    objWithdrawRequest.Status = TransactionStatus.ReadyToProcess;
                    objWithdrawRequest.TransactionId = obj.TransactionId;

                    TransactionInfo objTransactionInfo = objTransactionRepository.Withdraw(objWithdrawRequest);
                    if (objTransactionInfo != null && !string.IsNullOrEmpty(objTransactionInfo.TransactionId.ToString()))
                    {
                        isSuccess = 1;
                        if (objTransactionInfo.Amount == -1)
                        {
                            isSuccess = 2;
                        }
                        
                    }
                }
                return Json(new { success = isSuccess, email= email }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objEx)
            {
                WriteErrorLog.WriteLogsToFileText("Withdraw-->UpdateStatusWithdraw - Exception : " + objEx.Message);
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [Models.Authorization]
        [HttpGet]
        public string SearchWithdraw(string fromDate, string toDate, int page, int status)
        {
            Int32.TryParse(ConfigurationManager.AppSettings["GroupID"], out groupIDAdmin);
            if ((int)Session["GroupIDAdmin"] != groupIDAdmin)
            {
                Response.Redirect("/");
            }
            StringBuilder strBOBj = new StringBuilder();
            StringBuilder builderPaging = new StringBuilder();
            ModelRSA objRsaResult = null;
            string totalRecord = "0";

            //List<AwardWithdrawBO> lstAward = new List<AwardWithdrawBO>();
            IPConfigBO objConfig = new IPConfigBO();
            objConfig.IPAddress = Common.GetLocalIPAddress();
            objConfig.ServiceDomain = Common.GetHostServices("CommonSvc.svc");
            objConfig.UserName = (string)Session[BBH.Lotte.CMSAdmin.Models.SessionKey.SESSION_USERNAME];

            string strRsaParameter1 = Algorithm.EncryptionObjectRSA(objConfig);
            ModelRSA objRsa = new ModelRSA(strRsaParameter1);
            objRsa.UserName = objConfig.UserName;
            ObjResultMessage objResult = repositoryCommon.CheckAuthenticate(objRsa);
            if (!objResult.IsError)
            {
                objRsa.Token = Common.ParseObjectFromAsync<string>(objResult);
                if (objRsa.Token != null && objRsa.Token != string.Empty)
                {
                    int intPageSize = 10;
                    //int start = 0, end = 10;

                    int stt = 1;
                    if (page == 0)
                    {
                        stt = page;
                    }
                    else
                    {
                        stt = (page * intPageSize - intPageSize) + 1;
                    }

                    Int32.TryParse(ConfigurationManager.AppSettings["NumberRecordPage"], out intPageSize);

                    //if (page > 1)
                    //{
                    //    start = (page - 1) * intPageSize + 1;
                    //    end = (page * intPageSize);
                    //}

                    DateTime fromD = DateTime.Parse("01/01/1990");
                    DateTime toD = DateTime.Now;
                    if (fromDate != null && fromDate != string.Empty && fromDate != "")
                    {
                        fromD = Common.ConvertDateTime(fromDate);
                        if (toD != null)
                        {
                            toD = Common.ConvertDateTime(toDate);
                        }
                        else
                        {
                            toD = DateTime.Now;
                        }
                    }
                    DateTime awardDate = DateTime.Parse("01/01/1990");

                    TransactionFilterRequest transactionRequet = new TransactionFilterRequest();

                    transactionRequet.ClientID = short.Parse(ClientId);
                    transactionRequet.WalletId = short.Parse(WalletId);
                    transactionRequet.SystemID = short.Parse(SystemId);
                    transactionRequet.Limit = intPageSize;
                    //transactionRequet.UserId = (Session[Shared.SessionKey.USERNAME] != null ? Session[Shared.SessionKey.USERNAME].ToString() : null);
                    transactionRequet.TransactionType = TransactionType.Withdraw;
                    transactionRequet.FromDate = fromD;
                    transactionRequet.ToDate = toD;
                    transactionRequet.FromIndex = page -1;
                    transactionRequet.CurrencyCode = CurrencyCode.BTC;
                    transactionRequet.Status = (TransactionStatus)status;

                    string strParameterSearch = Algorithm.EncryptionObjectRSA<TransactionFilterRequest>(transactionRequet);
                    ModelRSA objRsa2 = new ModelRSA(strParameterSearch);
                    objRsa2.Token = objRsa.Token;
                    objRsa2.UserName = objConfig.UserName;

                    CWalletList<TransactionInfo> transactionInfo = transaction.List(transactionRequet);                   

                    if (transactionInfo != null && transactionInfo.Data != null && transactionInfo.Data.Count > 0)
                    {                      
                        totalRecord = transactionInfo.TotalRecord.ToString();
                        foreach (TransactionInfo item in transactionInfo.Data)
                        {
                            string dateTime = item.CreatedDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime);
                           string strTransactionID = item.TransactionId.ToString();
                            //string strValueTransaction = (item.Amount > 0 ? string.Format("{0:0.00000000}", item.Amount) : "0");
                            string strValueTransaction = item.Amount.ToString("G29");
                            string Link = item.Tx;
                            const int maxlenght = 30;
                            string dot = "...";

                            try
                            {
                                if (Link.Length > maxlenght)
                                {
                                    Link = Link.Substring(0, maxlenght) + "" + dot;
                                }


                            }
                            catch { }
                            strBOBj.Append("<tr class='none-top-border'>");
                            strBOBj.Append("<td class='center'>" + stt + "</td>");
                            strBOBj.Append("<td class='text-center'>" + item.CreatedBy + " </td>");
                            strBOBj.Append("<td class='text-center'>" + dateTime + " UTC </td>");
                            strBOBj.Append("<td id=\"txtTransactionID\" class='text-center'>" + strTransactionID + "</td>");//onclick=\"ShowTransactionWalletDetail('" + strTransactionID + "','" + item.FromAddress + "','" + item.ToAddress + "','" + strValueTransaction + "','" + Link + "','" + item.CreatedDate.ToString(BBH.Lotte.Shared.Utility.strFormmatDatetime) + "')\" data-toggle=\"modal\" data-target=\"#myModal\"
                            strBOBj.Append("<td class='text-center'>" + strValueTransaction + "</td>");
                            strBOBj.Append("<td class='text-center'><a href=\"" + Linktestnet + "" + item.Tx + "\" target =\"_blank\">" + Link + "</a></td>");

                            strBOBj.Append("<td class='text-center'>");
                            strBOBj.Append("<span class=\"label-success label label-default\">" + item.Status + "</span>");
                            strBOBj.Append("</td>");
                            strBOBj.Append("<td class='text-center'>");
                            strBOBj.Append("<img src='/Images/loading.gif' id='imgLoading_" + strTransactionID + "' style='position:absolute;display:none;float:right;width:30px;margin-left:45px;'>");
                            if (item.Status.ToString() == "Pending")
                            {
                                strBOBj.Append("<a class='btn btn-info btn-sm' href='javascript: void(0)' onclick=\"ConfirmWithdraw('" + strTransactionID + "')\" title='Confirm'>");
                                strBOBj.Append("<i class='glyphicon glyphicon-edit icon-white'></i>");
                                strBOBj.Append("Confirm");
                                strBOBj.Append("</a>");
                            }

                            strBOBj.Append("</td>");

                            strBOBj.Append("</tr>");
                            stt++;
                            ViewData["transactionID"] = strTransactionID;
                        }

                        if (int.Parse(totalRecord) > 0)
                        {
                            int pagerank = 5;
                            int next = 10;
                            int prev = 1; int nextPage = 0, previewPage = 0;
                            int totalPage = int.Parse(totalRecord) / intPageSize;
                            int balance = int.Parse(totalRecord) % intPageSize;
                            if (balance != 0)
                            {
                                totalPage += 1;
                            }
                            if (page >= totalPage)
                            {
                                nextPage = totalPage;
                            }
                            else
                            {
                                nextPage = (page + 1);
                            }
                            if (page <= 1)
                            {
                                previewPage = 1;
                            }
                            else
                            {
                                previewPage = page - 1;
                            }
                            int currentPage = page;
                            var m = 1;
                            if (totalPage > 10)
                            {
                                if (page > pagerank + 1)
                                {
                                    next = (page + pagerank) - 1;
                                    m = page - pagerank;
                                }
                                if (page <= pagerank)
                                {
                                    prev = (page - pagerank);
                                    m = 1;
                                }
                                if (next > totalPage)
                                {
                                    next = totalPage;
                                }
                                if (prev < 1)
                                {
                                    prev = 1;
                                }
                            }
                            else
                            {
                                next = totalPage;
                                prev = 1;
                            }

                            if (totalPage > 1)
                            {
                                if (currentPage > 1)
                                {

                                    builderPaging.Append("<li >");
                                    builderPaging.Append("<a onclick=\"PagingSearchWithdraw(" + previewPage + ")\" href=\"javascript:void(0)\" class=\"page-link waves-effect waves-effect\" aria-label=\"Previous\">Prev");
                                    //builderPaging.Append("<span aria-hidden=\"true\">");
                                    //builderPaging.Append("<i class=\"fa fa-angle-double-left\" area-hidden=\"hidden\"></i>");
                                    //builderPaging.Append("</span>");
                                    builderPaging.Append("</a>");
                                    builderPaging.Append(" </li>");
                                }
                            }

                            if (totalPage > 1)
                            {

                                for (; m <= next; m++)
                                {
                                    if (m == currentPage)
                                    {
                                        builderPaging.Append("<li class=\"page-item active\"><a class=\"page-link\">" + m + "</a></li>");
                                    }
                                    else
                                    {
                                        builderPaging.Append("<li onclick=\"PagingSearchWithdraw(" + m + ")\" class=\"page-item\"><a href=\"javascript:void(0)\" class=\"page-link\">" + m + "</a></li>");
                                    }
                                }
                            }

                            if (totalPage > 1)
                            {
                                if (currentPage < totalPage)
                                {
                                    builderPaging.Append("<li >");
                                    builderPaging.Append("<a onclick=\"PagingSearchWithdraw(" + nextPage + ")\" class=\"page-link waves-effect waves-effect\" aria-label=\"Next\"> Next");
                                    //builderPaging.Append("<span aria-hidden=\"true\">");
                                    //builderPaging.Append("<i class=\"fa fa-angle-double-right\" area-hidden=\"hidden\"></i>");
                                    //builderPaging.Append("</span>");
                                    builderPaging.Append("</a>");
                                    builderPaging.Append("</li>");
                                }
                            }
                        }
                    }
                    else
                    {
                        strBOBj.Append("<tr><td colspan='8'>No Data</td></tr>");
                    }
                }
            }

            SearchObj obj = new SearchObj();
            obj.ContentResult = strBOBj.ToString();
            obj.PagingResult = builderPaging.ToString();
            obj.Totalrecord = int.Parse(totalRecord);
            string json = JsonConvert.SerializeObject(obj);
            return json;
        }
    }
}