﻿function LockAndUnlockAward(awardID, status) {
    var confirmMessage = confirm('Are you sure lock this award?');
    if (!confirmMessage) {
        return false;
    }
    else {

        $.ajax({
            type: "post",
            async: true,
            url: "/Award/LockAndUnLockAward",
            data: { awardID: awardID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    window.location.reload();
                } else {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}

function ShowPopUpEditAward(awardID, awardNameEnglish, awardValue, awardFee, awardPercent, intPriority) {
    $('#hdAwardID').val(awardID);
    $('#txtAwardNameEnglish').val(awardNameEnglish);
    $('#txtAwardValue').val(awardValue);
    if (intPriority == 1) {
        //jackpot
        $('#txtAwardValue').attr('disabled', true);
    }
    else {
        $('#txtAwardValue').attr('disabled', false);
    }

    $('#txtAwardFee').val(awardFee);
    $('#txtpecent').val(awardPercent);

    $('#txtAwardFee').attr("readonly", true);
  
    $("#txtpecent").unbind('keyup').keyup(function () {
        if ($(this).val() >= 0 && $(this).val() <= 100 &&
            (($(this).val().indexOf('.') != -1 && $(this).val().length <= 4) ||
            ($(this).val().indexOf('.') <= -1 && $(this).val().length <= 2))) {
            var percent = awardValue * $(this).val() / 100;

            $("#txtAwardFee").val(percent.toFixed(8));
            }
            else {
                $(this).val($(this).val().substring(0,$(this).val().length-1));
                return false;
            }
        });
     
}

function ShowPopupCreateNewAward() {
    $('#hdAwardID').val(0);
    //$('#txtAwardName').val('');
    $('#txtAwardNameEnglish').val('');
    $('#txtAwardValue').val('');
    $('#txtAwardFee').val('');
    $('#txtpecent').val('');

    $('#txtAwardFee').attr("readonly", true);
    //$('#txtAwardName').attr("readonly", false);
    //$('#txtAwardNameEnglish').attr("readonly", false);
}

function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}
function onlyNumber(evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode == 45)
        return false;
    return true;
}
function OnChangeAwardFree(e) {
    var AmountBTC = $(e).val();
    var Check = AmountBTC / 3;
    var count = 0;
    var numberDecimal = AmountBTC.toString().split(".")[1];
    if (undefined !== numberDecimal) {
        count = numberDecimal.length;
    }
    if (count > 8) {
        $(e).val(AmountBTC.slice(0, -1));
    }
    if (Check.toString() == "NaN") {
        $(e).val(AmountBTC.slice(0, -1));
    }
    else {
        var FeeBTC = $("#hdFeeBTC").val();
        $("#TotalBTC").val((AmountBTC - FeeBTC).toFixed(8));
    }

}
function SaveAward() {
    var checkReg = true;
    var awardID = $('#hdAwardID').val();
    var awardNameEnglish = $('#txtAwardNameEnglish').val();
    var awardValue = $('#txtAwardValue').val();
    var awardFee = $('#txtAwardFee').val();
    var awardPercent = $('#txtpecent').val();
    if (awardNameEnglish == '') {
        $('#lbErrorAwardNameEnglish').text('English award name is required');
        $('#lbErrorAwardNameEnglish').css('display', '');
        checkReg = false;
    }
    if (awardValue == '') {
        $('#lbErrorAwardValue').text('Award value is required');
        $('#lbErrorAwardValue').css("display", "");
        checkReg = false;
    }
    //if (awardFee == '') {
    //    $('#lbErrorAwardFee').text('Award fee is required');
    //    $('#lbErrorAwardFee').css("display", "");
    //    checkReg = false;
    //}
    if (awardPercent == '') {
        $('#lbErrorAwardPecent').text('Award Percent is required');
        $('#lbErrorAwardPecent').css("display", "");
        checkReg = false;
    }

    if (!checkReg) {
        return false;
    }
    else {        
        $.ajax({
            type: "post",
            async: true,
            url: "/Award/SaveAward",
            data: { awardID: awardID, awardNameEnglish: awardNameEnglish, awardValue: awardValue, awardFee: awardFee, awardPercent: awardPercent },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d == 'failes')
                {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
                else if (d == 'AwardNameEnglishExit') {
                    $('#lbErrorAwardNameEnglish').text('Award Name English Exit');
                    $('#lbErrorAwardNameEnglish').css("display", "");
                    window.scrollTo(100, 100);
                }            


            },
            error: function (e) {

            }
        });
    }
}