﻿function LockAndUnlockCountry(countryID, status) {
    var confirmMessage = '';
    if (status == 0)
        confirmMessage = confirm('Are you sure lock this Country?');
    else
        confirmMessage = confirm('Are you sure unlock this Country?');
    if (!confirmMessage) {
        return false;
    }
    else {

        $.ajax({
            type: "post",
            async: true,
            url: "/Country/LockAndUnLockCountry",
            data: { countryID: countryID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    window.location.reload();
                } else {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}
//$(document).ready(function () {
//    var awardID = $('#hdAwardID').val();
//    if (awardID > 0) {
//        $('#txtAwardName').attr("readonly", true);
//        $('#txtAwardNameEnglish').attr("readonly", true);
//    }
//    else if (awardID==0) {
//        $('#txtAwardName').attr("readonly", false);
//        $('#txtAwardNameEnglish').attr("readonly", false);
//    }
//});

function ShowPopUpEditCountry(awardID, awardNameEnglish, awardValue, awardFee) {
    $('#hdCountryID').val(awardID);
    //$('#txtAwardName').val(awardName);
    $('#txtCountryName').val(awardNameEnglish);
    $('#txtPhoneSMS').val(awardValue);

    $('#txtCountryName').attr("readonly", true);
}

function ShowPopupCreateCountry() {
    $('#hdCountryID').val(0);
    $('#txtCountryName').val('');
    $('#txtPhoneSMS').val('');
    //$('#txtAwardName').attr("readonly", false);
    $('#txtCountryName').attr("readonly", false);
}

function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}
function onlyNumber(evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode == 45)
        return false;
    return true;
}

function SaveCountry() {
    var checkReg = true;
    var countryID = $('#hdCountryID').val();
    var countryName = $('#txtCountryName').val();
    var phoneCode = $('#txtPhoneSMS').val();

    if (countryName == '') {
        $('#lbErrorCountryName').text('Country name is required');
        $('#lbErrorCountryName').css('display', '');
        checkReg = false;
    }
    if (phoneCode == '') {
        $('#lbErrorPhoneSMS').text('Phone sms code is required');
        $('#lbErrorPhoneSMS').css("display", "");
        checkReg = false;
    }

    if (!checkReg) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/Country/SaveCountry",
            data: { countryID: countryID, countryName: countryName, phoneCode: phoneCode },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else if (d == 'failes') {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                }
                else if (d == 'CountryNameExist') {
                    $('#lbErrorCountryName').text('Country name is exits');
                    $('#lbErrorCountryName').css("display", "");
                    window.scrollTo(100, 100);
                }
                else if (d == 'PhoneCodeExist') {
                    $('#lbErrorPhoneSMS').text('Phone sms code name is exits');
                    $('#lbErrorPhoneSMS').css("display", "");
                    window.scrollTo(100, 100);
                }

            },
            error: function (e) {

            }
        });
    }
}
function SearchCountryName() {
    var keyword = $('#txtKeyword').val();

    var page = 1;
    if (keyword == '') {
        noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/Country/SearchCountryName",
            data: { keyword: keyword, page: page },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListCountry').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }
            },
            error: function (e) {
            }
        });
    }
}

function PagingSearchCountry(page) {
    var keyword = $('#txtKeyword').val();

    //if (keyword == '') {
    //    noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    //}
    $.ajax({
        type: "get",
        async: true,
        url: "/Country/SearchCountryName",
        data: { keyword: keyword, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListCountry').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function ProccessSearch(event, keyword) {

    if (event.which == 13 || event.keyCode == 13) {
        var page = 1;
        if (keyword == '') {
             noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
        }
        else {

            $.ajax({
                type: "get",
                async: true,
                url: "/Country/SearchCountryName",
                data: { keyword: keyword, page: page },
                beforeSend: function () {
                    showLoading();
                },
                success: function (d) {
                    hideLoading();
                    var json = JSON.parse(d);
                    if (json != "") {
                        $('#tbodyListCountry').html(json.ContentResult);
                        $('#ulPaging').html(json.PagingResult);
                    }
                },
                error: function (e) {

                }
            });
        }
        return false;
    }
}