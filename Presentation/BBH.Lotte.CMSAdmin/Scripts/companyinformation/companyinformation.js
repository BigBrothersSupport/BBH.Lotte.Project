﻿function SearchCompany() {
    var keyword = $('#txtKeyword').val();
    if (keyword == '') {
        noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/CompanyInfomation/SearchCompany",
            data: { keyword: keyword },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListCompanyInformation').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }
            },
            error: function (e) {

            }
        });
    }
}
function PagingSearchCompany() {
    var keyword = $('#txtKeyword').val();
    if (keyword == '') {
        noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    }
    var page;
    $.ajax({
        type: "get",
        async: true,
        url: "/CompanyInfomation/SearchCompany",
        data: { keyword: keyword, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var objJon = JSON.parse(d);
            if (objJon != null && objJon != "") {
                $('#tbodyListCompanyInformation').html(objJon.ContentResult);

                $('#ulPaging').html(objJon.PagingResult);
            }

        },
        error: function (e) {

        }
    });
}

function ProccessSearch(event, keyword) {

    if (event.which == 13 || event.keyCode == 13) {
        if (keyword == '') {
            noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
        }
        else {

            $.ajax({
                type: "get",
                async: true,
                url: "/CompanyInfomation/SearchCompany",
                data: { keyword: keyword },
                beforeSend: function () {
                    showLoading();
                },
                success: function (d) {
                    hideLoading();
                    $('#tbodyListCompanyInformation').html(d);
                },
                error: function (e) {

                }
            });
        }
        return false;
    }
}

function ShowPopupDetailMember(memberID) {
    var check = true;
    $('#hdMemberID').val(memberID);

    $.ajax({
        type: "POST",
        url: "/CompanyInfomation/ListCompanyByMemberID",

        async: true,
        data: { memberID: memberID },
        beforeSend: function () {

        },
        success: function (result) {
            $('#tbodyListCompanyInformation').html(result);

        }
    });
}
