﻿function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};
function RemoveTagHtml(str) {
    return str.replace(/<\/?[^>]+(>|$)/g, "");
}
function checkspace(e) {
    var unicode = e.charCode ? e.charCode : e.keyCode
    if (unicode == 32) {
        return false
    }
}
 
    function functionx(evt) {
        if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57)) {
            return false;
        }
    }  
    function onlyNumbers1(evt) {
        var e = event || evt;
        var charCode = e.which || e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }
function str_replace(search, replace, str) {
    var ra = replace instanceof Array, sa = str instanceof Array, l = (search = [].concat(search)).length, replace = [].concat(replace), i = (str = [].concat(str)).length;
    while (j = 0, i--)
        while (str[i] = str[i].split(search[j]).join(ra ? replace[j] || "" : replace[0]), ++j < l);
    return sa ? str : str[0];
}

function remove_vietnamese_accents(str) {
    accents_arr = new Array(
		"à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
		"ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
		"ế", "ệ", "ể", "ễ",
		"ì", "í", "ị", "ỉ", "ĩ",
		"ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
		"ờ", "ớ", "ợ", "ở", "ỡ",
		"ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
		"ỳ", "ý", "ỵ", "ỷ", "ỹ",
		"đ",
		"À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
		"Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
		"È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
		"Ì", "Í", "Ị", "Ỉ", "Ĩ",
		"Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
		"Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
		"Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
		"Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
		"Đ"
	);

    no_accents_arr = new Array(
		"a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
		"a", "a", "a", "a", "a", "a",
		"e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e",
		"i", "i", "i", "i", "i",
		"o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o",
		"o", "o", "o", "o", "o",
		"u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u",
		"y", "y", "y", "y", "y",
		"d",
		"A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A",
		"A", "A", "A", "A", "A",
		"E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E",
		"I", "I", "I", "I", "I",
		"O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O",
		"O", "O", "O", "O", "O",
		"U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U",
		"Y", "Y", "Y", "Y", "Y",
		"D"
	);

    return str_replace(accents_arr, no_accents_arr, str);
}
function isDoubleByte(str) {
    accents_arr = new Array(
		"à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
		"ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
		"ế", "ệ", "ể", "ễ",
		"ì", "í", "ị", "ỉ", "ĩ",
		"ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
		"ờ", "ớ", "ợ", "ở", "ỡ",
		"ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
		"ỳ", "ý", "ỵ", "ỷ", "ỹ",
		"đ",
		"À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
		"Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
		"È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
		"Ì", "Í", "Ị", "Ỉ", "Ĩ",
		"Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
		"Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
		"Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
		"Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
		"Đ"
	);
    for (var i = 0, n = str.length; i < n; i++) {
        if (str.charCodeAt(i) > 255) { return true; }
    }
    for (var j = 0; j < accents_arr.length; j++) {
        if (str.indexOf(accents_arr[j]) != -1) {
            return true;
        }
    }
    if (!CheckSpecialCharacter(str)) {
        return true;
    }
    return false;
}

function isDoubleByteUserName(str) {
    accents_arr = new Array(
		"à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
		"ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
		"ế", "ệ", "ể", "ễ",
		"ì", "í", "ị", "ỉ", "ĩ",
		"ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
		"ờ", "ớ", "ợ", "ở", "ỡ",
		"ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
		"ỳ", "ý", "ỵ", "ỷ", "ỹ",
		"đ",
		"À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
		"Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
		"È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
		"Ì", "Í", "Ị", "Ỉ", "Ĩ",
		"Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
		"Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
		"Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
		"Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
		"Đ"
	);
    for (var i = 0, n = str.length; i < n; i++) {
        if (str.charCodeAt(i) > 255) { return true; }
    }
    for (var j = 0; j < accents_arr.length; j++) {
        if (str.indexOf(accents_arr[j]) != -1) {
            return true;
        }
    }
    if (!CheckSpecialUserNameCharacter(str)) {
        return true;
    }
    return false;
}

function CheckPassword(str) {
    accents_arr = new Array(
    "à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă",
    "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề",
    "ế", "ệ", "ể", "ễ",
    "ì", "í", "ị", "ỉ", "ĩ",
    "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ",
    "ờ", "ớ", "ợ", "ở", "ỡ",
    "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ",
    "ỳ", "ý", "ỵ", "ỷ", "ỹ",
    "đ",
    "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă",
    "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ",
    "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ",
    "Ì", "Í", "Ị", "Ỉ", "Ĩ",
    "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ",
    "Ờ", "Ớ", "Ợ", "Ở", "Ỡ",
    "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ",
    "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ",
    "Đ"
);
    //for (var i = 0, n = str.length; i < n; i++) {
    //    if (str.charCodeAt(i) > 255) { return true; }
    //}
    for (var j = 0; j < accents_arr.length; j++) {
        if (str.indexOf(accents_arr[j]) != -1) {
            return true;
        }
    }
    return false;
}

function CheckSpecialCharacter(value) {
    var checkReg = true;
    var count = 0;
    //value = remove_unicode(value);
    if (value != "") {
        var characterReg = new Array('@', '$', '%', '^', '&', '*', '(', ')', '{', '}', '#', '~', '|', '*', '!', '~', '-', '=', '+', ',', '/', '?', '\'', '\"', ';', ':', '[', ']', '\\', '_', '`', '>', '<', '`', '.');
        //var characterReg = '@$%^&*(){}#~|*';
        for (var i = 0; i < characterReg.length; i++) {
            if (parseInt(value.indexOf(characterReg[i])) > -1) {
                count++;
            }
        }
        if (count > 0) {
            checkReg = false;
        }
    }
    return checkReg;
}

function CheckSpecialUserNameCharacter(value) {
    var checkReg = true;
    var count = 0;
    if (value != "") {
        var characterReg = new Array('$', '%', '^', '&', '*', '(', ')', '{', '}', '#', '~', '|', '*', '!', '~', '-', '=', '+', ',', '/', '?', '\'', '\"', ';', ':', '[', ']', '\\', '`', '>', '<', '`');
        for (var i = 0; i < characterReg.length; i++) {
            if (parseInt(value.indexOf(characterReg[i])) > -1) {
                count++;
            }
        }
        if (count > 0) {
            checkReg = false;
        }
    }
    return checkReg;
}

$(document).ready(function () {
    var adminID = $('#hdAdminID').val();
    if (adminID > 0) {
        $('#txtUserName').attr("readonly", true);
        $('#txtPass').attr("readonly", true);
    }
    else {
        $('#txtUserName').attr("readonly", false);
        $('#txtPass').attr("readonly", false);
    }
});

function LockAndUnlockAdmin(adminID, status) 
{
    var testMessage = '';
    if (status == 0)
    {
        testMessage = "Are you sure lock this admin?";     
       
    }
    else if (status==1)
    {
        testMessage = "Are you sure Unlock this admin?";    
      
    }
    var confirmMessage = confirm(testMessage);
    if (!confirmMessage) {
       
        return false;
    }
   
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/Admin/LockAndUnlockAdmin",
            data: { adminID: adminID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
            },
            error: function (e) {
            }
        });
    }
}

function ShowPopUpEditAdmin(adminID, userName,fullName,email,mobile,address,groupID) {
    $('#hdAdminID').val(adminID);
    $('#txtUserName').val(userName);
    $('#txtFullName').val(fullName);
    $('#txtEmail').val(email);
    $('#txtMobile').val(mobile);
    $('#txtAddress').val(address);
    $('#cbGroupAdmin option[value=' + groupID + ']').attr("selected", true);
    $('#txtUserName').attr("readonly", true);
    $('#txtPassword').attr("readonly", true);
}

function ShowPopupCreateNewAdmin() {
    $('#hdAdminID').val(0);
    $('#txtUserName').attr("readonly", false);
    $('#txtPassword').attr("readonly", false);

    $('#txtUserName').val('');
    $('#txtFullName').val('');
    $('#txtEmail').val('');
    $('#txtMobile').val('');
    $('#txtAddress').val('');
    $('#cbGroupAdmin option[value=0]').attr("selected", true);
}

function ResetForm(id, value) {
    $('#' + id).text('');
    $('#' + id).css('display', 'none');
}

function SaveAdmin() {
    var checkReg = true;
    var adminID = $('#hdAdminID').val();
    var userName = $('#txtUserName').val();
    var password = $('#txtPassword').val();
    var fullName = $('#txtFullName').val();
    var email = $('#txtEmail').val();
    var mobile = $('#txtMobile').val();
    var address = $('#txtAddress').val();
    var groupID = $('#cbGroupAdmin').val();

    if (userName == '') {
        $('#lbErrorUserName').text('Username is required');
        $('#lbErrorUserName').css('display', '');
        checkReg = false;
    }
    var checkUserName = isDoubleByteUserName(userName);
    if (checkUserName) {
        $('#lbErrorUserName').css("display", "");
        $('#lbErrorUserName').text('Login name can only enter unsigned characters and numbers. There are no special characters');
        checkReg = false;
        window.scrollTo(100, 100);
    }

    if (adminID == 0) {
        if (password == '') {
            $('#lbErrorPassword').text('Password is required');
            $('#lbErrorPassword').css('display', '');
            checkReg = false;
        }
    }

    if (password == '') {
        $('#lbErrorPassword').text('Pasword is required');
        $('#lbErrorPassword').css("display", "");
        checkReg = false;
    }
    var checkPassword = CheckPassword(password);
    if (checkPassword) {
        $('#lbErrorPassword').css("display", "");
        $('#lbErrorPassword').text('Password can not be entered in Vietnamese');
        window.scrollTo(100, 100)
        checkReg = false;
    }

    if (email == '') {
        $('#lbErrorEmail').text('Email is required');
        $('#lbErrorEmail').css('display', '');
        checkReg = false;
    }
    else {
        if (!isValidEmailAddress(email)) {
            checkReg = false;
            $('#lbErrorEmail').css("display", "");
            $('#lbErrorEmail').text('Invalid email format');
            window.scrollTo(100, 100);
        }
    }
        if (mobile == ''){
            
            $('#lbErrorMobie').css("display", "");
            $('#lbErrorMobie').text('Mobile is required');
            checkReg = false;
    }
        else if (mobile.length < 10)
        {
            $('#lbErrorMobie').text('Invalid mobile format');
            $('#lbErrorMobie').css("display", "");
            
            checkReg = false;
        }
        if (fullName == '') {
            $('#lbErrorFullName').text('Fullname is required');
            $('#lbErrorFullName').css('display', '');
            checkReg = false;
        }
        if (address == '') {
            $('#lbErrorAddress').text('Address is required');
            $('#lbErrorAddress').css('display', '');
            checkReg = false;
        }
        if (groupID == '0') {
            $('#lbErrorGroupAdmin').text('Function group is required');
            $('#lbErrorGroupAdmin').css('display', '');
            checkReg = false;
        }

        if (!checkReg) {
            return false;
        }
        else {
            $.ajax({
                type: "post",
                async: true,
                url: "/Admin/SaveAdmin",
                data: { adminID: adminID, userName: userName, password: password, fullName: fullName, email: email, mobile: mobile, address: address, groupID: groupID },
                beforeSend: function () {
                    showLoading();
                },
                success: function (d) {
                    hideLoading();
                    if (d == 'success') {
                        noty({ text: "Update success", layout: "bottomRight", type: "information" });
                        setTimeout(function () { window.location.reload(); }, 2000);
                    }
                    else if (d == 'failes')
                    {
                        noty({ text: "Update Error", layout: "bottomRight", type: "error" });
                    }
                    else if (d == 'UserNameExist') {
                        $('#lbErrorUserName').css("display", "");
                        $('#lbErrorUserName').text('UserName exit!!');
                        window.scrollTo(100, 100);
                    }
                    else if (d == 'EmailExist') {
                        $('#lbErrorEmail').css("display", "");
                        $('#lbErrorEmail').text('Email exit!!');
                        window.scrollTo(100, 100);
                    }                    
                },
                error: function (e) {
                    noty({ text: "Update Error, Please contact to admin.", layout: "bottomRight", type: "error" });
                }
            });
        }
    }
function onlyNumbers(evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}