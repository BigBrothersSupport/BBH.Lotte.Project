﻿function LockAndUnlockMember(memberID, status) {
    var confirmMessage = confirm('Are you sure lock this member?');
    if (!confirmMessage) {
        return false;
    }
    else {
        $.ajax({
            type: "post",
            async: true,
            url: "/Member/LockAndUnLockMember",
            data: { memberID: memberID, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Error", layout: "bottomRight", type: "error" });}
            },
            error: function (e) {

            }
        });
    }
}

function SearchMember() {
    var keyword = $('#txtKeyword').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1;
    //if (keyword == '') {
    //    noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    //}    
    //else {
        $.ajax({
            type: "get",
            async: true,
            url: "/Member/SearchMember",
            data: { keyword: keyword, fromDate: fromDate, toDate: toDate, page: page },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                 var json = JSON.parse(d);
                 if (json != "") {
                $('#tbodyListMember').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
            },
            error: function (e) {
            }
        });
    //}
}
function PagingSearchMember(page) {
    var keyword = $('#txtKeyword').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    //if (keyword == '') {
    //    noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    //}
    $.ajax({
        type: "get",
        async: true,
        url: "/Member/SearchMember",
        data: { keyword: keyword, fromDate: fromDate, toDate: toDate, page: page },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListMember').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}
//function PagingSearchMember() {
//    var keyword = $('#txtKeyword').val();
//     if (keyword == '') {
//        noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
//    }
//    var page=1;
//    $.ajax({
//        type: "get",
//        async: true,
//        url: "/Member/SearchMember",
//        data: { keyword: keyword, page: page },
//        beforeSend: function () {
//            showLoading();
//        },
//        success: function (d) {
//            hideLoading();
//            var objJon = JSON.parse(d);
//            if (objJon != null && objJon != "") {
//                $('#tbodyListMember').html(objJon.ContentResult);

//                $('#ulPaging').html(objJon.PagingResult);
//            }
     
//        },
//        error: function (e) {

//        }
//    });
//}

function ProccessSearch(event, keyword) {

    if (event.which == 13 || event.keyCode == 13) {
        if (keyword == '') {
            //alertify.error('Vui lòng nhập từ khóa cần tìm');
            noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
        }
        else {

            $.ajax({
                type: "get",
                async: true,
                url: "/Member/SearchMember",
                data: { keyword: keyword },
                beforeSend: function () {
                    $('#imgLoadingSearch').css("display", "")
                },
                success: function (d) {
                    $('#imgLoadingSearch').css("display", "none")
                    $('#tbodyListMember').html(d);
                },
                error: function (e) {

                }
            });
        }
        return false;
    }
}

function ExportExcelMember()
{
    var keyword = $('#txtKeyword').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();

    var keyword = $('#txtKeyword').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Member/ExportListMember",
        data: { keyword: keyword, fromDate: fromDate, toDate: toDate },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            window.location.href = '/FileExcels/Member/' + d;
        },
        error: function (e) {

        }
    });
}

function ShowPopupDetailCompany(memberID)
{
     var check = true;
    $('#hdMemberID').val(memberID);

    $.ajax({
        type: "POST",
        url: "/CompanyInfomation/ListCompanyByMemberID",

        async: true,
        data: { memberID: memberID },
        beforeSend: function () {
        
        },
        success: function (result) {
            $('#tbodyListCompanyInformation').html(result);
           
        }
    });
}