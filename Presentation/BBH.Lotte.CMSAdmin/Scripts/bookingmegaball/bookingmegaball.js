﻿function ConfirmBookingMegaball(bookingID, status, memberID) {
    var textMessage = '';
   
    if (status == 1) {
        textMessage = 'Are you sure confirm this transaction?';
    }
    else if (status == 2) {
        textMessage = 'Are you sure lock this transaction?';
    }
    var confirmMessage = confirm(textMessage);
    if (!confirmMessage) {
        return false;
    }
    else {
        $('#imgLoading_' + bookingID).css("display", "");
        $.ajax({
            type: "post",
            async: false,
            url: "/BookingMegaball/UpdateStatusBookingMegaball",
            data: { bookingID: bookingID, status: status, memberID: memberID },
            beforeSend: function () {
                $('#imgLoading_' + bookingID).css("display", "");
            },
            success: function (d) {
                $('#imgLoading_' + bookingID).css("display", "none");
                if (d == 'success') {
                    noty({ text: "Update Success", layout: "bottomRight", type: "information" });
                    setTimeout(function () { window.location.reload(); }, 2000);
                }
                else {
                    noty({ text: "Update Error. Please contact admin", layout: "bottomRight", type: "error" });
                }
            },
            error: function (e) {

            }
        });
    }
}

function SearchBookingMegaball() {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1;
    var status = $('#cbStatus').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/BookingMegaball/SearchBookingMegaball",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListBooking').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {
        }
    });
}

function PagingSearchBookingMegaball(page) {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var status = $('#cbStatus').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/BookingMegaball/SearchBookingMegaball",
        data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListBooking').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}

function PagingSearch_BookingMegaball(page) {
    var keyword = $('#txtKeyword').val();
    $.ajax({
        type: "get",
        async: true,
        url: "/Home/SearchMemberBookingMegaball",
        data: { keyword: keyword, page: page},
        beforeSend: function () {
            showLoading();
        },
        success: function (d) {
            hideLoading();
            var json = JSON.parse(d);
            if (json != "") {
                $('#tbodyListBooking').html(json.ContentResult);
                $('#ulPaging').html(json.PagingResult);
            }
        },
        error: function (e) {

        }
    });
}


function ExportExcelBookingMegaballTicket() {
    var memberID = $('#cbMember').val();
    var fromDate = $('#txtFromDate').val();
    var toDate = $('#txtToDate').val();
    var page = 1;
    var status = $('#cbStatus').val();
    if (toDate == '' && fromDate == '' && memberID == 0 && status == -1) {
        noty({ text: "Please choose keyword search", layout: "bottomRight", type: "error" });
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/BookingMegaball/ExportListBookingMegaballTicket",
            data: { memberID: memberID, fromDate: fromDate, toDate: toDate, page: page, status: status },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                window.location.href = '/FileExcels/BookingMegaballTicket/' + d;
            },
            error: function (e) {

            }
        });
    }
}
function ExportExcel_BookingMegaballTicket() {
    var keyword = $('#txtKeyword').val();
    //if (keyword == '') {
    //    noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    //}
    //else {
        $.ajax({
            type: "get",
            async: true,
            url: "/Home/ExportList_BookingMegaballTicket",
            data: { keyword: keyword },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                window.location.href = '/FileExcels/BookingMegaballTicket/' + d;
            },
            error: function (e) {
            }
        });
    //}
}
function SearchMemberBookingMegaball() {
    var keyword = $('#txtKeyword').val();
    var page = 1;
    if (keyword == '') {
        noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
    }
    else {
        $.ajax({
            type: "get",
            async: true,
            url: "/Home/SearchMemberBookingMegaball",
            data: { keyword: keyword, page:page },
            beforeSend: function () {
                showLoading();
            },
            success: function (d) {
                hideLoading();
                var json = JSON.parse(d);
                if (json != "") {
                    $('#tbodyListBooking').html(json.ContentResult);
                    $('#ulPaging').html(json.PagingResult);
                }
            },
            error: function (e) {

            }
        });
    }
}
function ProccessSearch(event, keyword) {

    if (event.which == 13 || event.keyCode == 13) {
        if (keyword == '') {
            //alertify.error('Vui lòng nhập từ khóa cần tìm');
            noty({ text: "Please input keyword search", layout: "bottomRight", type: "error" });
        }
        else {

            $.ajax({
                type: "get",
                async: true,
                url: "/Home/SearchMemberBookingMegaball",
                data: { keyword: keyword },
                beforeSend: function () {
                    showLoading();
                },
                success: function (d) {
                    hideLoading();
                    $('#tbodyListBooking').html(d);
                },
                error: function (e) {

                }
            });
        }
        return false;
    }
}

