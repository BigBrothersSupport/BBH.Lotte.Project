USE [LotteryCLP]
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertBooking_FE]    Script Date: 01/08/2018 11:00:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertBooking_FE]
(
	@memberID int,
	@createDate datetime,
	@openDate datetime,
	@status int,
	@transactionCode char(100),
	@note nvarchar(2500),
	@noteEnglish nvarchar(2500),
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@quantity int,
	@ValuePoint numeric(18,10),
	@DrawID int,
	@IsFreeTicket int

)
as begin 
	insert into BookingMegaball(MemberID,CreateDate,OpenDate,Status,TransactionCode,Note,NoteEnglish,FirstNumber,SecondNumber,ThirdNumber,FourthNumber,FivethNumber,ExtraNumber,Quantity,ValuePoint,DrawID,IsFreeTicket,IsRevenueFreeTicket)
	 values(@memberID,@createDate,@openDate,@status,@transactionCode,@note,@noteEnglish,@firstNumber,@secondNumber,@thirdNumber,@fourthNumber,@fivethNumber,@extraNumber,@quantity,@ValuePoint,@DrawID,@IsFreeTicket,0)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballForRevenueFreeByDraw]    Script Date: 01/08/2018 11:00:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllBookingMegaballForRevenueFreeByDraw]
(
		@IsRevenueFreeTicket int
)
as begin
		select *
		from BookingMegaball
		where IsRevenueFreeTicket = @IsRevenueFreeTicket
		order by CreateDate DESC 
end

GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateBookingMaegaball_FreeTicket]    Script Date: 01/08/2018 11:00:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateBookingMaegaball_FreeTicket]
(
	@BookingID varchar(1000) 
)
as begin 
	update BookingMegaball set IsRevenueFreeTicket =1 where BookingID in (select * from Stringsplit(@BookingID,','));
end

GO
