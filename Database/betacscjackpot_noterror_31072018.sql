USE [betacscjackpot_noterror]
GO
/****** Object:  Table [dbo].[CompanyInfomation]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CompanyInfomation](
	[MemberID] [int] NOT NULL,
	[CompanyName] [nvarchar](500) NULL,
	[Address] [nvarchar](500) NULL,
	[Phone] [char](50) NULL,
	[Email] [nvarchar](250) NULL,
	[Website] [nvarchar](500) NULL,
	[Description] [nvarchar](4000) NULL,
 CONSTRAINT [PK_CompanyInfomation] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[CompanyInfomation] ([MemberID], [CompanyName], [Address], [Phone], [Email], [Website], [Description]) VALUES (1, N'abcdsfgg', N'123HCM', N'01293847567                                       ', N'123@gmail.com', N'123www.com', N'asdfghjkdskfhhs')
INSERT [dbo].[CompanyInfomation] ([MemberID], [CompanyName], [Address], [Phone], [Email], [Website], [Description]) VALUES (2, N'qwert', N'123HCM', N'4354543444                                        ', N'1234@gmail.com', N'1243www.com', N'asdfghjkdskfhhs')
/****** Object:  Table [dbo].[Coin]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coin](
	[CoinID] [char](10) NOT NULL,
	[CoinName] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[CoinSymbol] [varchar](50) NULL,
 CONSTRAINT [PK_Coin] PRIMARY KEY CLUSTERED 
(
	[CoinID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Coin] ([CoinID], [CoinName], [IsActive], [CoinSymbol]) VALUES (N'BTC       ', N'Bitcoin', 1, NULL)
INSERT [dbo].[Coin] ([CoinID], [CoinName], [IsActive], [CoinSymbol]) VALUES (N'ETH       ', N'Carcoin', 1, NULL)
INSERT [dbo].[Coin] ([CoinID], [CoinName], [IsActive], [CoinSymbol]) VALUES (N'XRP       ', N'Ripple', 1, NULL)
/****** Object:  Table [dbo].[TransactionPoint]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionPoint](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[Points] [int] NULL,
	[CreateDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](2500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
	[TransactionBitcoin] [nvarchar](500) NULL,
	[CoinID] [nchar](10) NULL,
 CONSTRAINT [PK_TransactionPoint] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FreeTicketDaily]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FreeTicketDaily](
	[FreeTicketID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NULL,
	[Total] [int] NULL,
	[IsActive] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_FreeTicket] PRIMARY KEY CLUSTERED 
(
	[FreeTicketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[FreeTicketDaily] ON
INSERT [dbo].[FreeTicketDaily] ([FreeTicketID], [Date], [Total], [IsActive], [CreatedDate]) VALUES (1, CAST(0x0000A92D018B6A62 AS DateTime), 707, 0, CAST(0x0000A92D018B6A62 AS DateTime))
INSERT [dbo].[FreeTicketDaily] ([FreeTicketID], [Date], [Total], [IsActive], [CreatedDate]) VALUES (2, CAST(0x0000A92E00734B79 AS DateTime), 1416, 1, CAST(0x0000A92E00734B79 AS DateTime))
SET IDENTITY_INSERT [dbo].[FreeTicketDaily] OFF
/****** Object:  Table [dbo].[Wallet_Address]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Wallet_Address](
	[WalletAddressID] [int] IDENTITY(1,1) NOT NULL,
	[WalletID] [int] NOT NULL,
	[PrivateKey] [nvarchar](500) NULL,
	[IndexWallet] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_Wallet_Address] PRIMARY KEY CLUSTERED 
(
	[WalletAddressID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dia chi id cua vi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Wallet_Address', @level2type=N'COLUMN',@level2name=N'WalletAddressID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID cua vi' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Wallet_Address', @level2type=N'COLUMN',@level2name=N'WalletID'
GO
/****** Object:  Table [dbo].[TypeTransaction]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeTransaction](
	[TypeTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[TypeTransactionName] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
 CONSTRAINT [PK_TypeTransaction] PRIMARY KEY CLUSTERED 
(
	[TypeTransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TypeTransaction] ON
INSERT [dbo].[TypeTransaction] ([TypeTransactionID], [TypeTransactionName], [IsActive]) VALUES (1, N'Phí deposit', 1)
INSERT [dbo].[TypeTransaction] ([TypeTransactionID], [TypeTransactionName], [IsActive]) VALUES (2, N'Phí withdraw', 1)
INSERT [dbo].[TypeTransaction] ([TypeTransactionID], [TypeTransactionName], [IsActive]) VALUES (3, N'Phí rút thưởng', 1)
SET IDENTITY_INSERT [dbo].[TypeTransaction] OFF
/****** Object:  Table [dbo].[TypeNotification]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeNotification](
	[TypeNotificationID] [int] IDENTITY(1,1) NOT NULL,
	[TypeNotificationName] [nvarchar](500) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TypeNotification] PRIMARY KEY CLUSTERED 
(
	[TypeNotificationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteTypeNews_CMS]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_DeleteTypeNews_CMS]
(
	@IsDelete smallint,
	@DeleteDate datetime,
	@DeleteUser nvarchar(250),
	@TypeNewsID int
)
as
begin
update TypeNews
set
	IsDelete=@IsDelete,
	
	DeleteDate=@DeleteDate,
	DeleteUser=@DeleteUser
	where TypeNewsID=@TypeNewsID;

end;
GO
/****** Object:  Table [dbo].[TypeConfig]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeConfig](
	[TypeConfigID] [int] IDENTITY(1,1) NOT NULL,
	[TypeConfigName] [nvarchar](250) NOT NULL,
	[IsActive] [smallint] NOT NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TypeConfig] PRIMARY KEY CLUSTERED 
(
	[TypeConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransactionPackage]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionPackage](
	[TransactionID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[PackageID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](3500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
 CONSTRAINT [PK_TransactionPackage] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransactionFee]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TransactionFee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TransactionTypeID] [int] NULL,
	[Fee] [numeric](18, 8) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TransactionFee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TransactionCoin]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransactionCoin](
	[TransactionID] [char](150) NOT NULL,
	[WalletAddressID] [nvarchar](500) NOT NULL,
	[MemberID] [int] NOT NULL,
	[ValueTransaction] [numeric](18, 10) NOT NULL,
	[QRCode] [nvarchar](500) NULL,
	[CreateDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[Status] [smallint] NULL,
	[Note] [nvarchar](2500) NULL,
	[WalletID] [nvarchar](500) NULL,
	[TypeTransactionID] [int] NULL,
	[TransactionBitcoin] [nvarchar](500) NULL,
	[CoinID] [nchar](10) NULL,
 CONSTRAINT [PK_TransactionCoin] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ma Giao dich coin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'TransactionID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Dia chi vi cua nguoi muon giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'WalletAddressID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Nguoi giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'MemberID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gia tri giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'ValueTransaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'QR code' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'QRCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ngay giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'CreateDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Thoi gian het han giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'ExpireDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Trang thai giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ghi chu giao dich' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'Note'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Vi cua nguoi nhan' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TransactionCoin', @level2type=N'COLUMN',@level2name=N'WalletID'
GO
/****** Object:  Table [dbo].[TicketWinning]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketWinning](
	[TicketWinningID] [int] IDENTITY(1,1) NOT NULL,
	[BookingID] [int] NULL,
	[DrawID] [int] NULL,
	[BuyDate] [datetime] NULL,
	[Status] [int] NULL,
	[AwardID] [int] NULL,
	[FirstNumberBuy] [int] NULL,
	[SecondNumberBuy] [int] NULL,
	[ThirdNumberBuy] [int] NULL,
	[FourthNumberBuy] [int] NULL,
	[FivethNumberBuy] [int] NULL,
	[ExtraNumberBuy] [int] NULL,
	[FirstNumberAward] [int] NULL,
	[SecondNumberAward] [int] NULL,
	[ThirdNumberAward] [int] NULL,
	[FourthNumberAward] [int] NULL,
	[FivethNumberAward] [int] NULL,
	[ExtraNumberAward] [int] NULL,
	[NumberBallNormal] [int] NULL,
	[NumberBallGold] [int] NULL,
	[AwardValue] [decimal](18, 5) NULL,
	[AwardDate] [datetime] NULL,
	[MemberID] [int] NULL,
	[Priority] [int] NULL,
	[AwardFee] [decimal](18, 8) NULL,
 CONSTRAINT [PK_TicketWinning] PRIMARY KEY CLUSTERED 
(
	[TicketWinningID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TicketNumberTimeLog]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketNumberTimeLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[TicketNumber] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TicketNumberTimeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ticket Number Of Times' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TicketNumberTimeLog', @level2type=N'COLUMN',@level2name=N'TicketNumber'
GO
SET IDENTITY_INSERT [dbo].[TicketNumberTimeLog] ON
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (0, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D005850CC AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (1, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D005850D1 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (2, N'nhan.luong@bigbrothers.gold', 0, CAST(0x0000A92D007D70F2 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (3, N'hathanhpc20@gmail.com', 0, CAST(0x0000A92D008EB1EA AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (4, N'hathanhpc20@gmail.com', 35, CAST(0x0000A92D008EBB48 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (5, N'kienduc94@gmail.com', 99, CAST(0x0000A92D00915D91 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (6, N'kienduc94@gmail.com', 22, CAST(0x0000A92D00924A04 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (7, N'hathanhpc20@gmail.com', 2, CAST(0x0000A92D00950D88 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (8, N'hathanhpc20@gmail.com', 69, CAST(0x0000A92D00983E13 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (9, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D009A36A8 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (10, N'kienduc94@gmail.com', 0, CAST(0x0000A92D009E2E03 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (11, N'thanhha12196@gmail.com', 1, CAST(0x0000A92D009E4947 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (12, N'hathanhpc20@gmail.com', 82, CAST(0x0000A92D00A16131 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (13, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D00A60E7D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (14, N'hathanhpc20@gmail.com', 3, CAST(0x0000A92D00ABC42D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (15, N'thanhha12196@gmail.com', 100, CAST(0x0000A92D00ACC91E AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (16, N'hathanhpc20@gmail.com', 3, CAST(0x0000A92D00B14B6F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (17, N'hathanhpc20@gmail.com', 1, CAST(0x0000A92D00B1D07A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (18, N'nguyenlytruongson@gmail.com', 100, CAST(0x0000A92D00B7721A AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (19, N'nguyenlytruongson@gmail.com', 100, CAST(0x0000A92D00B7848F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (20, N'nguyenlytruongson@gmail.com', 97, CAST(0x0000A92D00B82396 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (21, N'daisy.k@cscjackpot.com', 99, CAST(0x0000A92D01717974 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (22, N'daisy.k@cscjackpot.com', 100, CAST(0x0000A92D01719E7F AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (23, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92D017D9C7B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (24, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92D017DAA2D AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (25, N'thanhha12196@gmail.com', 0, CAST(0x0000A92D017EF40B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (26, N'duc.ly@bigbrothers.gold', 0, CAST(0x0000A92D017F58E5 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (27, N'hathanhpc20@gmail.com', 100, CAST(0x0000A92D0188A0D3 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (28, N'thanhha12196@gmail.com', 100, CAST(0x0000A92D01897944 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (29, N'thanhha12196@gmail.com', 0, CAST(0x0000A92D0189BC90 AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (30, N'thanhha12196@gmail.com', 3, CAST(0x0000A92D0189CE9B AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (31, N'hathanhpc20@gmail.com', 0, CAST(0x0000A92E0000C58C AS DateTime))
INSERT [dbo].[TicketNumberTimeLog] ([ID], [Email], [TicketNumber], [CreatedDate]) VALUES (32, N'thanhha12196@gmail.com', 1, CAST(0x0000A92E0007A0D4 AS DateTime))
SET IDENTITY_INSERT [dbo].[TicketNumberTimeLog] OFF
/****** Object:  Table [dbo].[TicketConfig]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketConfig](
	[ConfigID] [int] IDENTITY(1,1) NOT NULL,
	[ConfigName] [nvarchar](250) NOT NULL,
	[NumberTicket] [int] NOT NULL,
	[CoinValues] [decimal](18, 10) NOT NULL,
	[CoinID] [nchar](10) NOT NULL,
	[IsActive] [smallint] NOT NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_TicketConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TicketConfig] ON
INSERT [dbo].[TicketConfig] ([ConfigID], [ConfigName], [NumberTicket], [CoinValues], [CoinID], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (1, N'So coin mua vé', 1, CAST(0.0002000000 AS Decimal(18, 10)), N'BTC       ', 0, 0, CAST(0x0000A8D700000000 AS DateTime), NULL, CAST(0x0000A8FE00B1012D AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[TicketConfig] OFF
/****** Object:  Table [dbo].[SystemConfig]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemConfig](
	[ConfigID] [int] IDENTITY(1,1) NOT NULL,
	[IsAutoLottery] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_SystemConfig] PRIMARY KEY CLUSTERED 
(
	[ConfigID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[SystemConfig] ON
INSERT [dbo].[SystemConfig] ([ConfigID], [IsAutoLottery], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (1, 1, CAST(0x0000A90700000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[SystemConfig] OFF
/****** Object:  Table [dbo].[StoreFreeTicket]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreFreeTicket](
	[StoreFreeTicketID] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [int] NULL,
 CONSTRAINT [PK_StoreFreeTicket] PRIMARY KEY CLUSTERED 
(
	[StoreFreeTicketID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[StoreFreeTicket] ON
INSERT [dbo].[StoreFreeTicket] ([StoreFreeTicketID], [Amount]) VALUES (1, 8572)
SET IDENTITY_INSERT [dbo].[StoreFreeTicket] OFF
/****** Object:  StoredProcedure [dbo].[SP_UpdatePriorityTopic_CMS]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_UpdatePriorityTopic_CMS]
(
	@TopicID int,
	@Priority int
)
as 
begin
update topic
set [Priority]=@Priority
where topicid=@topicid;
end;
GO
/****** Object:  Table [dbo].[ExchangeTicket]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExchangeTicket](
	[ExchangeID] [int] IDENTITY(1,1) NOT NULL,
	[PointValue] [float] NULL,
	[TicketNumber] [int] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[MemberID] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [date] NULL,
	[DeleteUser] [nvarchar](250) NULL,
	[CoinID] [nchar](10) NULL,
 CONSTRAINT [PK_ExchangeTicket] PRIMARY KEY CLUSTERED 
(
	[ExchangeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ExchangeTicket] ON
INSERT [dbo].[ExchangeTicket] ([ExchangeID], [PointValue], [TicketNumber], [CreateDate], [CreateUser], [MemberID], [IsActive], [IsDelete], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser], [CoinID]) VALUES (29, 1, 20, CAST(0x0000A86700F2B28E AS DateTime), N'administrator', 1, 1, 0, CAST(0x0000A86700F2B28E AS DateTime), N'', CAST(0xC23D0B00 AS Date), N'administrator', N'XRP       ')
SET IDENTITY_INSERT [dbo].[ExchangeTicket] OFF
/****** Object:  Table [dbo].[ExchangePoint]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExchangePoint](
	[ExchangeID] [int] IDENTITY(1,1) NOT NULL,
	[PointValue] [float] NULL,
	[BitcoinValue] [float] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[MemberID] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_ExchangePoint] PRIMARY KEY CLUSTERED 
(
	[ExchangeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ExchangePoint] ON
INSERT [dbo].[ExchangePoint] ([ExchangeID], [PointValue], [BitcoinValue], [CreateDate], [CreateUser], [MemberID], [IsActive], [IsDelete], [UpdateDate], [UpdateUser], [DeleteDate], [DeleteUser]) VALUES (4, 20000, 1, CAST(0x0000A8180023A1FC AS DateTime), N'administrator', 1, 1, 0, CAST(0x0000A86501265AAD AS DateTime), N'administrator', NULL, NULL)
SET IDENTITY_INSERT [dbo].[ExchangePoint] OFF
/****** Object:  Table [dbo].[Draw]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Draw](
	[DrawID] [int] IDENTITY(1,1) NOT NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_Draw] PRIMARY KEY CLUSTERED 
(
	[DrawID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Draw] ON
INSERT [dbo].[Draw] ([DrawID], [StartDate], [EndDate], [CreatedDate]) VALUES (33, CAST(0x0000A92D00D63BC0 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), CAST(0x0000A92D00EC1334 AS DateTime))
SET IDENTITY_INSERT [dbo].[Draw] OFF
/****** Object:  Table [dbo].[Country]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Country](
	[CountryID] [int] IDENTITY(1,1) NOT NULL,
	[CountryName] [nvarchar](250) NULL,
	[PhoneZipCode] [char](10) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[CountryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Country] ON
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (200, N'Afghanistan', N'+93       ', 1, 0, CAST(0x0000A92D011FC456 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (201, N'Albania', N'+355      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (202, N'Algeria', N'+213      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (203, N'AmericanSamoa', N'+1 684    ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (204, N'Andorra', N'+376      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (205, N'Angola', N'+244      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (206, N'Anguilla', N'+1 264    ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (207, N'Antigua and Barbuda', N'+1268     ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (208, N'Argentina', N'+54       ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (209, N'Armenia', N'+374      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (210, N'Aruba', N'+297      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (211, N'Australia', N'+61       ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (212, N'Austria', N'+43       ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (213, N'Azerbaijan', N'+994      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (214, N'Bahamas', N'+1 242    ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (215, N'Bahrain', N'+973      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (216, N'Bangladesh', N'+880      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (217, N'Barbados', N'+1 246    ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (218, N'Belarus', N'+375      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (219, N'Belgium', N'+32       ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (220, N'Belize', N'+501      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (221, N'Benin', N'+229      ', 1, 0, CAST(0x0000A92D011FC45B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (222, N'Bermuda', N'+1 441    ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (223, N'Bhutan', N'+975      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (224, N'Bosnia and Herzegovina', N'+387      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (225, N'Botswana', N'+267      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (226, N'Brazil', N'+55       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (227, N'British Indian Ocean Territory', N'+246      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (228, N'Bulgaria', N'+359      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (229, N'Burkina Faso', N'+226      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (230, N'Burundi', N'+257      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (231, N'Cambodia', N'+855      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (232, N'Cameroon', N'+237      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (233, N'Cape Verde', N'+238      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (234, N'Cayman Islands', N'+345      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (235, N'Central African Republic', N'+236      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (236, N'Chad', N'+235      ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (237, N'Chile', N'+56       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (238, N'China', N'+86       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (239, N'Christmas Island', N'+61       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (240, N'Colombia', N'+57       ', 1, 0, CAST(0x0000A92D011FC45F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (241, N'Comoros', N'+269      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (242, N'Congo', N'+242      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (243, N'Cook Islands', N'+682      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (244, N'Costa Rica', N'+506      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (245, N'Croatia', N'+385      ', 1, 0, CAST(0x0000A92D011FC464 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (246, N'Cuba', N'+53       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (247, N'Cyprus', N'+537      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (248, N'Czech Republic', N'+420      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (249, N'Denmark', N'+45       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (250, N'Djibouti', N'+253      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (251, N'Dominica', N'+1 767    ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (252, N'Dominican Republic', N'+1 849    ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (253, N'Ecuador', N'+593      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (254, N'Egypt', N'+20       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (255, N'El Salvador', N'+503      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (256, N'Equatorial Guinea', N'+240      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (257, N'Eritrea', N'+291      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (258, N'Estonia', N'+372      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (259, N'Ethiopia', N'+251      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (260, N'Faroe Islands', N'+298      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (261, N'Fiji', N'+679      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (262, N'Finland', N'+358      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (263, N'France', N'+33       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (264, N'French Guiana', N'+594      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (265, N'French Polynesia', N'+689      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (266, N'Gabon', N'+241      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (267, N'Gambia', N'+220      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (268, N'Georgia', N'+995      ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (269, N'Germany', N'+49       ', 1, 0, CAST(0x0000A92D011FC469 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (270, N'Ghana', N'+233      ', 1, 0, CAST(0x0000A92D011FC46D AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (271, N'Gibraltar', N'+350      ', 1, 0, CAST(0x0000A92D011FC472 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (272, N'Greece', N'+30       ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (273, N'Greenland', N'+299      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (274, N'Grenada', N'+1 473    ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (275, N'Guadeloupe', N'+590      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (276, N'Guam', N'+1 671    ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (277, N'Guatemala', N'+502      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (278, N'Guinea', N'+224      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (279, N'Guinea-Bissau', N'+245      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (280, N'Guyana', N'+595      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (281, N'Haiti', N'+509      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (282, N'Honduras', N'+504      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (283, N'Hungary', N'+36       ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (284, N'Iceland', N'+354      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (285, N'India', N'+91       ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (286, N'Indonesia', N'+62       ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (287, N'Iraq', N'+964      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (288, N'Ireland', N'+353      ', 1, 0, CAST(0x0000A92D011FC477 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (289, N'Israel', N'+972      ', 1, 0, CAST(0x0000A92D011FC47B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (290, N'Italy', N'+39       ', 1, 0, CAST(0x0000A92D011FC47B AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (291, N'Jamaica', N'+1 876    ', 1, 0, CAST(0x0000A92D011FC485 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (292, N'Japan', N'+81       ', 1, 0, CAST(0x0000A92D011FC489 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (293, N'Jordan', N'+962      ', 1, 0, CAST(0x0000A92D011FC492 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (294, N'Kazakhstan', N'+7 7      ', 1, 0, CAST(0x0000A92D011FC497 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (295, N'Kenya', N'+254      ', 1, 0, CAST(0x0000A92D011FC49C AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (296, N'Kiribati', N'+686      ', 1, 0, CAST(0x0000A92D011FC4A1 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (297, N'Kyrgyzstan', N'+996      ', 1, 0, CAST(0x0000A92D011FC4A5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (298, N'Latvia', N'+371      ', 1, 0, CAST(0x0000A92D011FC4AF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (299, N'Lesotho', N'+266      ', 1, 0, CAST(0x0000A92D011FC4B3 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (300, N'Liberia', N'+231      ', 1, 0, CAST(0x0000A92D011FC4B3 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (301, N'Liechtenstein', N'+423      ', 1, 0, CAST(0x0000A92D011FC4BD AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (302, N'Lithuania', N'+370      ', 1, 0, CAST(0x0000A92D011FC4BD AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (303, N'Luxembourg', N'+352      ', 1, 0, CAST(0x0000A92D011FC4C6 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (304, N'Madagascar', N'+261      ', 1, 0, CAST(0x0000A92D011FC4CB AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (305, N'Malawi', N'+265      ', 1, 0, CAST(0x0000A92D011FC4D4 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (306, N'Malaysia', N'+60       ', 1, 0, CAST(0x0000A92D011FC4DD AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (307, N'Maldives', N'+960      ', 1, 0, CAST(0x0000A92D011FC4E2 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (308, N'Mali', N'+223      ', 1, 0, CAST(0x0000A92D011FC4E7 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (309, N'Malta', N'+356      ', 1, 0, CAST(0x0000A92D011FC4E7 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (310, N'Marshall Islands', N'+692      ', 1, 0, CAST(0x0000A92D011FC4F0 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (311, N'Martinique', N'+596      ', 1, 0, CAST(0x0000A92D011FC4F0 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (312, N'Mauritania', N'+222      ', 1, 0, CAST(0x0000A92D011FC4F0 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (313, N'Mauritius', N'+230      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (314, N'Mayotte', N'+262      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (315, N'Mexico', N'+52       ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (316, N'Monaco', N'+377      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (317, N'Mongolia', N'+976      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (318, N'Montenegro', N'+382      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (319, N'Montserrat', N'+1664     ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (320, N'Morocco', N'+212      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (321, N'Myanmar', N'+95       ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (322, N'Namibia', N'+264      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (323, N'Nauru', N'+674      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (324, N'Nepal', N'+977      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (325, N'Netherlands', N'+31       ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (326, N'Netherlands Antilles', N'+599      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (327, N'New Caledonia', N'+687      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (328, N'New Zealand', N'+64       ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (329, N'Nicaragua', N'+505      ', 1, 0, CAST(0x0000A92D011FC4F5 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (330, N'Niger', N'+227      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (331, N'Nigeria', N'+234      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (332, N'Niue', N'+683      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (333, N'Norfolk Island', N'+672      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (334, N'Northern Mariana Islands', N'+1 670    ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (335, N'Norway', N'+47       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (336, N'Oman', N'+968      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (337, N'Pakistan', N'+92       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (338, N'Palau', N'+680      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (339, N'Panama', N'+507      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (340, N'Papua New Guinea', N'+675      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (341, N'Paraguay', N'+595      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (342, N'Peru', N'+51       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (343, N'Philippines', N'+63       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (344, N'Poland', N'+48       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (345, N'Portugal', N'+351      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (346, N'Puerto Rico', N'+1 939    ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (347, N'Romania', N'+40       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (348, N'Rwanda', N'+250      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (349, N'Samoa', N'+685      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (350, N'San Marino', N'+378      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (351, N'Saudi Arabia', N'+966      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (352, N'Senegal', N'+221      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (353, N'Serbia', N'+381      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (354, N'Seychelles', N'+248      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (355, N'Sierra Leone', N'+232      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (356, N'Slovakia', N'+421      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (357, N'Slovenia', N'+386      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (358, N'Solomon Islands', N'+677      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (359, N'South Africa', N'+27       ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (360, N'South Georgia and the South Sandwich Islands', N'+500      ', 1, 0, CAST(0x0000A92D011FC4FA AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (361, N'Spain', N'+34       ', 1, 0, CAST(0x0000A92D011FC4FE AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (362, N'Sri Lanka', N'+94       ', 1, 0, CAST(0x0000A92D011FC4FE AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (363, N'Sudan', N'+249      ', 1, 0, CAST(0x0000A92D011FC503 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (364, N'Suriname', N'+597      ', 1, 0, CAST(0x0000A92D011FC508 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (365, N'Swaziland', N'+268      ', 1, 0, CAST(0x0000A92D011FC511 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (366, N'Sweden', N'+46       ', 1, 0, CAST(0x0000A92D011FC511 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (367, N'Switzerland', N'+41       ', 1, 0, CAST(0x0000A92D011FC511 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (368, N'Tajikistan', N'+992      ', 1, 0, CAST(0x0000A92D011FC511 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (369, N'Thailand', N'+66       ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (370, N'Togo', N'+228      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (371, N'Tokelau', N'+690      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (372, N'Tonga', N'+676      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (373, N'Trinidad and Tobago', N'+1 868    ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (374, N'Tunisia', N'+216      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (375, N'Turkey', N'+90       ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (376, N'Turkmenistan', N'+993      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (377, N'Turks and Caicos Islands', N'+1 649    ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (378, N'Tuvalu', N'+688      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (379, N'Uganda', N'+256      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (380, N'Ukraine', N'+380      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (381, N'United Kingdom', N'+44       ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (382, N'Uruguay', N'+598      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (383, N'Uzbekistan', N'+998      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (384, N'Vanuatu', N'+678      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (385, N'Wallis and Futuna', N'+681      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (386, N'Yemen', N'+967      ', 1, 0, CAST(0x0000A92D011FC516 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (387, N'Zambia', N'+260      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (388, N'Zimbabwe', N'+263      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (389, N'Bolivia, Plurinational State of', N'+591      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (390, N'Cocos (Keeling) Islands', N'+61       ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (391, N'Congo, The Democratic Republic of the', N'+243      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (392, N'Cote d"Ivoire', N'+225      ', 1, 0, CAST(0x0000A92D011FC51A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (393, N'Falkland Islands (Malvinas)', N'+500      ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (394, N'Guernsey', N'+44       ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (395, N'Holy See (Vatican City State)', N'+379      ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (396, N'Hong Kong', N'+852      ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (397, N'Iran, Islamic Republic of', N'+98       ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (398, N'Isle of Man', N'+44       ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (399, N'Jersey', N'+44       ', 1, 0, CAST(0x0000A92D011FC51F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (400, N'Korea, Democratic People"s Republic of', N'+850      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (401, N'Korea, Republic of', N'+82       ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (402, N'Lao People"s Democratic Republic', N'+856      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (403, N'Libyan Arab Jamahiriya', N'+218      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (404, N'Macao', N'+853      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (405, N'Macedonia, The Former Yugoslav Republic of', N'+389      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (406, N'Micronesia, Federated States of', N'+691      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (407, N'Moldova, Republic of', N'+373      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (408, N'Mozambique', N'+258      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (409, N'Palestinian Territory, Occupied', N'+970      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (410, N'Pitcairn', N'+872      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (411, N'Réunion', N'+262      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (412, N'Russia', N'+7        ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (413, N'Saint Barthélemy', N'+590      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (414, N'Saint Helena, Ascension and Tristan Da Cunha', N'+290      ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (415, N'Saint Kitts and Nevis', N'+1 869    ', 1, 0, CAST(0x0000A92D012467FF AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (416, N'Saint Lucia', N'+1 758    ', 1, 0, CAST(0x0000A92D01246803 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (417, N'Saint Martin', N'+590      ', 1, 0, CAST(0x0000A92D01246803 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (418, N'Saint Pierre and Miquelon', N'+508      ', 1, 0, CAST(0x0000A92D01246803 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (419, N'Saint Vincent and the Grenadines', N'+1 784    ', 1, 0, CAST(0x0000A92D01246803 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (420, N'Sao Tome and Principe', N'+239      ', 1, 0, CAST(0x0000A92D01246808 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (421, N'Somalia', N'+252      ', 1, 0, CAST(0x0000A92D0124680D AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (422, N'Svalbard and Jan Mayen', N'+47       ', 1, 0, CAST(0x0000A92D01246811 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (423, N'Syrian Arab Republic', N'+963      ', 1, 0, CAST(0x0000A92D01246816 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (424, N'Taiwan, Province of China', N'+886      ', 1, 0, CAST(0x0000A92D01246816 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (425, N'Tanzania, United Republic of', N'+255      ', 1, 0, CAST(0x0000A92D0124681A AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (426, N'Timor-Leste', N'+670      ', 1, 0, CAST(0x0000A92D0124681F AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (427, N'Venezuela, Bolivarian Republic of', N'+58       ', 1, 0, CAST(0x0000A92D01246824 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (428, N'Viet Nam', N'+84       ', 1, 0, CAST(0x0000A92D01246828 AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (429, N'Virgin Islands, British', N'+1 284    ', 1, 0, CAST(0x0000A92D0124682D AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
INSERT [dbo].[Country] ([CountryID], [CountryName], [PhoneZipCode], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (430, N'Virgin Islands, U.S.', N'+1 340    ', 1, 0, CAST(0x0000A92D0124682D AS DateTime), N'accouting', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Country] OFF
/****** Object:  Table [dbo].[Package]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Package](
	[PackageID] [int] IDENTITY(1,1) NOT NULL,
	[PackageName] [nvarchar](250) NULL,
	[PackageValue] [float] NULL,
	[CreateDate] [datetime] NULL,
	[NumberExpire] [int] NULL,
	[IsActive] [smallint] NULL,
	[Priority] [int] NULL,
	[PackageNameEnglish] [nvarchar](250) NULL,
 CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED 
(
	[PackageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Package] ON
INSERT [dbo].[Package] ([PackageID], [PackageName], [PackageValue], [CreateDate], [NumberExpire], [IsActive], [Priority], [PackageNameEnglish]) VALUES (1, N'1 Euro/tuần', 1, CAST(0x0000A79000000000 AS DateTime), 7, 1, 1, N'1 Euro/week')
INSERT [dbo].[Package] ([PackageID], [PackageName], [PackageValue], [CreateDate], [NumberExpire], [IsActive], [Priority], [PackageNameEnglish]) VALUES (2, N'6 Euro/tháng', 5, CAST(0x0000A79000000000 AS DateTime), 30, 1, 2, N'6 Euro/month')
INSERT [dbo].[Package] ([PackageID], [PackageName], [PackageValue], [CreateDate], [NumberExpire], [IsActive], [Priority], [PackageNameEnglish]) VALUES (3, N'35 Euro/năm', 15, CAST(0x0000A79000000000 AS DateTime), 365, 1, 3, N'35 Euro/year')
SET IDENTITY_INSERT [dbo].[Package] OFF
/****** Object:  Table [dbo].[NotificationSystem]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationSystem](
	[NotificationID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NOT NULL,
	[Description] [nvarchar](4000) NULL,
	[SenderID] [int] NULL,
	[ReceiveMemberID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Status] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[TypeNotificationID] [int] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_NotificationSystem] PRIMARY KEY CLUSTERED 
(
	[NotificationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MemberReference]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemberReference](
	[MemberID] [int] NOT NULL,
	[LinkReference] [nvarchar](250) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[MemberIDReference] [int] NOT NULL,
	[LockID] [nvarchar](250) NULL,
	[Status] [smallint] NULL,
	[Amount] [decimal](18, 5) NULL,
 CONSTRAINT [PK_MemberReference] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC,
	[MemberIDReference] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (910, N'daisy.k@cscjackpot.com/2d697732311d73645cf44932ad30b95e', CAST(0x0000A92E0099F301 AS DateTime), 910, N'c8600268-9468-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (913, N'hathanhpc20@gmail.com/0702697ae6c70215153aa362fe60f54b', CAST(0x0000A92E00B1668A AS DateTime), 913, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (913, N'hathanhpc20@gmail.com/0702697ae6c70215153aa362fe60f54b', CAST(0x0000A92E00B2DCAD AS DateTime), 914, N'4bd0326b-9475-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (914, N'thanhha12196@gmail.com/e7dcb3321918c5ac7a7b8c8fa4e748e5', CAST(0x0000A92E00B2DCAD AS DateTime), 914, N'4bc7e30e-9475-11e8-8c19-00163ec5394d', 1, CAST(0.00000 AS Decimal(18, 5)))
INSERT [dbo].[MemberReference] ([MemberID], [LinkReference], [CreatedDate], [MemberIDReference], [LockID], [Status], [Amount]) VALUES (915, N'thienbui1987@gmail.com/b3eabf9a2a7747c1734743ded56c5141', CAST(0x0000A92E00B36B22 AS DateTime), 915, N'', 0, CAST(0.00020 AS Decimal(18, 5)))
/****** Object:  Table [dbo].[Member_Wallet]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Member_Wallet](
	[WalletID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[IndexWallet] [int] NULL,
	[NumberCoin] [float] NULL,
	[IsActive] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUser] [nvarchar](250) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateUser] [nvarchar](250) NULL,
	[DeleteDate] [datetime] NULL,
	[DeleteUser] [nvarchar](250) NULL,
	[IsDelete] [smallint] NULL,
	[WalletAddress] [nvarchar](250) NULL,
	[RippleAddress] [nvarchar](250) NULL,
	[SerectKeyRipple] [nvarchar](250) NULL,
 CONSTRAINT [PK_Member_Wallet] PRIMARY KEY CLUSTERED 
(
	[WalletID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Member]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Member](
	[MemberID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NULL,
	[E_Wallet] [nvarchar](250) NULL,
	[Password] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[Points] [numeric](18, 10) NULL,
	[CreateDate] [datetime] NULL,
	[FullName] [nvarchar](250) NULL,
	[Mobile] [char](15) NULL,
	[Avatar] [nvarchar](250) NULL,
	[Gender] [smallint] NULL,
	[Birthday] [datetime] NULL,
	[ClientID] [int] NULL,
	[IsCompany] [smallint] NULL,
	[IsUserType] [smallint] NULL,
	[UserTypeID] [varchar](100) NULL,
	[MemberIDSync] [varchar](50) NULL,
	[LinkLogin] [nvarchar](500) NULL,
	[NumberTicketFree] [int] NULL,
	[LinkReference] [nvarchar](500) NULL,
	[SmsCode] [nvarchar](50) NULL,
	[IsIndentifySms] [smallint] NULL,
	[ExpireSmSCode] [datetime] NULL,
	[TotalSmsCodeInput] [int] NULL,
 CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Login normal, 1: Login facebook, 2: Login google+' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'IsUserType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'FacebookID or GoogleID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Member', @level2type=N'COLUMN',@level2name=N'UserTypeID'
GO
SET IDENTITY_INSERT [dbo].[Member] ON
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (910, N'daisy.k@cscjackpot.com', NULL, N'dbda0a38a7ad4c1e018f3841a104ecfb', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E0099EE9F AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'daisy.k@cscjackpot.com/464032dacc912cd0c55f3a0390ba3500', 0, N'daisy.k@cscjackpot.com/2d697732311d73645cf44932ad30b95e', N'YnT4Jbyv', 1, CAST(0x0000A92E00CBFAC0 AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (913, N'hathanhpc20@gmail.com', NULL, N'36f7ca4c09d94ab6d695a6dec90842a9', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E00B165AC AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'hathanhpc20@gmail.com/49198241fc2f9176b4a550f40094b5c3', 0, N'hathanhpc20@gmail.com/0702697ae6c70215153aa362fe60f54b', NULL, NULL, NULL, NULL)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (914, N'thanhha12196@gmail.com', NULL, N'36f7ca4c09d94ab6d695a6dec90842a9', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E00B2DBA6 AS DateTime), N' ', N'01649663678    ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'thanhha12196@gmail.com/29aef278fb1fa3162a37278198ecf737', 0, N'thanhha12196@gmail.com/e7dcb3321918c5ac7a7b8c8fa4e748e5', N'1dmBaBWm', 1, CAST(0x0000A92E00E4C6CC AS DateTime), 1)
INSERT [dbo].[Member] ([MemberID], [Email], [E_Wallet], [Password], [IsActive], [IsDelete], [Points], [CreateDate], [FullName], [Mobile], [Avatar], [Gender], [Birthday], [ClientID], [IsCompany], [IsUserType], [UserTypeID], [MemberIDSync], [LinkLogin], [NumberTicketFree], [LinkReference], [SmsCode], [IsIndentifySms], [ExpireSmSCode], [TotalSmsCodeInput]) VALUES (915, N'thienbui1987@gmail.com', NULL, N'4e995768a6900f4ea22d2664a4c6691a', 1, 0, CAST(0.0000000000 AS Numeric(18, 10)), CAST(0x0000A92E00B369EE AS DateTime), N' ', N'               ', N'', 1, CAST(0x0000806800000000 AS DateTime), NULL, NULL, 0, NULL, NULL, N'thienbui1987@gmail.com/b291363355cf1b35011cfcdbc1f8aa92', 0, N'thienbui1987@gmail.com/b3eabf9a2a7747c1734743ded56c5141', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Member] OFF
/****** Object:  Table [dbo].[LinkResetPassword]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LinkResetPassword](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[LinkReset] [nvarchar](1000) NULL,
	[Status] [smallint] NULL,
	[NumberSend] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ExpireLink] [datetime] NULL,
	[IPAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_LinkResetPassword] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[LinkResetPassword] ON
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (55, N'hathanhpc20@gmail.com', N'/email=hathanhpc20@gmail.com&&key=0f1f4a8184b1c7f7459164a4c57fdbfe', 0, 1, CAST(0x0000A92D01446842 AS DateTime), CAST(0x0000A92D01487DC0 AS DateTime), N'')
INSERT [dbo].[LinkResetPassword] ([ID], [Email], [LinkReset], [Status], [NumberSend], [CreatedDate], [ExpireLink], [IPAddress]) VALUES (56, N'daisy.k@cscjackpot.com', N'/email=daisy.k@cscjackpot.com&&key=e65441999647aceb9fb464a14709a6a3', 0, 1, CAST(0x0000A92E00AFEE1C AS DateTime), CAST(0x0000A92E00B3E6B0 AS DateTime), N'')
SET IDENTITY_INSERT [dbo].[LinkResetPassword] OFF
/****** Object:  Table [dbo].[IPConfig]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IPConfig](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IPAddress] [nvarchar](50) NULL,
	[ServiceDomain] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
 CONSTRAINT [PK_IPConfig] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[IPConfig] ON
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (1, N'127.0.0.1', N'http://localhost:61923', 1, 0, CAST(0x0000A8D700000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (2, N'::1', N'http://localhost:61950/CommonSvc.svc', 1, 0, CAST(0x0000A8E100000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (3, N'::1', N'http://services.lotte.bigbrothers.gold/CommonSvc.svc', 1, 0, CAST(0x0000A8E100000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (4, N'127.0.0.1', N'http://services.lotte.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E100000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (5, N'::1', N'http://localhost:61923/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (6, N'::1', N'http://services.lotte.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (7, N'::1', N'http://localhost:61923/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (8, N'::1', N'http://lotte.services.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (9, N'127.0.0.1', N'http://lotte.services.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (10, N'::1', N'http://services.lotterycarcoin.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (11, N'14.161.24.184', N'http://services.lotterycarcoin.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E900000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (12, N'14.161.24.184', N'http://servicecmslotteclp.bigbrothers.gold/CommonSvc.svc', 1, 0, CAST(0x0000A8EB00000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (13, N'127.0.0.1', N'http://localhost:61950/CommonSvc.svc', 1, 0, CAST(0x0000A8D700000000 AS DateTime), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (14, N'192.168.1.7', N'http://servicecmslotteclp.bigbrothers.gold/CommonServices.svc', 1, 0, CAST(0x0000A8E100000000 AS DateTime), N'trungnhan', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (15, N'134.119.181.122', N'http://services.cscjackpot.com/CommonServices.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (16, N'134.119.181.122', N'http://cmsservices.cscjackpot.com/CommonSvc.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (17, N'127.0.0.1', N'http://services.cscjackpot.com/CommonServices.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'thienbui', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (18, N'134.119.181.122', N'http://noterrorservices.cscjackpot.com/CommonServices.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'kienduc', NULL, NULL, NULL, NULL)
INSERT [dbo].[IPConfig] ([ID], [IPAddress], [ServiceDomain], [IsActive], [IsDeleted], [CreatedDate], [CreatedUser], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser]) VALUES (19, N'134.119.181.122', N'http://noterrorcmsservices.cscjackpot.com/CommonSvc.svc', 1, 0, CAST(0x0000A91D00000000 AS DateTime), N'kienduc', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[IPConfig] OFF
/****** Object:  Table [dbo].[GroupAdmin]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupAdmin](
	[GroupID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
 CONSTRAINT [PK_GroupAdmin] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[GroupAdmin] ON
INSERT [dbo].[GroupAdmin] ([GroupID], [GroupName], [IsActive]) VALUES (1, N'administrator', 1)
INSERT [dbo].[GroupAdmin] ([GroupID], [GroupName], [IsActive]) VALUES (2, N'accounting', 1)
INSERT [dbo].[GroupAdmin] ([GroupID], [GroupName], [IsActive]) VALUES (3, N'staff', 1)
SET IDENTITY_INSERT [dbo].[GroupAdmin] OFF
/****** Object:  Table [dbo].[Award]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Award](
	[AwardID] [int] IDENTITY(1,1) NOT NULL,
	[AwardName] [nvarchar](250) NULL,
	[AwardValue] [float] NULL,
	[CreateDate] [datetime] NULL,
	[Priority] [int] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[AwardNameEnglish] [nvarchar](250) NULL,
	[NumberBallGold] [int] NULL,
	[NumberBallNormal] [int] NULL,
	[AwardFee] [decimal](18, 8) NULL,
	[AwardPercent] [float] NULL,
 CONSTRAINT [PK_Award] PRIMARY KEY CLUSTERED 
(
	[AwardID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Award] ON
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (1, N'Giải đặc biệt', 10, CAST(0x0000A79C00000000 AS DateTime), 1, 1, 0, N'Jackpot', 1, 5, CAST(0.50000000 AS Decimal(18, 8)), 5)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (2, N'16.0000', 4, CAST(0x0000A8E100000000 AS DateTime), 2, 1, 0, N'First pzire', 0, 5, CAST(0.00100000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (3, N'4.0000', 0.5, CAST(0x0000A8E20047E230 AS DateTime), 2, 1, 0, N'Second prize', 1, 4, CAST(0.00010000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (4, N'0.08', 0.02, CAST(0x0000A8E20047FD86 AS DateTime), 2, 1, 0, N'Third prize', 0, 4, CAST(0.00010000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (5, N'0.02', 0.006, CAST(0x0000A8E200483840 AS DateTime), 2, 1, 0, N'Fourth prize', 1, 3, CAST(0.00000000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (6, N'0.008', 0.001, CAST(0x0000A8E200484873 AS DateTime), 2, 1, 0, N'Fifth prize	', 0, 3, CAST(0.00000000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (7, N'0.004', 0.0008, CAST(0x0000A8E200485DB6 AS DateTime), 2, 1, 0, N'Sixth prize', 1, 2, CAST(0.00000000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (8, N'0.002', 0.0005, CAST(0x0000A8E200486EEC AS DateTime), 2, 1, 0, N'Seventh prize', 1, 1, CAST(0.00000000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (9, N'0.0012', 0.0004, CAST(0x0000A8E2004909D6 AS DateTime), 2, 1, 0, N'Eighth prize', 1, 0, CAST(0.00000000 AS Decimal(18, 8)), NULL)
INSERT [dbo].[Award] ([AwardID], [AwardName], [AwardValue], [CreateDate], [Priority], [IsActive], [IsDelete], [AwardNameEnglish], [NumberBallGold], [NumberBallNormal], [AwardFee], [AwardPercent]) VALUES (10, N'0.0004', 0.0002, CAST(0x0000A8E200491E16 AS DateTime), 2, 1, 0, N'Ninth prize', 0, 2, CAST(0.00000000 AS Decimal(18, 8)), NULL)
SET IDENTITY_INSERT [dbo].[Award] OFF
/****** Object:  Table [dbo].[AuthenticateLink]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthenticateLink](
	[AuthenticateID] [int] IDENTITY(1,1) NOT NULL,
	[LinkAuthenticate] [nvarchar](1000) NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[ExpireDate] [datetime] NULL,
	[Note] [nvarchar](3500) NULL,
	[CreateUser] [datetime] NULL,
 CONSTRAINT [PK_AuthenticateLink] PRIMARY KEY CLUSTERED 
(
	[AuthenticateID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AwardNumber]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardNumber](
	[NumberID] [int] IDENTITY(1,1) NOT NULL,
	[AwardID] [int] NULL,
	[NumberValue] [varchar](50) NOT NULL,
	[CreateDate] [datetime] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[Priority] [int] NULL,
	[StationName] [nvarchar](250) NULL,
	[StationNameEnglish] [nvarchar](250) NULL,
	[Gencode] [nvarchar](500) NULL,
 CONSTRAINT [PK_AwardNumber] PRIMARY KEY CLUSTERED 
(
	[NumberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AwardNumber] ON
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (1, 5, N'23790', CAST(0x0000A79F011F5480 AS DateTime), 1, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (2, 4, N'51783', CAST(0x0000A7AC011F5DD3 AS DateTime), 1, 0, 1, N'Long An', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (3, 3, N'63927', CAST(0x0000A7AD011F6817 AS DateTime), 1, 0, 1, N'An Giang', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (4, 2, N'84374', CAST(0x0000A7AE011F7187 AS DateTime), 1, 0, 1, N'Tiền Giang', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (5, 1, N'25176', CAST(0x0000A7AF011F86EA AS DateTime), 1, 0, 1, N'Vĩnh Long', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (6, 1, N'23324', CAST(0x0000A7B0009454D6 AS DateTime), 1, 0, 1, N'Tp Hồ Chí Minh', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (9, 1, N'35790', CAST(0x0000A7B9009454D6 AS DateTime), 1, 0, 1, N'Long An', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (10, 1, N'32432', CAST(0x0000A7BA0099671D AS DateTime), 1, 0, 1, N'Hồ Chí Minh', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (18, 1, N'54870', CAST(0x0000A7CE0099671D AS DateTime), 1, 0, 1, N'Hồ Chí Minh', NULL, NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (19, 1, N'46456', CAST(0x0000A7D0011D5643 AS DateTime), 1, 0, 1, N'Vĩnh Long', N'VL Television', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (20, 1, N'03273', CAST(0x0000A7D100BC1690 AS DateTime), 1, 0, 1, N'Tiền Giang', N'TG Television', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (21, 1, N'99999', CAST(0x0000A7D801112083 AS DateTime), 1, 0, 1, N'TP.HCM', N'TP.HCM', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (22, 2, N'88289', CAST(0x0000A7D801114F57 AS DateTime), 1, 0, 1, N'Bình Dương', N'Bình Dương', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (23, 2, N'87489', CAST(0x0000A7DC01114F57 AS DateTime), 1, 0, 1, N'Bình Dương', N'Bình Dương', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (24, 1, N'43545', CAST(0x0000A7DD011BCA1A AS DateTime), 1, 0, 1, N'Hà Nội', N'Ha Noi', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (25, 6, N'12345', CAST(0x0000A7F300B4DE00 AS DateTime), 1, 0, 1, N'Ha Noi', N'Ha Noi', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (26, 6, N'34544', CAST(0x0000A7F500E460A5 AS DateTime), 1, 0, 1, N'num1', N'num12', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (27, 2, N'23', CAST(0x0000A8120099B084 AS DateTime), 1, 0, 1, N'giai nhat', N'number one', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (28, 1, N'3.82212e+011', CAST(0x0000A86200BBACCE AS DateTime), 1, 0, 1, N'', N'', NULL)
INSERT [dbo].[AwardNumber] ([NumberID], [AwardID], [NumberValue], [CreateDate], [IsActive], [IsDelete], [Priority], [StationName], [StationNameEnglish], [Gencode]) VALUES (29, 9, N'1.21212e+011', CAST(0x0000A86200C76AB1 AS DateTime), 1, 0, 1, N'', N'', NULL)
SET IDENTITY_INSERT [dbo].[AwardNumber] OFF
/****** Object:  Table [dbo].[AwardMegaballLog]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AwardMegaballLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[Value] [decimal](18, 5) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AwardMegaballLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Ticket Number Of Times' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AwardMegaballLog', @level2type=N'COLUMN',@level2name=N'Value'
GO
SET IDENTITY_INSERT [dbo].[AwardMegaballLog] ON
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (0, N'hathanhpc20@gmail.com', CAST(10.01506 AS Decimal(18, 5)), CAST(0x0000A92D00A062AC AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (1, N'hathanhpc20@gmail.com', CAST(10.01506 AS Decimal(18, 5)), CAST(0x0000A92D00A062AC AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (2, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A062AC AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (3, N'hathanhpc20@gmail.com', CAST(10.01110 AS Decimal(18, 5)), CAST(0x0000A92D00A1AC7B AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (4, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A1AC7B AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (5, N'hathanhpc20@gmail.com', CAST(10.02354 AS Decimal(18, 5)), CAST(0x0000A92D00A3B0EA AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (6, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A3B0EA AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (7, N'hathanhpc20@gmail.com', CAST(10.01110 AS Decimal(18, 5)), CAST(0x0000A92D00A537CE AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (8, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A537CE AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (9, N'hathanhpc20@gmail.com', CAST(10.01856 AS Decimal(18, 5)), CAST(0x0000A92D00A91E5F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (10, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00A91E5F AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (11, N'hathanhpc20@gmail.com', CAST(10.02354 AS Decimal(18, 5)), CAST(0x0000A92D00AB0832 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (12, N'kienduc94@gmail.com', CAST(0.00570 AS Decimal(18, 5)), CAST(0x0000A92D00AB0832 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (13, N'daisy.k@cscjackpot.com', CAST(0.00700 AS Decimal(18, 5)), CAST(0x0000A92D01763967 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (14, N'hathanhpc20@gmail.com', CAST(0.00040 AS Decimal(18, 5)), CAST(0x0000A92D01763967 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (15, N'nguyenlytruongson@gmail.com', CAST(0.01080 AS Decimal(18, 5)), CAST(0x0000A92D01763967 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (16, N'thanhha12196@gmail.com', CAST(0.00620 AS Decimal(18, 5)), CAST(0x0000A92D01763967 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (17, N'hathanhpc20@gmail.com', CAST(0.00850 AS Decimal(18, 5)), CAST(0x0000A92D018A7C40 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (18, N'thanhha12196@gmail.com', CAST(0.00580 AS Decimal(18, 5)), CAST(0x0000A92D018A7C40 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (19, N'hathanhpc20@gmail.com', CAST(10.01662 AS Decimal(18, 5)), CAST(0x0000A92E00038A08 AS DateTime))
INSERT [dbo].[AwardMegaballLog] ([ID], [Email], [Value], [CreatedDate]) VALUES (20, N'thanhha12196@gmail.com', CAST(0.00580 AS Decimal(18, 5)), CAST(0x0000A92E00038A08 AS DateTime))
SET IDENTITY_INSERT [dbo].[AwardMegaballLog] OFF
/****** Object:  Table [dbo].[AwardMegaball]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AwardMegaball](
	[NumberID] [int] IDENTITY(1,1) NOT NULL,
	[AwardID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[IsActive] [smallint] NULL,
	[IsDelete] [smallint] NULL,
	[FirstNumber] [int] NULL,
	[SecondNumber] [int] NULL,
	[ThirdNumber] [int] NULL,
	[FourthNumber] [int] NULL,
	[FivethNumber] [int] NULL,
	[ExtraNumber] [int] NULL,
	[Priority] [int] NULL,
	[GenCode] [nvarchar](500) NULL,
	[JackPot] [float] NULL,
	[JackPotWinner] [float] NULL,
	[DrawID] [int] NULL,
	[TotalWon] [decimal](18, 5) NULL,
 CONSTRAINT [PK_AwardMegaball] PRIMARY KEY CLUSTERED 
(
	[NumberID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total JackPot previous won' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AwardMegaball', @level2type=N'COLUMN',@level2name=N'JackPotWinner'
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](250) NULL,
	[Hashkey] [nvarchar](250) NULL,
	[IsActive] [smallint] NULL,
	[CreateDate] [datetime] NULL,
	[FullName] [nvarchar](250) NULL,
	[Email] [nvarchar](250) NULL,
	[Mobile] [char](50) NULL,
	[Address] [nvarchar](250) NULL,
	[Avatar] [nvarchar](250) NULL,
	[IsDelete] [smallint] NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Admin] ON
INSERT [dbo].[Admin] ([AdminID], [UserName], [Password], [Hashkey], [IsActive], [CreateDate], [FullName], [Email], [Mobile], [Address], [Avatar], [IsDelete]) VALUES (1, N'administrator', N'78a8495d6940fc1207e964e88adbe90c', N'1234', 1, CAST(0x0000A79C00000000 AS DateTime), N'Admin', N'admin@gmail.com', N'34346456                                          ', N'Bình Thạnh Hồ Chí Minh', NULL, 0)
INSERT [dbo].[Admin] ([AdminID], [UserName], [Password], [Hashkey], [IsActive], [CreateDate], [FullName], [Email], [Mobile], [Address], [Avatar], [IsDelete]) VALUES (2, N'accounting', N'8d421e892a47dff539f46142eb09e56b', N'1234', 1, CAST(0x0000A8EB010406FC AS DateTime), N'Kế toán', N'accounting@gmail.com', N'3462456757                                        ', N'Hồ Chí Minh', N'', 0)
SET IDENTITY_INSERT [dbo].[Admin] OFF
/****** Object:  Table [dbo].[AccessRight]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccessRight](
	[GroupID] [int] NOT NULL,
	[AdminID] [int] NOT NULL
) ON [PRIMARY]
GO
INSERT [dbo].[AccessRight] ([GroupID], [AdminID]) VALUES (1, 1)
INSERT [dbo].[AccessRight] ([GroupID], [AdminID]) VALUES (2, 2)
/****** Object:  Table [dbo].[Booking]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Booking](
	[BookingID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[NumberValue] [varchar](50) NULL,
	[Quantity] [int] NULL,
	[CreateDate] [datetime] NULL,
	[OpenDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](2500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
 CONSTRAINT [PK_Booking] PRIMARY KEY CLUSTERED 
(
	[BookingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AwardWithdraw]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AwardWithdraw](
	[TransactionID] [char](50) NOT NULL,
	[RequestMemberID] [nvarchar](250) NULL,
	[RequestWalletAddress] [nvarchar](250) NULL,
	[RequestDate] [datetime] NULL,
	[RequestStatus] [smallint] NULL,
	[AwardDate] [datetime] NULL,
	[CoinIDWithdraw] [char](10) NULL,
	[ValuesWithdraw] [numeric](18, 8) NULL,
	[AdminIDApprove] [nvarchar](250) NULL,
	[ApproveDate] [datetime] NULL,
	[ApproveNote] [nvarchar](2000) NULL,
	[IsDeleted] [smallint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL,
	[BookingID] [int] NULL,
 CONSTRAINT [PK_AwardWithdraw] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[AwardWithdraw] ([TransactionID], [RequestMemberID], [RequestWalletAddress], [RequestDate], [RequestStatus], [AwardDate], [CoinIDWithdraw], [ValuesWithdraw], [AdminIDApprove], [ApproveDate], [ApproveNote], [IsDeleted], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser], [BookingID]) VALUES (N'3afadb44-9479-11e8-8c19-00163ec5394d              ', N'hathanhpc20@gmail.com', N'mtMhZzFFUE77MgR6Xgc3Jsf4F7xW9MvKTX', CAST(0x0000A92E00BB33EF AS DateTime), 1, CAST(0x0000A92E00457158 AS DateTime), N'BTC       ', CAST(10.00812000 AS Numeric(18, 8)), N'', CAST(0x0000000000000000 AS DateTime), N'', 0, CAST(0x0000000000000000 AS DateTime), N'', CAST(0x0000000000000000 AS DateTime), N'', 24414)
INSERT [dbo].[AwardWithdraw] ([TransactionID], [RequestMemberID], [RequestWalletAddress], [RequestDate], [RequestStatus], [AwardDate], [CoinIDWithdraw], [ValuesWithdraw], [AdminIDApprove], [ApproveDate], [ApproveNote], [IsDeleted], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser], [BookingID]) VALUES (N'54cb1763-9479-11e8-8c19-00163ec5394d              ', N'hathanhpc20@gmail.com', N'mtMhZzFFUE77MgR6Xgc3Jsf4F7xW9MvKTX', CAST(0x0000A92E00BB66A7 AS DateTime), 1, CAST(0x0000A92E00457158 AS DateTime), N'BTC       ', CAST(0.00020000 AS Numeric(18, 8)), N'', CAST(0x0000000000000000 AS DateTime), N'', 0, CAST(0x0000000000000000 AS DateTime), N'', CAST(0x0000000000000000 AS DateTime), N'', 24300)
INSERT [dbo].[AwardWithdraw] ([TransactionID], [RequestMemberID], [RequestWalletAddress], [RequestDate], [RequestStatus], [AwardDate], [CoinIDWithdraw], [ValuesWithdraw], [AdminIDApprove], [ApproveDate], [ApproveNote], [IsDeleted], [UpdatedDate], [UpdatedUser], [DeletedDate], [DeletedUser], [BookingID]) VALUES (N'5abfb7b5-9479-11e8-8c19-00163ec5394d              ', N'hathanhpc20@gmail.com', N'mtMhZzFFUE77MgR6Xgc3Jsf4F7xW9MvKTX', CAST(0x0000A92E00BB725B AS DateTime), 1, CAST(0x0000A92E00457158 AS DateTime), N'BTC       ', CAST(0.00020000 AS Numeric(18, 8)), N'', CAST(0x0000000000000000 AS DateTime), N'', 0, CAST(0x0000000000000000 AS DateTime), N'', CAST(0x0000000000000000 AS DateTime), N'', 24272)
/****** Object:  Table [dbo].[BookingMegaball]    Script Date: 07/31/2018 11:30:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BookingMegaball](
	[BookingID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NULL,
	[CreateDate] [datetime] NULL,
	[OpenDate] [datetime] NULL,
	[Status] [int] NULL,
	[TransactionCode] [char](100) NULL,
	[Note] [nvarchar](2500) NULL,
	[NoteEnglish] [nvarchar](2500) NULL,
	[FirstNumber] [int] NULL,
	[SecondNumber] [int] NULL,
	[ThirdNumber] [int] NULL,
	[FourthNumber] [int] NULL,
	[FivethNumber] [int] NULL,
	[ExtraNumber] [int] NULL,
	[Quantity] [int] NULL,
	[ValuePoint] [numeric](18, 10) NULL,
	[DrawID] [int] NULL,
	[IsFreeTicket] [smallint] NULL,
 CONSTRAINT [PK_BookingMegaball] PRIMARY KEY CLUSTERED 
(
	[BookingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[BookingMegaball] ON
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24210, 913, CAST(0x0000A92E003F0745 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 3, 9, 17, 19, 28, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24211, 913, CAST(0x0000A92E003F0745 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 14, 26, 28, 38, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24212, 913, CAST(0x0000A92E003F074A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 11, 26, 28, 39, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24213, 913, CAST(0x0000A92E003F074A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 23, 30, 32, 35, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24214, 913, CAST(0x0000A92E003F074A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 8, 12, 17, 30, 36, 10, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24215, 913, CAST(0x0000A92E003F074A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 4, 5, 13, 26, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24216, 913, CAST(0x0000A92E003F074F AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 4, 17, 27, 38, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24217, 913, CAST(0x0000A92E003F074F AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 18, 24, 27, 28, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24218, 913, CAST(0x0000A92E003F074F AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 18, 21, 24, 35, 38, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24219, 913, CAST(0x0000A92E003F0754 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 5, 19, 26, 37, 39, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24220, 913, CAST(0x0000A92E003F0754 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 15, 20, 23, 39, 18, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24221, 913, CAST(0x0000A92E003F0754 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 8, 16, 20, 25, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24222, 913, CAST(0x0000A92E003F0754 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 3, 8, 16, 28, 30, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24223, 913, CAST(0x0000A92E003F0754 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 5, 8, 18, 20, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24224, 913, CAST(0x0000A92E003F0758 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 6, 12, 19, 22, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24225, 913, CAST(0x0000A92E003F0758 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 14, 18, 26, 32, 5, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24226, 913, CAST(0x0000A92E003F0758 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 13, 22, 23, 29, 36, 5, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24227, 913, CAST(0x0000A92E003F0758 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 4, 7, 14, 20, 34, 5, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24228, 913, CAST(0x0000A92E003F075E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 8, 9, 11, 16, 17, 3, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24229, 913, CAST(0x0000A92E003F075E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 12, 18, 35, 36, 37, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24230, 913, CAST(0x0000A92E003F075E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 13, 14, 20, 26, 27, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24231, 913, CAST(0x0000A92E003F075E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 8, 18, 29, 34, 38, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24232, 913, CAST(0x0000A92E003F0762 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 5, 14, 31, 37, 38, 18, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24233, 913, CAST(0x0000A92E003F0766 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 7, 9, 35, 38, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24234, 913, CAST(0x0000A92E003F0766 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 15, 23, 27, 38, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24235, 913, CAST(0x0000A92E003F0766 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 5, 16, 23, 32, 34, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24236, 913, CAST(0x0000A92E003F0766 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 8, 29, 30, 33, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24237, 913, CAST(0x0000A92E003F0766 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 8, 12, 27, 38, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24238, 913, CAST(0x0000A92E003F076B AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 4, 15, 20, 22, 39, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24239, 913, CAST(0x0000A92E003F076B AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 15, 18, 19, 20, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24240, 913, CAST(0x0000A92E003F076B AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 5, 12, 14, 15, 32, 2, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24241, 913, CAST(0x0000A92E003F076B AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 5, 8, 13, 21, 38, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24242, 913, CAST(0x0000A92E003F0770 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 8, 30, 36, 37, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24243, 913, CAST(0x0000A92E003F0770 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 3, 18, 25, 27, 31, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24244, 913, CAST(0x0000A92E003F0770 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 15, 16, 25, 31, 3, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24245, 913, CAST(0x0000A92E003F0770 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 5, 14, 15, 32, 35, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24246, 913, CAST(0x0000A92E003F0770 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 13, 33, 36, 38, 39, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24247, 913, CAST(0x0000A92E003F0774 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 6, 20, 31, 37, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24248, 913, CAST(0x0000A92E003F0774 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 13, 21, 24, 25, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24249, 913, CAST(0x0000A92E003F0774 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 14, 18, 36, 37, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24250, 913, CAST(0x0000A92E003F0774 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 9, 11, 17, 29, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24251, 913, CAST(0x0000A92E003F0774 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 3, 6, 34, 38, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24252, 913, CAST(0x0000A92E003F077A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 14, 25, 32, 34, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24253, 913, CAST(0x0000A92E003F077C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 16, 23, 25, 34, 35, 2, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24254, 913, CAST(0x0000A92E003F077C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 3, 8, 9, 30, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24255, 913, CAST(0x0000A92E003F077D AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 3, 15, 19, 20, 32, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24256, 913, CAST(0x0000A92E003F0780 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 20, 26, 27, 35, 38, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24257, 913, CAST(0x0000A92E003F0781 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 5, 18, 21, 32, 35, 10, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24258, 913, CAST(0x0000A92E003F0782 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 13, 20, 22, 31, 35, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24259, 913, CAST(0x0000A92E003F0784 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 20, 25, 31, 38, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24260, 913, CAST(0x0000A92E003F0784 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 23, 31, 38, 39, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24261, 913, CAST(0x0000A92E003F0784 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 4, 7, 16, 27, 32, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24262, 913, CAST(0x0000A92E003F0787 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 4, 5, 7, 14, 29, 18, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24263, 913, CAST(0x0000A92E003F0787 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 18, 21, 25, 28, 39, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24264, 913, CAST(0x0000A92E003F0787 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 10, 23, 27, 38, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24265, 913, CAST(0x0000A92E003F0787 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 8, 10, 19, 30, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24266, 913, CAST(0x0000A92E003F078C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 5, 6, 18, 32, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24267, 913, CAST(0x0000A92E003F078C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 7, 14, 21, 26, 10, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24268, 913, CAST(0x0000A92E003F078C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 22, 28, 35, 39, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24269, 913, CAST(0x0000A92E003F078C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 15, 23, 29, 38, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24270, 913, CAST(0x0000A92E003F078C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 5, 15, 19, 33, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24271, 913, CAST(0x0000A92E003F0791 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 3, 9, 34, 37, 39, 3, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24272, 913, CAST(0x0000A92E003F0791 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 11, 12, 24, 26, 32, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24273, 913, CAST(0x0000A92E003F0791 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 18, 21, 28, 39, 3, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24274, 913, CAST(0x0000A92E003F0791 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 13, 15, 24, 36, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24275, 913, CAST(0x0000A92E003F0796 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 8, 15, 24, 34, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24276, 913, CAST(0x0000A92E003F0796 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 3, 7, 14, 30, 31, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24277, 913, CAST(0x0000A92E003F0796 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 17, 20, 22, 36, 38, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24278, 913, CAST(0x0000A92E003F079A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 11, 14, 22, 23, 37, 18, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24279, 913, CAST(0x0000A92E003F079A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 11, 13, 16, 17, 31, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24280, 913, CAST(0x0000A92E003F079A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 18, 24, 27, 36, 37, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24281, 913, CAST(0x0000A92E003F079E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 9, 11, 16, 21, 27, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24282, 913, CAST(0x0000A92E003F079E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 8, 15, 27, 36, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24283, 913, CAST(0x0000A92E003F079E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 19, 26, 28, 32, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24284, 913, CAST(0x0000A92E003F079E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 12, 17, 23, 35, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24285, 913, CAST(0x0000A92E003F079E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 13, 19, 21, 29, 37, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24286, 913, CAST(0x0000A92E003F07A3 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 7, 10, 24, 38, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24287, 913, CAST(0x0000A92E003F07A3 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 3, 6, 24, 30, 35, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24288, 913, CAST(0x0000A92E003F07A3 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 4, 9, 12, 25, 39, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24289, 913, CAST(0x0000A92E003F07A8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 9, 12, 20, 26, 31, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24290, 913, CAST(0x0000A92E003F07A8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 2, 16, 19, 38, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24291, 913, CAST(0x0000A92E003F07A8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 2, 17, 22, 25, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24292, 913, CAST(0x0000A92E003F07A8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 19, 29, 31, 35, 36, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24293, 913, CAST(0x0000A92E003F07AD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 17, 22, 28, 32, 39, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24294, 913, CAST(0x0000A92E003F07AD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 4, 18, 23, 28, 37, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24295, 913, CAST(0x0000A92E003F07AD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 8, 9, 13, 23, 29, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24296, 913, CAST(0x0000A92E003F07AD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 3, 20, 26, 32, 34, 2, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24297, 913, CAST(0x0000A92E003F07AD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 8, 13, 16, 27, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24298, 913, CAST(0x0000A92E003F07AD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 16, 22, 23, 30, 39, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24299, 913, CAST(0x0000A92E003F07B2 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 23, 27, 29, 30, 32, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24300, 913, CAST(0x0000A92E003F07B2 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 4, 7, 11, 14, 38, 3, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24301, 913, CAST(0x0000A92E003F07B2 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 6, 7, 8, 9, 22, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24302, 913, CAST(0x0000A92E003F07B2 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 2, 14, 16, 20, 22, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24303, 913, CAST(0x0000A92E003F07B2 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 9, 14, 15, 33, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24304, 913, CAST(0x0000A92E003F07B6 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 22, 28, 33, 36, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24305, 913, CAST(0x0000A92E003F07B7 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 4, 20, 24, 36, 39, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24306, 913, CAST(0x0000A92E003F07B8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 7, 10, 15, 21, 34, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24307, 913, CAST(0x0000A92E003F07B9 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 7, 10, 28, 31, 5, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24308, 913, CAST(0x0000A92E003F07BA AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 5, 6, 14, 28, 39, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24309, 913, CAST(0x0000A92E003F07BB AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'7fb9b6ba648520abfcb1372d5d0d3690                                                                    ', N'', N'', 1, 10, 13, 19, 39, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
GO
print 'Processed 100 total records'
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24310, 914, CAST(0x0000A92E003FDF80 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 23, 26, 37, 38, 2, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24311, 914, CAST(0x0000A92E003FDF85 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 5, 18, 28, 32, 38, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24312, 914, CAST(0x0000A92E003FDF85 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 14, 19, 27, 35, 39, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24313, 914, CAST(0x0000A92E003FDF85 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 9, 12, 16, 19, 26, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24314, 914, CAST(0x0000A92E003FDF85 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 4, 17, 25, 30, 31, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24315, 914, CAST(0x0000A92E003FDF8A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 6, 30, 32, 34, 37, 10, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24316, 914, CAST(0x0000A92E003FDF8A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 8, 11, 14, 36, 37, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24317, 914, CAST(0x0000A92E003FDF8A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 5, 11, 19, 30, 37, 2, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24318, 914, CAST(0x0000A92E003FDF8A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 2, 13, 24, 34, 38, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24319, 914, CAST(0x0000A92E003FDF8A AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 5, 11, 13, 35, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24320, 914, CAST(0x0000A92E003FDF8E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 21, 29, 30, 32, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24321, 914, CAST(0x0000A92E003FDF8E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 3, 7, 34, 37, 38, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24322, 914, CAST(0x0000A92E003FDF8E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 6, 16, 35, 36, 38, 10, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24323, 914, CAST(0x0000A92E003FDF8E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 26, 27, 28, 33, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24324, 914, CAST(0x0000A92E003FDF93 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 4, 17, 20, 21, 37, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24325, 914, CAST(0x0000A92E003FDF93 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 5, 9, 14, 25, 37, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24326, 914, CAST(0x0000A92E003FDF93 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 8, 10, 18, 39, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24327, 914, CAST(0x0000A92E003FDF98 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 18, 29, 36, 37, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24328, 914, CAST(0x0000A92E003FDF98 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 9, 12, 23, 27, 33, 3, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24329, 914, CAST(0x0000A92E003FDF98 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 5, 14, 20, 25, 27, 3, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24330, 914, CAST(0x0000A92E003FDF9C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 3, 17, 22, 36, 39, 10, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24331, 914, CAST(0x0000A92E003FDF9C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 11, 14, 28, 35, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24332, 914, CAST(0x0000A92E003FDF9C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 8, 21, 22, 25, 28, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24333, 914, CAST(0x0000A92E003FDF9C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 4, 5, 10, 33, 39, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24334, 914, CAST(0x0000A92E003FDFA1 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 2, 32, 37, 39, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24335, 914, CAST(0x0000A92E003FDFA1 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 2, 20, 29, 35, 38, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24336, 914, CAST(0x0000A92E003FDFA1 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 32, 34, 35, 36, 39, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24337, 914, CAST(0x0000A92E003FDFA1 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 5, 10, 12, 19, 23, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24338, 914, CAST(0x0000A92E003FDFA6 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 13, 16, 17, 18, 23, 3, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24339, 914, CAST(0x0000A92E003FDFA6 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 5, 6, 13, 25, 29, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24340, 914, CAST(0x0000A92E003FDFA6 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 9, 12, 20, 30, 33, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24341, 914, CAST(0x0000A92E003FDFA6 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 13, 23, 25, 32, 10, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24342, 914, CAST(0x0000A92E003FDFAA AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 4, 7, 11, 23, 26, 5, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24343, 914, CAST(0x0000A92E003FDFAA AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 9, 11, 17, 23, 26, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24344, 914, CAST(0x0000A92E003FDFAA AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 17, 22, 31, 35, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24345, 914, CAST(0x0000A92E003FDFAA AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 16, 27, 30, 37, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24346, 914, CAST(0x0000A92E003FDFAF AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 15, 17, 31, 38, 39, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24347, 914, CAST(0x0000A92E003FDFAF AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 6, 10, 31, 34, 38, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24348, 914, CAST(0x0000A92E003FDFAF AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 9, 16, 18, 27, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24349, 914, CAST(0x0000A92E003FDFAF AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 8, 23, 26, 29, 39, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24350, 914, CAST(0x0000A92E003FDFAF AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 15, 16, 23, 25, 30, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24351, 914, CAST(0x0000A92E003FDFB4 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 16, 27, 33, 35, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24352, 914, CAST(0x0000A92E003FDFB4 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 14, 16, 18, 31, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24353, 914, CAST(0x0000A92E003FDFB4 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 22, 30, 33, 34, 36, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24354, 914, CAST(0x0000A92E003FDFB4 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 16, 19, 28, 39, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24355, 914, CAST(0x0000A92E003FDFB4 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 3, 8, 12, 21, 22, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24356, 914, CAST(0x0000A92E003FDFB8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 6, 12, 19, 23, 26, 18, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24357, 914, CAST(0x0000A92E003FDFB8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 22, 23, 35, 39, 5, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24358, 914, CAST(0x0000A92E003FDFB8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 29, 31, 32, 34, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24359, 914, CAST(0x0000A92E003FDFB8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 2, 6, 11, 20, 26, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24360, 914, CAST(0x0000A92E003FDFB8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 12, 16, 23, 26, 28, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24361, 914, CAST(0x0000A92E003FDFBD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 6, 9, 27, 32, 36, 5, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24362, 914, CAST(0x0000A92E003FDFBD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 8, 22, 25, 30, 33, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24363, 914, CAST(0x0000A92E003FDFBD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 12, 18, 28, 29, 38, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24364, 914, CAST(0x0000A92E003FDFBD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 13, 18, 20, 34, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24365, 914, CAST(0x0000A92E003FDFC3 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 4, 12, 28, 35, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24366, 914, CAST(0x0000A92E003FDFC4 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 24, 28, 30, 35, 36, 2, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24367, 914, CAST(0x0000A92E003FDFC5 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 3, 7, 15, 23, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24368, 914, CAST(0x0000A92E003FDFC5 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 8, 20, 22, 28, 36, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24369, 914, CAST(0x0000A92E003FDFC8 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 13, 27, 30, 34, 36, 10, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24370, 914, CAST(0x0000A92E003FDFC9 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 10, 16, 23, 27, 37, 18, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24371, 914, CAST(0x0000A92E003FDFCA AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 2, 10, 16, 36, 38, 18, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24372, 914, CAST(0x0000A92E003FDFCB AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 2, 8, 14, 23, 36, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24373, 914, CAST(0x0000A92E003FDFCC AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 4, 13, 31, 38, 39, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24374, 914, CAST(0x0000A92E003FDFCD AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 8, 13, 24, 30, 33, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24375, 914, CAST(0x0000A92E003FDFCE AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 8, 9, 24, 27, 18, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24376, 914, CAST(0x0000A92E003FDFCF AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 3, 11, 19, 26, 30, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24377, 914, CAST(0x0000A92E003FDFD1 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 5, 22, 23, 32, 35, 2, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24378, 914, CAST(0x0000A92E003FDFD2 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 17, 20, 26, 33, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24379, 914, CAST(0x0000A92E003FDFD2 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 17, 27, 36, 39, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24380, 914, CAST(0x0000A92E003FDFD3 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 3, 4, 11, 14, 39, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24381, 914, CAST(0x0000A92E003FDFD4 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 2, 10, 16, 17, 29, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24382, 914, CAST(0x0000A92E003FDFD6 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 3, 4, 7, 15, 18, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24383, 914, CAST(0x0000A92E003FDFD6 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 13, 14, 15, 20, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24384, 914, CAST(0x0000A92E003FDFD6 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 12, 22, 30, 31, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24385, 914, CAST(0x0000A92E003FDFD6 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 8, 14, 29, 33, 35, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24386, 914, CAST(0x0000A92E003FDFD9 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 3, 5, 28, 33, 39, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24387, 914, CAST(0x0000A92E003FDFD9 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 21, 25, 27, 30, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24388, 914, CAST(0x0000A92E003FDFD9 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 23, 29, 34, 38, 39, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24389, 914, CAST(0x0000A92E003FDFD9 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 9, 25, 28, 39, 2, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24390, 914, CAST(0x0000A92E003FDFDE AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 21, 25, 26, 34, 36, 1, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24391, 914, CAST(0x0000A92E003FDFDE AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 4, 7, 14, 17, 19, 4, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24392, 914, CAST(0x0000A92E003FDFDE AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 5, 6, 23, 33, 37, 14, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24393, 914, CAST(0x0000A92E003FDFDE AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 4, 8, 11, 19, 23, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24394, 914, CAST(0x0000A92E003FDFDE AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 11, 17, 26, 31, 34, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24395, 914, CAST(0x0000A92E003FDFDE AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 8, 10, 14, 31, 39, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24396, 914, CAST(0x0000A92E003FDFE3 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 10, 20, 26, 29, 32, 8, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24397, 914, CAST(0x0000A92E003FDFE3 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 15, 17, 20, 21, 30, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24398, 914, CAST(0x0000A92E003FDFE3 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 12, 14, 17, 24, 32, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24399, 914, CAST(0x0000A92E003FDFE3 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 9, 13, 15, 35, 36, 9, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24400, 914, CAST(0x0000A92E003FDFE7 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 10, 30, 35, 38, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24401, 914, CAST(0x0000A92E003FDFE7 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 6, 11, 21, 24, 30, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24402, 914, CAST(0x0000A92E003FDFE7 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 18, 30, 33, 35, 36, 16, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24403, 914, CAST(0x0000A92E003FDFE7 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 11, 16, 23, 37, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24404, 914, CAST(0x0000A92E003FDFE7 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 5, 23, 31, 36, 39, 15, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24405, 914, CAST(0x0000A92E003FDFE7 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 7, 8, 15, 23, 37, 11, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24406, 914, CAST(0x0000A92E003FDFEC AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 3, 26, 28, 30, 31, 2, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24407, 914, CAST(0x0000A92E003FDFEC AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 9, 22, 24, 28, 38, 6, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24408, 914, CAST(0x0000A92E003FDFEC AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 2, 6, 18, 19, 36, 12, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24409, 914, CAST(0x0000A92E003FDFEC AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'e1e2111ce8ec02edd71a51809efa8e69                                                                    ', N'', N'', 1, 15, 26, 32, 33, 3, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24410, 914, CAST(0x0000A92E0040255E AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'f053f45b1b2928b6395b11466d7a966d                                                                    ', N'', N'', 34, 31, 2, 9, 20, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 1)
GO
print 'Processed 200 total records'
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24411, 914, CAST(0x0000A92E00403764 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'2daeede1f965a61b6e3909017e6743cb                                                                    ', N'', N'', 7, 8, 20, 33, 35, 7, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24412, 914, CAST(0x0000A92E00403768 AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'2daeede1f965a61b6e3909017e6743cb                                                                    ', N'', N'', 1, 3, 7, 34, 39, 17, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24413, 914, CAST(0x0000A92E0040376B AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'2daeede1f965a61b6e3909017e6743cb                                                                    ', N'', N'', 26, 32, 28, 4, 14, 10, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24414, 913, CAST(0x0000A92E0042B038 AS DateTime), CAST(0x0000A9300039ADA0 AS DateTime), 1, N'63f447c9848810b70e46363cb4dc9de2                                                                    ', N'', N'', 26, 28, 32, 14, 4, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 1)
INSERT [dbo].[BookingMegaball] ([BookingID], [MemberID], [CreateDate], [OpenDate], [Status], [TransactionCode], [Note], [NoteEnglish], [FirstNumber], [SecondNumber], [ThirdNumber], [FourthNumber], [FivethNumber], [ExtraNumber], [Quantity], [ValuePoint], [DrawID], [IsFreeTicket]) VALUES (24415, 914, CAST(0x0000A92E00498B9C AS DateTime), CAST(0x0000A92E00D63BC0 AS DateTime), 1, N'916346c123a37c75cab69e9258247281                                                                    ', N'', N'', 26, 28, 32, 4, 14, 13, 1, CAST(0.0002000000 AS Numeric(18, 10)), 33, 0)
SET IDENTITY_INSERT [dbo].[BookingMegaball] OFF
/****** Object:  Table [dbo].[PackageCoin]    Script Date: 07/31/2018 11:30:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackageCoin](
	[PackageID] [int] NOT NULL,
	[CoinID] [nchar](10) NOT NULL,
	[PackageValue] [numeric](18, 2) NULL,
	[IsActive] [smallint] NULL,
	[IsDeleted] [smallint] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUser] [nvarchar](250) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedUser] [nvarchar](250) NULL,
	[DeletedDate] [datetime] NULL,
	[DeletedUser] [nvarchar](250) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Package_Sel_All]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Package_Sel_All]   
AS   
    SET NOCOUNT ON;  
    SELECT * 
    FROM Package  
    WHERE IsActive = 1 
    order by Priority asc ;
GO
/****** Object:  StoredProcedure [dbo].[Package_Insert]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Package_Insert]   
    @PackageName nvarchar(50) = null,
    @PackageValue float,
    @CreateDate datetime,
    @IsActive int,
    @Priority int,
    @NumberExpire int
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Package(PackageName,PackageValue,CreateDate,IsActive,Priority,NumberExpire) values(@PackageName,@PackageValue,@CreateDate,@IsActive,@Priority,@NumberExpire) 
END
GO
/****** Object:  StoredProcedure [dbo].[Booking_Sel]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Booking_Sel]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select CreateDate, BookingID, MemberID, NumberValue, OpenDate, Quantity, Status,TransactionCode from Booking where MemberID = @MemberID order by CreateDate desc
END
GO
/****** Object:  StoredProcedure [dbo].[Booking_Insert]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Booking_Insert]   
    @MemberID int,
    @NumberValue nvarchar(50) = null,
    @Quantity int,
    @Status int,
    @OpenDate datetime,
    @CreateDate datetime,
    @TransactionCode nvarchar(50)
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Booking(MemberID,NumberValue,Quantity,Status,OpenDate,CreateDate,TransactionCode) values(@MemberID,@NumberValue,@Quantity,@Status,@OpenDate, @CreateDate, @TransactionCode) 
    update Member set Points = ((select Points from Member where MemberID = @MemberID) - @Quantity) where MemberID = @MemberID
END
GO
/****** Object:  StoredProcedure [dbo].[AwardNumber_Sel]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AwardNumber_Sel]   
    @fromDate datetime,
    @toDate datetime
AS
BEGIN 
     SET NOCOUNT ON 
		SELECT ROW_NUMBER() OVER (ORDER BY awn.Priority ASC) as Row,awn.*,aw.AwardName,aw.AwardValue from awardnumber awn left join award aw on awn.AwardID=aw.AwardID where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0
END
GO
/****** Object:  StoredProcedure [dbo].[AwardNumber_Insert]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[AwardNumber_Insert]   
    @AwardID int,
    @CreateDate datetime,
    @IsActive int,
    @IsDelete int,
    @NumberValue nvarchar(50) = null,
    @Priority int
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into AwardNumber(AwardID,CreateDate,IsActive,IsDelete,NumberValue,Priority) values(@AwardID,@CreateDate,@IsActive,@IsDelete,@NumberValue,@Priority) 
END
GO
/****** Object:  StoredProcedure [dbo].[Award_Insert]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Award_Insert]   
    @AwardName nvarchar(50) = null,
    @AwardValue float,
    @IsActive int,
    @IsDelete int,
    @Priority int
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Award(AwardName,AwardValue,IsActive,IsDelete,Priority) values(@AwardName,@AwardValue,@IsActive,@IsDelete,@Priority) 
    
END
GO
/****** Object:  StoredProcedure [dbo].[Admin_Sel]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Admin_Sel]   
    @UserName nvarchar(50),   
    @PassWord nvarchar(50)   
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Admin  
    WHERE UserName = @UserName AND Password = @PassWord  ;
GO
/****** Object:  UserDefinedFunction [dbo].[GetAwardWithdrawRequestStatus]    Script Date: 07/31/2018 11:30:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[GetAwardWithdrawRequestStatus]
(
	@MemberID nvarchar(250),
	@BookingID int,
	@AwardDate datetime
)
returns int
as
begin
declare @status int;
set @status = (select top 1 RequestStatus from AwardWithdraw
where convert(varchar(10), AwardDate, 101)=convert(varchar(10), @AwardDate, 101)
and RequestMemberID=@MemberID
and IsDeleted=0
and BookingID=@BookingID);
if @status is null 
begin
 set @status=-1
 end;
return @status;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckUserNameAdmin]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckUserNameAdmin]
   (
	@userName nvarchar(50)
 )
 as begin 
		select UserName from Admin where UserName=@userName
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckPhoneZipCodeExists_CMS]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckPhoneZipCodeExists_CMS]
 (@phoneZipCode char(10),@countryID int)
 as begin
	select PhoneZipCode from Country where PhoneZipCode=@phoneZipCode and IsDeleted=0 and IsActive=1 and CountryID!=@countryID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckOpenDateAwardMegaball]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckOpenDateAwardMegaball]
  (
	@createDate datetime

  )
  as begin 
	select CreateDate from AwardMegaball where CreateDate=@createDate  
  end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckNumberExit]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckNumberExit]
 (
	@num1 int,
	@num2 int,
	@num3 int,
	@num4 int,
	@num5 int,
	@extranum int
 )
 as begin
	select FirstNumber, SecondNumber, ThirdNumber, FourthNumber, FivethNumber, ExtraNumber from AwardMegaball
	where FirstNumber=@num1 or SecondNumber=@num2 or ThirdNumber=@num3 or FourthNumber=@num4 or FivethNumber=@num5 or ExtraNumber=@extranum
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckMobileMemberExist]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckMobileMemberExist](@mobile char(15), @email nvarchar(250))
 as begin 
	 select Mobile from Member where Mobile=@mobile and Email=@email
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckMobileExist]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckMobileExist](@mobile char(15))
as begin 
	select Mobile from Member where Mobile=@mobile
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckJackPotWinner_Draw_Sel]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckJackPotWinner_Draw_Sel]
 (
	@DrawID int,
	@FirstNumber int,
	@SecondNumber int,
	@ThirdNumber int,
	@FourthNumber int, 
	@FivethNumber int,
	@ExtraNumber int
 )
 as begin 
		
	Select Sum(1) As JackPotWinner
	From (
			Select BookingMegaball.MemberID,
					BookingMegaball.BookingID,
					BookingMegaball.DrawID,
					BookingMegaball.CreateDate,
					BookingMegaball.FirstNumber,
					BookingMegaball.SecondNumber,
					BookingMegaball.ThirdNumber,
					BookingMegaball.FourthNumber,
					BookingMegaball.FivethNumber,
					BookingMegaball.ExtraNumber,
					Case
						When BookingMegaball.FirstNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End + 
					Case
						When BookingMegaball.SecondNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End +
					Case
						When BookingMegaball.ThirdNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End +
					Case
						When BookingMegaball.FourthNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End +
					Case
						When BookingMegaball.FivethNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
						Else 0
					End As NumNormal,
					Case
						When BookingMegaball.ExtraNumber = @ExtraNumber Then 1
						Else 0
					End as NumGold
			From BookingMegaball 
			Where BookingMegaball.DrawID = @DrawID
		) BM 
	join Award A on BM.NumNormal = A.NumberBallNormal AND BM.NumGold = A.NumberBallGold
	Where A.Priority = 1;

 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckIPConfig_FE]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_CheckIPConfig_FE]
(
	@IPAddress nvarchar(50),
	@ServiceDomain nvarchar(250),
	@UserName nvarchar(250)
	
)
as
begin
select * from IPConfig ip, Member m  
where 
--ip.IPAddress=@IPAddress
--and 
ip.ServiceDomain=@ServiceDomain
and ip.IsActive=1
and ip.IsDeleted=0
and m.Email=@UserName 

and m.IsActive=1 
and m.IsDelete=0;

end;
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckIPConfig_CMS]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_CheckIPConfig_CMS]
(
	@IPAddress nvarchar(50),
	@ServiceDomain nvarchar(250),
	@UserName nvarchar(250)
	
)
as
begin
select * from IPConfig ip, [Admin] m  
where 
--ip.IPAddress=@IPAddress
--and 
ip.ServiceDomain=@ServiceDomain
and ip.IsActive=1
and ip.IsDeleted=0
and m.UserName=@UserName 

and m.IsActive=1 
and m.IsDelete=0;

end;
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckGroupAdminName]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckGroupAdminName] 
 (
	@groupName  nvarchar(250)
 )
 as begin 
	select GroupName from GroupAdmin where GroupName = @groupName 
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckExpirePackageFE]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckExpirePackageFE]
(
@memberID int,
@dateNow datetime
)
as begin 
	select * from TransactionPackage p where MemberID=@memberID and Status=1 and p.ExpireDate>=@dateNow
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckExistTransactionBitcoinFE]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckExistTransactionBitcoinFE]
(
	@TransactionBitcoin nvarchar(500),
	@MemberID int
)
as begin
	select * 
	from TransactionCoin 
	where TransactionBitcoin = @TransactionBitcoin
	      and MemberID = @MemberID;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckEmailExistsFE]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckEmailExistsFE]
(
@email nvarchar(250)
)
as begin 
	select Email from member where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckEmailAdmin]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckEmailAdmin]
  (
	@email nvarchar(50)
 )
 as begin 
		select Email from Admin where Email=@email
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckE_WalletExistsFE]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckE_WalletExistsFE]
	(
@e_wallet nvarchar(250)
)
as begin 
	select E_Wallet from member where E_Wallet=@e_wallet
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckCountryNameExits_CMS]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckCountryNameExits_CMS]
 (@countryName nvarchar(250))
 as begin
	select CountryName from Country where CountryName=@countryName  and IsDeleted=0 and IsActive=1
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckConfigName]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CheckConfigName]
  (
	@configName nvarchar(250),
	@configID int
 )
 as begin 
		select ConfigName 
		from TicketConfig 
		where ConfigName=@configName 
		and (@configID=0 or ConfigID!=@configID)
 end
GO
/****** Object:  StoredProcedure [guest].[SP_CheckCoinIDExist]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_CheckCoinIDExist]
(
	@coinID nchar(10),
	@exchangeID int
)
as begin 
	select * from ExchangeTicket where CoinID=@coinID and (@exchangeID=0 or ExchangeID!=@exchangeID) and IsActive =1 and IsDelete =0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckCoinIDExist]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_CheckCoinIDExist]
(
	@coinID nchar(10),
	@exchangeID int
)
as begin 
	select * from ExchangeTicket where CoinID=@coinID and (@exchangeID=0 or ExchangeID!=@exchangeID) and IsActive =1 and IsDelete =0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckCoinID]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckCoinID]
 (
	@coinID nvarchar(250),
	@configID int
 )
 as begin 
		select CoinID 
		from TicketConfig 
		where CoinID=@coinID and IsActive=1
		and (@configID=0 or ConfigID!=@configID)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_CheckAwardNameEnglish]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_CheckAwardNameEnglish]
 (
	@awardNameEnglish nvarchar(250),
	@AwardID int
 )
 as begin 
		select * 
		from Award 
		where AwardNameEnglish=@awardNameEnglish and IsActive=1 and IsDelete=0 and AwardID!=@AwardID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_BookingMegaball_AwardValue_Sel]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_BookingMegaball_AwardValue_Sel]
 (
	@DrawID int,
	@FirstNumber int,
	@SecondNumber int,
	@ThirdNumber int,
	@FourthNumber int, 
	@FivethNumber int,
	@ExtraNumber int,
	@JackPotValue decimal(18,10)
 )
 as begin 
		Declare @RowCount int = 0;
		Declare @IsExists int = 0;
		Begin
			Select @IsExists = 1
			From TicketWinning
			Where TicketWinning.DrawID = @DrawID;
		End

		If(@IsExists = 0) 
		Begin
			Update Award
			Set Award.AwardValue = @JackPotValue,
				Award.AwardFee = ((@JackPotValue * Award.AwardPercent)/100)
			Where Exists(
						  Select 1
						  From AwardMegaball
						  Where AwardMegaball.DrawID = @DrawID
								And Award.AwardID = AwardMegaball.AwardID);

			if(@@ROWCOUNT > 0)
			BEGIN
				Update AwardMegaball
				Set AwardMegaball.JackPot = @JackPotValue
				Where AwardMegaball.DrawID = @DrawID;

				if(@@ROWCOUNT > 0)
				Begin
					Set @RowCount = 1;
				End;
			End;

			if(@RowCount > 0)
			BEGIN
				--update success

				Select BM.MemberID,
					   BM.BookingID,
					   BM.DrawID,
					   BM.CreateDate BuyDate,
					   M.Email,
					   A.AwardValue,
					   A.AwardID,
					   A.NumberBallNormal,
					   A.NumberBallGold,
					   A.Priority,
					   A.AwardFee,
					   BM.FirstNumber FirstNumberBuy,
					   BM.SecondNumber SecondNumberBuy,
					   BM.ThirdNumber ThirdNumberBuy,
					   BM.FourthNumber FourthNumberBuy,
					   BM.FivethNumber FivethNumberBuy,
					   BM.ExtraNumber ExtraNumberBuy
				From (
						Select BookingMegaball.MemberID,
							   BookingMegaball.BookingID,
							   BookingMegaball.DrawID,
							   BookingMegaball.CreateDate,
							   BookingMegaball.FirstNumber,
							   BookingMegaball.SecondNumber,
							   BookingMegaball.ThirdNumber,
							   BookingMegaball.FourthNumber,
							   BookingMegaball.FivethNumber,
							   BookingMegaball.ExtraNumber,
							   Case
								  When BookingMegaball.FirstNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End + 
							   Case
								  When BookingMegaball.SecondNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End +
							   Case
								  When BookingMegaball.ThirdNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End +
							   Case
								  When BookingMegaball.FourthNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End +
							   Case
								  When BookingMegaball.FivethNumber IN (@FirstNumber, @SecondNumber, @ThirdNumber, @FourthNumber, @FivethNumber) Then 1
								  Else 0
							   End As NumNormal,
							   Case
								  When BookingMegaball.ExtraNumber = @ExtraNumber Then 1
								  Else 0
							   End as NumGold
						From BookingMegaball 
						Where BookingMegaball.DrawID = @DrawID
					) BM 
				join Award A on BM.NumNormal = A.NumberBallNormal AND BM.NumGold = A.NumberBallGold
				join Member M On M.MemberID = BM.MemberID;

			End;
		End;
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_AwardMegaballLog_Add]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_AwardMegaballLog_Add]
 (
	@Email nvarchar(250),
	@Value decimal(18, 5),
	@CreatedDate datetime,
	@Id int out
 )
 as begin 
		Insert into AwardMegaballLog(Email, Value, CreatedDate)
		Values (@Email, @Value, @CreatedDate);

		select @Id = Scope_Identity();
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_AwardMegaball_Upd]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_AwardMegaball_Upd]
 (
	@draw int,
	@totalwon decimal(18,5)
 )
 as begin
	update AwardMegaball 
	set TotalWon = @totalwon
	where NumberID=(select top 1 AwardMegaball.NumberID from AwardMegaball where DrawID = @draw)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_AWARDMEGABALL_SELRESULT]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_AWARDMEGABALL_SELRESULT]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 15.01.2018
	DESCRIPTION: LOAD THÔNG TIN KẾT QUẢ XỔ SỐ GẦN NHẤT
*/
AS BEGIN 
	SELECT AWN.*,
		   AW.AWARDNAME,
		   AW.AWARDVALUE 
	FROM AWARDMEGABALL AWN 
	LEFT JOIN AWARD AW 
			ON AWN.AWARDID = AW.AWARDID 
	WHERE AWN.ISDELETE = 0
	      --AND AWN.ISACTIVE = 1
    ORDER BY AWN.CreateDate DESC;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Award_JackPotValue_Upd]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_Award_JackPotValue_Upd]
 (
	@JackPotValue decimal(18, 8),
	@DrawID int
 )
 as begin 
		Update Award
		Set Award.AwardValue = @JackPotValue,
		    Award.AwardFee = ((@JackPotValue * Award.AwardPercent)/100)
		Where Exists(
					  Select 1
					  From AwardMegaball
					  Where AwardMegaball.DrawID = @DrawID
					        And Award.AwardID = AwardMegaball.AwardID);

		--if(@@ROWCOUNT > 0)
		--BEGIN
		--	Update AwardMegaball
		--	Set AwardMegaball.JackPot = @JackPotValue
		--	Where AwardMegaball.DrawID = @DrawID;
		--End;
 end
GO
/****** Object:  StoredProcedure [dbo].[Member_Update]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Member_Update] 
	@MemberID int,  
    @Email nvarchar(50) = null,
    @E_Wallet nvarchar(50) = null,
    @Password nvarchar(50) = null,
    @IsActive int,
    @IsDelete int,
    @Points float
AS 
BEGIN 
     SET NOCOUNT ON 
    update Member
	set Email = @Email,
	E_Wallet = @E_Wallet,
	Password = @Password,
	IsActive = @IsActive,
    IsDelete = @IsDelete,
    Points = @Points
    where MemberID = @MemberID
END
GO
/****** Object:  StoredProcedure [dbo].[Member_Insert]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_Insert]   
    @Email nvarchar(50) = null,
    @E_Wallet nvarchar(50) = null,
    @Password nvarchar(50) = null,
    @IsActive int,
    @IsDelete int,
    @Points float,
    @CreateDate datetime
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into Member(Email,E_Wallet,Password,IsActive,IsDelete,Points,CreateDate) values(@Email,@E_Wallet,@Password,@IsActive,@IsDelete,@Points,@CreateDate) 
    
END
GO
/****** Object:  StoredProcedure [dbo].[Member_Email_Sel]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_Email_Sel]   
    @UserName nvarchar(50),   
    @PassWord nvarchar(50)   
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE Password = @PassWord and IsActive = 1 and IsDelete = 0 and Email = @UserName or E_Wallet = @UserName;
GO
/****** Object:  StoredProcedure [dbo].[Member_Email_Check]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Member_Email_Check]   
    @UserName nvarchar(50)
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE Email = @UserName ;
GO
/****** Object:  StoredProcedure [dbo].[Member_E_Wallet_Sel]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_E_Wallet_Sel]   
    @UserName nvarchar(50),   
    @PassWord nvarchar(50)   
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE E_Wallet = @UserName AND Password = @PassWord and IsActive = 1 and IsDelete = 0 ;
GO
/****** Object:  StoredProcedure [dbo].[Member_E_Wallet_Check]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Member_E_Wallet_Check]   
    @UserName nvarchar(50)
AS   

    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE E_Wallet = @UserName ;
GO
/****** Object:  StoredProcedure [dbo].[Member_Check]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Member_Check]   
    @Email nvarchar(50),
    @E_Wallet nvarchar(50)
AS   
    SET NOCOUNT ON;  
    SELECT * 
    FROM Member  
    WHERE E_Wallet = @E_Wallet or Email = @Email;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewPoint]    Script Date: 07/31/2018 11:30:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewPoint]()
returns int
as
begin
declare @numberPoints int
set @numberPoints=(select COUNT(p.TransactionID) from TransactionPoint p
where DATEADD(DD, DATEDIFF(DD, 0, p.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberPoints);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewPackage]    Script Date: 07/31/2018 11:30:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewPackage]()
returns int
as
begin
declare @numberPackage int
set @numberPackage=(select COUNT(p.TransactionID) from TransactionPackage p
where DATEADD(DD, DATEDIFF(DD, 0, p.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberPackage);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewMember]    Script Date: 07/31/2018 11:30:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewMember]()
returns int
as
begin
declare @numberMember int
set @numberMember=(select COUNT(m.MemberID) from Member m
where DATEADD(DD, DATEDIFF(DD, 0, m.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberMember);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewCoin]    Script Date: 07/31/2018 11:30:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewCoin]()
returns int
as
begin
declare @numberCoin int
set @numberCoin=(select COUNT(c.TransactionID) from TransactionCoin c
where DATEADD(DD, DATEDIFF(DD, 0, c.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberCoin);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewBookingMegaball]    Script Date: 07/31/2018 11:30:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewBookingMegaball]()
returns int
as begin 
	declare @numberBookingMegaball int
	set @numberBookingMegaball=(select COUNT(b.BookingID) from BookingMegaball b
	where DATEADD(DD,DATEDIFF(DD,0,b.CreateDate),0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0));
	return(@numberBookingMegaball);
end;
GO
/****** Object:  UserDefinedFunction [dbo].[CountNewBooking]    Script Date: 07/31/2018 11:30:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[CountNewBooking]()
returns int
as
begin
declare @numberBooking int
set @numberBooking=(select COUNT(b.BookingID) from Booking b
where DATEADD(DD, DATEDIFF(DD, 0, b.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) );
return(@numberBooking);
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballBySearchFrontEnd]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListBookingMegaballBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email from BookingMegaball b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
and (@fromDate IS NULL or b.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballBySearchCMS]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListBookingMegaballBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime = null,
	--@fromDate datetime,
	--@toDate datetime,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email from BookingMegaball b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
--and (b.CreateDate between @fromDate and @toDate)
--and (@fromDate= '01/01/1990 12:01:00' or b.CreateDate between @fromDate and @toDate)
and (@fromDate= null or b.CreateDate between @fromDate and @toDate)
and (@status=-1 or b.Status=@status)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballByDraw]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_ListBookingMegaballByDraw]
(@DrawID int,@start int, @end int)
as begin
    select * from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email
	 from bookingmegaball b left join member m on b.MemberID=m.MemberID and b.DrawID = @DrawID) as Products  where Row>=@start and Row<=@end
		 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingMegaballByDate]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_ListBookingMegaballByDate]
(
@Date datetime
)
as begin
		select * 
		from BookingMegaball
		where Convert(varchar,OpenDate,103) = CONVERT(varchar, @Date, 103)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingBySearchFrontEnd]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListBookingBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingBySearchCMS]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListBookingBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID 
where (@memberID=0 or b.MemberID=@memberID)
and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)
and (@status=-1 or b.Status=@status)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingByDraw_FE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListBookingByDraw_FE](@DrawID int)
as begin
	select * from BookingMegaball where DrawID=@DrawID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingByDraw]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListBookingByDraw](@draw int)
as begin 
	select * from BookingMegaball where DrawID=@draw
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAwardWithdraw_CMS]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListAwardWithdraw_CMS]
(
	@UserName nvarchar(250),
	@FromDate datetime,
	@ToDate datetime,
	@RequestStatus smallint,
	@AwardDate datetime,
	@Start int,
	@End int
)
as
begin
select *from 
(
SELECT ROW_NUMBER() OVER (ORDER BY aw.RequestDate desc)as row, 
sum(1) over() as TOTALROWS,
		aw.AdminIDApprove,
		aw.ApproveDate,
		aw.ApproveNote,
		aw.AwardDate,
		aw.CoinIDWithdraw,
		aw.IsDeleted,
		aw.RequestDate,
		aw.RequestMemberID,
		aw.RequestStatus,
		aw.RequestWalletAddress,
		aw.TransactionID,
		aw.ValuesWithdraw
		from AwardWithdraw aw 
		where
		(@UserName is null or @UserName='' or aw.RequestMemberID=@UserName)
		and (@FromDate='01/01/1990' or (aw.RequestDate between @FromDate and @ToDate))
		and (@RequestStatus=-1 or aw.RequestStatus=@RequestStatus)
		and (@AwardDate='01/01/1990' or aw.AwardDate =@AwardDate)
		) as awardresult  
		where Row>=@Start and Row<=@End
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAwardMegaballBySearch]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAwardMegaballBySearch]
(
	@start int,
	@end int,	
	@fromDate datetime = null,
	@toDate datetime = null
	--@awardNameEnglish nvarchar(250)
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY a.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
a.*,aw.AwardNameEnglish from AwardMegaball a left join Award aw on a.AwardID=aw.AwardID
where @fromDate=null or a.CreateDate between @fromDate and @toDate
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllTransactionPackage]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllTransactionPackage]
(
	@start int,
	@end int
)
as begin 
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, tp.*,p.PackageName,m.Email,m.E_Wallet from transactionpackage tp left join package p on tp.PackageID=p.PackageID left join member m on tp.MemberID=m.MemberID) as Products
		  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllPackageFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllPackageFE]

as begin 
	select * from package where IsActive=1 order by Priority ASC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllMember]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllMember]
 as begin
	Select Email from Member order by CreateDate DESC
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangeTicket]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangeTicket]
(
	@start int,
	@end int
)
as 
begin
	
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY ex.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, ex.*,ad.UserName from ExchangeTicket ex left join Admin ad on ex.CreateUser= ad.UserName) as Products 
	 where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangePointPaging]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangePointPaging]
(
	@start int,
	@end int
)
as 
begin
	
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY ex.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, ex.*,ad.UserName from ExchangePoint ex left join Admin ad on ex.CreateUser= ad.UserName) as Products 
	 where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangePointBySearch]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllExchangePointBySearch]
(
	@start int,
	@end int,	
	@fromDate datetime =null,
	@toDate datetime =null
)
as begin 
	select * from (Select ROW_NUMBER() over(Order by ex.CreateDate DESC)as Row,SUM(1) over() as TOTALROWS,
	ex.* from ExchangePoint ex where @fromDate=null or ex.CreateDate between @fromDate and @toDate)
	as Products where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangePoint]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangePoint]
as begin
	select * from ExchangePoint order by CreateDate desc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangeNoPaging]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllExchangeNoPaging]
as begin
	select * from ExchangeTicket order by CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllExchangeBySearch]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllExchangeBySearch]
(
	@start int,
	@end int,	
	@fromDate datetime = null,
	@toDate datetime=null
)
as begin 
	select * from (Select ROW_NUMBER() over(Order by ex.CreateDate DESC)as Row,SUM(1) over() as TOTALROWS,
	ex.* from ExchangeTicket ex where @fromDate=null or ex.CreateDate between @fromDate and @toDate)
	as Products where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllCompanypaging]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllCompanypaging]
 (
	@start int,
	@end int
)

 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.MemberID  DESC) as Row,SUM(1) OVER()AS TOTALROWS,c.*, m.FullName from  CompanyInfomation c left join Member m on c.MemberID=m.MemberID
		) as Products
		  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [guest].[SP_ListAllCoin]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_ListAllCoin]
as begin
	select * from Coin where IsActive =1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllCoin]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_ListAllCoin]
as begin
	select * from Coin where IsActive =1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingPaging]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingPaging]
(
		@start int,
		@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet 
		from booking b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballPaging]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaballPaging]
(
	@start int,
	@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, b.*,m.Email 
		from BookingMegaball b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballByDraw]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllBookingMegaballByDraw]
(
		@DrawID int
)
as begin
		select b.*,m.Email 
		from BookingMegaball b 
		left join Member m on b.MemberID=m.MemberID 
		where b.DrawID = @DrawID
		order by b.CreateDate DESC 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaballByDate]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaballByDate]
(
		@date datetime
)
as begin
		select b.*,m.Email 
		from BookingMegaball b 
		left join Member m on b.MemberID=m.MemberID 
		where CONVERT(nvarchar(250),b.CreateDate, 103) = CONVERT(nvarchar(250), @date, 103)
		order by b.CreateDate DESC 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaball_FE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaball_FE]
as begin
	select * from BookingMegaball
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBookingMegaball]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBookingMegaball]
as begin
		select b.*,m.Email from BookingMegaball b left join Member m on b.MemberID=m.MemberID order by b.CreateDate DESC 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllBooking]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllBooking]
as begin
		select b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID  order by b.CreateDate DESC
end
GO
/****** Object:  StoredProcedure [guest].[SP_ListAllAdminPaging]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_ListAllAdminPaging]
(   @start int,
	@end int)
as begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY a.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,a.*,g.GroupName
		 from Admin a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID)
		 as Products  where Row>=@start and Row<=@end

end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllAdminPaging]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListAllAdminPaging]
(   @start int,
	@end int)
as begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY a.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,a.*,g.GroupName,g.GroupID
		 from Admin a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID)
		 as Products  where Row>=@start and Row<=@end

end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAllAdmin]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAllAdmin]
as begin
		select a.*,ac.GroupID,g.GroupName
		 from Admin a left join AccessRight ac on a.AdminID=ac.AdminID left join GroupAdmin g on ac.GroupID=g.GroupID;

end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListAccessRightByManagerID]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListAccessRightByManagerID]
(@adminID int)
as begin
		select *from accessright where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_List_GroupAdmin]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_List_GroupAdmin]
as begin
		select *from groupadmin
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTransactionPointfe]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertTransactionPointfe]
(
	@memberID int,
	@points int,
	@createDate datetime,
	@status int,
	@transactionCode nvarchar(500),
	@note nvarchar(2500)

)
as begin 
	insert into transactionpoint(MemberID,Points,CreateDate,Status,TransactionCode,Note) values(@memberID,@points,@createDate,@status,@transactionCode,@note)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTransactionPackageFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertTransactionPackageFE]
(
	@memberID int,
	@packageID int,
	@createDate datetime,
	@expireDate datetime, 
	@status int,
	@transactionValue  nvarchar(250),
	@note nvarchar(3500)

)
as begin 
	insert into transactionpackage(MemberID,PackageID,CreateDate,ExpireDate,Status,TransactionCode,Note) values(@memberID,@packageID,@createDate,@expireDate,@status,@transactionValue,@note)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTransactionCoinFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertTransactionCoinFE]
(
	@MemberID int,
	@WalletAddressID char(150),
	@ValueTransaction numeric(18, 10),
	@QRCode  nvarchar(500),
	@CreateDate datetime,
	@ExpireDate datetime,
	@Status smallint,
	@Note nvarchar(2500),
	@WalletID nvarchar(500),
	@TypeTransactionID int,
	@TransactionBitcoin nvarchar(500),
	@TransactionID char(150),
	@CoinID nchar(10)
)
as begin
	insert into TransactionCoin(TransactionID,WalletAddressID,MemberID,ValueTransaction,QRCode,CreateDate,ExpireDate,Status,Note,WalletID,TypeTransactionID,TransactionBitcoin,CoinID) 
                                values(@TransactionID,@WalletAddressID,@MemberID,@ValueTransaction,@QRCode,@CreateDate,@ExpireDate,@Status,@Note,@WalletID,@TypeTransactionID,@TransactionBitcoin,@CoinID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTicketWinning]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_InsertTicketWinning]   
    @AwardNumber nvarchar(50),
	@BookingID int,
	@CreatedDate datetime,
	@DrawID int,
	@ExtraNumber nvarchar(50),
	@FirstNumber nvarchar(50),
	@FivethNumber nvarchar(50),
	@FourthNumber nvarchar(50),
	@MemberID int,
	@SecondNumber nvarchar(50),
	@ThirdNumber nvarchar(50),
	@Value decimal(18,5)
AS 
BEGIN 
     SET NOCOUNT ON 
    insert into TicketWinning(AwardNumber,BookingID,CreatedDate,DrawID,ExtraNumber,FirstNumber,FivethNumber,FourthNumber,MemberID,SecondNumber,ThirdNumber,Value) values(@AwardNumber,@BookingID,@CreatedDate,@DrawID,@ExtraNumber,@FirstNumber,@FivethNumber,@FourthNumber,@MemberID,@SecondNumber,@ThirdNumber,@Value) 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertTicketConfig_CMS]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertTicketConfig_CMS]
(
 	@ConfigName nvarchar(250),
	@CreatedDate datetime,
	@NumberTicket int,
	@IsActive smallint,
	@IsDeleted smallint,
	@CoinID nvarchar(250),
	@CreatedUser nvarchar(250),
	@CoinValues numeric(18,10)
)
as begin 
		insert into TicketConfig(ConfigName,NumberTicket,CoinValues,CoinID,IsActive,IsDeleted,CreatedDate,CreatedUser,UpdatedDate,UpdatedUser,DeletedDate,DeletedUser)
		 values(@ConfigName,@NumberTicket,@CoinValues,@CoinID,@IsActive,@IsDeleted,@CreatedDate,@CreatedUser,'','','','')
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertMemberWalletFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertMemberWalletFE]
(
	@IndexWallet int,
	@IsActive smallint,
	@IsDelete smallint,
	@MemberID int,
 	@NumberCoin float,
	@WalletAddress nvarchar(250),
	@RippleAddress nvarchar(250),
	@SerectKeyRipple nvarchar(250)
)
as begin 
	insert into Member_Wallet(IndexWallet,IsActive,IsDelete,MemberID,NumberCoin,WalletAddress,RippleAddress,SerectKeyRipple) values(@IndexWallet,@IsActive,@IsDelete,@MemberID,@NumberCoin,@WalletAddress,@RippleAddress,@SerectKeyRipple)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertMemberReferenceFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertMemberReferenceFE]
(
	@MemberID int,
	@LinkReference nvarchar(1000),
	@MemberIDReference int,
	@LockID nvarchar(1000),
	@Status int,
	@Amount decimal(18,5)
)
as begin 
	insert into MemberReference(MemberID, CreatedDate, LinkReference, MemberIDReference, LockID, Status, Amount)
	 values(@MemberID, SYSDATETIME(), @LinkReference, @MemberIDReference, @LockID, @Status, @Amount)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertMemberFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertMemberFE]
/*
	Edited By: Lý Kiến Đức
	Edited date: 16.01.2018
	Description: Bổ sung field @FBUserID
*/
(
	@email nvarchar(250),
	@password nvarchar(250) = null,
	@isActive smallint,
	@isDelete smallint,
	@points numeric(18,10),
	@createDate datetime = null,
	@fullName nvarchar(250),
	@mobile char(15) = null,
	@avatar nvarchar(250) = null,
	@gender smallint,
	@birthday datetime = null,
	@IsUserType smallint = 0,
	@UserTypeID varchar(100) = null,
	@LinkLogin nvarchar(250),
	@NumberTicketFree int,
	@LinkReference nvarchar(250),
	@outMemberID int OUTPUT

)
as begin 
	insert into member(Email, Password, IsActive, IsDelete, Points, CreateDate, FullName, Mobile, Avatar, Gender, Birthday, IsUserType, UserTypeID, LinkLogin, NumberTicketFree, LinkReference)
	 values(@email, @password, @isActive, @isDelete, @points, @createDate, @fullName, @mobile, @avatar, @gender, @birthday, @IsUserType, @UserTypeID, @LinkLogin,@NumberTicketFree, @LinkReference)

	 SELECT @outMemberID = SCOPE_IDENTITY() 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertLinkResetPasswordFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertLinkResetPasswordFE]
(
	@Email nvarchar(250),
	@LinkReset nvarchar(1000),
	@ExpireLink nvarchar(1000),
	@IPAddress nvarchar(50)
)
as begin 
		update LinkResetPassword set Status = 1 where CreatedDate <= SYSDATETIME() and Email = @Email
		insert into LinkResetPassword(Email,LinkReset,Status,NumberSend,CreatedDate,ExpireLink,IPAddress)
		 values(@Email,@LinkReset,0,1,SYSDATETIME(),@ExpireLink,@IPAddress)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertGroupAdmin]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertGroupAdmin]
(
	@groupName nvarchar(250),
	@isActive smallint
)
as begin
		insert into groupadmin(GroupName,IsActive) values(@groupName,@isActive)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertFreeTicketDaily]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_InsertFreeTicketDaily] (@NumberTicket int)
AS
BEGIN
  DECLARE @ISEXISTS int;
  DECLARE @TotalTicket int;
  DECLARE @AmountFreeTicket int;
  SET @ISEXISTS = 0;
  SET @TotalTicket = 0;
  BEGIN
    SELECT
      @ISEXISTS = 1
    FROM FreeTicketDaily
    WHERE CONVERT(nvarchar, FreeTicketDaily.Date, 103) = CONVERT(nvarchar, SYSDATETIME(), 103)
    AND CONVERT(nvarchar, FreeTicketDaily.CreatedDate, 103) = CONVERT(nvarchar, SYSDATETIME(), 103)
  END;
  IF (@ISEXISTS = 0)
  BEGIN
    SET @AmountFreeTicket = (SELECT
      Amount
    FROM StoreFreeTicket)
    IF (@AmountFreeTicket > 0)
    BEGIN
      IF (@AmountFreeTicket >= @NumberTicket)
      BEGIN
        SET @TotalTicket = (SELECT
          Total
        FROM FreeTicketDaily
        WHERE IsActive = 1);

        UPDATE FreeTicketDaily
        SET IsActive = 0
        WHERE IsActive = 1;

        INSERT INTO FreeTicketDaily (Date, Total, IsActive, CreatedDate)
          VALUES (SYSDATETIME(), (@TotalTicket + @NumberTicket), 1, SYSDATETIME())

        UPDATE StoreFreeTicket
        SET amount = (@AmountFreeTicket - @NumberTicket)

      END;
      ELSE
      BEGIN
        UPDATE FreeTicketDaily
        SET IsActive = 0
        WHERE IsActive = 1;

        INSERT INTO FreeTicketDaily (Date, Total, IsActive, CreatedDate)
          VALUES (SYSDATETIME(), (@TotalTicket + @AmountFreeTicket), 1, SYSDATETIME())

        UPDATE StoreFreeTicket
        SET amount = 0
      END;
    END;
  END;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertExchangeTicket]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertExchangeTicket]
(
	@pointValue float,
	@ticketNumber int,
	@isActive smallint,
	@isDelete smallint,
	@createDate datetime,
	@createUser nvarchar(250),
	@memberID int,
	@updateDate datetime,
	@updateUser nvarchar(250),
	@deleteDate date,
	@deleteUser nvarchar(250),
	@coinID char(10)
)
as begin
	insert into ExchangeTicket(PointValue,TicketNumber,IsActive,IsDelete,CreateDate,CreateUser,MemberID,UpdateDate,UpdateUser,DeleteDate,DeleteUser,CoinID)
	values(@pointValue,@ticketNumber,@isActive,@isDelete,@createDate,@createUser,@memberID,@updateDate,@updateUser,@deleteDate,@deleteUser,@coinID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertExchangePoint]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertExchangePoint]
(
	@pointValue float,
	@bitcoinValue float,
	@isActive smallint,
	@isDelete smallint,
	@createDate datetime,
	@createUser nvarchar(250),
	@memberID int,
	@updateDate datetime,
	@updateUser nvarchar(250),
	@deleteDate date,
	@deleteUser nvarchar(250)
)
as begin
	insert into ExchangePoint(PointValue,BitcoinValue,IsActive,IsDelete,CreateDate,CreateUser,MemberID,UpdateDate,UpdateUser,DeleteDate,DeleteUser)
	values(@pointValue,@bitcoinValue,@isActive,@isDelete,@createDate,@createUser,@memberID,@updateDate,@updateUser,@deleteDate,@deleteUser)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertDraw]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_InsertDraw]
(
--@AWARDNAME NVARCHAR(250),
@STARTDATE DATETIME,
@ENDDATE DATETIME,
@CREATEDDATE DATETIME
)
AS BEGIN 
declare @ISEXISTS INT;
SET @ISEXISTS = 0;
BEGIN
	SELECT @ISEXISTS = 1 
	FROM DRAW
	WHERE CONVERT(NVARCHAR, DRAW.CREATEDDATE, 103) = CONVERT(NVARCHAR, @CREATEDDATE, 103);
END;
	IF(@ISEXISTS = 0) 
	BEGIN
	INSERT INTO DRAW(STARTDATE,ENDDATE,CREATEDDATE)
		VALUES(@STARTDATE,@ENDDATE,@CREATEDDATE)
		END;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertCountry_CMS]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertCountry_CMS]
(
 	@CountryName nvarchar(250),
	@IsActive smallint,
	@IsDeleted smallint,
	@CreatedDate DateTime,
	@CreatedUser nvarchar(250),
	@PhoneZipCode varchar(50)
)
as begin 
	insert into Country(CountryName,PhoneZipCode,IsActive,IsDeleted,CreatedDate,CreatedUser,UpdatedDate,UpdatedUser,DeletedDate,DeletedUser) 
	values(@CountryName,@PhoneZipCode,@IsActive,@IsDeleted,@CreatedDate,@CreatedUser,'','','','')
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertBookingFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertBookingFE]
(
	@memberID int,
	@numberValue int,
	@quantity int,
	@createDate datetime,
	@openDate datetime,
	@status int,
	@transactionCode char(100)
)
as begin 
	insert into booking(MemberID,NumberValue,Quantity,CreateDate,OpenDate,Status,TransactionCode) values(@memberID,@numberValue,@quantity,@createDate,@openDate,@status,@transactionCode)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertBooking_FE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertBooking_FE]
(
	@memberID int,
	@createDate datetime,
	@openDate datetime,
	@status int,
	@transactionCode char(100),
	@note nvarchar(2500),
	@noteEnglish nvarchar(2500),
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@quantity int,
	@ValuePoint numeric(18,10),
	@DrawID int,
	@IsFreeTicket int

)
as begin 
	insert into BookingMegaball(MemberID,CreateDate,OpenDate,Status,TransactionCode,Note,NoteEnglish,FirstNumber,SecondNumber,ThirdNumber,FourthNumber,FivethNumber,ExtraNumber,Quantity,ValuePoint,DrawID,IsFreeTicket)
	 values(@memberID,@createDate,@openDate,@status,@transactionCode,@note,@noteEnglish,@firstNumber,@secondNumber,@thirdNumber,@fourthNumber,@fivethNumber,@extraNumber,@quantity,@ValuePoint,@DrawID,@IsFreeTicket)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardWithdraw]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertAwardWithdraw]
(
	@TransactionID char(50),
	@RequestWalletAddress nvarchar(250),
	@RequestMemberID nvarchar(250),
	@CoinIDWithdraw char(10),
	@RequestStatus int,
	@RequestDate datetime,
	@IsDeleted int,
	@AwardDate datetime,
	@ValuesWithdraw numeric(18, 8),
	@BookingID int	
)
as begin 
	insert into AwardWithdraw(TransactionID,RequestMemberID,RequestWalletAddress,RequestDate,RequestStatus,AwardDate,CoinIDWithdraw,ValuesWithdraw,AdminIDApprove,ApproveDate,ApproveNote,IsDeleted,UpdatedDate,UpdatedUser,DeletedDate,DeletedUser,BookingID)
	values(@TransactionID,@RequestMemberID,@RequestWalletAddress,@RequestDate,@RequestStatus,@AwardDate,@CoinIDWithdraw,@ValuesWithdraw,'','','',@IsDeleted,'','','','',@BookingID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardNumberFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAwardNumberFE]
(
	@numberID int,
	@awardID int,
	@createDate datetime,
	@priority smallint,
	@isDelete smallint,
	@isActive smallint,
	@numberValue int,
	@stationName nvarchar(250)
)
as begin 
	insert into awardnumber(NumberID,AwardID,NumberValue,CreateDate,IsActive,IsDelete,Priority,StationName) values(@numberID,@awardID,@numberValue,@createDate,@isActive,@isDelete,@priority,@stationName)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardNumber]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAwardNumber]
 (
	@awardID int,
	@numberValue float,
	@createDate datetime,
	@isActive smallint,
	@isDelete smallint,
	@priority int,
	@stationName nvarchar(250),
	@stationNameEnglish nvarchar(250)
 )
 as begin 
		insert into awardnumber(AwardID,NumberValue,CreateDate,IsActive,IsDelete,Priority,StationName,StationNameEnglish)
		 values(@awardID,@numberValue,@createDate,@isActive,@isDelete,@priority,@stationName,@stationNameEnglish)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardMegaball]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertAwardMegaball]
/*
	DESCRIPTION: @AWARDDATEPREVIOUS, @JACKPOTPREVIOUS, @TOTALJACKPOT
*/
(
	@awardID int,
	@createDate datetime,
	@isActive smallint,
	@isDelete smallint,
	@priority smallint,
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@genCode nvarchar(500),
	@jackPotDefault float,
	@DrawID int,
	@isExists int out
)
as begin
    DECLARE @AWARDDATEPREVIOUS DATETIME = null,
	        @JACKPOTPREVIOUS FLOAT = @JACKPOTDEFAULT,
	        @TOTALJACKPOT FLOAT = @JACKPOTDEFAULT,
			@JACKPOTWINNER FLOAT = 0;

    Set @isExists = 0;
	BEGIN
		SELECT @isExists = 1 
		FROM AWARDMEGABALL
		WHERE Convert(nvarchar, AWARDMEGABALL.CreateDate, 103) = Convert(nvarchar, @createDate, 103);
	END;

	IF(@isExists = 0) 
	BEGIN
			BEGIN  
				--GET RESULT AWARD WITH CREATEDDATE LAST
				SELECT TOP 1 @AWARDDATEPREVIOUS = CREATEDATE,
							 @JACKPOTPREVIOUS = AWARDMEGABALL.JackPot
				FROM AWARDMEGABALL
				WHERE AWARDMEGABALL.ISDELETE = 0
					  AND AWARDMEGABALL.ISACTIVE = 1
					  ORDER BY CREATEDATE DESC;
			END;

			BEGIN  
				SELECT @TOTALJACKPOT = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
				FROM BOOKINGMEGABALL
				WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
					  AND NOT EXISTS (SELECT 1
								  FROM BOOKINGMEGABALL
								  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
										and (
											   BOOKINGMEGABALL.FirstNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
											   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.SecondNumber = @firstNumber 
											   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
											   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
											   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
											   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
											   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
											   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
											   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.FourthNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
											   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.FivethNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
											   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
											)
										and (
											   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
											   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
											   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
											));
			END

			IF(@TOTALJACKPOT IS NULL)
			BEGIN
				--ELSE IF NOW HAVE LEAST 1 MEMBER WINNER -> TOTAL JACKPOT WILL BE DEFAULT (CONFIG SOURCES) 
				--@JACKPOTWINNER: Total jackpot won -> save history
			   BEGIN  
					SELECT @JACKPOTWINNER = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
					FROM BOOKINGMEGABALL
					WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
						  AND EXISTS (SELECT 1
									  FROM BOOKINGMEGABALL
									  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and @CREATEDATE))
											and (
											   BOOKINGMEGABALL.FirstNumber = @firstNumber 
											   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
											   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
											   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
											   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
											)
											and (
												   BOOKINGMEGABALL.SecondNumber = @firstNumber 
												   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
												   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
												   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
												   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
												   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
												   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
												   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.FourthNumber = @firstNumber 
												   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
												   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
												   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
												   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
												   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.FivethNumber = @firstNumber 
												   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
												   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
												   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
												   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
												   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
												)
											and (
												   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
												   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
												   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
												));
				END
				--set @JACKPOTWINNER = @JACKPOTPREVIOUS;
			   SET @TOTALJACKPOT = @jackPotDefault;
			END
			--ELSE
			--BEGIN
			--   --IF NOT ANY LEAST 1 MEMBER WINNER -> TOTAL = DEFAULT (CONFIG) + JACTPOT PREVIOUS (IF HAVE)
			--   SET @TOTALJACKPOT = @JACKPOTPREVIOUS;
			--END;
			--UPDATE AWARDMEGABALL
			--SET AWARDMEGABALL.AWARDID = @awardID,
			--    AWARDMEGABALL.CreateDate = @createDate,
			--	AWARDMEGABALL.IsActive = @isActive,
			--	AWARDMEGABALL.IsDelete = @isDelete,
			--	AWARDMEGABALL.Priority = @priority,
			--	AWARDMEGABALL.FirstNumber = @firstNumber,
			--	AWARDMEGABALL.SecondNumber = @secondNumber,
			--	AWARDMEGABALL.ThirdNumber = @thirdNumber,
			--	AWARDMEGABALL.FourthNumber = @fourthNumber,
			--	AWARDMEGABALL.FivethNumber = @fivethNumber,
			--	AWARDMEGABALL.ExtraNumber = @extraNumber,
			--	AWARDMEGABALL.GenCode = @genCode,
			--	AWARDMEGABALL.JackPot = @TOTALJACKPOT,
			--	AWARDMEGABALL.JACKPOTWINNER = @JACKPOTWINNER
		 --   WHERE AWARDMEGABALL.IsDelete = 0
			--	  AND AWARDMEGABALL.IsActive = 0;
    
			--IF(@@ROWCOUNT = 0)
			--BEGIN
				INSERT 
				INTO AWARDMEGABALL(AWARDID,
								   CREATEDATE,
								   ISACTIVE,
								   ISDELETE,
								   PRIORITY,
								   FIRSTNUMBER,
								   SECONDNUMBER,
								   THIRDNUMBER,
								   FOURTHNUMBER,
								   FIVETHNUMBER,
								   EXTRANUMBER,
								   GENCODE,
								   JACKPOT, 
								   JackPotWinner,
								   DrawID)
					VALUES(@AWARDID,
						   @CREATEDATE,
						   @ISACTIVE,
						   @ISDELETE,
						   @PRIORITY,
						   @FIRSTNUMBER,
						   @SECONDNUMBER,
						   @THIRDNUMBER,
						   @FOURTHNUMBER,
						   @FIVETHNUMBER,
						   @EXTRANUMBER,
						   @GENCODE,
						   @TOTALJACKPOT,
						   @JACKPOTWINNER,
						   @DrawID)
			--END;
	END;
	
	
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAwardFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAwardFE]
(
	@awardName nvarchar(250),
	@awardValue float,
	@createDate datetime,
	@priority smallint,
	@isDelete smallint,
	@isActive smallint

)
as begin 
	insert into award(AwardName,AwardValue,CreateDate,Priority,IsActive,IsDelete) values(@awardName,@awardValue,@createDate,@priority,@isActive,@isDelete)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAward]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_InsertAward]
(
	--@awardName nvarchar(250),
	@awardValue decimal(18,10),
	@createDate datetime,
	@priority int,
	@isActive smallint,
	@isDelete smallint,
	@awardNameEnglish nvarchar(250),
	@awardFee decimal(18,10),
	@AwardPercent decimal(18,10)
)
as begin 
		insert into award(AwardName,AwardValue,CreateDate,Priority,IsActive,IsDelete,AwardNameEnglish,AwardFee,AwardPercent)
		 values('',@awardValue,@createDate,@priority,@isActive,@isDelete,@awardNameEnglish,@awardFee,@AwardPercent)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAdmin]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_InsertAdmin]
(
	@userName nvarchar(50),
	@pass nvarchar(250),
	@hashKey nvarchar(250),
	@isActive smallint,
	@createDate datetime,
	@fullName nvarchar(250),
	@email nvarchar(250),
	@mobile char(50),
	@address nvarchar(250),
	@avatar nvarchar(250),
	@isDelete smallint
)
as
begin
	insert into admin(UserName,Password,Hashkey,IsActive,CreateDate,FullName,Email,Mobile,Address,Avatar,IsDelete)
				 values(@userName,@pass,@hashKey,@isActive,@createDate,@fullName,@email,@mobile,@address,@avatar,@isDelete);
				  SELECT SCOPE_IDENTITY()

end
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertAccessRight]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_InsertAccessRight]
(
	@groupID int,
	@adminID int
)
as begin
		insert into accessright(GroupID,AdminID) values(@groupID,@adminID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetTotalsSMSMember_FE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetTotalsSMSMember_FE](@email nvarchar(250),@totalsSMS int)
as begin 
	select * from Member where Email=@email and TotalSmsCodeInput=@totalsSMS
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetPointsMemberFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetPointsMemberFE]
(
@memberID int
)
as begin 
	select Points from member where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetPackageDetail]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetPackageDetail]
(
	@packageID int
)
as begin 
	select * from package where PackageID=@packageID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetNewestDrawID]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetNewestDrawID]
as begin 
	select top 1 * from draw  order by createddate desc, DrawID asc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetMemberDetailByIDFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetMemberDetailByIDFE]
(
@memberID int
)
as begin 
	select * from member where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetMemberDetailByEmailFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetMemberDetailByEmailFE]
(
@email nvarchar(250)
)
as begin 
	select * from member where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetManagerIDNewest]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetManagerIDNewest]
as begin
		select top 1 AdminID from admin order by AdminID desc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListWinner]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListWinner]
 (
	--@fromDate datetime
	--@toDate datetime,
	--@start int,
	--@end int
	@numberID int
 )
 as begin 
     DECLARE @firstNumber int,
	         @SecondNumber int,
			 @ThirdNumber int,
			 @FourthNumber int,
			 @FivethNumber int,
			 @ExtraNumber int,
			 @AWARDDATENOW datetime,
			 @AWARDDATEPREV datetime = NULL,
			 @JACKPOTWINNER FLOAT,
			 @AwardNameEnglish nvarchar(250);
	BEGIN  
	    --GET RESULT AWARD WITH CREATEDDATE LAST
		SELECT TOP 1 @AWARDDATENOW = AWARDMEGABALL.CREATEDATE,
		             @FirstNumber = AWARDMEGABALL.FirstNumber,
					 @SecondNumber = AWARDMEGABALL.SecondNumber,
					 @ThirdNumber = AWARDMEGABALL.ThirdNumber,
					 @FourthNumber = AWARDMEGABALL.FourthNumber,
					 @FivethNumber = AWARDMEGABALL.FivethNumber,
					 @ExtraNumber = AWARDMEGABALL.ExtraNumber,
					 @JACKPOTWINNER = AWARDMEGABALL.JACKPOTWINNER,
					 @AwardNameEnglish = Award.AwardNameEnglish
		FROM AWARDMEGABALL
		JOIN Award
		  ON Award.AwardID = AWARDMEGABALL.AwardID
		WHERE AWARDMEGABALL.ISDELETE = 0
		      AND AWARDMEGABALL.ISACTIVE = 1
			  AND AwardMegaball.NumberID = @numberID;

		--GET AWARD DATE PREVIOUS NOT WON
		SELECT TOP 1 @AWARDDATEPREV = CREATEDATE
		FROM AWARDMEGABALL
		WHERE AWARDMEGABALL.ISDELETE = 0
		      AND AWARDMEGABALL.ISACTIVE = 1
			  AND AWARDMEGABALL.NumberID != @numberID
			  AND AWARDMEGABALL.CreateDate < @AWARDDATENOW
			  ORDER BY CREATEDATE DESC;
	END;

	SELECT BOOKINGMEGABALL.BookingID,
	       MEMBER.Email,
		   BookingMegaball.TransactionCode,
		   BOOKINGMEGABALL.FirstNumber,
		   BOOKINGMEGABALL.SecondNumber,
		   BOOKINGMEGABALL.ThirdNumber,
		   BOOKINGMEGABALL.FourthNumber,
		   BOOKINGMEGABALL.FivethNumber,
		   BOOKINGMEGABALL.ExtraNumber, 
		   @JACKPOTWINNER JACKPOTWINNER,
		   BOOKINGMEGABALL.CreateDate,
		   @AWARDDATENOW OpenDate,
		   @AwardNameEnglish AwardNameEnglish
	FROM Member
	JOIN BOOKINGMEGABALL
	  ON BOOKINGMEGABALL.MemberID = Member.MemberID
	WHERE ((@AWARDDATEPREV is null 
	        and BOOKINGMEGABALL.CreateDate < @AWARDDATENOW
	        )
		    or (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREV and @AWARDDATENOW))
		and (
				BOOKINGMEGABALL.FirstNumber = @firstNumber 
				Or BOOKINGMEGABALL.FirstNumber = @secondNumber
				Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
				Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
				Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
				Or BOOKINGMEGABALL.FirstNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.SecondNumber = @firstNumber 
				Or BOOKINGMEGABALL.SecondNumber = @secondNumber
				Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
				Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
				Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
				Or BOOKINGMEGABALL.SecondNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.ThirdNumber = @firstNumber 
				Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
				Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
				Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
				Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
				Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.FourthNumber = @firstNumber 
				Or BOOKINGMEGABALL.FourthNumber = @secondNumber
				Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
				Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
				Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
				Or BOOKINGMEGABALL.FourthNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.FivethNumber = @firstNumber 
				Or BOOKINGMEGABALL.FivethNumber = @secondNumber
				Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
				Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
				Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
				Or BOOKINGMEGABALL.FivethNumber = @extraNumber
			)
		and (
				BOOKINGMEGABALL.ExtraNumber = @firstNumber 
				Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
				Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
				Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
				Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
				Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
			)
		--and BOOKINGMEGABALL.FirstNumber = @firstNumber
		--AND BOOKINGMEGABALL.SecondNumber = @secondNumber
		--AND BOOKINGMEGABALL.ThirdNumber = @thirdNumber
		--AND BOOKINGMEGABALL.FourthNumber = @fourthNumber
		--AND BOOKINGMEGABALL.FivethNumber = @fivethNumber
		--AND BOOKINGMEGABALL.ExtraNumber = @extraNumber

		--select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.BookingID ASC) 
		--				as Row,SUM(1) OVER()AS TOTALROWS,
		--					m.Email,b.TransactionCode,b.BookingID,b.OpenDate,b.CreateDate,b.FirstNumber,
		--					b.SecondNumber,b.ThirdNumber,b.FourthNumber,b.FivethNumber,
		--					b.ExtraNumber,aw.AwardNameEnglish,a.JackPot
		--				from Member m 
		--				left join BookingMegaball b on m.MemberID=b.MemberID
		--				left join AwardMegaball a 
		--					   on (a.FirstNumber=b.FirstNumber and a.SecondNumber=b.SecondNumber and a.ThirdNumber=b.ThirdNumber
		--					 and a.FourthNumber=a.FourthNumber
		--					and a.FivethNumber=b.FivethNumber and a.ExtraNumber=b.ExtraNumber) 
		--				left join Award aw 
		--				       on a.AwardID=aw.AwardID
		--				where a.IsActive=1 and a.IsDelete=0 and b.Status=1
		--				 --and DATEADD(DD, DATEDIFF(DD, 0, a.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, b.OpenDate), 0)
		--				 and a.CreateDate between @fromDate and @toDate )
		--   as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointsMemberFE]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdatePointsMemberFE]
(
@points numeric(18, 10),
@memberID int
)
as begin 
	update member set Points=Points+@points where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointsMember_FE]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePointsMember_FE]
(
@memberID INT,
@points numeric(18, 10)
)
as begin 
	update member set Points=Points+@points where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointsMember]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePointsMember]
(
	@points float,
	@memberID int
)
as begin
		update member set Points=Points+@points where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePointMemberFE]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdatePointMemberFE]
/*
	EDITED BY: LÝ KIẾN ĐỨC
	EDITED DATE: 25.01.2018
	DESCRIPTION: UPDATE JACKPOT POINT AwardMegaball
*/
(
	@Points numeric(18, 10),
	@MemberID int,
	@NumberID int,
	@TotalJackPot float
)
as begin 
	update Member set Points=@Points where MemberID=@MemberID;
	--IF(@@ROWCOUNT > 0)
	--BEGIN 
	--	--if query update success -> update jackpot AwardMegaball
	--	DECLARE @ISEXISTS INT;
	--	BEGIN
	--		SELECT @ISEXISTS = 1
	--		FROM AwardMegaball
	--		WHERE AwardMegaball.IsDelete = 0;
	--	END;
	--	IF(@ISEXISTS = 1)
	--		BEGIN
	--		    --IF TABLE AwardMegaball EXISTS LEAST 1 RECORD -> UPDATE JACKPOT
	--			UPDATE AwardMegaball
	--			SET AwardMegaball.JackPot = @TotalJackPot
	--			WHERE AwardMegaball.IsDelete = 0
	--			      AND EXISTS(SELECT 1
	--							FROM AwardMegaball AW
	--							WHERE AW.IsDelete = 0
	--								AND AwardMegaball.CreateDate >=AW.CreateDate
	--							)
	--		END
	--	ELSE 
	--		BEGIN
	--		    --IF TABLE AwardMegaball NOT EXISTS LEAST 1 RECORD -> INSERT 1 RECORD DEFAULT
	--			INSERT 
	--			INTO AWARDMEGABALL(CreateDate, IsDelete,ISACTIVE, JACKPOT)
	--				VALUES(SYSDATETIME(),0,0,@TotalJackPot)
	--		END;
		
	--END;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePasswordMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePasswordMemberFE]
(
	@password nvarchar(250),
	@email nvarchar(250)
)
as begin 
	update member set Password=@password where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdatePasswordMember]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdatePasswordMember]
(
	@password nvarchar(250),
	@fullname nvarchar(250)
)
as begin
		update member set Password=@password where Fullname=@fullname
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateNewPasswordMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateNewPasswordMemberFE]
(
	@newpassword nvarchar(250),
	@email nvarchar(250),
	@password nvarchar(250)
)
as begin 
	update member set Password=@newpassword where Email=@email and Password=@password
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateMemberReferenceFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateMemberReferenceFE]
(
	@MemberID int,
	@LockID nvarchar(1000),
	@MemberIDReference varchar(1000),
	@Status int,
	@Amount decimal(18,5)
)
as begin 
		declare @TotalNumberTicket int;
		set @TotalNumberTicket = 0;
		set @TotalNumberTicket = (select total from FreeTicketDaily where IsActive = 1);

		update MemberReference set LockID = @LockID, Status = @Status,Amount=@Amount
		where MemberID = @MemberID and MemberIDReference = @MemberIDReference

		if(@Status = 0)
		begin
			if(@TotalNumberTicket > 0)
				begin
					update FreeTicketDaily set Total =@TotalNumberTicket - 1 where IsActive = 1;
				end;
		end;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateJackpotAwardMegaball]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateJackpotAwardMegaball]
 (
	@jackpot decimal(18,5),
	@draw int,
	@totalwon decimal(18,5)
 )
 as begin
	update AwardMegaball 
	set 
		JackPot = @jackpot,
		TotalWon = @totalwon
	where NumberID=(select top 1 AwardMegaball.NumberID from AwardMegaball where DrawID = @draw)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateInformationMember_FE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateInformationMember_FE]
(
	@email nvarchar(250),
	@mobile char(15),
	@smsCode nvarchar(50),
	@isIndentifySms int,
	@expireSmSCode datetime,
	@totalSmsCodeInput int
)
as begin
	update Member set Mobile=@mobile, SmsCode=@smsCode,IsIndentifySms=@isIndentifySms,TotalSmsCodeInput=@totalSmsCodeInput,ExpireSmSCode=@expireSmSCode
	where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateGroupAdmin]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateGroupAdmin]
(
		@groupName nvarchar(250),
		@groupID int
)
as begin 
		update groupadmin set GroupName=@groupName where GroupID=@groupID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateFreeTicketMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateFreeTicketMemberFE]
(
	@numberticketfree int,
	@memberID int
)
as begin
		update member set NumberTicketFree=@numberticketfree where MemberID=@memberID;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateExchangeTicket]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateExchangeTicket]
 (
	@exchangeID int,
	@pointValue float,
	@ticketNumber int,
	@createUser nvarchar(250),
	@updateUser nvarchar(250),
	@updateDate datetime,
	@coinID char(10)
	
 )
 as begin
	update ExchangeTicket set PointValue=@pointValue, TicketNumber=@ticketNumber, CreateUser=@createUser, UpdateDate=@updateDate,UpdateUser=@updateUser,CoinID=@coinID
	where ExchangeID=@exchangeID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateExchangePoint]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateExchangePoint]
 (
	@exchangeID int,
	@pointValue float,
	@bitcoinValue float,
	@createUser nvarchar(250),
	@updateUser nvarchar(250),
	@updateDate datetime
	
 )
 as begin
	update ExchangePoint set PointValue=@pointValue, BitcoinValue=@bitcoinValue, CreateUser=@createUser, UpdateUser=@updateUser,UpdateDate=@updateDate
	where ExchangeID=@exchangeID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateCountry_CMS]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateCountry_CMS]
(
    @CountryID int,
 	@CountryName nvarchar(250),
	@IsActive smallint,
	@IsDeleted smallint,
 	@UpdatedDate DateTime,
	@UpdatedUser nvarchar(250),
	@PhoneZipCode varchar(50)
)
as begin 
	Update Country set CountryName=@CountryName, PhoneZipCode=@PhoneZipCode,IsActive=@IsActive,IsDeleted=@IsDeleted,UpdatedDate=@UpdatedDate,UpdatedUser=@UpdatedUser
	Where CountryID=@CountryID and IsActive=1 and IsDeleted=0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAwardNumber]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateAwardNumber]
 (
	@numberID int,
	@awardID int,
	@numberValue float,
	@stationName nvarchar(250),
	@stationNameEnglish nvarchar(250)

 )
 as begin  
		update awardnumber set AwardID=@awardID,NumberValue=@numberValue,StationName=@stationName,StationNameEnglish=@stationNameEnglish
		 where NumberID=@numberID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAwardMegaball]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAwardMegaball]
 (
	@numberID int,
	@awardID int,	
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fivethNumber int,
	@extraNumber int,
	@genCode nvarchar(500),
	@jackPotDefault float
 )
 as begin
	  DECLARE @AWARDDATEPREVIOUS DATETIME = null,
	        @JACKPOTPREVIOUS FLOAT = @JACKPOTDEFAULT,
	        @TOTALJACKPOT FLOAT = @JACKPOTDEFAULT,
			@JACKPOTWINNER FLOAT = 0;
	BEGIN  
	    --GET RESULT AWARD WITH CREATEDDATE LAST
		SELECT TOP 1 @AWARDDATEPREVIOUS = CREATEDATE,
		             @JACKPOTPREVIOUS = AWARDMEGABALL.JackPot
		FROM AWARDMEGABALL
		WHERE AWARDMEGABALL.ISDELETE = 0
		      AND AWARDMEGABALL.ISACTIVE = 1
			  ORDER BY CREATEDATE DESC;
	END;

	BEGIN  
		SELECT @TOTALJACKPOT = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
		FROM BOOKINGMEGABALL
		WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
		      AND NOT EXISTS (SELECT 1
			              FROM BOOKINGMEGABALL
						  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
								and (
									   BOOKINGMEGABALL.FirstNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
									   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.SecondNumber = @firstNumber 
									   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
									   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
									   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
									   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
									   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
									   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
									   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.FourthNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
									   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.FivethNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
									   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
									)
								and (
									   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
									   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
									   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
									));
	END

	IF(@TOTALJACKPOT IS NULL)
	BEGIN
		--ELSE IF NOW HAVE LEAST 1 MEMBER WINNER -> TOTAL JACKPOT WILL BE DEFAULT (CONFIG SOURCES) 
		--@JACKPOTWINNER: Total jackpot won -> save history
	   BEGIN  
			SELECT @JACKPOTWINNER = (@JACKPOTPREVIOUS + SUM(BOOKINGMEGABALL.ValuePoint))
			FROM BOOKINGMEGABALL
			WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
				  AND EXISTS (SELECT 1
							  FROM BOOKINGMEGABALL
							  WHERE (@AWARDDATEPREVIOUS IS NULL OR (BOOKINGMEGABALL.CREATEDATE BETWEEN @AWARDDATEPREVIOUS and SYSDATETIME()))
									and (
									   BOOKINGMEGABALL.FirstNumber = @firstNumber 
									   Or BOOKINGMEGABALL.FirstNumber = @secondNumber
									   Or BOOKINGMEGABALL.FirstNumber = @thirdNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fourthNumber
									   Or BOOKINGMEGABALL.FirstNumber = @fivethNumber
									   Or BOOKINGMEGABALL.FirstNumber = @extraNumber
									)
									and (
										   BOOKINGMEGABALL.SecondNumber = @firstNumber 
										   Or BOOKINGMEGABALL.SecondNumber = @secondNumber
										   Or BOOKINGMEGABALL.SecondNumber = @thirdNumber
										   Or BOOKINGMEGABALL.SecondNumber = @fourthNumber
										   Or BOOKINGMEGABALL.SecondNumber = @fivethNumber
										   Or BOOKINGMEGABALL.SecondNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.ThirdNumber = @firstNumber 
										   Or BOOKINGMEGABALL.ThirdNumber = @secondNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @thirdNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @fourthNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @fivethNumber
										   Or BOOKINGMEGABALL.ThirdNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.FourthNumber = @firstNumber 
										   Or BOOKINGMEGABALL.FourthNumber = @secondNumber
										   Or BOOKINGMEGABALL.FourthNumber = @thirdNumber
										   Or BOOKINGMEGABALL.FourthNumber = @fourthNumber
										   Or BOOKINGMEGABALL.FourthNumber = @fivethNumber
										   Or BOOKINGMEGABALL.FourthNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.FivethNumber = @firstNumber 
										   Or BOOKINGMEGABALL.FivethNumber = @secondNumber
										   Or BOOKINGMEGABALL.FivethNumber = @thirdNumber
										   Or BOOKINGMEGABALL.FivethNumber = @fourthNumber
										   Or BOOKINGMEGABALL.FivethNumber = @fivethNumber
										   Or BOOKINGMEGABALL.FivethNumber = @extraNumber
										)
									and (
										   BOOKINGMEGABALL.ExtraNumber = @firstNumber 
										   Or BOOKINGMEGABALL.ExtraNumber = @secondNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @thirdNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @fourthNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @fivethNumber
										   Or BOOKINGMEGABALL.ExtraNumber = @extraNumber
										));
		END
		--set @JACKPOTWINNER = @JACKPOTPREVIOUS;
	   SET @TOTALJACKPOT = @jackPotDefault;
	END

	update AwardMegaball 
	set AwardID = @awardID,
	    FirstNumber = @firstNumber,
		SecondNumber = @secondNumber,
		ThirdNumber = @thirdNumber,
		FourthNumber = @fourthNumber,
		FivethNumber = @fivethNumber,
		ExtraNumber = @extraNumber, 
		GenCode = @genCode, 
		JackPot = @TOTALJACKPOT,
		JACKPOTWINNER = @JACKPOTWINNER
	where NumberID=@numberID

 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAwardFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAwardFE]
(
	@awardName nvarchar(250),
	@awardValue float,
	@awardID int

)
as begin 
	update award set AwardName=@awardName,AwardValue=@awardValue where AwardID=@awardID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAward]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAward]
(
	@awardID int,
	--@awardName nvarchar(250),
	--@awardValue float,
	@awardNameEnglish nvarchar(250),
	@awardValue decimal(18,10),
	@awardFee decimal(18, 10),
	@AwardPercent decimal(18,10)
)
as begin 
		update award set AwardNameEnglish=@awardNameEnglish, AwardValue =@awardValue, AwardFee=@awardFee ,AwardPercent=@AwardPercent
		 where AwardID=@awardID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateAdmin]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateAdmin]
(
	@adminID int,
	@fullName nvarchar(250),
	@email nvarchar(250),
	@mobile char(50),
	@address nvarchar(250)
)
as begin
		update admin set FullName=@fullName,Email=@email,Mobile=@mobile,Address=@address where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateActive]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateActive]
(
	@email nvarchar(250)
)
as begin 
	update member set IsActive=1 where Email=@email
end
GO
/****** Object:  StoredProcedure [dbo].[SP_TicketWinning_Add]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_TicketWinning_Add]
 (
	@BookingID int,
	@DrawID int,
	@BuyDate DateTime,
	@AwardID int,
	@FirstNumberBuy int,
	@SecondNumberBuy int,
	@ThirdNumberBuy int,
	@FourthNumberBuy int, 
	@FivethNumberBuy int,
	@ExtraNumberBuy int,
	@FirstNumberAward int,
	@SecondNumberAward int,
	@ThirdNumberAward int,
	@FourthNumberAward int, 
	@FivethNumberAward int,
	@ExtraNumberAward int,
	@NumberBallNormal int,
	@NumberBallGold int, 
	@AwardValue decimal(18, 5),
	@AwardDate DateTime,
	@MemberID int,
	@Priority int,
	@AwardFee decimal(18, 8),
	@TicketWinningID int out
 )
 as begin 
		Declare @IsExists int = 0;
		Declare @TicketWinID int = 0;
		Begin
			Select @IsExists = 1
			From TicketWinning
			Where TicketWinning.DrawID = @DrawID
			      And TicketWinning.BookingID = @BookingID;
		End

		If(@IsExists = 0) 
		Begin
			Insert into TicketWinning(BookingID, 
									DrawID, 
									BuyDate, 
									AwardID, 
									FirstNumberBuy, 
									SecondNumberBuy, 
									ThirdNumberBuy, 
									FourthNumberBuy, 
									FivethNumberBuy, 
									ExtraNumberBuy,
									FirstNumberAward,
									SecondNumberAward,
									ThirdNumberAward,
									FourthNumberAward, 
									FivethNumberAward,
									ExtraNumberAward,
									NumberBallNormal,
									NumberBallGold, 
									AwardValue,
									AwardDate,
									MemberID,
									Priority,
									AwardFee)
			Values(
					@BookingID, 
					@DrawID, 
					@BuyDate, 
					@AwardID, 
					@FirstNumberBuy, 
					@SecondNumberBuy, 
					@ThirdNumberBuy, 
					@FourthNumberBuy, 
					@FivethNumberBuy, 
					@ExtraNumberBuy,
					@FirstNumberAward,
					@SecondNumberAward,
					@ThirdNumberAward,
					@FourthNumberAward, 
					@FivethNumberAward,
					@ExtraNumberAward,
					@NumberBallNormal,
					@NumberBallGold, 
					@AwardValue,
					@AwardDate,
					@MemberID,
					@Priority,
					@AwardFee
				  );

				  Set @TicketWinID = Scope_Identity();
		End;
		

	Select @TicketWinningID = @TicketWinID; 
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_TicketNumberTimeLog_Add]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_TicketNumberTimeLog_Add]
 (
	@Email nvarchar(250),
	@TicketNumber int,
	@CreatedDate datetime,
	@Id int out
 )
 as begin 
		Insert into TicketNumberTimeLog(Email, TicketNumber, CreatedDate)
		Values (@Email, @TicketNumber, @CreatedDate);

		select @Id = Scope_Identity();
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_SumValuePoints]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[SP_SumValuePoints]
  as begin
	select Sum(ValuePoint) as TotalCount from BookingMegaball 
  end
GO
/****** Object:  StoredProcedure [dbo].[SP_SumValuePointByMemberID]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_SumValuePointByMemberID]
	 (
		@memberID int
	 )
	 as begin 
		 select sum(ValuePoint) as TotalSum from bookingmegaball where MemberID=@memberID
	end
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberPaging]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_SearchMemberPaging]
(
	@keyword nvarchar(250)
)	
as
begin
select COUNT(Email) as Totalkeyword from Member where Email like N'%'+@keyword+'%';
end
GO
/****** Object:  StoredProcedure [guest].[SP_SearchMemberNoPaging]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_SearchMemberNoPaging]
(
	@Keyword nvarchar(400))
as begin 
	select * from Member m

    where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberNoPaging]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_SearchMemberNoPaging]
(
	@Keyword nvarchar(400))
as begin 
	select * from Member m

    where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
end
GO
/****** Object:  StoredProcedure [guest].[SP_SearchMemberByBookingMegaball]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [guest].[SP_SearchMemberByBookingMegaball]
(
	@Keyword nvarchar(400),
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
	)
as begin 
select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,b.*,m.Email 
	 from BookingMegaball b left join Member m on b.MemberID=m.MemberID
	-- where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	--) as product
	--where Row>=@start and Row<=@end
    where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)) as product
	where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberByBookingMegaball]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_SearchMemberByBookingMegaball]
(
	@Keyword nvarchar(400),
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
	)
as begin 
select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,b.*,m.Email 
	 from BookingMegaball b left join Member m on b.MemberID=m.MemberID
	-- where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	--) as product
	--where Row>=@start and Row<=@end
    where ((@Keyword is null) or (@Keyword='0') or m.Email like '%' + @Keyword + '%' )
	and (@fromDate='1/1/1990 12:00:00 AM' or b.CreateDate between @fromDate and @toDate)) as product
	where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchMemberByAdmin]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_SearchMemberByAdmin]
(
	@Keyword nvarchar(400),
	@start int,
	@end int,
	@fromDate datetime = null,
	@toDate datetime = null
)

as
begin

--select * from Member m

--where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,m.* from Member m
where ((@Keyword is null) or (@Keyword='0') or (m.E_Wallet like '%' + @Keyword + '%') or m.Email like '%' + @Keyword + '%' )
and (@fromDate= null or m.CreateDate between @fromDate and @toDate))
as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_SearchCompanyByKeyword]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_SearchCompanyByKeyword]
(
	@Keyword nvarchar(400)
)

as
begin

select * from CompanyInfomation c

where ((@Keyword is null) or (@Keyword='0') or (c.CompanyName like '%' + @Keyword + '%') or c.Email like '%' + @Keyword + '%' )

end;
GO
/****** Object:  StoredProcedure [dbo].[SP_Member_Wallet_SELPOINT]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_Member_Wallet_SELPOINT]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 11.01.2018
	DESCRIPTION: LOAD INFO POINT AND ADDRESS BY MEMBERID
*/
(
	@MemberID int
)
as
begin
	SELECT MEM.Points,
	       MW.WalletAddress,
		   MW.RippleAddress,
		   MW.SerectKeyRipple
	FROM Member_Wallet MW
	JOIN Member MEM
	  ON MEM.MemberID = MW.MemberID
	WHERE MW.MemberID = @memberID;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_MEMBER_SELPOINT]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROC [dbo].[SP_MEMBER_SELPOINT]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 02.03.2018
	DESCRIPTION: LOAD POINT BY MEMBERID
*/
(
	@MemberID int
)
AS BEGIN 
	SELECT MEMBER.Points
	FROM MEMBER
	WHERE MEMBER.MemberID = @MemberID;
END
GO
/****** Object:  StoredProcedure [dbo].[SP_MEMBER_SELBYMEMID]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_MEMBER_SELBYMEMID]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 16.01.2018
	DESCRIPTION: Load Info Member By MemberID
*/
(
	@MemberID int
)
AS BEGIN
	SELECT M.MemberID,
	       M.Email,
		   M.E_Wallet,
		   M.Password,
		   M.Points,
		   M.FullName,
		   M.IsUserType,
		   M.UserTypeID,
		   --M_W.WalletAddress,
		   --M_W.RippleAddress,
		   --M_W.SerectKeyRipple,
		   M.Points,
		   M.NumberTicketFree,
		   M.LinkReference
	FROM MEMBER M
	--JOIN Member_Wallet M_W
	--  ON M_W.MemberID = M.MemberID
	WHERE M.IsDelete = 0
	      AND M.MemberID = @MemberID;
END;
GO
/****** Object:  StoredProcedure [dbo].[SP_MEMBER_SEL_CHECKMEM]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_MEMBER_SEL_CHECKMEM]
/*
	CREATED BY: LÝ KIẾN ĐỨC
	CREATED DATE: 16.01.2018
	DESCRIPTION: CHECK MEMBER FACEBOOK EXISTS
*/
(
	@Email nvarchar(250)
)
AS BEGIN
	SELECT M.MemberID
	FROM MEMBER M
	WHERE M.IsDelete = 0
	      AND M.Email = @Email;
END;
GO
/****** Object:  StoredProcedure [dbo].[SP_Member_ResetPassword]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Member_ResetPassword]   
    @Email nvarchar(250)
AS   
    Update Member set Password = '' where Email = @Email
GO
/****** Object:  StoredProcedure [dbo].[SP_Member_DEL]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_Member_DEL]   
    @memberID int
AS   
    delete Member where MemberID = @memberID
GO
/****** Object:  StoredProcedure [dbo].[SP_LoginMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LoginMemberFE]
(
	@email nvarchar(250),
	@password nvarchar(250)


)
as begin 
	select m.* from member m where Email=@email and Password=@password and m.IsActive=1 and m.IsDelete=0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LoginManagerAccount]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LoginManagerAccount]
(
	@userName nvarchar(50),
	@pass nvarchar(250)
)
as begin
        select UserName,Password,ac.GroupID from admin a left join AccessRight ac on a.AdminID=ac.AdminID
		 where UserName=@userName and Password=@pass and IsActive=1 and IsDelete=0;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LoginAdmin]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_LoginAdmin]
(
	@userName nvarchar(250),
	@pass nvarchar(250)
)
as 
begin
select UserName,Password,ac.GroupID from admin a left join AccessRight ac on a.AdminID=ac.AdminID 
where UserName=@userName 
and Password=@pass 
and IsActive=1 
and IsDelete=0;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LockAndUnlockMemberFE]
(
	@isActive smallint,
	@memberID int
)
as begin 
	update member set IsActive=@isActive where MemberID=@memberID and IsActive = 0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockMember]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LockAndUnlockMember]
(
	@isActive smallint,
	@memberID int
)
as begin 

		update member set IsActive=@isActive where MemberID=@memberID and IsActive = 0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockExchangeTicket]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockExchangeTicket]
 (
 @exchangeID int,
 @isActive smallint
 )
 as begin
	update ExchangeTicket set IsActive=@isActive where ExchangeID=@exchangeID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockExchangePoint]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockExchangePoint]
 (
 @exchangeID int,
 @isActive smallint
 )
 as begin
	update ExchangePoint set IsActive=@isActive where ExchangeID=@exchangeID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockCountry_CMS]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockCountry_CMS](
	@CountryID int,
	@IsActive smallint
 )
 as begin
		update Country set IsActive=@IsActive where CountryID=@CountryID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAwardNumber]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAwardNumber]
 (
	@isActive smallint,
	@numberID int
 )
 as begin 
		update awardnumber set IsActive=@isActive where NumberID=@numberID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAwardMegaball]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAwardMegaball]
 (
	@numberID int,
	@isActive smallint
 )
 as begin
		update AwardMegaball set IsActive=@isActive where NumberID=@numberID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAwardFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAwardFE]
(
	@isActive smallint,
	@awardID int
)
as begin 
	update award set IsActive=@isActive where AwardID=@awardID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndUnlockAward]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_LockAndUnlockAward]
 (
	@awardID int,
	@isActive smallint
 )
 as begin
		update award set IsActive=@isActive where AwardID=@awardID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_LockAndLockConfigTicket_CMS]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_LockAndLockConfigTicket_CMS]
 (
	@configID int,
	@isActive smallint
 )
 as begin
		update TicketConfig set IsActive=@isActive where ConfigID=@configID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionWalletCoinFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionWalletCoinFE]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime,
	@coinID nchar(10)
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,m.Email,m.E_Wallet 
from TransactionCoin tp left join Coin c on tp.CoinID= c.CoinID
						 left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and(tp.CoinID=@coinID)
and (@fromDate ='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
and status=0
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionWalletBySearchFrontEnd]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionWalletBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,m.Email,m.E_Wallet 
from TransactionCoin tp left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@fromDate IS NULL or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionWalletByMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionWalletByMemberFE]
(
@memberID int,
@start int,
@end int
)
as begin
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, tp.*,m.Email,m.E_Wallet from TransactionCoin tp left join member m on tp.MemberID=m.MemberID 
	where tp.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionsBookingByMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionsBookingByMemberFE]
(
	@memberID int,
	@start int,
	@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID
	 where b.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointStatus]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPointStatus]
(
	@status int,
	@transactionID int
)
as begin 
		update transactionpoint set Status=@status where TransactionID=@transactionID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointPage]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionPointPage]
(
	@start int,
	@end int
)
as begin 
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID) as Products
		  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointBySearchFrontEnd]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPointBySearchFrontEnd]
/*
	EDITED BY: LÝ KIẾN ĐỨC
	EDITED DATE: 05.03.2018
	DESCRIPTION: CHANGE @FROMDATE = '1/1/1990 12:00:00 AM' -> @FROMDATE IS NULL
*/
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime = null,
	@toDate datetime
	
)
as
begin
--select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
--tp.*,m.Email,m.E_Wallet 
--from transactionpoint tp left join member m on tp.MemberID=m.MemberID 
--where (@memberID=0 or tp.MemberID=@memberID)

--and (@fromDate='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
--) as Products  where Row>=@start and Row<=@end

	SELECT *
	FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY TP.CREATEDATE DESC) AS ROW,
			       SUM(1) OVER()AS TOTALROWS, 
				   TP.*,
				   M.EMAIL,
				   M.E_WALLET 
			FROM TRANSACTIONPOINT TP 
			LEFT JOIN MEMBER M 
			       ON TP.MEMBERID = M.MEMBERID 
			WHERE (@MEMBERID = 0 OR TP.MEMBERID = @MEMBERID)
				   AND (@FROMDATE IS NULL 
					    OR TP.CREATEDATE BETWEEN @FROMDATE AND @TODATE)
	) AS PRODUCTS  
	WHERE ROW >= @START 
		  AND ROW <= @END
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointBySearchCMS]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPointBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime =null,
	@toDate datetime=null,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,m.Email,m.E_Wallet 
from transactionpoint tp left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@status=-1 or tp.Status=@status)
and (@fromDate=null or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPointByMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPointByMemberFE]
(
	@memberID int,
	@start int,
	@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID 
	where tp.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPoint]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPoint]
as begin 
	select tp.*,m.Email,m.E_Wallet from transactionpoint tp left join member m on tp.MemberID=m.MemberID  order by tp.CreateDate DESC	
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageBySearchFrontEnd]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPackageBySearchFrontEnd]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,p.PackageName,p.PackageNameEnglish,m.Email,m.E_Wallet 
from transactionpackage tp left join package p on tp.PackageID=p.PackageID 
left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@fromDate='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageBySearchCMS]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_ListTransactionPackageBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime,
	@toDate datetime,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tp.*,p.PackageName,p.PackageNameEnglish, m.Email,m.E_Wallet 
from transactionpackage tp left join package p on tp.PackageID=p.PackageID 
left join member m on tp.MemberID=m.MemberID 
where (@memberID=0 or tp.MemberID=@memberID)
and (@status=-1 or tp.Status=@status)
and (@fromDate='1/1/1990 12:00:00 AM' or tp.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageByMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPackageByMemberFE]
(
@memberID int
)
as begin 
	select tp.*,p.PackageName,m.Email,m.E_Wallet from transactionpackage tp left join package p on tp.PackageID=p.PackageID left join member m on tp.MemberID=m.MemberID where tp.MemberID=@memberID  order by CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageByMember_FE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPackageByMember_FE]
(
@end int,
@start int,
@memberID int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY tp.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS,tp.*,p.PackageName,p.PackageNameEnglish,m.Email,m.E_Wallet from transactionpackage tp left join package p on tp.PackageID=p.PackageID left join member m on tp.MemberID=m.MemberID
	 where tp.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionPackageActiveByMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionPackageActiveByMemberFE]
(
@memberID int
)
as begin 
	select top 1 * from TransactionPackage where MemberID=@memberID and Status=1 order by ExpireDate desc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinStatus]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionCoinStatus]
(
	@status int,
	@transactionID char(150)
)
as begin 
		update TransactionCoin set Status=@status where TransactionID=@transactionID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinPaging]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionCoinPaging]
(
	@start int,
	@end int
)
as begin 
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY tc.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, tc.* from TransactionCoin tc) as Products
		  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinID]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionCoinID]
(
	@start int,
	@end int,
	@fromDate Datetime,
	@toDate Datetime
)
as begin 
select * from (
							SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,
							tc.* from TransactionCoin tc left join member m on tc.MemberID=m.MemberID
												left join Coin c on tc.CoinID=c.CoinID 
							where (@fromDate= null or m.CreateDate between @fromDate and @toDate)
							)
as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoinBySearchCMS]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionCoinBySearchCMS]
(
	@start int,
	@end int,
	@memberID int,
	@fromDate datetime =null,
	@toDate datetime=null,
	@status int
)
as
begin
select *from (SELECT ROW_NUMBER() OVER (ORDER BY tc.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS, 
tc.*,m.Email
from TransactionCoin tc left join member m on tc.MemberID=m.MemberID 
where (@memberID=0 or tc.MemberID=@memberID)
and (@status=-1 or tc.Status=@status)
and (@fromDate=null or tc.CreateDate between @fromDate and @toDate)
) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionCoin]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionCoin]
as begin 
	select tc.*,m.Email from TransactionCoin tc left join member m on tc.MemberID=m.MemberID  order by tc.CreateDate DESC	
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingOrderByDateFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingOrderByDateFE]
(
	@start int,
	@end int
)
as begin 
	select * from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet
	 from bookingmegaball b left join member m on b.MemberID=m.MemberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingMegaballByMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingMegaballByMemberFE]
(
	@memberID int,
	@start int,
	@end int
)
as begin 
	select * from(SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,
	 b.*,m.Email,m.E_Wallet from BookingMegaball b left join member m on b.MemberID=m.MemberID where b.MemberID=@memberID and b.Status = 1  
	 ) as product
	where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByMemberPagingFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingByMemberPagingFE]
(
	@memberID int,
	@start int,
	@end int
)	
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row, SUM(1) OVER()AS TOTALROWS, b.*,m.Email,m.E_Wallet 
	from bookingmegaball b left join member m on b.MemberID=m.MemberID left join AwardMegaball aw on b.CreateDate=aw.CreateDate
	 where b.MemberID=@memberID) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListTransactionBookingByMemberFE]
(
	@memberID int
)
as begin 
	select b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID where b.MemberID=@memberID  order by CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByDateFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingByDateFE]
(
	@fromDate datetime,
	@toDate datetime
)
as begin 
	select Distinct b.*,m.Email,m.E_Wallet from booking b left join member m on b.MemberID=m.MemberID where b.CreateDate between @fromDate and @toDate and b.Status=1 order by b.CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTransactionBookingByDate_FE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTransactionBookingByDate_FE]
(
	@fromDate datetime,
	@toDate datetime
)
as begin 
	select b.*,m.Email,m.E_Wallet 
	from bookingmegaball b 
	left join member m 
	       on b.MemberID=m.MemberID 
    where (b.CreateDate between @fromDate and @toDate) 
	      and b.Status=1 
    order by b.CreateDate DESC
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTicketWinningByMemberID]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTicketWinningByMemberID]
(
	@memberID int
)
as begin
		select BM.*
		from 
		BookingMegaball BM
		join AwardMegaball AM
		on BM.DrawID = am.DrawID
		where BM.MemberID = @memberID;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListTicketConfig]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListTicketConfig]

as begin 
	select *from TicketConfig 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListMemberReferenceFEByMemberID]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListMemberReferenceFEByMemberID]
(
	@MemberID int
)
as begin 
	select *from MemberReference where MemberID = @MemberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListMember_FE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_ListMember_FE]
(
	@start int,
	@end int
)
as
begin
select *from 
(SELECT ROW_NUMBER() OVER (ORDER BY m.MemberID DESC) as Row,
m.MemberID,
mb.RippleAddress,
m.FullName,
m.Avatar,
m.Mobile,
m.Email,
m.Points,
mb.NumberCoin,
mb.IndexWallet,
mb.WalletAddress
from Member m left join Member_Wallet mb on m.MemberID=mb.MemberID where m.IsDelete=0 
group by m.MemberID,
mb.RippleAddress,
m.FullName,
m.Avatar,
m.Mobile,
m.Email,
m.Points,
mb.NumberCoin,
mb.IndexWallet,
mb.WalletAddress) as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_ListGroupAdmin]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListGroupAdmin]
as begin
		select *from groupadmin
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListExchangeTicketByMemberFE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListExchangeTicketByMemberFE]
(
	@memberID int,
	@CoinID nchar(10)
)
as begin
	select *from ExchangeTicket where memberID = @memberID and isdelete = 0 and isactive = 1 and CoinID = @CoinID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListExchangePointByMember]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_ListExchangePointByMember]
(
@memberID int
)
as begin 
	select *from ExchangePoint where memberID = @memberID and isdelete = 0 and isactive = 1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListDetailsByMemberIDRef]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListDetailsByMemberIDRef]
(
	@MemberID int
)
as begin 
	select *from MemberReference where MemberIDReference = @MemberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateUserNamedAdmin]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateUserNamedAdmin]
(
	@userName nvarchar(50),
	@adminID int
)
as begin
		update admin set UserName=@userName where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateUserNameAndPasswordAdmin]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateUserNameAndPasswordAdmin]
(
	@adminID int,
	@userName nvarchar(50),
	@password nvarchar(250)
)
as begin 
		update admin set UserName=@userName,Password=@password where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateTicketConfig_CMS]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateTicketConfig_CMS]
  (
	@ConfigName nvarchar(250),
	@ConfigID int,
	@NumberTicket int,
	@CoinValues numeric(18,10),
	@CoinID nvarchar(250),
	@UpdateDate Datetime
 )
 as begin  
		update TicketConfig set CoinID=@CoinID,NumberTicket=@NumberTicket,CoinValues=@CoinValues,ConfigName=@ConfigName, UpdatedDate=@UpdateDate
		 where ConfigID=@ConfigID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusTransaction]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusTransaction]
(
	@status int,
	@transactionID int
)
as begin 
	update TransactionPackage set Status=@status where TransactionID=@transactionID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusMember]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusMember]
(
	@memberID int
)
as begin 
	update member set IsActive=1 where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusGroupAdmin]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusGroupAdmin]
(
	@isActive smallint,
	@groupID int
)
as begin
			update groupadmin set IsActive=@isActive where GroupID=@groupID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusExchangeTicket]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_UpdateStatusExchangeTicket]
(
	@status int,
	@exchangeID int,
	@deleteUser nvarchar(250),
	@deleteDate datetime
)
as begin 
		update ExchangeTicket set IsDelete =@status, DeleteUser=@deleteUser,DeleteDate=@deleteDate where ExchangeID=@exchangeID 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusExchangePoint]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusExchangePoint]
(
	@status int,
	@exchangeID int,
	@deleteUser nvarchar(250),
	@deleteDate datetime
)
as begin 
		update ExchangePoint set IsDelete =@status, DeleteUser=@deleteUser,DeleteDate=@deleteDate where ExchangeID=@exchangeID 
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBookingMegaball]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBookingMegaball]
(
	@status int,
	@bookingID int
)
as begin 
		update BookingMegaball set status =@status where BookingID=@bookingID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBookingFE]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBookingFE]
(
	@bookingID int,
	@status int
)
as begin 
	update booking set Status=@status where BookingID=@bookingID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBooking_FE]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBooking_FE]
(
	@bookingID int,
	@status int
)
as begin 
	update bookingmegaball set Status=@status where BookingID=@bookingID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusBooking]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusBooking]
(
	@status int,
	@bookingID int
)
as begin
		update booking set Status=@status where BookingID=@bookingID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusAwardWithdraw_CMS]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_UpdateStatusAwardWithdraw_CMS]
(
	@TransactionID char(50),
	@RequestStatus smallint,
	@AdminIDApprove nvarchar(250),
	@UpdatedUser nvarchar(250)
)
as
begin
update AwardWithdraw
set RequestStatus=@RequestStatus,
AdminIDApprove=@AdminIDApprove,
UpdatedUser=@UpdatedUser,
UpdatedDate=GETDATE(),
ApproveDate=GETDATE()
where TransactionID=@TransactionID
and IsDeleted=0;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusAdmin]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusAdmin]
(
	@isDelete smallint,
	@adminID int
)
as begin
		update admin set IsDelete=@isDelete where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UpdateStatusActive_CMS]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_UpdateStatusActive_CMS]
(
	@isAutoLottery smallint,
	@configID int
)
as begin 
	update SystemConfig set IsAutoLottery=@isAutoLottery where ConfigID=@configID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListTicketWinningByBookingID]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetListTicketWinningByBookingID]   
	@BookingID int,
	@awardvalue decimal(18,5),
	@awardfee decimal(18,8),
	@priority int,
	@DrawID int,
	@MemberID int
AS 
BEGIN 
     SET NOCOUNT ON 
    select *
	from TicketWinning
	where BookingID = @BookingID 
	and AwardValue = @awardvalue
	and AwardFee = @awardfee
	and [Priority] = @priority
	and DrawID = @DrawID
	and MemberID = @MemberID
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListSystemConfig_CMS]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListSystemConfig_CMS]
 as begin 
	select * from SystemConfig
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListMember]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListMember]
(
	@start int,
	@end int
)

 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.MemberID DESC) as Row,SUM(1) OVER()AS TOTALROWS,m.* from  member m ) as Products
		  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListLinkResetPasswordFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListLinkResetPasswordFE]
(
	@Date datetime,
	@Email nvarchar(250)
)

 as begin
		select *from LinkResetPassword 
		where CONVERT(nvarchar(250),CreatedDate, 103) = CONVERT(nvarchar(250),@Date, 103) and Email = @Email
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListCountryBySearch_CMS]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListCountryBySearch_CMS]
(
	@Keyword nvarchar(400),
	@start int,
	@end int
)
as begin
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.CreatedDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,c.* from Country c
where ((@Keyword is null) or (@Keyword='0') or c.CountryName like '%' + @Keyword + '%' ))
as Products  where Row>=@start and Row<=@end
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListCountry_CMS]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListCountry_CMS]
(
		@start int,
		@end int
)
as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY c.CountryName Asc) as Row,SUM(1) OVER()AS TOTALROWS, c.* 
		from Country c where IsActive=1 and IsDeleted=0) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListBookingByRaw]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListBookingByRaw]
 (
	@Raw int
 )
 as begin

		SELECT BookingMegaball.*,Member.Email
		from BookingMegaball
		join Member 
		on BookingMegaball.MemberID = Member.MemberID
		where Convert(varchar,BookingMegaball.OpenDate,103) in (select Convert(varchar,AwardMegaball.CreateDate,103) from AwardMegaball where AwardMegaball.NumberID = @Raw)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardOrderByDateFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardOrderByDateFE]

as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.drawID desc) as Row,awn.*,aw.AwardName,aw.AwardValue 
	from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID 
	 where awn.IsActive=1 and awn.IsDelete=0 ) as Products
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardNumberByDate]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardNumberByDate]
 (
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,aw.*,a.AwardName,a.AwardNameEnglish from awardnumber aw left join award a on aw.AwardID=a.AwardID 
		where aw.CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardNumber]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardNumber]
 (
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,aw.*,a.AwardName,a.AwardNameEnglish
		 from awardnumber aw left join award a on aw.AwardID=a.AwardID) as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [guest].[SP_GetListAwardMegaballResultByDate]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_GetListAwardMegaballResultByDate]
 (
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,
		aw.*,a.AwardName,a.AwardNameEnglish from awardmegaball aw left join award a on aw.AwardID=a.AwardID 
		where aw.CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMegaballResultByDate]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardMegaballResultByDate]
 (
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY aw.Priority ASC, aw.CreateDate DESC) as Row,
		aw.*,a.AwardName,a.AwardNameEnglish from awardmegaball aw left join award a on aw.AwardID=a.AwardID 
		where aw.CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end and IsActive=1 and IsDelete=0
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMegaballFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardMegaballFE]
(
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.NumberID desc) as Row, sum(1) over() as TOTALROWS,awn.*,aw.AwardName,aw.AwardValue from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID 
	where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0 ) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMegaball_FE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardMegaball_FE]
as begin 
	select * from AwardMegaball where IsActive=1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardMagaball]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardMagaball]
  (
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY DrawID DESC)as row, sum(1) over() as TOTALROWS
		 ,aw.*,a.AwardNameEnglish from awardmegaball aw left join  Award a on aw.AwardID=a.AwardID where aw.IsActive=1 and aw.IsDelete=0) as Products  where Row>=@start and Row<=@end 
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAwardFE]
(
@start int,
@end int
)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY AwardID DESC) as Row,aw.* from award aw) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardDetail_CMS]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardDetail_CMS]
 (@awardID int)
 as begin
	select * from Award where AwardID=@awardID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardByDayFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardByDayFE]
(
	@fromDate datetime,
	@toDate datetime

)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Priority ASC) as Row,awn.*,aw.AwardName,aw.AwardValue from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID
	 where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0 ) as Products
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardByDateFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardByDateFE]
(
	@fromDate datetime,
	@toDate datetime,
	@start int,
	@end int

)
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Priority ASC) as Row,awn.*,aw.AwardName,aw.AwardValue from awardnumber awn left join award aw on awn.AwardID=aw.AwardID 
	where awn.CreateDate between @fromDate and @toDate and awn.IsActive=1 and awn.IsDelete=0 ) as Products  where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAwardByDate]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetListAwardByDate]
(
	@date datetime
 )
as begin 
	select *from (SELECT ROW_NUMBER() OVER (ORDER BY awn.Createdate desc) as Row,awn.*,aw.AwardName,aw.AwardValue 
	from AwardMegaball awn left join award aw on awn.AwardID=aw.AwardID 
	 where awn.IsActive=1 and awn.IsDelete=0 and Convert(varchar,awn.CreateDate,103) = CONVERT(varchar,@date, 103) ) as Products
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListAward]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetListAward]
 (
	@start int,
	@end int
 )
 as begin
		select *from (SELECT ROW_NUMBER() OVER (ORDER BY AwardID DESC)
		 as Row,sum(1) over() as TOTALROWS,
		  aw.* from award aw) as Products  where Row>=@start and Row<=@end
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetGroupAdminDetails]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetGroupAdminDetails]
(@groupID int)
as begin
		select *from groupadmin where GroupID=@groupID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetGroupAdminDetail]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetGroupAdminDetail]
(@groupID int)
as begin
		select *from groupadmin where GroupID=@groupID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetFreeTicketDailyActive]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetFreeTicketDailyActive]
as begin
		select * from FreeTicketDaily where IsActive=1
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetDrawIDByDate]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetDrawIDByDate]
(
	@date datetime
)
as begin 
	select * from draw where @date >= startdate and @date <= enddate order by createddate desc
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetDetailLinkResetPasswordFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GetDetailLinkResetPasswordFE]
(
	@LinkReset nvarchar(1000)
)
as
begin
	SELECT *
	FROM LinkResetPassword
	WHERE LinkReset = @LinkReset and status = 0 and ExpireLink >= SYSDATETIME();
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetCompanyByMemberID]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetCompanyByMemberID]
(
	@memberID int
)
as begin 
	select * from CompanyInfomation where MemberID=@memberID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardWithdrawDetail_FE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GetAwardWithdrawDetail_FE]
(
	@RequestMemberID nvarchar(250),
	@AwardDate datetime,
	@BookingID int
)
as
begin
select * from AwardWithdraw
where convert(varchar(10), AwardDate, 101)=convert(varchar(10), @AwardDate, 101)
and RequestMemberID=@RequestMemberID
and IsDeleted=0
and (@BookingID=-1 or BookingID=@BookingID);
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardWithdrawDetail_CMS]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_GetAwardWithdrawDetail_CMS]
(
	@TransactionID nvarchar(250)
)
as
begin
select * from AwardWithdraw
where TransactionID=@TransactionID
and IsDeleted=0;
end;
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardMegabalByDrawID]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GetAwardMegabalByDrawID]
  (
	@DrawID int
 )
 as begin
		
		select * from AwardMegaball where DrawID=@DrawID
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAwardMagaballByID]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_GetAwardMagaballByID]
(
	@NumberID int
)
as begin 
	select *from AwardMegaball where NumberID = @NumberID and IsActive = 1 and IsDelete = 0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAwardWithDraw_FE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GetAllAwardWithDraw_FE]
as begin 
	select *from AwardWithdraw where AwardWithdraw.IsDeleted=0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetAllAwardFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_GetAllAwardFE]

as begin 
	select *from award where IsActive = 1 and IsDelete = 0
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GerAdminDetailByID]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GerAdminDetailByID](@adminID nvarchar(50))
as begin
		select * from admin where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GerAdminDetail]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_GerAdminDetail](@userName nvarchar(50))
as begin
		select * from admin where UserName=@userName
end
GO
/****** Object:  StoredProcedure [dbo].[TransactionPoint_Sel]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPoint_Sel]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select t.MemberID, t.CreateDate, t.Points, t.Status, t.TransactionID, m.E_Wallet,t.TransactionCode from TransactionPoint t, Member m where t.MemberID = m.MemberID and m.MemberID = @MemberID order by CreateDate desc
END
GO
/****** Object:  StoredProcedure [dbo].[TransactionPoint_Insert]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPoint_Insert]   
    @MemberID int,
    @Status int,
    @Points int,
    @CreateDate datetime,
    @TransactionCode nvarchar(50)
AS
BEGIN 
     SET NOCOUNT ON 
    insert into TransactionPoint(MemberID,Status,Points,CreateDate,TransactionCode) values(@MemberID,@Status,@Points,@CreateDate,@TransactionCode) 
    
END
GO
/****** Object:  UserDefinedFunction [guest].[fc_CountNewAward]    Script Date: 07/31/2018 11:30:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [guest].[fc_CountNewAward]()
returns int
as begin
	declare @numberAward int
	set @numberAward=(select COUNT(aw.AwardID) from AwardMegaball aw
	where DATEADD(DD, DATEDIFF(DD, 0, aw.CreateDate), 0) = DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0) 
	); 
	return(@numberAward);
end;
GO
/****** Object:  StoredProcedure [dbo].[TransactionPackage_Sel_OrderByDate]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[TransactionPackage_Sel_OrderByDate]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select top 1 t.CreateDate, t.ExpireDate, t.MemberID, t.PackageID, t.Status, t.TransactionID, p.PackageName,t.TransactionCode from TransactionPackage t, Package p where t.PackageID = p.PackageID and MemberID = @MemberID and t.Status = 1 order by CreateDate desc
END
GO
/****** Object:  StoredProcedure [dbo].[TransactionPackage_Sel]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPackage_Sel]   
    @MemberID int
AS
BEGIN 
     SET NOCOUNT ON 
    select t.CreateDate, t.ExpireDate, t.MemberID, t.PackageID, t.Status, t.TransactionID, p.PackageName,t.TransactionCode from TransactionPackage t, Package p where t.PackageID = p.PackageID and MemberID = @MemberID order by CreateDate desc
END
GO
/****** Object:  StoredProcedure [dbo].[TransactionPackage_Insert]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[TransactionPackage_Insert]   
    @MemberID int,
    @PackageID int,
    @Status int,
    @ExpireDate datetime,
    @CreateDate datetime,
    @TransactionCode nvarchar(50)
AS
BEGIN 
     SET NOCOUNT ON 
    insert into TransactionPackage(MemberID,PackageID,Status,ExpireDate,CreateDate,TransactionCode) values(@MemberID,@PackageID,@Status,@ExpireDate,@CreateDate,@TransactionCode) 
    
END
GO
/****** Object:  StoredProcedure [dbo].[CheckEmailExists]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[CheckEmailExists]
(
	@email nvarchar(250)
)
 as begin
		select Email from member where Email=@email
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_DeleteAccessRight]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_DeleteAccessRight]
(@adminID int)
as begin
		delete from accessright where AdminID=@adminID
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountMemberBuyNumberFE]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountMemberBuyNumberFE]
(
	@fromDate datetime,
	@toDate datetime,
	@numberValue int,
	@memberID int
)
as begin 
	select count(BookingID) as TotalRecord from booking where CreateDate between @fromDate and @toDate and NumberValue=@numberValue and MemberID not in(@memberID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountMemberBuyNumber_FE]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountMemberBuyNumber_FE]
(
	@fromDate datetime,
	@toDate datetime,
	@firstNumber int,
	@secondNumber int,
	@thirdNumber int,
	@fourthNumber int,
	@fiveNumber int,
	@memberID int


)
as begin 
	select count(BookingID) as TotalRecord from bookingmegaball 
	where CreateDate between @fromDate and @toDate and FirstNumber=@firstNumber and SecondNumber=@secondNumber and ThirdNumber=@thirdNumber and FourthNumber=@fourthNumber and FivethNumber=@fiveNumber and MemberID not in(@memberID)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountMember]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountMember](@curdate datetime)
  as begin 
  select count(MemberID) as TotalCount from BookingMegaball where CreateDate=@curdate
  end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountAllBookingMegaball]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SP_CountAllBookingMegaball]
as begin
	select COUNT(b.BookingID) as TotalAllBookingMegaball,
	dbo.CountNewBookingMegaball() as TotalNewBookingMegaball
	from BookingMegaball b;
end
GO
/****** Object:  StoredProcedure [guest].[SP_CountAllAwardMegaball]    Script Date: 07/31/2018 11:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [guest].[SP_CountAllAwardMegaball]
as begin 
	select COUNT(aw.AwardID) as TotalAllAward,
	guest.fc_CountNewAward() as TotalNewAward 
	from AwardMegaball aw
	where aw.IsActive=1 and aw.IsDelete=0;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CountAllAwardMegaball]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[SP_CountAllAwardMegaball]
as begin 
	select COUNT(aw.AwardID) as TotalAllAward,
	guest.fc_CountNewAward() as TotalNewAward 
	from AwardMegaball aw
	where aw.IsActive=1 and aw.IsDelete=0;
end
GO
/****** Object:  StoredProcedure [dbo].[SP_ListBookingWinningBySearch_FE]    Script Date: 07/31/2018 11:30:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_ListBookingWinningBySearch_FE]
(
	@memberID int,
	@start int,
	@end int,
	@fromDate Datetime,
	@todate Datetime
)
as begin 
	select * from(SELECT ROW_NUMBER() OVER (ORDER BY b.CreateDate DESC) as Row,SUM(1) OVER()AS TOTALROWS,
	 b.*,m.Email,m.E_Wallet,
	 dbo.GetAwardWithdrawRequestStatus(m.Email,b.BookingID,b.OpenDate) as AwardWithdrawStatus
	  from BookingMegaball b left join member m on b.MemberID=m.MemberID
	  where b.MemberID=@memberID and b.Status = 1  and (@fromDate IS NULL or b.CreateDate between @fromDate and @toDate)
	 ) as product
	where Row>=@start and Row<=@end
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListTicketWinningBySearch]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetListTicketWinningBySearch]   
	@memberID int,
	@start int,
	@end int,
	@startdate datetime,
	@enddate datetime
AS 
BEGIN 
     SET NOCOUNT ON 
    select *from (SELECT ROW_NUMBER() OVER (ORDER BY tw.AwardDate DESC) as row, sum(1) over() as TotalRecord,tw.*,
	dbo.GetAwardWithdrawRequestStatus(mb.Email,tw.BookingID,tw.AwardDate) as AwardWithdrawStatus
	from TicketWinning tw
	join Member mb on mb.MemberID = tw.MemberID
	where (@memberID=0 or tw.MemberID = @memberID) and (tw.AwardDate between @startdate and @enddate)) as Products  where row>=@start and row<=@end
	
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GetListTicketWinningByMemberID]    Script Date: 07/31/2018 11:30:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetListTicketWinningByMemberID]   
	@MemberID int,
	@start int,
	@end int
AS 
BEGIN 
     SET NOCOUNT ON 
    select *from (SELECT ROW_NUMBER() OVER (ORDER BY tw.buydate DESC) as row, sum(1) over() as TotalRecord,tw.*,
	dbo.GetAwardWithdrawRequestStatus(mb.Email,tw.BookingID,tw.AwardDate) as AwardWithdrawStatus
	from TicketWinning tw
	join Member mb on mb.MemberID = tw.MemberID
	where tw.MemberID = @MemberID) as Products  where row>=@start and row<=@end
	
END
GO
/****** Object:  StoredProcedure [dbo].[CountAllPoints]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllPoints]
as
begin
select COUNT(p.TransactionID) as TotalAllPoints,
dbo.CountNewPoint() as TotalNewPoints
 from TransactionPoint p;
end;
GO
/****** Object:  StoredProcedure [dbo].[CountAllPackage]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllPackage]
as
begin
select COUNT(p.TransactionID) as TotalAllPackage,
dbo.CountNewPackage() as TotalNewPackage
 from TransactionPackage p;
end;
GO
/****** Object:  StoredProcedure [dbo].[CountAllMember]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllMember]
as
begin
select COUNT(m.MemberID) as TotalAllMember,
dbo.CountNewMember() as TotalNewMember
 from Member m
where m.IsActive=1 
and m.IsDelete=0;
end;
GO
/****** Object:  StoredProcedure [dbo].[CountAllCoin]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[CountAllCoin]
as
begin
select COUNT(c.TransactionID) as TotalAllCoin,
dbo.CountNewCoin() as TotalNewCoin
 from TransactionCoin c;
end;
GO
/****** Object:  StoredProcedure [dbo].[CountAllBooking]    Script Date: 07/31/2018 11:30:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[CountAllBooking]
as
begin
select COUNT(b.BookingID) as TotalAllBooking,
dbo.CountNewBooking() as TotalNewBooking
 from Booking b;
end;
GO
/****** Object:  Default [DF_Award_NumberBallGold]    Script Date: 07/31/2018 11:30:18 ******/
ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_NumberBallGold]  DEFAULT ((0)) FOR [NumberBallGold]
GO
/****** Object:  Default [DF_Award_NumberBallNormal]    Script Date: 07/31/2018 11:30:18 ******/
ALTER TABLE [dbo].[Award] ADD  CONSTRAINT [DF_Award_NumberBallNormal]  DEFAULT ((0)) FOR [NumberBallNormal]
GO
/****** Object:  Default [DF_AwardMegaball_JackPotWinner]    Script Date: 07/31/2018 11:30:18 ******/
ALTER TABLE [dbo].[AwardMegaball] ADD  CONSTRAINT [DF_AwardMegaball_JackPotWinner]  DEFAULT ((0)) FOR [JackPotWinner]
GO
/****** Object:  Default [DF_AwardMegaball_TotalWon]    Script Date: 07/31/2018 11:30:18 ******/
ALTER TABLE [dbo].[AwardMegaball] ADD  CONSTRAINT [DF_AwardMegaball_TotalWon]  DEFAULT ((0)) FOR [TotalWon]
GO
/****** Object:  Default [DF_BookingMegaball_IsFreeTicket]    Script Date: 07/31/2018 11:30:18 ******/
ALTER TABLE [dbo].[BookingMegaball] ADD  CONSTRAINT [DF_BookingMegaball_IsFreeTicket]  DEFAULT ((0)) FOR [IsFreeTicket]
GO
/****** Object:  Default [DF_Country_IsActive]    Script Date: 07/31/2018 11:30:19 ******/
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_Country_IsDeleted]    Script Date: 07/31/2018 11:30:19 ******/
ALTER TABLE [dbo].[Country] ADD  CONSTRAINT [DF_Country_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_IPConfig_IsDeleted]    Script Date: 07/31/2018 11:30:19 ******/
ALTER TABLE [dbo].[IPConfig] ADD  CONSTRAINT [DF_IPConfig_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_Member_IsLoginType]    Script Date: 07/31/2018 11:30:19 ******/
ALTER TABLE [dbo].[Member] ADD  CONSTRAINT [DF_Member_IsLoginType]  DEFAULT ((0)) FOR [IsUserType]
GO
/****** Object:  Default [DF_Member_NumberTicketFree]    Script Date: 07/31/2018 11:30:19 ******/
ALTER TABLE [dbo].[Member] ADD  CONSTRAINT [DF_Member_NumberTicketFree]  DEFAULT ((0)) FOR [NumberTicketFree]
GO
/****** Object:  Default [DF_SystemConfig_IsAutoLottery]    Script Date: 07/31/2018 11:30:19 ******/
ALTER TABLE [dbo].[SystemConfig] ADD  CONSTRAINT [DF_SystemConfig_IsAutoLottery]  DEFAULT ((0)) FOR [IsAutoLottery]
GO
/****** Object:  Default [DF_TicketConfig_IsDeleted]    Script Date: 07/31/2018 11:30:19 ******/
ALTER TABLE [dbo].[TicketConfig] ADD  CONSTRAINT [DF_TicketConfig_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  Default [DF_TicketWinning_Status]    Script Date: 07/31/2018 11:30:19 ******/
ALTER TABLE [dbo].[TicketWinning] ADD  CONSTRAINT [DF_TicketWinning_Status]  DEFAULT ((0)) FOR [Status]
GO
/****** Object:  Default [DF_TypeConfig_IsDeleted]    Script Date: 07/31/2018 11:30:19 ******/
ALTER TABLE [dbo].[TypeConfig] ADD  CONSTRAINT [DF_TypeConfig_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
